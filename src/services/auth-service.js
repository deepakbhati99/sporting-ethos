import { toast } from "react-toastify";
import HttpServiceAuth from "./http.service.auth";
import HttpService from "./htttp.service";
import axios from 'axios';
import htttpService from "./htttp.service";
const token = localStorage.getItem("token");
// console.log("token_auth=====>",token)
const API_URL = process.env.REACT_APP_API_URL;
axios.defaults.baseURL = API_URL;
axios.defaults.headers.Authorization = `Bearer ${token}`;
const headers = {
  'Content-Type': 'multipart/form-data',
  Authorization: `'Bearer '  ${token}`,
}
class AuthService {
  // authEndpoint = process.env.API_URL;

  login = async (promise) => {
    const loginEndpoint = 'login';
    return  await axios.post(loginEndpoint, promise );
    // return await HttpService.post(loginEndpoint, payload);
  };

  logout = async () => {
    const logoutEndpoint = 'logout';
    return await axios.post(logoutEndpoint);
  };

  create_user = async (promise) => {
    const createUser = 'create_user';
    return await axios.post(createUser, promise);
  };

  get_user = async () => {
    const getUser ='get_users';
    return await axios.get(getUser);
  }

  register = async (credentials) => {
    const registerEndpoint = 'register';
    return await HttpService.post(registerEndpoint, credentials);
  };

  logout = async () => {
    const logoutEndpoint = 'logout';
    return await HttpService.post(logoutEndpoint);
  };

  forgotPassword = async (payload) => {
    const forgotPassword = 'password-forgot';
    return await HttpService.post(forgotPassword, payload);
  }

  resetPassword = async (credentials) => {
    const resetPassword = 'password-reset';
    return await HttpService.post(resetPassword, credentials);
  }

  getProfile = async() => {
    const getProfile = 'get_admin_profile';
    return await HttpService.get(getProfile);
  }

  updateProfile = async (newInfo) => {
    const updateProfile = "update_admin_profile";
    return await axios.post(updateProfile, newInfo, headers);
  }

  deleteUser = async (id) => {
    // console.log('----===>', id);
    const deleteUser = "delete_user";
    return await htttpService.post(deleteUser, id, headers);
  }
}

export default new AuthService();
