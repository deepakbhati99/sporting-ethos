
import axios from "axios";
import { fetcher } from "./ApiBase";
import { useContext } from "react";
import { AuthContext } from "../context";

const API_URL = process.env.REACT_APP_API_URL;


export async function getUserList(params) {
  try {
    const response = await fetcher('GET', `${API_URL}get_users`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function deleteUserData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}delete_user`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function createUserData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_user`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getSingleUserData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_single_user`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updateUserData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_user`, params);
    
    return response;
  } catch (err) {
    return null;
  }
}

// PFA 

export async function getPfaByRGst(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_list`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaInstruction(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_instruction`, params);
    return response;
  } catch (err) {
    return null;
  }
}

// PFA one 

export async function createPfaStepOne(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_pfa_step_one`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaStepOneData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_step_one`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updatePfaStepOne(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_pfa_step_one`, params);
    return response;
  } catch (err) {
    return null;
  }
}

// PFA two

export async function createPfaStepTwo(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_pfa_step_two`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaStepTwoData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_step_two`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updatePfaStepTwo(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_pfa_step_two`, params);
    return response;
  } catch (err) {
    return null;
  }
}

// PFA three

export async function createPfaStepThree(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_pfa_step_three`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaStepThreeData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_step_three`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updatePfaStepThree(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_pfa_step_three`, params);
    return response;
  } catch (err) {
    return null;
  }
}

// PFA Four

export async function createPfaStepFour(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_pfa_step_four`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaStepFour(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_step_four`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updatePfaStepFour(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_pfa_step_four`, params);
    return response;
  } catch (err) {
    return null;
  }
}

// PFA Five

export async function createPfaStepFive(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_pfa_step_five`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaStepFive(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_step_five`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updatePfaStepFive(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_pfa_step_five`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function createPfaProtocol(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_pfa_first`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function deletePfaProtocol(params) {
  try {
    const response = await fetcher('POST', `${API_URL}delete_pfa`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaByRgstData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_by_rgst`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getPfaStepDetail(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_pfa_create_form`, params);
    return response;
  } catch (err) {
    return null;
  }
}


// Acl Form Data  

export async function createAclProtocol(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_acl`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updateAclProtocol(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_acl`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function deleteAclProtocol(params) {
  try {
    const response = await fetcher('POST', `${API_URL}delete_acl`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getAclByRGst(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_acl_by_rgst`, params);
    return response;
  } catch (err) {
  return null;
  }
}

export async function getAclSingleData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}edit_acl`, params);
    return response;
  } catch (err) {
    return null;
  }
}

// FMS

export async function getFmsByRGst(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_fms_by_rgst`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getFmsInstruction(params) {
  try {
    const response = await fetcher('POST', `${API_URL}get_fms_instruction`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function deleteFmsProtocol(params) {
  try {
    const response = await fetcher('POST', `${API_URL}delete_fms`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function createFmsForm(params) {
  try {
    const response = await fetcher('POST', `${API_URL}create_fms`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function updateFmsForm(params) {
  try {
    const response = await fetcher('POST', `${API_URL}update_fms`, params);
    return response;
  } catch (err) {
    return null;
  }
}

export async function getFmsSingleData(params) {
  try {
    const response = await fetcher('POST', `${API_URL}edit_fms`, params);
    return response;
  } catch (err) {
    return null;
  }
}



