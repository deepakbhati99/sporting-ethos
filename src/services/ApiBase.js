import Axios from 'axios';
import Cookies from 'js-cookie';

// Api Headers
function setHeader() {
  const _headers = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  if (localStorage.getItem("token")) {
    _headers['headers']['Authorization'] = `Bearer ${localStorage.getItem("token")}`;
  }
  return _headers;
}

  


// API fetch Base
export async function fetcher(method, url, params) {
  try {
    let response;
    if (method == 'GET') {
      const updateHeader = setHeader();
      updateHeader['params'] = params;
      response = await Axios.get(`${url}`, updateHeader);
    } else {
      response = await Axios.post(`${url}`, params, setHeader());
    }
    console.log('response====>',response);
    if (response.status == 200) {
      return successResponse(response.data);
    } else if (response.status == 201) {
      return successResponse(response.data);
    } else if (response.status == 400){
      localStorage.removeItem("token");
      return errorResponse(response);
    } else  if (response.status == 401){
      return errorResponse(response);
    } else {
      return errorResponse(response.data);
    }
  } catch (err) {
    const response = err.response;
    if (response.status == 400){
      return errorResponse(response);
    } else  if (response.status == 401){
      localStorage.removeItem("token");
      return errorResponse(response);
    }
    return unErrorResponse({ resultMessage: err });
  }
}

function successResponse(response) {
  return {
    status: true,
    data: response['data'],
    message: response['message'],
  };
}

function errorResponse(response) {
  return {
    status: false,
    data: response['data'],
    message: response['message'],
  };
}

function unErrorResponse(response) {
  const data = response.resultMessage.response.data;
  return {
    status: false,
    data: data['data'],
    message: data['message'],
  };
}
