import { useState, useEffect } from "react";
// Material Dashboard 2 React components
import MDBox from "../../components/MDBox";
import MDTypography from "../../components/MDTypography";
import MDInput from "../../components/MDInput";
import MDButton from "../../components/MDButton";
import MDAlert from "../../components/MDAlert";

// Material Dashboard 2 React example components
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import Footer from "../../examples/Footer";

// Overview page components
import Header from "../../layouts/user-profile/Header";

import AuthService from "../../services/auth-service";
import { toast } from "react-toastify";
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from "@mui/material";

const UserProfile = () => {

  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const [isDemo, setIsDemo] = useState(false);
  const [notification, setNotification] = useState(false);
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    mobileNo: "",
    newPassword: "",
    confirmPassword: "",
    dob: "",
    gender: "male",
  });

  const [errors, setErrors] = useState({
    firstNameError: false,
    lastNameError: false,
    emailError: false,
    newPassError: false,
    confirmPassError: false,
    dobError: false,
    genderError: false,
  });

  const getUserData = async () => {
    const response = await AuthService.getProfile();
    if (response.data.id == 1) {
      setIsDemo(process.env.REACT_APP_IS_DEMO === "true");
    }

    const firstName = response.data[0].first_name;
    const lastName = response.data[0].last_name;
    const email = response.data[0].email;
    const mobile = response.data[0].mobile;
    const dob = response.data[0].dob;
    const gender = response.data[0].gender;

    setUser({
      firstName: firstName,
      lastName: lastName,
      email: email,
      mobileNo: mobile,
      dob: dob,
      gender: gender
    })
  };
  // console.log("user data", user);

  useEffect(() => {
    getUserData();
  }, []);

  useEffect(() => {
    if (notification === true) {
      setTimeout(() => {
        setNotification(false);
      }, 5000);
    }
  }, [notification]);

  const changeHandler = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const submitHandler = async (e) => {
    e.preventDefault();

    // validation
    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (user.firstName.trim().length === 0) {
      setErrors({ ...errors, firstNameError: true });
      return;
    }

    if (user.lastName.trim().length === 0) {
      setErrors({ ...errors, lastNameError: true });
      return;
    }

    if (user.email.trim().length === 0 || !user.email.trim().match(mailFormat)) {
      setErrors({ ...errors, emailError: true });
      return;
    }

    if (user.mobileNo.trim().length === 0) {
      setErrors({ ...errors, mobileNoError: true });
      return;
    }

    if (!user.dob) {
      setErrors({ ...errors, dobError: true });
      return;
    }

    if (user.confirmPassword || user.newPassword) {
      // in the api the confirmed password should be the same with the current password, not the new one
      if (user.confirmPassword.trim() !== user.newPassword.trim()) {
        setErrors({ ...errors, confirmPassError: true });
        return;
      }
      if (user.newPassword.trim().length < 8) {
        setErrors({ ...errors, newPassError: true });
        return;
      }
    }

    let userData = {
      data: {
        type: "profile",
        attributes: {
          name: user.name,
          email: user.email,
        },
      },
    };

    userData = {
      first_name: user.firstName,
      last_name: user.lastName,
      email: user.email,
      mobile: user.mobileNo,
      dob: user.dob,
      gender: user.gender,
      password: user.newPassword,
      confirm_password: user.confirmPassword,
    };

    // call api for update
    const response = await AuthService.updateProfile(userData);
    if (response.data?.success == true) {
      toast.success(response.data?.message, toastConfig);
    } else {
      toast.error(response.data?.message, toastConfig);
    }

    // reset errors
    setErrors({
      firstNameError: false,
      lastNameError: false,
      emailError: false,
      passwordError: false,
      newPassError: false,
      confirmPassError: false,
    });

    setNotification(true);
  };


  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox mb={2} />
      <Header >
        {notification && (
          <MDAlert color="info" mt="20px">
            <MDTypography variant="body2" color="white">
              Your profile has been updated
            </MDTypography>
          </MDAlert>
        )}
        <MDBox
          component="form"
          role="form"
          onSubmit={submitHandler}
          display="flex"
          flexDirection="column"
        >
          <MDBox display="flex" flexDirection="row" mt={5}>
            <MDBox
              display="flex"
              flexDirection="column"
              alignItems="flex-start"
              width="100%"
              mr={2}
            >
              <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                First Name
              </MDTypography>
              <MDBox mb={2} width="100%">
                <MDInput
                  type="name"
                  fullWidth
                  name="firstName"
                  value={user.firstName}
                  onChange={changeHandler}
                  error={errors.firstNameError}
                />
                {errors.firstNameError && (
                  <MDTypography variant="caption" color="error" fontWeight="light">
                    The first name can not be null
                  </MDTypography>
                )}
              </MDBox>
            </MDBox>

            <MDBox
              display="flex"
              flexDirection="column"
              alignItems="flex-start"
              width="100%"
            >
              <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                Last Name
              </MDTypography>
              <MDBox mb={2} width="100%">
                <MDInput
                  type="name"
                  fullWidth
                  name="lastName"
                  value={user.lastName}
                  onChange={changeHandler}
                  error={errors.lastNameError}
                />
                {errors.lastNameError && (
                  <MDTypography variant="caption" color="error" fontWeight="light">
                    The last name can not be null
                  </MDTypography>
                )}
              </MDBox>
            </MDBox>
          </MDBox>

          <MDBox display="flex" flexDirection="row" mb={3}>
            <MDBox
              display="flex"
              flexDirection="column"
              alignItems="flex-start"
              width="100%"
              mr={2}
            >
              <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                Email
              </MDTypography>
              <MDBox mb={1} width="100%">
                <MDInput
                  type="email"
                  fullWidth
                  name="email"
                  value={user.email}
                  onChange={changeHandler}
                  error={errors.emailError}
                  disabled={isDemo}
                />
                {errors.emailError && (
                  <MDTypography variant="caption" color="error" fontWeight="light">
                    The email must be valid
                  </MDTypography>
                )}
              </MDBox>
              {isDemo && (
                <MDTypography variant="caption" color="text" fontWeight="light">
                  In the demo version the email can not be updated
                </MDTypography>
              )}
            </MDBox>

            <MDBox
              display="flex"
              flexDirection="column"
              alignItems="flex-start"
              width="100%"
            >
              <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                Mobile no.
              </MDTypography>
              <MDBox mb={1} width="100%">
                <MDInput
                  type="number"
                  fullWidth
                  name="mobileNo"
                  value={user.mobileNo}
                  onChange={changeHandler}
                  error={errors.mobileNoError}
                  disabled={isDemo}
                />
                {errors.mobileNoError && (
                  <MDTypography variant="caption" color="error" fontWeight="light">
                    The mobile no. can not be null
                  </MDTypography>
                )}
              </MDBox>
              {isDemo && (
                <MDTypography variant="caption" color="text" fontWeight="light">
                  In the demo version the email can not be updated
                </MDTypography>
              )}
            </MDBox>
          </MDBox>

          <MDBox display="flex" flexDirection="row" mb={3}>
            <MDBox
              display="flex"
              flexDirection="column"
              alignItems="flex-start"
              width="100%"
              mr={2}
            >
              <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                DOB
              </MDTypography>
              <MDBox mb={1} width="100%">
                <MDInput
                  type="date"
                  fullWidth
                  name="dob"
                  value={user.dob}
                  onChange={changeHandler}
                  error={errors.dobError}
                  disabled={isDemo}
                />
                {errors.dobError && (
                  <MDTypography variant="caption" color="error" fontWeight="light">
                    The dob can not be null
                  </MDTypography>
                )}
              </MDBox>
              {isDemo && (
                <MDTypography variant="caption" color="text" fontWeight="light">
                  In the demo version the email can not be updated
                </MDTypography>
              )}
            </MDBox>

            <MDBox
              display="flex"
              flexDirection="column"
              alignItems="flex-start"
              width="100%"
            >
              <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                Gender
              </MDTypography>
              <MDBox mb={1} width="100%">
           
                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    onChange={changeHandler}
                    defaultChecked={user.gender}
                    error={errors.dobError}
                    name="gender"
                    row
                  >
                    <FormControlLabel 
                      value="male"
                      checked={user.gender === "male"}
                      control={<Radio />}
                      label="Male"
                      ml={2}
                    />
                    <FormControlLabel
                      value="female"
                      checked={user.gender === "female"}
                      control={<Radio />}
                      label="Female" 
                    />
                    <FormControlLabel
                     value="other"
                     checked={user.gender === "other"}
                      control={<Radio />}
                      label="Other" 
                    />
                  </RadioGroup>
                </FormControl>
                {errors.genderError && (
                  <MDTypography variant="caption" color="error" fontWeight="light">
                    The gender can not be null
                  </MDTypography>
                )}
              </MDBox>
              {isDemo && (
                <MDTypography variant="caption" color="text" fontWeight="light">
                  In the demo version the email can not be updated
                </MDTypography>
              )}
            </MDBox>
          </MDBox>

          <MDBox display="flex" flexDirection="column" mb={3}>
            <MDBox display="flex" flexDirection="row">
              <MDBox
                display="flex"
                flexDirection="column"
                alignItems="flex-start"
                width="100%"
                mr={2}
              >
                <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                  New Password
                </MDTypography>
                <MDBox mb={2} width="100%">
                  <MDInput
                    type="password"
                    fullWidth
                    name="newPassword"
                    placeholder="New Password"
                    value={user.newPassword}
                    onChange={changeHandler}
                    error={errors.newPassError}
                    disabled={isDemo}
                    inputProps={{
                      autoComplete: "new-password",
                      form: {
                        autoComplete: "off",
                      },
                    }}
                  />
                  {errors.newPassError && (
                    <MDTypography variant="caption" color="error" fontWeight="light">
                      The password must be of at least 8 characters
                    </MDTypography>
                  )}
                </MDBox>
              </MDBox>

              <MDBox
                display="flex"
                flexDirection="column"
                alignItems="flex-start"
                width="100%"
              >
                <MDTypography variant="body2" color="text" ml={1} fontWeight="regular">
                  Password Confirmation
                </MDTypography>
                <MDBox mb={1} width="100%">
                  <MDInput
                    type="password"
                    fullWidth
                    name="confirmPassword"
                    placeholder="Confirm Password"
                    value={user.confirmPassword}
                    onChange={changeHandler}
                    error={errors.confirmPassError}
                    disabled={isDemo}
                    inputProps={{
                      autoComplete: "confirmPassword",
                      form: {
                        autoComplete: "off",
                      },
                    }}
                  />
                  {errors.confirmPassError && (
                    <MDTypography variant="caption" color="error" fontWeight="light">
                      The password confirmation must match the current password
                    </MDTypography>
                  )}
                </MDBox>
                {isDemo && (
                  <MDTypography variant="caption" color="text" ml={1} fontWeight="light">
                    In the demo version the password can not be updated
                  </MDTypography>
                )}
              </MDBox>
            </MDBox>

            <MDBox mt={4} display="flex" justifyContent="end">
              <MDButton variant="gradient" color="info" type="submit">
                Save changes
              </MDButton>
            </MDBox>
          </MDBox>
        </MDBox>
      </Header>
      {/* <Footer /> */}
    </DashboardLayout>
  );
};

export default UserProfile;
