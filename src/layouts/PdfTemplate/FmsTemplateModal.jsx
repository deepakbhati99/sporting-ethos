// DeleteModal.js
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import AuthService from "../../services/auth-service";
import {
  deleteUserData,
  getFmsSingleData,
  getUserList,
} from "../../services/service_api";
import { toast } from "react-toastify";
import TemplateLogo from "../../assets/images/Sporting-Ethos-Logo.png";

export default function FmsTemplateModal({
  isOpen,
  onClose,
  onDelete,
  registration_id,
  id,
  userID,
}) {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const [singleUserData, setSingleUserData] = useState([]);

  useEffect(() => {
    getSingleUserList();
    async function getSingleUserList() {
      const params = { registration_no: userID, id: registration_id };
      const response = await getFmsSingleData(params);
      setSingleUserData(response.data);
      // console.log("response--->", response.data);
    }
  }, [userID, registration_id]);

  const w100 = {
    width: "100%",
  };
  const w50 = {
    width: "50%",
  };
  const textDark = {
    color: "#000",
  };
  const textCenter = {
    textAlign: "center",
  };
  const textStart = {
    textAlign: "left",
  };
  const f12 = {
    fontSize: "12px",
  };
  const f13 = {
    fontSize: "13px",
  };
  const lh1 = {
    lineHeight: "1.2",
  };
  const m0 = {
    margin: "0px",
  };
  const padding10 = {
    padding: "10px",
  };
  const fontWeight600 = {
    fontWeight: "600",
  };
  const fontWeight400 = {
    fontWeight: "400",
  };
  const paddingY10 = {
    paddingTop: "10px",
    paddingBottom: "10px",
  };
  const paddingX10 = {
    paddingLeft: "10px",
    paddingRight: "10px",
  };
  const paddingX50 = {
    paddingLeft: "50px",
    paddingRight: "50px",
  };
  const borderLeft = {
    borderLeft: "1px solid #000",
  };
  const borderRight = {
    borderRight: "1px solid #000",
  };
  const borderTop = {
    borderTop: "1px solid #000",
  };
  const borderBottom = {
    borderBottom: "1px solid #000",
  };
  const border = {
    border: "1px solid #000",
  };
  const textUnderLine = {
    textDecoration: "underline",
  };
  const textFiled = { ...textStart, ...fontWeight400, ...f12 };
  const topAddress = {
    ...f12,
    ...lh1,
    ...m0,
    ...fontWeight600,
  };
  const main = {
    ...w100,
    ...textDark,
    ...textCenter,
  };
  const mainHeading = {
    ...fontWeight600,
    ...textUnderLine,
    ...textCenter,
  };
  const topContent = {
    ...textStart,
    ...f12,
    ...paddingY10,
  };

  const w45 = {
    width: "45%",
  };
  const w15 = {
    width: "15%",
  };

  const divToPrintRef = useRef(null);

  // Function to handle the print action for a specific div
  // const handlePrint = () => {
  //   const divToPrint = divToPrintRef.current;

  //   if (divToPrint) {
  //     const printWindow = window;
  //     const printDocument = printWindow.document;

  //     // Clone the content of the div
  //     const clone = divToPrint.cloneNode(true);

  //     // Create a new window with the cloned content
  //     printDocument.body.innerHTML = "";
  //     printDocument.body.appendChild(clone);

  //     // Trigger the browser's print dialog
  //     printWindow.print();
  //   }
  // };
  const handlePrint = () => {
    const divToPrint = divToPrintRef.current;

    if (divToPrint) {
      const printWindow = window;
      const printDocument = printWindow.document;

      // Clone the content of the div
      const clone = divToPrint.cloneNode(true);

      // Create a new window with the cloned content
      printDocument.body.innerHTML = "";
      printDocument.body.appendChild(clone);

      // Trigger the browser's print dialog
      printWindow.print();
      window.location.reload();
    }
  };

  const printButton = {
    fontSize: "15px",
    border: "0px",
    borderRadius: "5px",
    color: "#fff",
    background: "#478fed",
  };

  return (
    <Dialog
      fullScreen
      open={isOpen}
      keepMounted
      onClose={onClose}
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogActions>
        <div className="">
          <div className="text-end">
            <button
              onClick={handlePrint}
              style={{ ...printButton, ...paddingX10 }}
            >
              Print
            </button>
            &nbsp;
            <button onClick={onClose} style={{ ...printButton, ...paddingX10 }}>
              Cancel
            </button>
          </div>
          <div style={main} id="divToPrint" ref={divToPrintRef}>
            <table style={w100} collapse="1">
              <tr>
                <td colspan="3">
                  <img src={TemplateLogo} alt="" width={600} height={110} />
                  <br />
                  <p style={topAddress}>
                    95, 96, 97, 3rd Floor Wazir Nagar, Kotla Mubarakpur, New
                    Delhi. 110003.
                    <br />
                    Phone: +91 11 4632 11 77, 9717118308 Email:
                    reception@sportingethos.com
                  </p>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <table style={{ ...w100 }}>
                    <tbody>
                      <tr>
                        <td style={textFiled}>
                          Name: <b>{singleUserData?.name}</b>
                        </td>
                        <td style={textFiled}>
                          Age(years): <b>{singleUserData?.age}</b>
                        </td>
                        <td style={textFiled}>
                          Registration No:{" "}
                          <b>{singleUserData?.registration_no}</b>
                        </td>
                      </tr>
                      <tr>
                        <td style={textFiled}>
                          Sport/Activity: <b>{singleUserData?.sport}</b>
                        </td>
                        <td style={textFiled}>
                          Date: <b>{singleUserData?.date}</b>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <br />
              <tr>
                <td style={textFiled}>
                  Injury history: <b>{singleUserData?.injury_history}</b>
                </td>
              </tr>
              <tr>
                <td style={textFiled}>
                  Other observation: <b>{singleUserData?.other_observation}</b>
                </td>
              </tr>
              <br />
              <tr style={mainHeading}>
                <td colspan="3">Functional Movement Screening</td>
              </tr>
              <tr>
                <td colspan="3" style={topContent}>
                  The Functional Movement Screen (FMS) is a system used to
                  evaluate functional movement quality for athletes. It consists
                  of simple tests with a grading system that show limitations or
                  asymmetries in healthy individuals with respect to basic
                  movement patterns and eventually correlate them to outcomes.
                  The tests place the athlete in extreme positions where
                  weaknesses and imbalances become noticeable if appropriate
                  mobility and stability is not utilized.
                </td>
              </tr>
              <tr>
                <td style={textFiled}>
                  Final Score: <b>{singleUserData?.final_sorce}/21</b>
                </td>
              </tr>
              &nbsp;
              <tr>
                <td colspan="3">
                  <table style={{ ...border, ...w100 }} collapse="3">
                    <tr>
                      <th style={{ ...borderBottom, ...padding10, ...f13 }}>
                        NO.
                      </th>
                      <th style={{ ...border, ...padding10, ...f13 }}>
                        NAME OF TEST
                      </th>
                      <th style={{ ...border, ...padding10, ...f13, ...w15 }}>
                        SCORE
                      </th>
                      <th style={{ ...border, ...padding10, ...f13, ...w15 }}>
                        FINAL SCORE
                      </th>
                      <th style={{ ...border, ...padding10, ...f13, ...w45 }}>
                        COMMENTS
                      </th>
                    </tr>
                    {/* 1 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        1.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        Deep Squat
                      </td>
                      <td
                        colspan="2"
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...fontWeight600,
                        }}
                      >
                        {singleUserData?.deep_squat}
                      </td>
                      <td
                        colspan="1"
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...fontWeight600,
                        }}
                      >
                        {singleUserData?.deep_squat_comments}
                      </td>
                    </tr>
                    {/* 2 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        2.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        Hurdle Step
                      </td>
                      <td colspan="2" style={{ ...borderBottom }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <tr>
                              <td style={{ ...borderRight }}>
                                <table style={{ ...w100 }}>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table style={{ ...w100 }}>
                                          <tbody>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Right:
                                              </td>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...fontWeight600,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                {
                                                  singleUserData?.hurdle_step_right
                                                }
                                              </td>
                                            </tr>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Left:
                                              </td>
                                              <td
                                                style={{
                                                  ...padding10,
                                                  ...f13,
                                                  ...fontWeight600,
                                                }}
                                              >
                                                {
                                                  singleUserData?.hurdle_step_left
                                                }
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>

                              <td style={{ ...w45, ...fontWeight600 }}>
                                {singleUserData?.hurdle_step_final_score}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td
                        colspan="1"
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...fontWeight600,
                        }}
                      >
                        {singleUserData?.hurdle_step_comments}
                      </td>
                    </tr>
                    {/* 3 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        3.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        In-Line Lunge
                      </td>

                      <td colspan="2" style={{ ...borderBottom }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <tr>
                              <td style={{ ...borderRight }}>
                                <table style={{ ...w100 }}>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table style={{ ...w100 }}>
                                          <tbody>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Right:
                                              </td>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...fontWeight600,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                {
                                                  singleUserData?.in_line_lunge_right
                                                }
                                              </td>
                                            </tr>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Left:
                                              </td>
                                              <td
                                                style={{
                                                  ...padding10,
                                                  ...f13,
                                                  ...fontWeight600,
                                                }}
                                              >
                                                {
                                                  singleUserData?.in_line_lunge_left
                                                }
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>

                              <td style={{ ...w45, ...fontWeight600 }}>
                                {singleUserData?.in_line_lunge_final_score}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td
                        colspan="1"
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...fontWeight600,
                        }}
                      >
                        {singleUserData?.in_line_lunge_comments}
                      </td>
                    </tr>
                    {/* 4 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        4.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        Shoulder Mobility
                      </td>

                      <td colspan="2" style={{ ...borderBottom }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <tr style={{ ...borderBottom }}>
                              <td style={{ ...borderRight }}>
                                <table style={{ ...w100 }}>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table style={{ ...w100 }}>
                                          <tbody>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Right:
                                              </td>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...fontWeight600,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                {
                                                  singleUserData?.shoulder_mobility_right
                                                }
                                              </td>
                                            </tr>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Left:
                                              </td>
                                              <td
                                                style={{
                                                  ...padding10,
                                                  ...f13,
                                                  ...fontWeight600,
                                                }}
                                              >
                                                {
                                                  singleUserData?.shoulder_mobility_left
                                                }
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>

                              <td style={{ ...w45, ...fontWeight600 }}>
                                {singleUserData?.shoulder_mobility_final_score}
                              </td>
                            </tr>
                            <tr>
                              <td
                                style={{
                                  ...borderRight,
                                  ...padding10,
                                  ...f13,
                                }}
                              >
                                Clearance:{" "}
                                {/* {singleUserData?.shoulder_mobility_clearance} */}
                              </td>
                              <td
                                style={{
                                  ...f13,
                                  ...fontWeight600,
                                }}
                              >
                                {singleUserData?.shoulder_mobility_clearance}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>

                      <td colspan="1" style={{ ...borderLeft }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <td
                              style={{
                                ...borderBottom,
                                ...padding10,
                                ...f13,
                                ...fontWeight600,
                              }}
                            >
                              {singleUserData?.shoulder_mobility_comments}
                            </td>
                            <tr>
                              <td
                                style={{
                                  ...padding10,
                                  ...f13,
                                  ...fontWeight600,
                                }}
                              >
                                {
                                  singleUserData?.shoulder_mobility_clearance_comments
                                }
                              </td>
                            </tr>
                            <tr></tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    {/* 5 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        5.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        Active Straight Leg Raise
                      </td>
                      <td colspan="2" style={{ ...borderBottom }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <tr>
                              <td style={{ ...borderRight }}>
                                <table style={{ ...w100 }}>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table style={{ ...w100 }}>
                                          <tbody>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Right:
                                              </td>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...fontWeight600,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                {
                                                  singleUserData?.active_straight_leg_raise_right
                                                }
                                              </td>
                                            </tr>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Left:
                                              </td>
                                              <td
                                                style={{
                                                  ...padding10,
                                                  ...f13,
                                                  ...fontWeight600,
                                                }}
                                              >
                                                {
                                                  singleUserData?.active_straight_leg_raise_left
                                                }
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>

                              <td style={{ ...w45, ...fontWeight600 }}>
                                {
                                  singleUserData?.active_straight_leg_raise_final_score
                                }
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td
                        colspan="1"
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...fontWeight600,
                        }}
                      >
                        {singleUserData?.active_straight_leg_raise_comments}
                      </td>
                    </tr>
                    {/* 6 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        6.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        Trunk Stability Push- up
                      </td>
                      <td colspan="2" style={{ ...borderBottom }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <td
                              style={{
                                ...borderBottom,
                                ...padding10,
                                ...f13,
                                ...fontWeight600,
                              }}
                            >
                              {singleUserData?.trunk_stability_pushup}
                            </td>
                            <tr>
                              <td style={{ ...f13 }}>
                                <table style={{ ...w100 }}>
                                  <tbody>
                                    <tr>
                                      <td
                                        style={{
                                          ...padding10,
                                          ...f13,
                                          ...borderRight,
                                        }}
                                      >
                                        Clearance:{" "}
                                        {/* {
                                          singleUserData?.trunk_stability_pushup_clearance
                                        } */}
                                      </td>
                                      <td
                                        style={{
                                          ...padding10,
                                          ...f13,
                                          ...fontWeight600,
                                        }}
                                      >
                                        {
                                          singleUserData?.trunk_stability_pushup_clearance
                                        }
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr></tr>
                          </tbody>
                        </table>
                      </td>

                      <td colspan="1" style={{ ...borderLeft, ...f13 }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <td
                              style={{
                                ...borderBottom,
                                ...padding10,
                                ...f13,
                                ...fontWeight600,
                              }}
                            >
                              {singleUserData?.trunk_stability_pushup_comments}
                            </td>
                            <tr>
                              <td
                                style={{
                                  ...padding10,
                                  ...f13,
                                  ...fontWeight600,
                                }}
                              >
                                {
                                  singleUserData?.trunk_stability_pushup_clearance_comments
                                }
                              </td>
                            </tr>
                            <tr></tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    {/* 7 */}
                    <tr>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        7.
                      </td>
                      <td
                        colspan="1"
                        style={{ ...border, ...padding10, ...f13 }}
                      >
                        Rotary Stability
                      </td>
                      <td colspan="2" style={{ ...borderBottom }}>
                        <table style={{ ...w100 }}>
                          <tbody>
                            <tr>
                              <td style={{ ...borderRight }}>
                                <table style={{ ...w100 }}>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table style={{ ...w100 }}>
                                          <tbody>
                                            <tr>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                Right:
                                              </td>
                                              <td
                                                style={{
                                                  ...borderBottom,
                                                  ...fontWeight600,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                {
                                                  singleUserData?.rotary_stability_right
                                                }
                                              </td>
                                            </tr>
                                            {/* <tr>
                                              <td
                                                style={{
                                                  ...borderRight,
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                -
                                              </td>
                                              <td
                                                style={{
                                                  ...padding10,
                                                  ...f13,
                                                }}
                                              >
                                                &nbsp;
                                              </td>
                                            </tr> */}
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>

                              <td style={{ ...w45, ...fontWeight600 }}>
                                {singleUserData?.rotary_stability_final_score}
                              </td>
                            </tr>
                            <tr>
                              <td
                                style={{
                                  ...borderRight,
                                  ...padding10,
                                  ...f13,
                                }}
                              >
                                Clearance:
                                {/* {singleUserData?.rotary_stability_clearance} */}
                              </td>
                              <td
                                style={{
                                  ...f13,
                                  ...fontWeight600,
                                  ...borderTop,
                                }}
                              >
                                {/* Negative:{" "} */}
                                {(singleUserData?.rotary_stability_clearance &&
                                  singleUserData?.rotary_stability_clearance) ||
                                  "-"}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td
                        colspan="1"
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...fontWeight600,
                        }}
                      >
                        {singleUserData?.rotary_stability_comments}
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <br />
              <tr>
                <td>
                  <p style={{ ...textUnderLine, ...textStart }}>
                    <b>Recommendations based on Functional Movement Screen:</b>
                  </p>
                </td>
              </tr>
              <br />
              <br />
              <tr>
                <td colspan="3">
                  <img src={TemplateLogo} alt="" width={600} height={110} />
                  <br />
                  <p style={topAddress}>
                    95, 96, 97, 3rd Floor Wazir Nagar, Kotla Mubarakpur, New
                    Delhi. 110003.
                    <br />
                    Phone: +91 11 4632 11 77, 9717118308 Email:
                    reception@sportingethos.com
                  </p>
                </td>
              </tr>
              <br />
              <br />
              <tr>
                <td>
                  <ul style={{ ...textStart, ...f13 }}>
                    <li>
                      <b>Deep squat:</b> Upper extremity mobility,lower
                      extremity mobility, closed kinetic chain hip flexion and
                      ankle dorsi flexion needs improvement.
                    </li>
                    <li>
                      <b>Hurdle step:</b> Stance leg stability or step leg’s
                      mobility needs improvement.
                    </li>
                    <li>
                      <b>Inline lunge:</b> Inadequate hip mobility of both step
                      and stance leg, inadequate stance leg knee or ankle
                      stability, balance between adductor weakness and abductor
                      tightness needs to be improved, stance leg hip flexor
                      mobility needs to be improved.
                    </li>
                    <li>
                      <b>Shoulder mobility:</b> Glenohumeral mobility needs to
                      be improved.
                    </li>
                    <li>
                      <b>Active straight leg raise:</b>
                      Functional hip mobility needs improvement.
                    </li>
                    <li>
                      <b>Trunk stability push up:</b>
                      Symmetric trunk stability while performing symmetric upper
                      extremity movement in sagittal plane needs improvement.
                    </li>
                    <li>
                      <b>Rotary stability:</b> Asymmetric trunk stability in
                      sagittal and transverse plane while performing upper and
                      lower extremity movement needs improvement.
                    </li>
                  </ul>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </DialogActions>
    </Dialog>
  );
}
