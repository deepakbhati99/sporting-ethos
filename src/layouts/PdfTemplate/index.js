import React from 'react'
import FmsTemplate from './FmsTemplate'
import DashboardLayout from '../../examples/LayoutContainers/DashboardLayout'
import DashboardNavbar from '../../examples/Navbars/DashboardNavbar'
import MDBox from '../../components/MDBox'

export default function Template() {
  return (
    <DashboardLayout>
        <DashboardNavbar />
        <MDBox mb={2} />
        <FmsTemplate />
    </DashboardLayout>
  )
}
