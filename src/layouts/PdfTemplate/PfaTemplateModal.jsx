// DeleteModal.js
import { Dialog, DialogActions } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { getPfaByRgstData } from "../../services/service_api";
import TemplateLogo from "../../assets/images/Sporting-Ethos-Logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFaceFrown as LightFaceFrown } from "@fortawesome/free-regular-svg-icons";
import {
  faFaceFrown as DarkFaceFrown,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";

import { faFaceMeh as LightFaceMeh } from "@fortawesome/free-regular-svg-icons";
import { faFaceMeh as DarkFaceMeh } from "@fortawesome/free-solid-svg-icons";

import { faFaceSmile as LightFaceSmile } from "@fortawesome/free-regular-svg-icons";
import { faFaceSmile as DarkFaceSmile } from "@fortawesome/free-solid-svg-icons";

export default function PfaTemplateModal({
  isOpen,
  onClose,
  onDelete,
  registration_id,
  id,
  userID,
}) {
  const [singleUserData, setSingleUserData] = useState([]);

  useEffect(() => {
    getSingleUserList();
    async function getSingleUserList() {
      const params = { registration_no: userID, id: registration_id };
      const response = await getPfaByRgstData(params);
      setSingleUserData(response.data);
      console.log("response--->", response.data);
    }
  }, [userID, registration_id]);

  const w100 = {
    width: "100%",
  };
  const w50 = {
    width: "50%",
  };
  const textDark = {
    color: "#000",
  };
  const textCenter = {
    textAlign: "center",
  };
  const textStart = {
    textAlign: "left",
  };
  const f12 = {
    fontSize: "12px",
  };
  const f13 = {
    fontSize: "13px",
  };
  const lh1 = {
    lineHeight: "1.2",
  };
  const m0 = {
    margin: "0px",
  };
  const padding10 = {
    padding: "10px",
  };
  const fontWeight600 = {
    fontWeight: "600",
  };
  const fontWeight400 = {
    fontWeight: "400",
  };
  const paddingY10 = {
    paddingTop: "10px",
    paddingBottom: "10px",
  };
  const paddingX10 = {
    paddingLeft: "10px",
    paddingRight: "10px",
  };
  const paddingX50 = {
    paddingLeft: "50px",
    paddingRight: "50px",
  };
  const borderLeft = {
    borderLeft: "1px solid #000",
  };
  const borderRight = {
    borderRight: "1px solid #000",
  };
  const borderTop = {
    borderTop: "1px solid #000",
  };
  const borderBottom = {
    borderBottom: "1px solid #000",
  };
  const border = {
    border: "1px solid #000",
  };
  const textUnderLine = {
    textDecoration: "underline",
  };
  const textFiled = { ...textStart, ...fontWeight400, ...f12 };
  const topAddress = {
    ...f12,
    ...lh1,
    ...m0,
    ...fontWeight600,
  };
  const main = {
    ...w100,
    ...textDark,
    ...textCenter,
  };
  const mainHeading = {
    ...fontWeight600,
    ...textUnderLine,
    ...textCenter,
  };
  const topContent = {
    ...textStart,
    ...f12,
    ...paddingY10,
  };

  const w45 = {
    width: "45%",
  };
  const w15 = {
    width: "15%",
  };

  const divToPrintRef = useRef(null);

  // Function to handle the print action for a specific div
  // const handlePrint = () => {
  //   const divToPrint = divToPrintRef.current;

  //   if (divToPrint) {
  //     const printWindow = window;
  //     const printDocument = printWindow.document;

  //     // Clone the content of the div
  //     const clone = divToPrint.cloneNode(true);

  //     // Create a new window with the cloned content
  //     printDocument.body.innerHTML = "";
  //     printDocument.body.appendChild(clone);

  //     // Trigger the browser's print dialog
  //     printWindow.print();
  //   }
  // };
  const handlePrint = () => {
    const divToPrint = divToPrintRef.current;

    if (divToPrint) {
      const printWindow = window;
      const printDocument = printWindow.document;

      // Clone the content of the div
      const clone = divToPrint.cloneNode(true);

      // Create a new window with the cloned content
      printDocument.body.innerHTML = "";
      printDocument.body.appendChild(clone);

      // Trigger the browser's print dialog
      printWindow.print();
      window.location.reload();
    }
  };

  const printButton = {
    fontSize: "15px",
    border: "0px",
    borderRadius: "5px",
    color: "#fff",
    background: "#478fed",
  };
  const fs18 = {
    fontSize: "18px",
  };

  const textCapitalize = {
    textTransform: "capitalize",
  };

  return (
    <Dialog
      fullScreen
      open={isOpen}
      keepMounted
      onClose={onClose}
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogActions>
        <div className="">
          <div className="text-end">
            <button
              onClick={handlePrint}
              style={{ ...printButton, ...paddingX10 }}
            >
              Print
            </button>
            &nbsp;
            <button onClick={onClose} style={{ ...printButton, ...paddingX10 }}>
              Cancel
            </button>
          </div>
          <div style={main} id="divToPrint" ref={divToPrintRef}>
            <table style={w100} collapse="1">
              <tr>
                <td colspan="3">
                  <img src={TemplateLogo} alt="" width={600} height={110} />
                  <br />
                  <p style={topAddress}>
                    95, 96, 97, 3rd Floor Wazir Nagar, Kotla Mubarakpur, New
                    Delhi. 110003.
                    <br />
                    Phone: +91 11 4632 11 77, 9717118308 Email:
                    reception@sportingethos.com
                  </p>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr style={mainHeading}>
                <td colspan="3">PHYSICAL FITNESS ASSESSMENT RESULTS</td>
              </tr>
              &nbsp;
              <tr>
                <td colspan="1">
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Name: <b>{singleUserData?.name}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Age(years): <b>{singleUserData?.age}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          M/F: <b>{singleUserData?.mf}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Sport/Activity : <b>{singleUserData?.sport}</b>
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Registration No:{" "}
                          <b>{singleUserData?.registration_no}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Height(cm): <b>{singleUserData?.height}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Weight(kg): <b>{singleUserData?.weight}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          Date: <b>{singleUserData?.date}</b>
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          Room Temp/Humidity (Day 1):{" "}
                          <b>{singleUserData?.room_temp_humidity_day1}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          Room Temp/Humidity (Day 2):{" "}
                          <b>{singleUserData?.room_temp_humidity_day2}</b>
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          Timing of last meal (Day 1):{" "}
                          <b>{singleUserData?.timing_of_last_meal_day1}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          What did you eat (Day 1):{" "}
                          <b>{singleUserData?.what_did_you_eat_day1}</b>
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          Timing of last meal (Day 2):{" "}
                          <b>{singleUserData?.timing_of_last_meal_day2}</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          What did you eat (Day 2):{" "}
                          <b>{singleUserData?.what_did_you_eat_day2}</b>
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          Feeling (Day 1):{" "}
                          <FontAwesomeIcon
                            icon={
                              (singleUserData?.feeling_day1 == 1 &&
                                DarkFaceSmile) ||
                              LightFaceSmile
                            }
                            style={{ ...fs18 }}
                          />
                          &nbsp;&nbsp;
                          <FontAwesomeIcon
                            icon={
                              (singleUserData?.feeling_day1 == 2 &&
                                DarkFaceMeh) ||
                              LightFaceMeh
                            }
                            style={{ ...fs18 }}
                          />
                          &nbsp;&nbsp;
                          <FontAwesomeIcon
                            icon={
                              (singleUserData?.feeling_day1 == 3 &&
                                DarkFaceFrown) ||
                              LightFaceFrown
                            }
                            style={{ ...fs18 }}
                          />
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="2"
                        >
                          Feeling (Day 2):{" "}
                          <FontAwesomeIcon
                            icon={
                              (singleUserData?.feeling_day2 == 1 &&
                                DarkFaceSmile) ||
                              LightFaceSmile
                            }
                            style={{ ...fs18 }}
                          />
                          &nbsp;&nbsp;
                          <FontAwesomeIcon
                            icon={
                              (singleUserData?.feeling_day2 == 2 &&
                                DarkFaceMeh) ||
                              LightFaceMeh
                            }
                            style={{ ...fs18 }}
                          />
                          &nbsp;&nbsp;
                          <FontAwesomeIcon
                            icon={
                              (singleUserData?.feeling_day2 == 3 &&
                                DarkFaceFrown) ||
                              LightFaceFrown
                            }
                            style={{ ...fs18 }}
                          />
                        </td>
                      </tr>
                      {/* Note */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          <b>Note:</b>
                          <br /> {singleUserData?.notes}
                        </td>
                      </tr>
                      {/* Medical and Injury History */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          <b> Medical and Injury History</b> <br />
                          {singleUserData?.medical_and_injury_history}
                        </td>
                      </tr>
                      {/* 1. Body Composition */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          <b> 1. Body Composition</b> <br />
                          {singleUserData?.body_composition}
                        </td>
                      </tr>
                      {/* 2. Flexibility, Joint Stability and Posture Screening */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          <b>
                            2. Flexibility, Joint Stability and Posture
                            Screening
                          </b>{" "}
                          <br />
                          {
                            singleUserData?.flexibility_joint_stability_and_posture_screening
                          }
                        </td>
                      </tr>
                      {/* 3. Hand Grip Test */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          3. Hand Grip Test
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          2
                        </td>
                      </tr>
                      {/* Grip Strength (kg) R */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b> Grip Strength (kg) R</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.hand_grip_test_grip_strength_kg_r1}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {singleUserData?.hand_grip_test_grip_strength_kg_r2}
                        </td>
                      </tr>
                      {/* Grip Strength (kg) L */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b> Grip Strength (kg) L</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.hand_grip_test_grip_strength_kg_l1}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {singleUserData?.hand_grip_test_grip_strength_kg_l2}
                        </td>
                      </tr>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.hand_grip_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 4.Flexibility, Joint Stability and Posture Screening  */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="3"
                        >
                          4.Flexibility, Joint Stability and Posture Screening
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...fontWeight600,
                          }}
                        >
                          Test
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...fontWeight600,
                          }}
                        >
                          Score
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...fontWeight600,
                          }}
                        >
                          Comments
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...fontWeight600,
                            ...textStart,
                          }}
                        >
                          Deep Squat
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.flexibilityjointscreeningtestdeepsquartscore
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.flexibilityjointscreeningtestdeepsquartcomments
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...fontWeight600,
                            ...textStart,
                          }}
                        >
                          Ankle Clearance
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.flexibilityjointscreeningtestankleclearancescore
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.flexibilityjointscreeningtestankleclearancecomments
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...fontWeight600,
                            ...textStart,
                          }}
                        >
                          Active Straight Leg Raise
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.flexibilityjointscreeningtestactivestraightlegraisescore
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.flexibilityjointscreeningtestactivestraightlegraisecomments
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="3"
                        >
                          {singleUserData?.flexibilityjointscreeningtwo}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 5.Sprint & Agility Tests */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          5.Sprint & Agility Tests
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          10m Sprint
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          5m(sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_10msprint_5msec1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_10msprint_5msec2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_10msprint_5msec3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          10m (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_10msprint10msec1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_10msprint_10msec2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_10msprint_10msec3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          &nbsp;
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          Pro Agility
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Time Taken (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_pro_agility_time_taken_sec_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_pro_agility_time_taken_sec_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprint_agility_tests_pro_agility_time_taken_sec_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.sprint_agility_tests}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 6.Sprint & Agility Tests */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          6.Sprint & Agility Tests
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          20m Sprint
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          10m(sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.sprintagilitytests_20msprint10msec1}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.sprintagilitytests_20msprint10msec2}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          20m (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.sprintagilitytests_20msprint20msec1}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.sprintagilitytests_20msprint20msec2}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          &nbsp;
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          Arrowhead Agility
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Time Taken (sec)Right
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprintagilitytests_arrowheadagility_timetakensecright1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprintagilitytests_arrowheadagility_timetakensecright2
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Time Taken (sec)Left
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprintagilitytests_arrowheadagility_timetakensecleft1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sprintagilitytests_arrowheadagility_timetakensecleft2
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.sprintagilitytests_20m}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 7.Lower Body Explosive Power */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          7.Lower Body Explosive Power
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Squat Jump</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Height (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_explosive_power_squat_jump_heightm_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_explosive_power_squat_jump_heightm_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_explosive_power_squat_jump_heightm_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="4"
                        >
                          &nbsp;
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b> Counter Movement Jump (Hands on Hip)</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Height (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_counter_movement_jump_handsonhip_heightm_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_counter_movement_jump_handsonhip_heightm_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_counter_movement_jump_handsonhip_heightm_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Arm Swing: Height (m)</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_counter_movement_jump_heightm_armswing_heightm_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_counter_movement_jump_heightm_armswing_heightm_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.lower_body_counter_movement_jump_heightm_armswing_heightm_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.lower_body_explosive_power}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          <b>Counter Movement Jump (Single leg)</b>
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Right Leg </b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Height (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.counter_movement_jump_singleleg_rightleg_heightm_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.counter_movement_jump_singleleg_rightleg_heightm_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.counter_movement_jump_singleleg_rightleg_heightm_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Left Leg </b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Height (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.counter_movement_jump_singleleg_leftleg_heightm_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.counter_movement_jump_singleleg_leftleg_heightm_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.counter_movement_jump_singleleg_leftleg_heightm_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.counter_movement_jump_singleleg}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 8.Lower Body Explosive Power (Horizontal) */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          8.Lower Body Explosive Power (Horizontal)
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Broad Jump</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Distance (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.loverbodyhorizontal_boardjump_distance_m1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.loverbodyhorizontal_boardjump_distance_m2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.loverbodyhorizontal_boardjump_distance_m3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="4"
                        >
                          &nbsp;
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b> Single leg broad jump (Right)</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Distance (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.loverbodyhorizontal_singlelegboardjump_right_distance_m1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.loverbodyhorizontal_singlelegboardjump_right_distance_m2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.loverbodyhorizontal_singlelegboardjump_right_distance_m3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {
                            singleUserData?.lover_body_explosive_power_horizontal
                          }
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 9.Flamingo Balance Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          8.Flamingo Balance Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b> &nbsp;</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Right
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Left
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Time (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.flamingoblancetext_right_time}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.flamingoblancetext_left_time}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.flamingoblancetext}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 10.Star Excursion Balance Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          10.Star Excursion Balance Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Anterior
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Posterior-medial
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Posterior-lateral
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Right
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.starexcursionbalancetest_anterior_right
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.starexcursionbalancetest_posterior_medial_right
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.starexcursionbalancetest_posterior_lateral_right
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Left
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.starexcursionbalancetest_anterior_left
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.starexcursionbalancetest_posterior_medial_left
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.starexcursionbalancetest_posterior_lateral_left
                          }
                        </td>
                      </tr>

                      {/* <tr>
                      <td
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                          ...textStart,
                        }}
                        colspan="4"
                      >
                        {singleUserData?.star_excursion_balance_test}
                      </td>
                    </tr> */}
                    </tbody>
                  </table>
                  <br />
                  {/* 11.Reaction Time Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          11.Reaction Time Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Average Reaction Time (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reaction_time_test_average_reaction_time_sec_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reaction_time_test_average_reaction_time_sec_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reaction_time_test_average_reaction_time_sec_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.reaction_time_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 12.Reactive Agility Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          12.Reactive Agility Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Average Reaction Time (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Decision errors
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Misses
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Attempt 1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reactive_time_test_average_reaction_time_sec_attempt_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reactive_time_test_decisionerrors_attempt_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.reactive_time_test_misses_attempt_1}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Attempt 2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reactive_time_test_average_reaction_time_sec_attempt_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.reactive_time_test_decisionerrors_attempt_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.reactive_time_test_misses_attempt_2}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.reactive_agility_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 13.Sports Specific Reaction Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          13.Sports Specific Reaction Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Average Reaction Time (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sportsspecificreactiontest_awg_reaction_time_sec_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sportsspecificreactiontest_awg_reaction_time_sec_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sportsspecificreactiontest_awg_reaction_time_sec_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.sports_specific_reaction_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 14.Upper Body Explosive Power */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          14.Upper Body Explosive Power
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Distance 1kg (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upper_body_explosive_power_distance_1kg_m_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upper_body_explosive_power_distance_1kg_m_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upper_body_explosive_power_distance_1kg_m_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Distance 2kg (m)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sportsspecificreactiontest_awg_reaction_time_sec_1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sportsspecificreactiontest_awg_reaction_time_sec_2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.sportsspecificreactiontest_awg_reaction_time_sec_3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.upper_body_explosive_power}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 15.Upper Body Explosive Power */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          15.Upper Body Explosive Power
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1 RM Bench Press (kg)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          30%={" "}
                          {singleUserData?.upperbodypower_1rmbench_press_30}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          40%={" "}
                          {singleUserData?.upperbodypower_1rmbench_press_30}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          50%={" "}
                          {singleUserData?.upperbodypower_1rmbench_press_30}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Barbell Sensei (W)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.upperbodypower_barbell_sensei_w30}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.upperbodypower_barbell_sensei_w40}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.upperbodypower_barbell_sensei_w50}
                        </td>
                      </tr>

                      <tr
                        style={{
                          ...border,
                          ...padding10,
                          ...f13,
                        }}
                        colspan="4"
                      >
                        <td>
                          <b>Rotational Power</b>
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Right Side</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Peak Power (W)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upperbody_rotationalpower_rightside_peakpower_w1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upperbody_rotationalpower_rightside_peakpower_w2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upperbody_rotationalpower_rightside_peakpower_w3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          <b>Left Side</b>
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Peak Power (W)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upperbody_rotationalpower_leftside_peakpower_w1
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upperbody_rotationalpower_leftside_peakpower_w2
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.upperbody_rotationalpower_leftside_peakpower_w3
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.upper_body_explosive_power}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 16.Rotational Power */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          16.Rotational Power
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          3
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Right (W)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.rotationalpower_right_w1}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.rotationalpower_right_w2}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.rotationalpower_right_w3}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Left (W)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.rotationalpower_left_w1}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.rotationalpower_left_w2}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {singleUserData?.rotationalpower_left_w3}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.rotational_power}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 17.Core Endurance */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          17.Core Endurance
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          Planks
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Total time (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {singleUserData?.core_endurance_planks_total_time_sec}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          Side Planks
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Right
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Left
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Total time (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.core_endurance_side_planks_total_time_sec_right
                          }
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.core_endurance_side_planks_total_time_sec_left
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.core_endurance}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 18.Local Muscular Endurance */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          18.Local Muscular Endurance
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Squats to Exhaustion (reps) (1min/2mins)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.localmuscularendurance_squatstoexhaustionreps1min2mins
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Pull ups/TRX Pullups
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.local_muscular_endurance_pull_ups_trx_pullups
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Push Ups
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {singleUserData?.local_muscular_endurance_push_ups}
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.local_muscular_endurance}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 19.Anaerobic Capacity (Shuttle Run) */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          19.Anaerobic Capacity (Shuttle Run)
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          1
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          2
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Time Taken (sec)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.anaerobic_capacity_shuttle_run_time_taken_sec1
                          }
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.anaerobic_capacity_shuttle_run_time_taken_sec2
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Average Time (sec)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.anaerobic_capacity_shuttle_run_average_time_sec
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.anaerobic_capacity_shuttle_run}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 20.Aerobic Capacity - Beep Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          20.Aerobic Capacity - Beep Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Beep Test Level
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.aerobic_capacity_beep_test_beep_test_level
                          }
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Number of Shuttles
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.aerobic_capacity_beep_test_number_of_shuttles
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          VO2 Max (ml/kg/min)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.aerobic_capacity_beep_test_vo2_max_ml_kg_min
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.aerobic_capacity_beep_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 21.Aerobic Capacity – Yo-Yo IR1 Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          21.Aerobic Capacity – Yo-Yo IR1 Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Yo-Yo Test Level
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.aerobiccapacity_yoyoir1test_yoyo_test_level
                          }
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Number of Shuttles
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.aerobiccapacity_yoyoir1test_number_of_shuttles
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Total distance (m)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.aerobiccapacity_yoyoir1test_total_distance_m
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.aerobic_capacity_yo_yo_ir1_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 22.Aerobic Capacity – Bruce Protocol */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          22.Aerobic Capacity – Bruce Protocol
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Bruce Protocol (mins)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.aerobic_bruceprotocol_bruceprotocol_min
                          }
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          VO2 Max (ml/kg/min)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          {
                            singleUserData?.aerobic_bruceprotocol_vo2max_ml_kg_min
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          HR (20sec/1min/2mins)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.aerobic_bruceprotocol_hr_20sec_1min_2_min
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.aerobic_capacity_bruce_protocol}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 23.Aerobic Capacity – Cooper Test */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          23.Aerobic Capacity – Cooper Test
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Total distance (m)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.aerobic_capacity_cooper_test_total_distance_m
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          VO2 Max (ml/kg/min)
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.aerobic_capacity_cooper_test_vo2_max_ml_kg_min
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.aerobic_capacity_cooper_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  {/* 24.Maximal Strength Test:  */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                            ...fontWeight600,
                          }}
                          colspan="4"
                        >
                          24.Maximal Strength Test:
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          &nbsp;
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          1-RM (kg)
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Back Squat
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.maximal_strength_test_1rm_kg_back_squat
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Bench Press
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.maximal_strength_test_1rm_kg_bench_press
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                        >
                          Deadlift
                        </td>

                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                          }}
                          colspan="2"
                        >
                          {
                            singleUserData?.maximal_strength_test_1rm_kg_deadlift
                          }
                        </td>
                      </tr>

                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textStart,
                          }}
                          colspan="4"
                        >
                          {singleUserData?.maximal_strength_test}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <br />
                  <p>
                    <b>OBSERVATIONS AND RECOMMENDATIONS</b>
                  </p>
                  {/* 24.Maximal Strength Test:  */}
                  <table style={{ ...w100, ...border }}>
                    <tbody>
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...fontWeight600,
                          }}
                        >
                          Parameter
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...fontWeight600,
                          }}
                        >
                          Priority
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...fontWeight600,
                          }}
                        >
                          Needs Improvement
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...fontWeight600,
                          }}
                        >
                          At Par
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...fontWeight600,
                          }}
                        >
                          Good
                        </td>
                      </tr>
                      {/* 1 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Injury/Niggle Management
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.injury_niggle_management_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.injury_niggle_management_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.injury_niggle_management_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.injury_niggle_management_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                      {/* 2 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Functional Movement
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.functional_movement_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.functional_movement_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.functional_movement_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.functional_movement_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                      {/* 3 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Core Endurance
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.core_endurance_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.core_endurance_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.core_endurance_at_par === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.core_endurance_good === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                      </tr>
                      {/* 4 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Reaction Time
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.reaction_time_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.reaction_time_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.reaction_time_at_par === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.reaction_time_good === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                      </tr>
                      {/* 5 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Muscular Endurance (lower)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.muscular_endurance_lower_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.muscular_endurance_lower_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.muscular_endurance_lower_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.muscular_endurance_lower_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                      {/* 6 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Muscular Endurance (upper)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.muscular_endurance_upper_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.muscular_endurance_upper_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.muscular_endurance_upper_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.muscular_endurance_upper_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                      {/* 7 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Speed
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.speed_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.speed_needs_improvement === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.speed_at_par === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.speed_good === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                      </tr>
                      {/* 8 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Agility
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.agility_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.agility_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.agility_at_par === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.agility_good === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                      </tr>
                      {/* 9 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Balance
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.balance_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.balance_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.balance_at_par === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.balance_good === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                      </tr>
                      {/* 10 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Anaerobic Fitness
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.anaerobic_fitness_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.anaerobic_fitness_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.anaerobic_fitness_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.anaerobic_fitness_good === "1" && (
                            <FontAwesomeIcon icon={faCheck} />
                          )) ||
                            ""}
                        </td>
                      </tr>
                      {/* 11 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Cardiovascular Endurance
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.cardiovascular_endurance_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.cardiovascular_endurance_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.cardiovascular_endurance_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.cardiovascular_endurance_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                      {/* 12 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Explosive Power (lower body)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.explosive_power_lower_body_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.explosive_power_lower_body_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.explosive_power_lower_body_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.explosive_power_lower_body_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                      {/* 13 */}
                      <tr>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          Explosive Power (upper body)
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                            ...textCapitalize,
                          }}
                        >
                          {singleUserData?.explosive_power_upper_body_priority}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.explosive_power_upper_body_needs_improvement ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.explosive_power_upper_body_at_par ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                        <td
                          style={{
                            ...border,
                            ...padding10,
                            ...f13,
                            ...textCenter,
                          }}
                        >
                          {(singleUserData?.explosive_power_upper_body_good ===
                            "1" && <FontAwesomeIcon icon={faCheck} />) ||
                            ""}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                </td>
              </tr>
              <tr>
                <td
                  style={{
                    ...padding10,
                    ...f13,
                    ...textStart,
                  }}
                >
                  <p>
                    <b> Next Assessment:</b> We recommend that repeat
                    assessments should be conducted every 3 months to track
                    ATHLETE’s progress. We recommend that the next assessment
                    takes place in the month of -2023.
                  </p>
                  <br />
                  <p style={{ ...textUnderLine }}>
                    <b>Team Sporting Ethos</b>
                  </p>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </DialogActions>
    </Dialog>
  );
}
