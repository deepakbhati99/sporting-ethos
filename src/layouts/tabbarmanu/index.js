import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import MDBox from "../../components/MDBox";
import { HandGripTestFormStep1, AclRehabilitationOrotocol, FunctionalMovementScreeningForm } from "../../components/Form";
import MDTypography from '../../components/MDTypography';
import MDButton from '../../components/MDButton';
import DataTable from '../../examples/Tables/DataTable';
import authorsTableData from './data';

import { Link, useParams } from "react-router-dom";
import { useState } from 'react';
import { getAclByRGst, getPfaByRGst } from '../../services/service_api';
import { useEffect } from 'react';
interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 0 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function FullWidthTabs() {
  const columns  = [
    { Header: "registration", accessor: "registration", align: "left" },
    { Header: "name", accessor: "name", align: "left" },
    { Header: "created_at", accessor: "created_at", align: "left" },
    { Header: "updated_at", accessor: "updated_at", align: "left" },
    // { Header: "sport", accessor: "sport", align: "left" },
    // { Header: "date", accessor: "date", align: "center" }, 
    { Header: "action", accessor: "action", align: "center" },
  ];
  const [formType, setFormType] = useState('get_pfa_by_rgst');
  const [rowData, setRowData] = useState([]);
  const [tabData, setTabData] = useState();
  const [loading, setLoading] = useState(false);
  // const { rows } = rowData;
  const { rows } = authorsTableData(formType);

  const  promies  = useParams();
  const userId = promies?.id;

  // console.log('userID', userId);

  const theme = useTheme();
  const formTypeTab = localStorage.getItem("formType");
  const [value, setValue] = React.useState(0);
  

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  // useEffect(() => {
  //   setLoading(true);
  // }, [formType])

  useEffect(() => {
    setLoading(true);

    const loadingTimer = setTimeout(() => {
      setLoading(false); // Set loading back to false after the first timeout
    }, 2000); // Delay for 2 seconds (adjust the time as needed)

    return () => {
      clearTimeout(loadingTimer); // Clear the loading timer if the component unmounts before the timeout completes
    };
  }, [formType]);
  

  // console.log(localStorage.getItem("formType"));

  return (
    <>
    {loading && <div class="loading">Loading&#8230;</div>}
    <DashboardLayout>
    <DashboardNavbar />
    <MDBox mr={2}>
        <AppBar position="static" ml={5}> 
          <Tabs 
            value={value}
            onChange={handleChange}
            indicatorColor="secondary"
            textColor="inherit"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Physical Fitness Assessment" {...a11yProps(0)}
                  onClick={()=>setFormType('get_pfa_by_rgst')}/>

            <Tab label="Acl Rehabilitation Protocol" {...a11yProps(1)}
                  onClick={()=>setFormType('get_acl_by_rgst')}/>

            <Tab label="Functional Movement Screening Form" {...a11yProps(2)}
                  onClick={()=>setFormType('get_fms_by_rgst')}/>

            {/* <Tab label="Weekly Workload & Recovery Report" {...a11yProps(3)} /> */}
          </Tabs>
        </AppBar>
      </MDBox>
    <MDBox mb={2} />
    {/* <Box sx={{ bgcolor: 'background.paper', width: "100%" }}> */}
      <MDBox mr={2}
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}>
        <TabPanel value={value} index={0} dir={theme.direction}>
          <MDBox mt={3} display="flex" justifyContent="flex-end">
            <Link to={`/forms/step1/${userId}`}>
              <MDButton
                  variant="gradient"
                  color="info"  >
                  Add Form
              </MDButton>
            </Link>
            </MDBox>
          <MDBox pt={3}>
                <DataTable  table={{ columns, rows }} isSorted={false} entriesPerPage={false}  showTotalEntries={false} noEndBorder />
              </MDBox>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          {/* <AclRehabilitationOrotocol /> */}
          <MDBox mt={3} display="flex" justifyContent="flex-end">   
          <Link to={`/forms/AclRehabilitationOrotocol/${userId}`}>
                      <MDButton
                          variant="gradient"
                          color="info"  >
                          Add Form
                      </MDButton></Link>
                    </MDBox>
              <MDBox pt={3}>
                <DataTable  table={{ columns, rows }} isSorted={false} entriesPerPage={false}  showTotalEntries={false} noEndBorder />
              </MDBox>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          {/* <FunctionalMovementScreeningForm /> */}
          <MDBox mt={3} display="flex" justifyContent="flex-end">   
          <Link to={`/forms/FunctionalMovementScreeningForm/${userId}`}>
                      <MDButton
                          variant="gradient"
                          color="info"  >
                          Add Form
                      </MDButton></Link>
                    </MDBox>
          <MDBox pt={3}>
                <DataTable table={{ columns, rows }} isSorted={false} entriesPerPage={false}  showTotalEntries={false} noEndBorder />
              </MDBox>
        </TabPanel>
          {/* Weekly Workload & Recovery Report */}
        <TabPanel value={value} index={3} dir={theme.direction}   >
          <MDBox mt={3} display="flex" justifyContent="flex-end">   
          <Link to={""}>
                      <MDButton
                          variant="gradient"
                          color="info"  >
                          Add Form
                      </MDButton></Link>

                    </MDBox>
            <MDBox pt={3}>
                <DataTable  table={{ columns, rows }} isSorted={false} entriesPerPage={false}  showTotalEntries={false} noEndBorder  />
              </MDBox>
        </TabPanel>
      </MDBox>
    {/* </Box> */}
    </DashboardLayout>
    </>
  );
}

