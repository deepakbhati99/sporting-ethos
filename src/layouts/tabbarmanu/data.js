// Material Dashboard 2 React components
import MDBox from "../../components/MDBox";
import MDTypography from "../../components/MDTypography";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getAclByRGst, getFmsByRGst, getPfaByRGst } from "../../services/service_api";
import DeleteUserModal from "../modal/DeleteUserModal";
import FmsTemplate from "../PdfTemplate/FmsTemplate";
import FmsTemplateModal from "../PdfTemplate/FmsTemplateModal";
import PfaTemplateModal from "../PdfTemplate/PfaTemplateModal";

export default function Data(formType) {
  const [tableData, setTableData] = useState([]);
  const navigate = useNavigate();

  const  promies  = useParams();
  const userId = promies?.id;
  const [registrationNo, setRegistrationNo] = useState(null);
  const [tableId, setTableId] = useState(null)
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

 const handleDelete = () => {
   setIsDeleteModalOpen(false);
 };

 const openDeleteModal = (id) => {
   setIsDeleteModalOpen(true);
   setRegistrationNo(userId);
   setTableId(id);
 };


  useEffect(() => {
    getUserListData();
      async function getUserListData() {
        const params = {registration_no: userId}
        // console.log("params",params);
        if(formType === "get_pfa_by_rgst"){
          const response = await getPfaByRGst(params);
          setTableData(response['data']);
          // console.log("table Data",tableData);
        } else if(formType === "get_acl_by_rgst"){
          const response = await getAclByRGst(params);
          setTableData(response['data']);
        } else if(formType === "get_fms_by_rgst"){
          const response = await getFmsByRGst(params);
          setTableData(response['data']);
        }
      }
  }, [userId, formType, isDeleteModalOpen]);


  function handleEditData(obj){
    if(formType === "get_pfa_by_rgst"){
      navigate(`/forms/step1/edit-pfa/${userId}/${obj}`);
    } else if(formType === "get_acl_by_rgst"){
      // console.log('get_acl_by_rgst');
      navigate(`/forms/edit-acl/AclRehabilitationProtocol/${userId}/${obj}`);
    } else if(formType === "get_fms_by_rgst"){
      navigate(`/forms/edit-fms/FunctionalMovementScreeningForm/${userId}/${obj}`);
    }
  }

  const [isPrintModalOpen, setIsPrintModalOpen] = useState(false);
  const [isPrintPfaModalOpen, setIsPrintPfaModalOpen] = useState(false);

 const handleFmsPrint = () => {
  setIsPrintModalOpen(false);
};

const handlePfaPrint = () => {
  setIsPrintPfaModalOpen(false);
};

  function handlePrintFormData(obj){
    setRegistrationNo(obj);
    if(formType === "get_pfa_by_rgst"){
      setIsPrintPfaModalOpen(true);
    } else if(formType === "get_acl_by_rgst"){
      navigate(``);
    } else if(formType === "get_fms_by_rgst"){
      setIsPrintModalOpen(true);
    }
  }
  

  // const Author = ({ image, name, email }) => (
  //   <MDBox display="flex" alignItems="center" lineHeight={1}>
  //     <MDAvatar src={image} name={name} size="sm" />
  //     <MDBox ml={2} lineHeight={1}>
  //       <MDTypography display="block" variant="button" fontWeight="medium">
  //         {name}
  //       </MDTypography>
  //       <MDTypography variant="caption">{email}</MDTypography>
  //     </MDBox>
  //   </MDBox>
  // );

  const Job = ({ title, description }) => (
    <MDBox lineHeight={1} textAlign="left">
      <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
        {title}
      </MDTypography>
      <MDTypography variant="caption">{description}</MDTypography>
    </MDBox>
  );

  // console.log('tableData', tableData);

  return {
    columns: [
      { Header: "registration", accessor: "registration", align: "left" },
      { Header: "name", accessor: "name", align: "left" },
      { Header: "created_at", accessor: "created_at", align: "left" },
      { Header: "updated_at", accessor: "updated_at", align: "left" },
      // { Header: "sport", accessor: "sport", align: "left" },
      // { Header: "date", accessor: "date", align: "center" }, 
      { Header: "action", accessor: "action", align: "center" },
    ],

    rows : tableData && 
    (tableData?.map((rowData, index) => ({
      // registration: <Author image={team2} name="John Michael" email="" />,
      registration:(<>
        <MDTypography
          component="a"
          href="#"
          variant="caption"
          color="text"
          fontWeight="medium"
          key={`registration_no_${index}`}
        >{rowData.id}</MDTypography>
          </>
      ),
      name:(
        <MDTypography
          component="a"
          href="#"
          variant="caption"
          color="text"
          fontWeight="medium"
          className="text-capitalize"
          >
            {rowData.name}
        </MDTypography>
      ),
      role: <Job title="Admin" description="" />,
      created_at: (
        <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
          {rowData.createdAt}
        </MDTypography>
      ),
      updated_at: (
        <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
          {rowData.updatedAt}
        </MDTypography>
      ),
      // date: (
      //     <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
      //      25/08/2023
      //     </MDTypography>
      //   ),
      action: (
          <MDBox>
              <MDTypography style={{ cursor: 'pointer' }} component="a" variant="caption" color="text" fontWeight="medium" mr={2} onClick={() => handleEditData(rowData.id)}>
              Edit
              </MDTypography>
              <MDTypography style={{ cursor: 'pointer' }} component="a" onClick={()=>openDeleteModal(rowData.id)} mr={2} variant="caption" color="text" fontWeight="medium">
              Delete
              </MDTypography>
              <DeleteUserModal
                isOpen={isDeleteModalOpen}
                onClose={() => setIsDeleteModalOpen(false)} // Close the modal when Cancel is clicked
                onDelete={handleDelete} // Call the handleDelete function when Delete is confirmed
                registration_id={userId}
                id={tableId}
                formType={formType}
              />
              <MDTypography
                style={{ cursor: 'pointer' }}
                component="a"
                variant="caption"
                color="text"
                fontWeight="medium"
                onClick={() => handlePrintFormData(rowData.id)}>
              Print
              </MDTypography>
              <FmsTemplateModal
                isOpen={isPrintModalOpen}
                onClose={() => setIsPrintModalOpen(false)} // Close the modal when Cancel is clicked
                onDelete={handleFmsPrint} // Call the handleDelete function when Delete is confirmed
                registration_id={registrationNo}
                id={index}
                userID={promies?.id}
              />
              <PfaTemplateModal
              isOpen={isPrintPfaModalOpen}
              onClose={() => setIsPrintPfaModalOpen(false)} // Close the modal when Cancel is clicked
              onDelete={handlePfaPrint} // Call the handleDelete function when Delete is confirmed
              registration_id={registrationNo}
              id={index}
              userID={promies?.id}
            />
          </MDBox>
      ),
    })))|| []
  };
}
