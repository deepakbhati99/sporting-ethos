// DeleteModal.js
import { Button, Dialog, DialogActions, DialogContent, DialogContentText } from '@mui/material';
import React from 'react';
import AuthService from "../../services/auth-service";
import { deleteAclProtocol, deleteFmsProtocol, deletePfaProtocol, deleteUserData, getUserList } from '../../services/service_api';
import { toast } from 'react-toastify';


export default function DeleteUserModal({ isOpen, onClose, onDelete, registration_id, formType, id }) {
  const toastConfig = {
    position: 'top-right',
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

    async function deleteUserListData() {
        const params = {
            id: id,
            registration_no: registration_id,
          };
          if(formType == "get_pfa_by_rgst"){
            const response = await deletePfaProtocol(params);
            if (response.status == true) {
                toast.success(response.message, toastConfig);
                onDelete();
              } else {
                toast.error(response.message, toastConfig);
              }
          } else if(formType == "get_acl_by_rgst"){
            const response = await deleteAclProtocol(params);
            if (response.status == true) {
                toast.success(response.message, toastConfig);
                onDelete();
              } else {
                toast.error(response.message, toastConfig);
              }
          } else if(formType == "get_fms_by_rgst"){
            const response = await deleteFmsProtocol(params);
            if (response.status == true) {
                toast.success(response.message, toastConfig);
                onDelete();
              } else {
                toast.error(response.message, toastConfig);
              }
          }
      }

  return (
        <Dialog
        open={isOpen}
        keepMounted
        onClose={onClose}
        aria-describedby="alert-dialog-slide-description"
        >
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                    Are you sure you want to delete this item?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={deleteUserListData}>Delete</Button>
                <Button onClick={onClose}>Cancel</Button>
            </DialogActions>
        </Dialog>
  );
}
