import { useLocation, Link, useNavigate } from "react-router-dom";
import { useContext, useState } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../components/MDBox";
import MDTypography from "../../components/MDTypography";
import MDAlert from "../../components/MDAlert";

// Material Dashboard 2 React example components
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import Footer from "../../examples/Footer";
import DataTable from "../../examples/Tables/DataTable";
// Data
import authorsTableData from "./data";
import MDButton from "../../components/MDButton";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import MDInput from "../../components/MDInput";
import { Icon, IconButton } from "@mui/material";
import Data from "./data";
import AuthService from "../../services/auth-service";
import { AuthContext } from "../../context";
import {
  createUserData,
  getSingleUserData,
  getUserList,
  updateUserData,
} from "../../services/service_api";
import { toast } from "react-toastify";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import moment from "moment";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  // borderRadius: 'rounded',
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function AddUserModal({ isOpen, onClose, edit, registration_id }) {
  const { columns, rows } = authorsTableData();
  //forms filed define
  const [registrationNo, setRegistrationNo] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [dob, setDob] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [gender, setGender] = useState("male");
  //   const [credentialsErros, setCredentialsError] = useState(null);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});
  const [isDisable, setIsDisable] = useState(false);

  React.useEffect(() => {
    setError(false);
    setRegistrationNo("");
    setEmail("");
    setMobile("");
    setDob("");
    setFirstName("");
    setLastName("");
  }, [onClose]);

  React.useEffect(() => {
    if (edit == "edit") {
      submitCreateUserForm();
    }
    async function submitCreateUserForm() {
      const params = {
        registration_no: registration_id,
      };
      const response = await getSingleUserData(params);
      const data = response.data;
      setIsDisable(true);
      setRegistrationNo(registration_id);
      setEmail(data?.email);
      setMobile(data?.mobile);
      setDob(data?.dob);
      setFirstName(data?.first_name);
      setLastName(data?.last_name);
      setGender(data?.gender);
    }
  }, [isOpen, registration_id]);

  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

  async function updateUserForm() {
    if (formValidation()) {
      const params = {
        email: email,
        first_name: firstName,
        last_name: lastName,
        registration_no: registrationNo,
        mobile: mobile,
        dob: dob,
        gender: gender,
      };
      const response = await updateUserData(params);
      if (response.status) {
        setRegistrationNo("");
        setEmail("");
        setMobile("");
        setDob("");
        setFirstName("");
        setLastName("");
        onClose();
        toast.success(response.message, toastConfig);
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  async function submitCreateUserForm() {
    if (formValidation()) {
      const params = {
        email: email,
        first_name: firstName,
        last_name: lastName,
        registration_no: registrationNo,
        mobile: mobile,
        dob: dob,
        gender: gender,
      };
      const response = await createUserData(params);
      if (response.status) {
        setRegistrationNo("");
        setEmail("");
        setMobile("");
        setDob("");
        setFirstName("");
        setLastName("");
        onClose();
        toast.success(response.message, toastConfig);
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  function formValidation() {
    let msgFirstName = "";
    let msgLastName = "";
    let msgMobileNo = "";
    let msgRegistrationNo = "";
    let msgDob = "";
    let msgEmail = "";
    let isValid = false;

    const emailRegex = /^[A-Za-z0-9._%+-]+@gmail\.com$/; // Regex for Gmail email addresses
    const mobileRegex = /^\d{10}$/; // Regex for 10-digit mobile numbers

    if (firstName === "") {
      msgFirstName = "Please enter first name";
    }

    if (email === "") {
      msgEmail = "Please enter Gmail";
    } else if (!emailRegex.test(email)) {
      msgEmail = "Please enter a valid Gmail address";
    }

    if (lastName === "") {
      msgLastName = "Please enter last name";
    }

    if (mobile === "") {
      msgMobileNo = "Please enter mobile no";
    } else if (!mobileRegex.test(mobile)) {
      msgMobileNo = "Please enter a valid 10-digit mobile number";
    }

    if (registrationNo === "") {
      msgRegistrationNo = "Please enter registration no";
    }

    if (dob === "") {
      msgDob = "Please enter dob";
    }

    if (
      !msgFirstName &&
      !msgLastName &&
      !msgMobileNo &&
      !msgRegistrationNo &&
      !msgDob &&
      !msgEmail
    ) {
      isValid = true;
    }

    if (isValid) {
      setError(true);
      setErrorMessage({
        email: "",
        firstName: "",
        lastName: "",
        registrationNo: "",
        mobile: "",
        dob: "",
      });
      return true;
    } else {
      setError(true);
      setErrorMessage({
        email: msgEmail,
        firstName: msgFirstName,
        lastName: msgLastName,
        registrationNo: msgRegistrationNo,
        mobile: msgMobileNo,
        dob: msgDob,
      });
      return false;
    }
  }

  const modalWidth = {
    width: "600px",
  };
  return (
    <>
      <Modal
        keepMounted
        open={isOpen}
        onClose={onClose}
        aria-labelledby="keep-mounted-modal-title"
        aria-describedby="keep-mounted-modal-description"
      >
        <MDBox sx={style} style={{ ...modalWidth }} borderRadius="lg">
          <MDBox component="form" role="form" method="POST">
            <MDBox
              mt={-7}
              py={2}
              px={2}
              variant="gradient"
              bgColor="info"
              borderRadius="lg"
              coloredShadow="info"
              flexDirection="row"
              display="flex"
              width="100%"
            >
              <MDTypography
                display="flex"
                width="100%"
                variant="body1"
                color="white"
                fontWeight="regular"
              >
                User Details
              </MDTypography>
              <MDBox display="flex" width="100%" justifyContent="flex-end">
                <IconButton
                  size="small"
                  width="100%"
                  aria-label="close"
                  color="inherit"
                  onClick={onClose}
                >
                  <Icon fontSize="small">close</Icon>
                </IconButton>
              </MDBox>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Registration No */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
                mr={2}
              >
                Registration No
                <MDBox mb={1} display="flex" width="100%">
                  <MDInput
                    required
                    type="text"
                    fullWidth
                    placeholder="Registration No"
                    name="registrationNo"
                    value={registrationNo}
                    id="registrationNo"
                    onChange={(e) => setRegistrationNo(e.target.value)}
                    disabled={(isDisable && true) || false}
                  />
                </MDBox>
                {error && errorMessage["registrationNo"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["registrationNo"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              {/* Email */}
              <MDTypography
                variant="body2"
                width="100%"
                color="text"
                fontWeight="regular"
              >
                Email
                <MDBox mb={2} display="flex" width="100%">
                  <MDInput
                    required
                    type="email"
                    fullWidth
                    placeholder="Email"
                    name="email"
                    value={email}
                    id="email"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["email"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["email"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* First Name */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
                mr={2}
              >
                First Name
                <MDBox mb={1} display="flex" width="100%">
                  <MDInput
                    required
                    type="text"
                    fullWidth
                    name="firstName"
                    value={firstName}
                    placeholder="First Name"
                    id="firstName"
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["firstName"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["firstName"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              {/* Last Name */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Last Name
                <MDBox mb={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    placeholder="Last Name"
                    name="lastName"
                    value={lastName}
                    id="lastName"
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["lastName"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["lastName"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Mobile */}
              <MDTypography
                variant="body2"
                width="100%"
                color="text"
                fontWeight="regular"
                mr={2}
              >
                Mobile
                <MDBox mb={2} display="flex" width="100%">
                  <MDInput
                    required
                    type="number"
                    fullWidth
                    placeholder="Mobile No."
                    name="mobile"
                    value={mobile}
                    id="mobile"
                    onChange={(e) => setMobile(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["mobile"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["mobile"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              {/* Dob */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Dob
                <MDBox mb={2} display="flex" width="100%">
                  <MDInput
                    type="date"
                    fullWidth
                    name="dob"
                    value={moment(dob).format("YYYY-MM-DD")}
                    id="dob"
                    onChange={(e) => setDob(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["dob"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["dob"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            {/* Gender */}
            <MDTypography
              width="100%"
              variant="body2"
              color="text"
              fontWeight="regular"
            >
              Gender
              <MDBox mb={2} display="flex" width="100%">
                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue="female"
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={gender == "male"}
                      onChange={() => setGender("male")}
                      control={<Radio />}
                      label="Male"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="Female"
                      checked={gender == "female"}
                      onChange={() => setGender("female")}
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="Other"
                      checked={gender == "other"}
                      onChange={() => setGender("other")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
              {/* {error && errorMessage["dob"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["dob"]}
                        </div>
                      ) : (
                        ""
                      )} */}
            </MDTypography>
            {/* {credentialsErros && (
              <MDTypography variant="caption" color="error" fontWeight="light">
                {credentialsErros}
              </MDTypography>
            )} */}
            <MDBox mt={3} display="flex" justifyContent="flex-end">
              {(isDisable && (
                <MDButton
                  variant="gradient"
                  color="info"
                  type="button"
                  onClick={updateUserForm}
                >
                  Update
                </MDButton>
              )) || (
                <MDButton
                  variant="gradient"
                  color="info"
                  type="button"
                  onClick={submitCreateUserForm}
                >
                  Submit
                </MDButton>
              )}
            </MDBox>
          </MDBox>
        </MDBox>
      </Modal>
    </>
  );
}

export default AddUserModal;
