// Material Dashboard 2 React components
import MDBox from "../../components/MDBox";
import MDTypography from "../../components/MDTypography";
import MDAvatar from "../../components/MDAvatar";
import { useLocation,Link, useNavigate } from "react-router-dom";

import AuthService from "../../services/auth-service";
import React, { useState, useEffect } from 'react';
import DeleteModal from "../modal/DeleteModal";
import { getUserList } from "../../services/service_api";
import AddUserModal from "./AddUserModal";


export default function Data(data) {
// console.log('data', data);
  const route = useLocation().pathname.split("/").slice(1);
  let navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [registrationNo, setRegistrationNo] = useState(null);
  const [userData, setUserData] = useState(data);
  
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setUserData(data);
  }, [data])
  

  function handleEditModal(id){
    setOpen(true)
    setRegistrationNo(id);
  }

  // console.log("call api",open);

  // useEffect(() => {
  //   getUserListData();
  // }, [open]);


 const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

 const handleDelete = () => {
   setIsDeleteModalOpen(false);
 };

 const openDeleteModal = (id) => {
   setIsDeleteModalOpen(true);
   setRegistrationNo(id);
 };

  useEffect(() => {
    getUserListData();
  }, [isDeleteModalOpen]);
  
  async function getUserListData() {
    const response = await getUserList();
    if (response.status==true) {
      const datas = response['data'];
      setUserData(datas);
    }
  }

  
  const handleNavigate = async (promies) => {
    // console.log(promies);
    navigate(`/all/forms/${promies}`);
  };

  const Author = ({ image, name, email }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} size="sm" />
      <MDBox ml={2} lineHeight={1}>
        <MDTypography display="block" variant="button" fontWeight="medium">
        {name}
        </MDTypography>
        <MDTypography variant="caption">{email}</MDTypography>
      </MDBox>
    </MDBox>
  );

  const Job = ({ title, description }) => (
    <MDBox lineHeight={1} textAlign="left">
      <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
        {title}
      </MDTypography>
      <MDTypography variant="caption">{description}</MDTypography>
    </MDBox>
  );


  return {
    columns: [
      // { Header: "user", accessor: "user", align: "left" },
      { Header: "registration_no", accessor: "registration_no", aling: "left"},
      { Header: "name", accessor: "name", align: "left" },
      { Header: "email", accessor: "email", align: "left" },
      { Header: "createdAt", accessor: "createdAt", align: "center" },
      { Header: "updatedAt", accessor: "updatedAt", align: "center" },
      { Header: "action", accessor: "action", align: "center" },
    ],

     rows : userData && (userData?.map((rowData, index) => ({
      registration_no: (
        <MDTypography
          key={`registration_no_${index}`}
          component="a"
          variant="caption"
          color="text"
          fontWeight="medium"
            style={{ cursor: 'pointer' }}
          onClick={() => handleNavigate(rowData.registration_no)}
        >
          {rowData.registration_no}
        </MDTypography>
      ),
      name: (
        <MDTypography
          key={`name_${index}`}
          component="a"
          variant="bold"
          color="text"
          fontWeight="medium"
          className="text-capitalize"
            style={{ cursor: 'pointer' }}
          onClick={() => handleNavigate(rowData.registration_no)}
        >
          {rowData.first_name +" "+ rowData.last_name}
        </MDTypography>
      ),
      email: (
        <MDTypography
          key={`email_${index}`}
          component="a"
          variant="caption"
          color="text"
          fontWeight="medium"
            style={{ cursor: 'pointer' }}
          onClick={() => handleNavigate(rowData.registration_no)}
        >
          {rowData.email}
        </MDTypography>
      ),
      createdAt: (
        <MDTypography
          key={`createdAt_${index}`}
          component="a"
          variant="caption"
          color="text"
          fontWeight="medium"
            style={{ cursor: 'pointer' }}
          onClick={() => handleNavigate(rowData.registration_no)}
        >
          {rowData.createdAt}
        </MDTypography>
      ),
      updatedAt: (
        <MDTypography
          key={`updatedAt_${index}`}
          component="a"
          variant="caption"
          color="text"
          fontWeight="medium"
            style={{ cursor: 'pointer' }}
          onClick={() => handleNavigate(rowData.registration_no)}
        >
          {rowData.updatedAt}
        </MDTypography>
      ),
      action: (
        <MDBox key={`action_${index}`}>
         <MDTypography
            component="a"
            variant="caption"
            color="text"
            fontWeight="medium"
            mr={2}
            style={{ cursor: 'pointer' }}
            onClick={(id)=>handleEditModal(rowData.registration_no)}
          >
            Edit
          </MDTypography>
          <MDTypography
            component="a"
            variant="caption"
            color="text"
            fontWeight="medium"
            style={{ cursor: 'pointer' }}
            onClick={(id)=>openDeleteModal(rowData.registration_no)}// Open the delete modal when Delete is clicked
            >
              Delete
            </MDTypography>
            <DeleteModal
              isOpen={isDeleteModalOpen}
              onClose={() => setIsDeleteModalOpen(false)} // Close the modal when Cancel is clicked
              onDelete={handleDelete} // Call the handleDelete function when Delete is confirmed
              registration_id={registrationNo}
            />
            <AddUserModal
               
                isOpen={open}
                onClose={() => setOpen(false)}
                edit={"edit"}
                registration_id={registrationNo}
              />
        </MDBox>
      ),
    })
    )) || []
  };
}
