import { useContext, useState } from "react";
import "../../style/loader.css";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../components/MDBox";
import MDTypography from "../../components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import DataTable from "../../examples/Tables/DataTable";
// Data
import authorsTableData from "./data";
import MDButton from "../../components/MDButton";
import AddUserModal from "./AddUserModal";
import { getUserList } from "../../services/service_api";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  // borderRadius: 'rounded',
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const AddUser = () => {
  const [data, setData] = useState([]);

  const [loading, setLoading] = useState("");
  //popup handler
  const [open, setOpen] = useState(false);

  async function getUserListData() {
    setLoading(true);
    // console.log("run api --=-===>");
    const response = await getUserList();
    if (response.status == true) {
      const data = response["data"];
      setData(data);
      setLoading(false);
    }
  }
  React.useEffect(() => {
    getUserListData();
  }, []);

  // console.log("open---->", open);

  React.useEffect(() => {
    getUserListData();
    // console.log("run api --=-===> in USe effect");
  }, [open]);

  const { columns, rows } = authorsTableData(data);

  // console.log("authorsTableData", authorsTableData());

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <DashboardLayout>
        <DashboardNavbar />
        <MDBox pt={2} pb={3}>
          <Grid container spacing={6}>
            <Grid item xs={12}>
              <MDBox display="flex" justifyContent="flex-end" mr={2} mb={5}>
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={() => setOpen(true)}
                >
                  {" "}
                  Add User
                </MDButton>
              </MDBox>
              <Card>
                <MDBox
                  mx={2}
                  mt={-3}
                  py={3}
                  px={2}
                  variant="gradient"
                  bgColor="info"
                  borderRadius="lg"
                  coloredShadow="info"
                  flexDirection="row"
                  display="flex"
                  width="98%"
                >
                  <MDTypography
                    flexDirection="column"
                    display="flex"
                    variant="h6"
                    color="white"
                    width="100%"
                  >
                    Users Table
                  </MDTypography>
                  {/* <MDBox borderRadius="lg"   mr={2} flexDirection="column" bgColor="white" >
                  <MDInput label="Search here"  color="white"  />
                </MDBox> */}
                  {/* <MDBox   borderRadius="lg" lexDirection="column" width="10%" justifyContent="flex-end"  >
                  <MDButton variant="gradient"   color="dark" onClick={handleOpen} > Add User</MDButton>
                </MDBox> */}
                </MDBox>
                <AddUserModal isOpen={open} onClose={() => setOpen(false)} />
                {/* renderUserData={getUserListData} */}
                <MDBox pt={3}>
                  <DataTable
                    table={{ columns, rows }}
                    isSorted={false}
                    entriesPerPage={false}
                    showTotalEntries={false}
                    noEndBorder
                  />
                </MDBox>
              </Card>
            </Grid>
          </Grid>
        </MDBox>
      </DashboardLayout>
    </>
  );
};

export default AddUser;

{
  /* <Modal keepMounted open={openTwo} aria-labelledby="keep-mounted-modal-title" aria-describedby="keep-mounted-modal-description">
                <MDBox sx={style} borderRadius="lg">
                  <MDBox  
                          mt={-7}
                          py={2}
                          px={2}
                          variant="gradient"
                          bgColor="info"
                          borderRadius="lg"
                          coloredShadow="info"
                          flexDirection="row"
                          display="flex"
                          width="100%" >
                            <MDTypography display="flex"   width="100%" variant="body1" color="white"  fontWeight="regular">
                              Forms</MDTypography>
                            <MDBox display="flex" width="100%" justifyContent="end">
                              <IconButton size="small"  width="100%"  aria-label="close" color="inherit" onClick={handleCloseTwo}>
                                <Icon fontSize="small">close</Icon>
                              </IconButton>
                            </MDBox>
                  </MDBox>
                  <MDBox mt={2} display="flex">   <Link to={"/forms/step1"}>
                    <MDButton
                        variant="gradient"
                        color="info">
                        PHYSICAL FITNESS ASSESSMENT
                    </MDButton>
                    </Link>
                  </MDBox>
                  <MDBox mt={2} display="flex">   <Link to={"/forms/AclRehabilitationOrotocol"}>
                    <MDButton
                        variant="gradient"
                        color="info">
                          ACL Rehabilitation Protocol
                    </MDButton>
                    </Link>
                  </MDBox>
                  <MDBox mt={2} display="flex">   <Link to={"/forms/FunctionalMovementScreeningForm"}>
                    <MDButton
                        variant="gradient"
                        color="info">
                        Functional Movement Screening
                    </MDButton>
                    </Link>
                  </MDBox>
                  <MDBox mt={2} display="flex">   <Link to={""}>
                    <MDButton
                        variant="gradient"
                        color="info">
                        Weekly Workload & Recovery Report
                    </MDButton>
                    </Link>
                  </MDBox>
                  {/* <MDBox mt={2} display="flex">   <Link to={"/forms/step1"}>
                    <MDButton
                        variant="gradient"
                        color="info">
                        OBSERVATIONS AND RECOMMENDATIONS
                    </MDButton>
                    </Link>
                  </MDBox> 
                </MDBox>
              </Modal> */
}
