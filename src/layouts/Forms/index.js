import { useState, useEffect } from "react";
// Material Dashboard 2 React components
import MDBox from "../../components/MDBox";
import MDTypography from "../../components/MDTypography";
import MDInput from "../../components/MDInput";
import MDButton from "../../components/MDButton";
import MDAlert from "../../components/MDAlert";

// Material Dashboard 2 React example components
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import Footer from "../../examples/Footer";

// Overview page components
import Header from "../../layouts/user-profile/Header";

// import HandGripTestFormStep2 from "../../components/Form";
import {HandGripTestFormStep1, HandGripTestFormStep3, HandGripTestFormStep2, HandGripTestFormStep4, FunctionalMovementScreeningForm, AclRehabilitationOrotocol} from "../../components/Form";

import AuthService from "../../services/auth-service";
import { Outlet } from 'react-router-dom';
import { Card, Link } from "@mui/material";

const Forms = () => {
    return (
        <DashboardLayout>
          <DashboardNavbar />
          <MDBox mb={2} />
          {/* <Card p={5}>
          <MDBox p={5}>
            <Link to={"/forms/step1"}>
                <MDButton variant="gradient" color="info">PHYSICAL FITNESS ASSESSMENT</MDButton> 
            </Link>
            <Link to={"#"}>
                <MDButton variant="gradient" color="info">OBSERVATIONS AND RECOMMENDATIONS</MDButton> 
            </Link>
            <Link to={"#"}>
                <MDButton variant="gradient" color="info">ACL Rehabilitation Protocol</MDButton> 
            </Link>
            <Link to={"#"}>
                <MDButton variant="gradient" color="info">Functional Movement Screening</MDButton> 
            </Link>
            <Link to={"#"}>
                <MDButton variant="gradient" color="info">Weekly Workload & Recovery Report</MDButton> 
            </Link>
        
          </MDBox>
          </Card> */}
          
          <Outlet/>
        </DashboardLayout>
      );
    };
    
export default Forms;