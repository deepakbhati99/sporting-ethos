
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import { AuthContextProvider } from "./context";

// Material Dashboard 2 React Context  Provider
import { MaterialUIControllerProvider } from "./context";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
  
    <BrowserRouter basename="sporting">
      <AuthContextProvider>
        <MaterialUIControllerProvider>
          <App />
          <ToastContainer />
        </MaterialUIControllerProvider>
      </AuthContextProvider>
    </BrowserRouter>,
  
  document.getElementById("root")
);
