<MDInput
  type="text"
  fullWidth
  name="LeftLegHeight1"
  name="Triple Hop"
  placeholder=""
  value={singleLegJumpRight}
  id="age"
  onChange={(e) => setSingleLegJumpRight(e.target.value)}
/>;
{
  error && errorMessage["singleLegJumpRight"] ? (
    <div style={{ color: "red", fontSize: "14px" }}>
      {errorMessage["singleLegJumpRight"]}
    </div>
  ) : (
    ""
  );
}
