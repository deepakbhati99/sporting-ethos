import * as React from "react";
import PfaStep1 from "./PhysicalFitnessAssessment/PfaStep1";
import PfaStep2 from "./PhysicalFitnessAssessment/PfaStep2";
import PfaStep3 from "./PhysicalFitnessAssessment/PfaStep3";
import FunctionalMovementForm from "./FunctionalMovementScreening/FunctionalMovementForm";
import AclProtocol from "./AclForm/AclProtocol";
import PfaStep4 from "./PhysicalFitnessAssessment/PfaStep4";
import PfaStep5 from "./PhysicalFitnessAssessment/PfaStep5";
import AClStep1 from "./AclForm/AClStep1";
import AclStep2 from "./AclForm/AclStep2";
import AclStep3 from "./AclForm/AclStep3";


export const AclRehabilitationOrotocol = () => {
  return (
    <AClStep1 />
  );
};

export const AclRehabilitationProtocolStep2 = () => {
  return (
    <AclStep2 />
  );
};

export const AclRehabilitationProtocolStep3 = () => {
  return (
    <AclStep3 />
  );
};

export const HandGripTestFormStep1 = () => {
  return (
    <PfaStep1 />
  );
};

export const HandGripTestFormStep2 = () => {
  return (
   <PfaStep2 />
  );
};

export const HandGripTestFormStep3 = () => {
  return (
    <PfaStep3 />
  );
};

export const HandGripTestFormStep4 = () => {
  return (
    <PfaStep4 />
  );
};

export const HandGripTestFormStep5 = () => {
  return (
    <PfaStep5/>
  );
};

export const FunctionalMovementScreeningForm = () => {
  return (
    <FunctionalMovementForm />
  );
};
