import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { TextareaAutosize } from "@mui/material";
import { toast } from "react-toastify";
import "../../../style/loader.css";
import {
  createPfaProtocol,
  createPfaStepFour,
  getPfaInstruction,
  getPfaStepDetail,
  getPfaStepFour,
  updatePfaStepFour,
} from "../../../services/service_api";

const style = {
  height: "100%",
};

function PfaStep4() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editPfa = route.pathname?.split("/")?.slice(3, 4) == "edit-pfa";
  const backPfa = route.pathname?.split("/")?.slice(3, 4) == "back-pfa";
  // console.log("editPfa===>", editPfa, userId, id);
  const [updateId, setUpdateId] = useState("");

  const flexibilityRulesData =
    "0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds. Sprint & Agility Tests 0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds.";

  // 15.Core Endurance
  const [coreEndurancePlanksTotalTime, setCoreEndurancePlanksTotalTime] =
    useState("");
  const [
    coreEnduranceSidePlanksTotalTimeRight,
    setCoreEnduranceSidePlanksTotalTimeRight,
  ] = useState("");
  const [
    coreEnduranceSidePlanksTotalTimeLeft,
    setCoreEnduranceSidePlanksTotalTimeLeft,
  ] = useState("");
  const [sprintAndAgilityTestsRules, setSprintAndAgilityTestsRules] =
    useState("");

  // 16.Local Muscular Endurance
  const [squatsExhaustionReps, setSquatsExhaustionReps] = useState("");
  const [pullUpsTRXPullups, setPullUpsTRXPullups] = useState("");
  const [pushUps, setPushUps] = useState("");
  const [sprintAgilityTestsRules, setSprintAgilityTestsRules] = useState("");

  // 17.Anaerobic Capacity (Shuttle Run)
  const [timeTaken1, setTimeTaken1] = useState("");
  const [timeTaken2, setTimeTaken2] = useState("");
  const [averageTime, setAverageTime] = useState("");
  const [textareaValueRules, setTextareaValueRules] = useState("");

  // 18.Aerobic Capacity - Beep Test
  const [beepTestLevel, setBeepTestLevel] = useState("");
  const [numberOfShuttles, setNumberOfShuttles] = useState("");
  const [vo2Max, setVO2Max] = useState("");
  const [sprintAgilityRules, setSprintAgilityRules] = useState("");

  // 19.Aerobic Capacity – Yo-Yo IR1 Test
  const [yoYoTestLevel, setYoYoTestLevel] = useState("");
  const [numberOfShuttlesYoYo, setNumberOfShuttlesYoYo] = useState("");
  const [totalDistance, setTotalDistance] = useState("");
  const [sprintAndAgilityTests, setSprintAndAgilityTests] = useState("");

  // 20.Aerobic Capacity – Bruce Protocol
  const [bruceProtocolMins, setBruceProtocolMins] = useState("");
  const [vO2MaxMin, setVO2MaxMin] = useState("");
  const [hrSecMinMin, setHrSecMinMin] = useState("");
  const [bruceProtocolRules, setBruceProtocolRules] = useState("");

  // 21.Aerobic Capacity – Cooper Test
  const [vO2MaxCooperTest, setVO2MaxCooperTest] = useState("");
  const [totalDistanceCooperTest, setTotalDistanceCooperTest] = useState("");
  const [cooperTestRules, setCooperTestRules] = useState("");

  // 22.Maximal Strength Test
  const [backSquat, setBackSquat] = useState("");
  const [benchPress, setBenchPress] = useState("");
  const [deadLift, setDeadLift] = useState("");
  const [aerobicCapacity, setAerobicCapacity] = useState("");

  const [loading, setLoading] = useState(false);
  const [stepDetail, setStepDetail] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    getPfaFormData();
    if (editPfa) {
      getPfaFormData();
    } else {
      getPfaInstructionData();
    }
  }, [editPfa]);

  React.useEffect(() => {
    handleUserSteps();
    if (stepDetail) {
      getPfaFormData();
    }
  }, [stepDetail]);

  async function handleUserSteps() {
    // console.log("getPfaStepDetail");
    const params = {
      registration_no: userId,
      id: id,
    };
    const response = await getPfaStepDetail(params);
    const data = response.data;
    // console.log("getPfaStepDetail response.data", data);
    if (response) {
      setStepDetail(data?.step_four_data);
    }
  }

  async function getPfaInstructionData() {
    setLoading(true);
    const response = await getPfaInstruction();
    const data = response.data[0];
    if (response) {
      // console.log("response", data);
      setSprintAndAgilityTestsRules(data?.core_endurance);
      setSprintAgilityTestsRules(data?.local_muscular_endurance);
      setTextareaValueRules(data?.anaerobic_capacity_shuttle_run);
      setSprintAgilityRules(data?.aerobic_capacity_beep_test);
      setSprintAndAgilityTests(data?.aerobic_capacity_yo_yo_ir1_test);
      setBruceProtocolRules(data?.aerobic_capacity_bruce_protocol);
      setCooperTestRules(data?.aerobic_capacity_cooper_test);
      setAerobicCapacity(data?.maximal_strength_test);
      setLoading(false);
    }
  }

  async function getPfaFormData() {
    setLoading(true);
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
    };
    // console.log("params=====>", params);
    const response = await getPfaStepFour(params);
    const data = response.data;
    // console.log("response-------->", response?.data);
    if (response) {
      setUpdateId(data?.id);
      setCoreEndurancePlanksTotalTime(
        data?.core_endurance_planks_total_time_sec
      );
      setCoreEnduranceSidePlanksTotalTimeRight(
        data?.core_endurance_side_planks_total_time_sec_right
      );
      setCoreEnduranceSidePlanksTotalTimeLeft(
        data?.core_endurance_side_planks_total_time_sec_left
      );
      setSprintAndAgilityTestsRules(data?.core_endurance);

      setSquatsExhaustionReps(
        data?.localmuscularendurance_squatstoexhaustionreps1min2mins
      );
      setPullUpsTRXPullups(data?.local_muscular_endurance_pull_ups_trx_pullups);
      setPushUps(data?.local_muscular_endurance_push_ups);
      setSprintAgilityTestsRules(data?.local_muscular_endurance);

      setTimeTaken1(data?.anaerobic_capacity_shuttle_run_time_taken_sec1);
      setTimeTaken2(data?.anaerobic_capacity_shuttle_run_time_taken_sec2);
      setAverageTime(data?.anaerobic_capacity_shuttle_run_average_time_sec);
      setTextareaValueRules(data?.anaerobic_capacity_shuttle_run);

      setBeepTestLevel(data?.aerobic_capacity_beep_test_beep_test_level);
      setNumberOfShuttles(data?.aerobic_capacity_beep_test_number_of_shuttles);
      setVO2Max(data?.aerobic_capacity_beep_test_vo2_max_ml_kg_min);
      setSprintAgilityRules(data?.aerobic_capacity_beep_test);

      setYoYoTestLevel(data?.aerobiccapacity_yoyoir1test_yoyo_test_level);
      setNumberOfShuttlesYoYo(
        data?.aerobiccapacity_yoyoir1test_number_of_shuttles
      );
      setTotalDistance(data?.aerobiccapacity_yoyoir1test_total_distance_m);
      setSprintAndAgilityTests(data?.aerobic_capacity_yo_yo_ir1_test);

      setBruceProtocolMins(data?.aerobic_bruceprotocol_bruceprotocol_min);
      setVO2MaxMin(data?.aerobic_bruceprotocol_vo2max_ml_kg_min);
      setHrSecMinMin(data?.aerobic_bruceprotocol_hr_20sec_1min_2_min);
      setBruceProtocolRules(data?.aerobic_capacity_bruce_protocol);

      setTotalDistanceCooperTest(
        data?.aerobic_capacity_cooper_test_total_distance_m
      );
      setVO2MaxCooperTest(data?.aerobic_capacity_cooper_test_vo2_max_ml_kg_min);
      setCooperTestRules(data?.aerobic_capacity_cooper_test);

      setBackSquat(data?.maximal_strength_test_1rm_kg_back_squat);
      setBenchPress(data?.maximal_strength_test_1rm_kg_bench_press);
      setDeadLift(data?.maximal_strength_test_1rm_kg_deadlift);
      setAerobicCapacity(data?.maximal_strength_test);
      setLoading(false);
    }
  }

  async function updatePfaStep() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,

      core_endurance_planks_total_time_sec: coreEndurancePlanksTotalTime,
      core_endurance_side_planks_total_time_sec_right:
        coreEnduranceSidePlanksTotalTimeRight,
      core_endurance_side_planks_total_time_sec_left:
        coreEnduranceSidePlanksTotalTimeLeft,
      core_endurance: sprintAndAgilityTestsRules,

      localmuscularendurance_squatstoexhaustionreps1min2mins:
        squatsExhaustionReps,
      local_muscular_endurance_pull_ups_trx_pullups: pullUpsTRXPullups,
      local_muscular_endurance_push_ups: pushUps,
      local_muscular_endurance: sprintAgilityTestsRules,

      anaerobic_capacity_shuttle_run_time_taken_sec1: timeTaken1,
      anaerobic_capacity_shuttle_run_time_taken_sec2: timeTaken2,
      anaerobic_capacity_shuttle_run_average_time_sec: averageTime,
      anaerobic_capacity_shuttle_run: textareaValueRules,

      aerobic_capacity_beep_test_beep_test_level: beepTestLevel,
      aerobic_capacity_beep_test_number_of_shuttles: numberOfShuttles,
      aerobic_capacity_beep_test_vo2_max_ml_kg_min: vo2Max,
      aerobic_capacity_beep_test: sprintAgilityRules,

      aerobiccapacity_yoyoir1test_yoyo_test_level: yoYoTestLevel,
      aerobiccapacity_yoyoir1test_number_of_shuttles: numberOfShuttlesYoYo,
      aerobiccapacity_yoyoir1test_total_distance_m: totalDistance,
      aerobic_capacity_yo_yo_ir1_test: sprintAndAgilityTests,

      aerobic_bruceprotocol_bruceprotocol_min: bruceProtocolMins,
      aerobic_bruceprotocol_vo2max_ml_kg_min: vO2MaxMin,
      aerobic_bruceprotocol_hr_20sec_1min_2_min: hrSecMinMin,
      aerobic_capacity_bruce_protocol: bruceProtocolRules,

      aerobic_capacity_cooper_test_total_distance_m: totalDistanceCooperTest,
      aerobic_capacity_cooper_test_vo2_max_ml_kg_min: vO2MaxCooperTest,
      aerobic_capacity_cooper_test: cooperTestRules,

      maximal_strength_test_1rm_kg_back_squat: backSquat,
      maximal_strength_test_1rm_kg_bench_press: benchPress,
      maximal_strength_test_1rm_kg_deadlift: deadLift,
      maximal_strength_test: aerobicCapacity,
    };
    // console.log("params===>", params);
    const response = await updatePfaStepFour(params);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(
        (backPfa && `/forms/step5/${userId}/${id}`) ||
          `/forms/step5/${"edit-pfa"}/${userId}/${id}`
      );
      setCoreEndurancePlanksTotalTime("");
      setCoreEnduranceSidePlanksTotalTimeRight("");
      setCoreEnduranceSidePlanksTotalTimeLeft("");
      setSprintAndAgilityTestsRules("");
      setSquatsExhaustionReps("");
      setPullUpsTRXPullups("");
      setPushUps("");
      setSprintAgilityTestsRules("");
      setTimeTaken1("");
      setTimeTaken2("");
      setAverageTime("");
      setTextareaValueRules("");
      setBeepTestLevel("");
      setNumberOfShuttles("");
      setVO2Max("");
      setSprintAgilityRules("");
      setYoYoTestLevel("");
      setNumberOfShuttlesYoYo("");
      setTotalDistance("");
      setSprintAndAgilityTests("");
      setBruceProtocolMins("");
      setVO2MaxMin("");
      setHrSecMinMin("");
      setBruceProtocolRules("");
      setVO2MaxCooperTest("");
      setTotalDistanceCooperTest("");
      setCooperTestRules("");
      setBackSquat("");
      setBenchPress("");
      setDeadLift("");
      setAerobicCapacity("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  async function handleUpdatePfaStep() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,

      core_endurance_planks_total_time_sec: coreEndurancePlanksTotalTime,
      core_endurance_side_planks_total_time_sec_right:
        coreEnduranceSidePlanksTotalTimeRight,
      core_endurance_side_planks_total_time_sec_left:
        coreEnduranceSidePlanksTotalTimeLeft,
      core_endurance: sprintAndAgilityTestsRules,

      localmuscularendurance_squatstoexhaustionreps1min2mins:
        squatsExhaustionReps,
      local_muscular_endurance_pull_ups_trx_pullups: pullUpsTRXPullups,
      local_muscular_endurance_push_ups: pushUps,
      local_muscular_endurance: sprintAgilityTestsRules,

      anaerobic_capacity_shuttle_run_time_taken_sec1: timeTaken1,
      anaerobic_capacity_shuttle_run_time_taken_sec2: timeTaken2,
      anaerobic_capacity_shuttle_run_average_time_sec: averageTime,
      anaerobic_capacity_shuttle_run: textareaValueRules,

      aerobic_capacity_beep_test_beep_test_level: beepTestLevel,
      aerobic_capacity_beep_test_number_of_shuttles: numberOfShuttles,
      aerobic_capacity_beep_test_vo2_max_ml_kg_min: vo2Max,
      aerobic_capacity_beep_test: sprintAgilityRules,

      aerobiccapacity_yoyoir1test_yoyo_test_level: yoYoTestLevel,
      aerobiccapacity_yoyoir1test_number_of_shuttles: numberOfShuttlesYoYo,
      aerobiccapacity_yoyoir1test_total_distance_m: totalDistance,
      aerobic_capacity_yo_yo_ir1_test: sprintAndAgilityTests,

      aerobic_bruceprotocol_bruceprotocol_min: bruceProtocolMins,
      aerobic_bruceprotocol_vo2max_ml_kg_min: vO2MaxMin,
      aerobic_bruceprotocol_hr_20sec_1min_2_min: hrSecMinMin,
      aerobic_capacity_bruce_protocol: bruceProtocolRules,

      aerobic_capacity_cooper_test_total_distance_m: totalDistanceCooperTest,
      aerobic_capacity_cooper_test_vo2_max_ml_kg_min: vO2MaxCooperTest,
      aerobic_capacity_cooper_test: cooperTestRules,

      maximal_strength_test_1rm_kg_back_squat: backSquat,
      maximal_strength_test_1rm_kg_bench_press: benchPress,
      maximal_strength_test_1rm_kg_deadlift: deadLift,
      maximal_strength_test: aerobicCapacity,
    };
    // console.log("params===>", params);
    const response = await updatePfaStepFour(params);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/forms/step5/${userId}/${id}`);
      setCoreEndurancePlanksTotalTime("");
      setCoreEnduranceSidePlanksTotalTimeRight("");
      setCoreEnduranceSidePlanksTotalTimeLeft("");
      setSprintAndAgilityTestsRules("");
      setSquatsExhaustionReps("");
      setPullUpsTRXPullups("");
      setPushUps("");
      setSprintAgilityTestsRules("");
      setTimeTaken1("");
      setTimeTaken2("");
      setAverageTime("");
      setTextareaValueRules("");
      setBeepTestLevel("");
      setNumberOfShuttles("");
      setVO2Max("");
      setSprintAgilityRules("");
      setYoYoTestLevel("");
      setNumberOfShuttlesYoYo("");
      setTotalDistance("");
      setSprintAndAgilityTests("");
      setBruceProtocolMins("");
      setVO2MaxMin("");
      setHrSecMinMin("");
      setBruceProtocolRules("");
      setVO2MaxCooperTest("");
      setTotalDistanceCooperTest("");
      setCooperTestRules("");
      setBackSquat("");
      setBenchPress("");
      setDeadLift("");
      setAerobicCapacity("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  async function createPfaUser() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,

      core_endurance_planks_total_time_sec: coreEndurancePlanksTotalTime,
      core_endurance_side_planks_total_time_sec_right:
        coreEnduranceSidePlanksTotalTimeRight,
      core_endurance_side_planks_total_time_sec_left:
        coreEnduranceSidePlanksTotalTimeLeft,
      core_endurance: sprintAndAgilityTestsRules,

      localmuscularendurance_squatstoexhaustionreps1min2mins:
        squatsExhaustionReps,
      local_muscular_endurance_pull_ups_trx_pullups: pullUpsTRXPullups,
      local_muscular_endurance_push_ups: pushUps,
      local_muscular_endurance: sprintAgilityTestsRules,

      anaerobic_capacity_shuttle_run_time_taken_sec1: timeTaken1,
      anaerobic_capacity_shuttle_run_time_taken_sec2: timeTaken2,
      anaerobic_capacity_shuttle_run_average_time_sec: averageTime,
      anaerobic_capacity_shuttle_run: textareaValueRules,

      aerobic_capacity_beep_test_beep_test_level: beepTestLevel,
      aerobic_capacity_beep_test_number_of_shuttles: numberOfShuttles,
      aerobic_capacity_beep_test_vo2_max_ml_kg_min: vo2Max,
      aerobic_capacity_beep_test: sprintAgilityRules,

      aerobiccapacity_yoyoir1test_yoyo_test_level: yoYoTestLevel,
      aerobiccapacity_yoyoir1test_number_of_shuttles: numberOfShuttlesYoYo,
      aerobiccapacity_yoyoir1test_total_distance_m: totalDistance,
      aerobic_capacity_yo_yo_ir1_test: sprintAndAgilityTests,

      aerobic_bruceprotocol_bruceprotocol_min: bruceProtocolMins,
      aerobic_bruceprotocol_vo2max_ml_kg_min: vO2MaxMin,
      aerobic_bruceprotocol_hr_20sec_1min_2_min: hrSecMinMin,
      aerobic_capacity_bruce_protocol: bruceProtocolRules,

      aerobic_capacity_cooper_test_total_distance_m: totalDistanceCooperTest,
      aerobic_capacity_cooper_test_vo2_max_ml_kg_min: vO2MaxCooperTest,
      aerobic_capacity_cooper_test: cooperTestRules,

      maximal_strength_test_1rm_kg_back_squat: backSquat,
      maximal_strength_test_1rm_kg_bench_press: benchPress,
      maximal_strength_test_1rm_kg_deadlift: deadLift,
      maximal_strength_test: aerobicCapacity,
    };
    // console.log("params===>", params);
    const response = await createPfaStepFour(params);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/forms/step5/${userId}/${id}`);
      setCoreEndurancePlanksTotalTime("");
      setCoreEnduranceSidePlanksTotalTimeRight("");
      setCoreEnduranceSidePlanksTotalTimeLeft("");
      setSprintAndAgilityTestsRules("");
      setSquatsExhaustionReps("");
      setPullUpsTRXPullups("");
      setPushUps("");
      setSprintAgilityTestsRules("");
      setTimeTaken1("");
      setTimeTaken2("");
      setAverageTime("");
      setTextareaValueRules("");
      setBeepTestLevel("");
      setNumberOfShuttles("");
      setVO2Max("");
      setSprintAgilityRules("");
      setYoYoTestLevel("");
      setNumberOfShuttlesYoYo("");
      setTotalDistance("");
      setSprintAndAgilityTests("");
      setBruceProtocolMins("");
      setVO2MaxMin("");
      setHrSecMinMin("");
      setBruceProtocolRules("");
      setVO2MaxCooperTest("");
      setTotalDistanceCooperTest("");
      setCooperTestRules("");
      setBackSquat("");
      setBenchPress("");
      setDeadLift("");
      setAerobicCapacity("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  // function formValidation() {
  //   let msgName = "";
  //   let msgAge = "";
  //   let msgWeight = "";
  //   let msgHeight = "";
  //   let msgRegistrationNo = "";
  //   let msgSport = "";
  //   let msgDate = "";
  //   let msgMfData = "";
  //   let isValid = false;

  //   if (name == "") {
  //     msgName = "Please enter name";
  //   }

  //   if (age == "") {
  //     msgAge = "Please enter age";
  //   }

  //   if (weight === "") {
  //     msgWeight = "Please enter weight";
  //   }

  //   if (height === "") {
  //     msgHeight = "Please enter height";
  //   }

  //   if (mfData === "") {
  //     msgMfData = "Please enter M/F";
  //   }

  //   if (registrationNo === "") {
  //     msgRegistrationNo = "Please enter registration number";
  //   }

  //   if (sport === "") {
  //     msgSport = "Please select a sport";
  //   }

  //   if (date === "") {
  //     msgDate = "Please select a date";
  //   }

  //   if (
  //     (!msgName,
  //     !msgAge,
  //     !msgWeight,
  //     !msgRegistrationNo,
  //     !msgSport,
  //     !msgDate,
  //     !msgHeight,
  //     !msgMfData)
  //   ) {
  //     isValid = true;
  //   }

  //   if (isValid) {
  //     setError(true);
  //     setErrorMessage({
  //       name: "",
  //       age: "",
  //       weight: "",
  //       registrationNo: "",
  //       sport: "",
  //       date: "",
  //       height: "",
  //       mfData: "",
  //     });
  //     return true;
  //   } else {
  //     setError(true);
  //     setErrorMessage({
  //       name: msgName,
  //       age: msgAge,
  //       weight: msgWeight,
  //       registrationNo: msgRegistrationNo,
  //       sport: msgSport,
  //       date: msgDate,
  //       height: msgHeight,
  //       mfData: msgMfData,
  //     });
  //     return false;
  //   }
  // }

  // console.log("stepDetail=====------------>", stepDetail);

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <Card pt={5} mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body2"
              color="text"
              ml={1}
              fontWeight="regular"
              fontSize={30}
            >
              PHYSICAL FITNESS ASSESSMENT
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            {/* 15.Core Endurance */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                15.Core Endurance
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    pt={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Planks
                  </MDTypography>
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Total time (sec)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="core_endurance_planks_total_time_sec"
                          placeholder="Total time (sec)"
                          value={coreEndurancePlanksTotalTime}
                          id="coreEndurancePlanksTotalTime"
                          onChange={(e) =>
                            setCoreEndurancePlanksTotalTime(e.target.value)
                          }
                        />
                      </MDBox>
                      {error && errorMessage["coreEndurancePlanksTotalTime"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["coreEndurancePlanksTotalTime"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    pt={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    {" "}
                    Side Planks
                  </MDTypography>
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="core_endurance_side_planks_total_time_sec_Right"
                          placeholder="Right"
                          value={coreEnduranceSidePlanksTotalTimeRight}
                          id="coreEnduranceSidePlanksTotalTimeRight"
                          onChange={(e) =>
                            setCoreEnduranceSidePlanksTotalTimeRight(
                              e.target.value
                            )
                          }
                        />
                      </MDBox>
                      {error &&
                      errorMessage["coreEnduranceSidePlanksTotalTimeRight"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {
                            errorMessage[
                              "coreEnduranceSidePlanksTotalTimeRight"
                            ]
                          }
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    pt={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    {" "}
                    Side Planks
                  </MDTypography>
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="core_endurance_side_planks_total_time_sec_Left"
                          placeholder="Left"
                          value={coreEnduranceSidePlanksTotalTimeLeft}
                          id="coreEnduranceSidePlanksTotalTimeLeft"
                          onChange={(e) =>
                            setCoreEnduranceSidePlanksTotalTimeLeft(
                              e.target.value
                            )
                          }
                        />
                      </MDBox>
                      {error &&
                      errorMessage["coreEnduranceSidePlanksTotalTimeLeft"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["coreEnduranceSidePlanksTotalTimeLeft"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Core Endurance"
                    value={sprintAndAgilityTestsRules}
                    id="sprintAndAgilityTestsRules"
                    onChange={(e) =>
                      setSprintAndAgilityTestsRules(e.target.value)
                    }
                  />
                </MDBox>
                {error && errorMessage["sprintAndAgilityTestsRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sprintAndAgilityTestsRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 16.Local Muscular Endurance */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                16.Local Muscular Endurance
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Squats to Exhaustion (reps) (1min/2mins)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="local_muscular_endurance_squatstoexhaustionreps_1min_2mins"
                          placeholder="Squats to Exhaustion (reps) (1min/2mins)"
                          value={squatsExhaustionReps}
                          id="squatsExhaustionReps"
                          onChange={(e) =>
                            setSquatsExhaustionReps(e.target.value)
                          }
                        />
                      </MDBox>
                      {error && errorMessage["squatsExhaustionReps"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["squatsExhaustionReps"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Pull ups/TRX Pullups
                  </MDTypography>

                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="local_muscular_endurance_pull_ups_trx_pullups"
                          placeholder="Pull ups/TRX Pullups"
                          value={pullUpsTRXPullups}
                          id="pullUpsTRXPullups"
                          onChange={(e) => setPullUpsTRXPullups(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["pullUpsTRXPullups"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["pullUpsTRXPullups"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Push Ups
                  </MDTypography>
                  <MDBox display="flex" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox mb={1} mr={2} display="flex" width="100%">
                        <MDInput
                          type="text"
                          fullWidth
                          name="local_muscular_endurance_push_ups"
                          placeholder="Push Ups"
                          value={pushUps}
                          id="pushUps"
                          onChange={(e) => setPushUps(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["pushUps"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["pushUps"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Sprint & Agility Tests"
                    value={sprintAgilityTestsRules}
                    id="sprintAgilityTestsRules"
                    onChange={(e) => setSprintAgilityTestsRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sprintAgilityTestsRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sprintAgilityTestsRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 17.Anaerobic Capacity (Shuttle Run) */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                17.Anaerobic Capacity (Shuttle Run)
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Time Taken (sec)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="anaerobic_capacity_shuttle_run_time_taken_sec"
                          placeholder="Time Taken (sec) 1"
                          value={timeTaken1}
                          id="timeTaken1"
                          onChange={(e) => setTimeTaken1(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["timeTaken1"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["timeTaken1"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="anaerobic_capacity_shuttle_run_time_taken_sec2"
                          placeholder="Time Taken (sec) 2"
                          value={timeTaken2}
                          id="timeTaken2"
                          onChange={(e) => setTimeTaken2(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["timeTaken2"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["timeTaken2"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Average Time (sec)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="anaerobic_capacity_shuttle_run_average_time_sec"
                          placeholder="Average Time (sec)"
                          value={averageTime}
                          id="averageTime"
                          onChange={(e) => setAverageTime(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["averageTime"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["averageTime"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Sprint & Agility Tests"
                    value={textareaValueRules}
                    id="textareaValueRules"
                    onChange={(e) => setTextareaValueRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["textareaValueRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["textareaValueRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 18.Aerobic Capacity - Beep Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                18.Aerobic Capacity - Beep Test
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Beep Test Leve
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_beep_test_level"
                          placeholder="Beep Test Level"
                          value={beepTestLevel}
                          id="beepTestLevel"
                          onChange={(e) => setBeepTestLevel(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["beepTestLevel"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["beepTestLevel"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Number of Shuttles
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_number_of_shuttles"
                          placeholder="Number of Shuttles"
                          value={numberOfShuttles}
                          id="numberOfShuttles"
                          onChange={(e) => setNumberOfShuttles(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["numberOfShuttles"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["numberOfShuttles"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    VO2 Max (ml/kg/min)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_vo2_max_ml_kg_min"
                          placeholder="VO2 Max (ml/kg/min)"
                          value={vo2Max}
                          id="vo2Max"
                          onChange={(e) => setVO2Max(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["vo2Max"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["vo2Max"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Aerobic Capacity - Beep Test"
                    value={sprintAgilityRules}
                    id="sprintAgilityRules"
                    onChange={(e) => setSprintAgilityRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sprintAgilityRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sprintAgilityRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 19.Aerobic Capacity – Yo-Yo IR1 Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                19.Aerobic Capacity – Yo-Yo IR1 Test
              </MDTypography>

              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Yo-Yo Test Level
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_beep_test_level"
                          placeholder="Yo-Yo Test Level"
                          value={yoYoTestLevel}
                          id="yoYoTestLevel"
                          onChange={(e) => setYoYoTestLevel(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["yoYoTestLevel"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["yoYoTestLevel"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Number of Shuttles
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_number_of_shuttles"
                          placeholder="Number of Shuttles"
                          value={numberOfShuttlesYoYo}
                          id="numberOfShuttlesYoYo"
                          onChange={(e) =>
                            setNumberOfShuttlesYoYo(e.target.value)
                          }
                        />
                      </MDBox>
                      {error && errorMessage["numberOfShuttlesYoYo"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["numberOfShuttlesYoYo"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Total distance (m)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_vo2_max_ml_kg_min"
                          placeholder="Total distance (m)"
                          value={totalDistance}
                          id="totalDistance"
                          onChange={(e) => setTotalDistance(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["totalDistance"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["totalDistance"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Aerobic Capacity – Yo-Yo IR1 Test"
                    value={sprintAndAgilityTests}
                    id="sprintAndAgilityTests"
                    onChange={(e) => setSprintAndAgilityTests(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sprintAndAgilityTests"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sprintAndAgilityTests"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 20.Aerobic Capacity – Bruce Protocol */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                20.Aerobic Capacity – Bruce Protocol
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Bruce Protocol (mins)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_beep_test_level"
                          placeholder="Bruce Protocol (mins)"
                          value={bruceProtocolMins}
                          id="bruceProtocolMins"
                          onChange={(e) => setBruceProtocolMins(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["bruceProtocolMins"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["bruceProtocolMins"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    VO2 Max (ml/kg/min)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_number_of_shuttles"
                          placeholder="VO2 Max (ml/kg/min)"
                          value={vO2MaxMin}
                          id="vO2MaxMin"
                          onChange={(e) => setVO2MaxMin(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["vO2MaxMin"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["vO2MaxMin"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    HR (20sec/1min/2mins)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_vo2_max_ml_kg_min"
                          placeholder="HR (20sec/1min/2mins)"
                          value={hrSecMinMin}
                          id="hrSecMinMin"
                          onChange={(e) => setHrSecMinMin(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["hrSecMinMin"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["hrSecMinMin"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Aerobic Capacity – Bruce Protocol"
                    value={bruceProtocolRules}
                    id="bruceProtocolRules"
                    onChange={(e) => setBruceProtocolRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["bruceProtocolRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["bruceProtocolRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 21.Aerobic Capacity – Cooper Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                21.Aerobic Capacity – Cooper Test
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Total distance (m)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_beep_test_level"
                          placeholder="Total distance (m)"
                          value={totalDistanceCooperTest}
                          id="totalDistanceCooperTest"
                          onChange={(e) =>
                            setTotalDistanceCooperTest(e.target.value)
                          }
                        />
                      </MDBox>
                      {error && errorMessage["totalDistanceCooperTest"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["totalDistanceCooperTest"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    VO2 Max (ml/kg/min)
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_number_of_shuttles"
                          placeholder="VO2 Max (ml/kg/min)"
                          value={vO2MaxCooperTest}
                          id="vO2MaxCooperTest"
                          onChange={(e) => setVO2MaxCooperTest(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["vO2MaxCooperTest"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["vO2MaxCooperTest"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Aerobic Capacity – Cooper Test"
                    value={cooperTestRules}
                    id="cooperTestRules"
                    onChange={(e) => setCooperTestRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["cooperTestRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["cooperTestRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 22.Maximal Strength Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                22.Maximal Strength Tes
              </MDTypography>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Back Squat (1-RM (kg))
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_beep_test_level"
                          placeholder="Back Squat"
                          value={backSquat}
                          id="backSquat"
                          onChange={(e) => setBackSquat(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["backSquat"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["backSquat"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Bench Press (1-RM (kg))
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_number_of_shuttles"
                          placeholder="Bench Press"
                          value={benchPress}
                          id="benchPress"
                          onChange={(e) => setBenchPress(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["benchPress"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["benchPress"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Deadlift (1-RM (kg))
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDTypography
                      flexDirection="column"
                      mr={1}
                      variant="body2"
                      width="100%"
                      color="text"
                      ml={0}
                      fontWeight="regular"
                    >
                      <MDBox
                        mb={1}
                        mr={2}
                        flexDirection="column"
                        display="flex"
                        width="100%"
                      >
                        <MDInput
                          type="text"
                          fullWidth
                          name="aerobic_capacity_beep_test_vo2_max_ml_kg_min"
                          placeholder="Deadlift"
                          value={deadLift}
                          id="deadLift"
                          onChange={(e) => setDeadLift(e.target.value)}
                        />
                      </MDBox>
                      {error && errorMessage["deadLift"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["deadLift"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDTypography>
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Maximal Strength Tes"
                    value={aerobicCapacity}
                    id="aerobicCapacity"
                    onChange={(e) => setAerobicCapacity(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["aerobicCapacity"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["aerobicCapacity"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            <MDBox display="flex" flexDirection="row" justifyContent="flex-end">
              <MDBox mt={2} mr={3} flexDirection="column" display="flex">
                {" "}
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step3/${"edit-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step3/${"back-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )}
              </MDBox>
              <MDBox mt={2} flexDirection="column" display="flex">
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updatePfaStep}
                  >
                    Update / Next
                  </MDButton>
                )) ||
                  (backPfa && (
                    <MDButton
                      variant="gradient"
                      color="info"
                      onClick={updatePfaStep}
                    >
                      Save / Next
                    </MDButton>
                  )) || (
                    <MDButton
                      variant="gradient"
                      color="info"
                      onClick={
                        (stepDetail && handleUpdatePfaStep) || createPfaUser
                      }
                    >
                      Save / Next
                    </MDButton>
                  )}
              </MDBox>
            </MDBox>

            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step4/Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default PfaStep4;
