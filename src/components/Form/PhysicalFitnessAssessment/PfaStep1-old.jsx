import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { TextareaAutosize } from "@mui/material";
import { toast } from "react-toastify";
import {
  createPfaProtocol,
  createPfaStepOne,
  getPfaInstruction,
  getPfaStepDetail,
  getPfaStepOneData,
  getSingleUserData,
  updatePfaStepOne,
} from "../../../services/service_api";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faFaceFrown as LightFaceFrown } from "@fortawesome/free-regular-svg-icons";
import { faFaceFrown as DarkFaceFrown } from "@fortawesome/free-solid-svg-icons";

import { faFaceMeh as LightFaceMeh } from "@fortawesome/free-regular-svg-icons";
import { faFaceMeh as DarkFaceMeh } from "@fortawesome/free-solid-svg-icons";

import { faFaceSmile as LightFaceSmile } from "@fortawesome/free-regular-svg-icons";
import { faFaceSmile as DarkFaceSmile } from "@fortawesome/free-solid-svg-icons";
import "../../../style/loader.css";

const style = {
  height: "100%",
};

function PfaStep1() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

  const flexibilityRulesData =
    "0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds. Sprint & Agility Tests 0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds.";

  const navigate = useNavigate();
  const { id } = useParams();
  const { userId } = useParams();
  const route = useLocation();
  const editPfa = route.pathname?.split("/")?.slice(2, 3) == "edit-pfa";
  const backPfa = route.pathname?.split("/")?.slice(3, 4) == "back-pfa";
  // console.log("editPfa===>", editPfa, userId, id);
  // console.log(
  //   "route.pathname?.split)?.slice(2, 3)",
  //   route.pathname?.split("/")?.slice(2, 3)
  // );

  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [weight, setWeight] = useState("");
  const [height, setHeight] = useState("");
  const [mfData, setMfData] = useState("");
  const [registrationNo, setRegistrationNo] = useState(userId);
  const [sport, setSport] = useState("");
  const [date, setDate] = useState("");
  //  Room Temp/Humidity
  React.useEffect(() => {
    if (!userId) {
      setRegistrationNo(id);
    }
  }, [id, userId]);

  console.log("userId,id---====>", userId, id);

  const [roomTempHumidityDay1, setRoomTempHumidityDay1] = useState("");
  const [roomTempHumidityDay2, setRoomTempHumidityDay2] = useState("");
  // Timing of last meal
  const [timingLastMealDay1, setTimingLastMealDay1] = useState("");
  const [timingLastMealDay2, setTimingLastMealDay2] = useState("");
  // What did you eat
  const [whatDidYouEatDay1, setWhatDidYouEatDay1] = useState("");
  const [whatDidYouEatDay2, setWhatDidYouEatDay2] = useState("");
  // Feeling
  const [feelingDay1, setFeelingDay1] = useState("0");
  const [feelingDay2, setFeelingDay2] = useState("0");
  // Note
  const [note, setNote] = useState("");
  // Medical and Injury History
  const [medicalAndInjuryHistory, setMedicalAndInjuryHistory] = useState("");
  // Body Composition
  const [bodyComposition, setBodyComposition] = useState("");
  // Flexibility, Joint Stability and Posture Screening
  const [flexibilityJoint, setFlexibilityJoint] = useState("");
  // 3. Hand Grip Test
  const [handGripStrengthKgR1, setHandGripStrengthKgR1] = useState("");
  const [handGripStrengthKgR2, setHandGripStrengthKgR2] = useState("");
  const [handGripStrengthKgL1, setHandGripStrengthKgL1] = useState("");
  const [handGripStrengthKgL2, setHandGripStrengthKgL2] = useState("");
  const [handGripTest, setHandGripTest] = useState("");
  // 4.Flexibility, Joint Stability and Posture Screening
  const [deepSquatScore, setDeepSquatScore] = useState("");
  const [deepSquatComment, setDeepSquatComment] = useState("");
  const [ankleClearanceScore, setAnkleClearanceScore] = useState("");
  const [ankleClearanceComment, setAnkleClearanceComment] = useState("");
  const [legRaiseScore, setLegRaiseScore] = useState("");
  const [legRaiseComment, setLegRaiseComment] = useState("");
  const [flexibilityRules, setFlexibilityRules] = useState("");
  //5.Sprint & Agility Tests
  const [sprint5mTime1, setSprint5mTime1] = useState("");
  const [sprint5mTime2, setSprint5mTime2] = useState("");
  const [sprint5mTime3, setSprint5mTime3] = useState("");
  const [sprint10mTime1, setSprint10mTime1] = useState("");
  const [sprint10mTime2, setSprint10mTime2] = useState("");
  const [sprint10mTime3, setSprint10mTime3] = useState("");
  const [proAgilityTime1, setProAgilityTime1] = useState("");
  const [proAgilityTime2, setProAgilityTime2] = useState("");
  const [proAgilityTime3, setProAgilityTime3] = useState("");
  const [sprintRules, setSprintRules] = useState("");
  // 6.Sprint & Agility Tests
  const [sprint10m1, setSprint10m1] = useState("");
  const [sprint10m2, setSprint10m2] = useState("");
  const [sprint20m1, setSprint20m1] = useState("");
  const [sprint20m2, setSprint20m2] = useState("");
  const [proAgilityRight1, setProAgilityRight1] = useState("");
  const [proAgilityRight2, setProAgilityRight2] = useState("");
  const [proAgilityLeft1, setProAgilityLeft1] = useState("");
  const [proAgilityLeft2, setProAgilityLeft2] = useState("");
  const [agilityRules, setAgilityRules] = useState("");
  const [loading, setLoading] = useState(false);

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});
  const [stepDetail, setStepDetail] = useState("");

  const [adminUserData, setAdminUserData] = useState("");

  React.useEffect(() => {
    if (editPfa) {
      getPfaFormData();
    } else {
      getMainUserData();
      getPfaInstructionData();
    }
  }, [editPfa]);

  React.useEffect(() => {
    if (backPfa) {
      getPfaFormData();
      handleUserSteps();
    } else {
      getMainUserData();
      getPfaInstructionData();
    }
  }, [backPfa]);

  async function handleUserSteps() {
    // console.log("getPfaStepDetail");
    setLoading(true);
    const params = {
      registration_no: userId,
      id: id,
    };
    const response = await getPfaStepDetail(params);
    const data = response.data;
    // console.log("getPfaStepDetail response.data", data);
    if (response) {
      setStepDetail(data?.step_two_data);
    }
  }

  async function getMainUserData() {
    // console.log(userId);
    setLoading(true);
    const params = {
      registration_no: userId || id,
    };
    const response = await getSingleUserData(params);
    const data = response.data;
    // console.log("response.data", data);
    if (response) {
      setName(`${data?.first_name}${" "}${data?.last_name}`);
      setAdminUserData(data?.dob);
      setLoading(false);
    }
  }

  React.useEffect(() => {
    const birthDate = new Date(adminUserData);
    const currentDate = new Date();
    const ageInMilliseconds = currentDate - birthDate;

    // Calculate age in years
    const ageInYears = Math.floor(ageInMilliseconds / 31536000000); // 1 year = 31536000000 milliseconds
    setAge(ageInYears ? ageInYears : "");
    // console.log(`Years Difference: ${ageInYears}`);
  }, [adminUserData]);

  async function getPfaInstructionData() {
    setLoading(true);
    const response = await getPfaInstruction();
    const data = response.data[0];

    if (response) {
      setMedicalAndInjuryHistory(data.medical_and_injury_history);
      setBodyComposition(data.body_composition);
      setFlexibilityJoint(
        data.flexibility_joint_stability_and_posture_screening
      );
      setHandGripTest(data.hand_grip_test);
      setFlexibilityRules(data.flexibilityjointscreeningtwo);
      setSprintRules(data.sprint_agility_tests);
      setAgilityRules(data.sprintagilitytests_20m);
      // console.log("response", data);
      setLoading(false);
    }
  }

  async function getPfaFormData() {
    setLoading(true);
    const params = {
      registration_no: userId,
      id: id,
    };
    const response = await getPfaStepOneData(params);
    const data = response.data;
    // console.log("response-------->", response?.data);
    if (response) {
      setName(data?.name);
      setAge(data?.age);
      setWeight(data?.weight);
      setHeight(data?.height);
      setSport(data?.sport);
      setDate(data?.date);
      setMfData(data?.mf);
      setRoomTempHumidityDay1(data?.room_temp_humidity_day1);
      setRoomTempHumidityDay2(data?.room_temp_humidity_day2);
      setTimingLastMealDay1(data?.timing_of_last_meal_day1);
      setTimingLastMealDay2(data?.timing_of_last_meal_day2);
      setWhatDidYouEatDay1(data?.what_did_you_eat_day1);
      setWhatDidYouEatDay2(data?.what_did_you_eat_day2);
      setFeelingDay1(data?.feeling_day1);
      setFeelingDay2(data?.feeling_day2);
      setNote(data?.notes);
      setMedicalAndInjuryHistory(data?.medical_and_injury_history);
      setBodyComposition(data?.body_composition);
      setFlexibilityJoint(
        data?.flexibility_joint_stability_and_posture_screening
      );
      setHandGripStrengthKgR1(data?.hand_grip_test_grip_strength_kg_r1);
      setHandGripStrengthKgR2(data?.hand_grip_test_grip_strength_kg_r2);
      setHandGripStrengthKgL1(data?.hand_grip_test_grip_strength_kg_l1);
      setHandGripStrengthKgL2(data?.hand_grip_test_grip_strength_kg_l2);
      setHandGripTest(data?.hand_grip_test);
      setDeepSquatScore(data?.flexibilityjointscreeningtestdeepsquartscore);
      setAnkleClearanceScore(
        data?.flexibilityjointscreeningtestankleclearancescore
      );
      setLegRaiseScore(
        data?.flexibilityjointscreeningtestactivestraightlegraisescore
      );
      setDeepSquatComment(
        data?.flexibilityjointscreeningtestdeepsquartcomments
      );
      setAnkleClearanceComment(
        data?.flexibilityjointscreeningtestankleclearancecomments
      );
      setLegRaiseComment(
        data?.flexibilityjointscreeningtestactivestraightlegraisecomments
      );
      setFlexibilityRules(data?.flexibilityjointscreeningtwo);
      setSprint5mTime1(data?.sprint_agility_tests_10msprint_5msec1);
      setSprint5mTime2(data?.sprint_agility_tests_10msprint_5msec2);
      setSprint5mTime3(data?.sprint_agility_tests_10msprint_5msec3);
      setSprint10mTime1(data?.rint_agility_tests_10msprint10msec1);
      setSprint10mTime2(data?.sprint_agility_tests_10msprint_10msec2);
      setSprint10mTime3(data?.sprint_agility_tests_10msprint_10msec3);
      setProAgilityTime1(
        data?.sprint_agility_tests_pro_agility_time_taken_sec_1
      );
      setProAgilityTime2(
        data?.sprint_agility_tests_pro_agility_time_taken_sec_2
      );
      setProAgilityTime3(
        data?.sprint_agility_tests_pro_agility_time_taken_sec_3
      );
      setSprintRules(data?.sprint_agility_tests);
      setSprint10m1(data?.sprintagilitytests_20msprint10msec1);
      setSprint10m2(data?.sprintagilitytests_20msprint10msec2);
      setSprint20m1(data?.sprintagilitytests_20msprint20msec1);
      setSprint20m2(data?.sprintagilitytests_20msprint20msec2);
      setProAgilityRight1(
        data?.sprintagilitytests_arrowheadagility_timetakensecright1
      );
      setProAgilityRight2(
        data?.sprintagilitytests_arrowheadagility_timetakensecright2
      );
      setProAgilityLeft1(
        data?.sprintagilitytests_arrowheadagility_timetakensecleft1
      );
      setProAgilityLeft2(
        data?.sprintagilitytests_arrowheadagility_timetakensecleft2
      );
      setAgilityRules(data?.sprintagilitytests_20m);
      setLoading(false);
    }
  }

  async function updatePfaStep() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        id: id,
        name: name,
        age: age,
        weight: weight,
        height: height,
        sport: sport,
        date: date,
        mf: mfData,
        room_temp_humidity_day1: roomTempHumidityDay1,
        room_temp_humidity_day2: roomTempHumidityDay2,
        timing_of_last_meal_day1: timingLastMealDay1,
        timing_of_last_meal_day2: timingLastMealDay2,
        what_did_you_eat_day1: whatDidYouEatDay1,
        what_did_you_eat_day2: whatDidYouEatDay2,
        feeling_day1: feelingDay1,
        feeling_day2: feelingDay2,
        notes: note,
        medical_and_injury_history: medicalAndInjuryHistory,
        body_composition: bodyComposition,
        flexibility_joint_stability_and_posture_screening: flexibilityJoint,
        // 3. Hand Grip Test
        hand_grip_test_grip_strength_kg_r1: handGripStrengthKgR1,
        hand_grip_test_grip_strength_kg_r2: handGripStrengthKgR2,
        hand_grip_test_grip_strength_kg_l1: handGripStrengthKgL1,
        hand_grip_test_grip_strength_kg_l2: handGripStrengthKgL2,
        hand_grip_test: handGripTest,
        // 4.Flexibility, Joint Stability and Posture Screening
        flexibilityjointscreeningtwo: flexibilityRules,
        flexibilityjointscreeningtestdeepsquartscore: deepSquatScore,
        flexibilityjointscreeningtestdeepsquartcomments: deepSquatComment,
        flexibilityjointscreeningtestankleclearancescore: ankleClearanceScore,
        flexibilityjointscreeningtestankleclearancecomments:
          ankleClearanceComment,
        flexibilityjointscreeningtestactivestraightlegraisescore: legRaiseScore,
        flexibilityjointscreeningtestactivestraightlegraisecomments:
          legRaiseComment,
        //5.Sprint & Agility Tests
        sprint_agility_tests_10msprint_5msec1: sprint5mTime1,
        sprint_agility_tests_10msprint_5msec2: sprint5mTime2,
        sprint_agility_tests_10msprint_5msec3: sprint5mTime3,
        rint_agility_tests_10msprint10msec1: sprint10mTime1,
        sprint_agility_tests_10msprint_10msec2: sprint10mTime2,
        sprint_agility_tests_10msprint_10msec3: sprint10mTime3,
        sprint_agility_tests_pro_agility_time_taken_sec_1: proAgilityTime1,
        sprint_agility_tests_pro_agility_time_taken_sec_2: proAgilityTime2,
        sprint_agility_tests_pro_agility_time_taken_sec_3: proAgilityTime3,
        sprint_agility_tests: sprintRules,
        // 6.Sprint & Agility Tests
        sprintagilitytests_20msprint10msec1: sprint10m1,
        sprintagilitytests_20msprint10msec2: sprint10m2,
        sprintagilitytests_20msprint20msec1: sprint20m1,
        sprintagilitytests_20msprint20msec2: sprint20m2,
        sprintagilitytests_arrowheadagility_timetakensecright1:
          proAgilityRight1,
        sprintagilitytests_arrowheadagility_timetakensecright2:
          proAgilityRight2,
        sprintagilitytests_arrowheadagility_timetakensecleft1: proAgilityLeft1,
        sprintagilitytests_arrowheadagility_timetakensecleft2: proAgilityLeft2,
        sprintagilitytests_20m: agilityRules,
      };
      // console.log("params===>", params);
      const response = await updatePfaStepOne(params);
      const stepId = response?.data?.id;
      // console.log("response", response.status, id, stepId);

      if (response.status == true) {
        navigate(
          (backPfa && `/forms/step2/${userId}/${id}`) ||
            `/forms/step2/${"edit-pfa"}/${userId}/${id}`
        );
        toast.success(response.message, toastConfig);
        setName("");
        setAge("");
        setWeight("");
        setHeight("");
        setSport("");
        setDate("");
        setMfData("");
        setRoomTempHumidityDay1("");
        setRoomTempHumidityDay2("");
        setTimingLastMealDay1("");
        setTimingLastMealDay2("");
        setWhatDidYouEatDay1("");
        setWhatDidYouEatDay2("");
        setFeelingDay1("");
        setFeelingDay2("");
        setNote("");
        setMedicalAndInjuryHistory("");
        setBodyComposition("");
        setFlexibilityJoint("");
        setHandGripStrengthKgR1("");
        setHandGripStrengthKgR2("");
        setHandGripStrengthKgL1("");
        setHandGripStrengthKgL2("");
        setHandGripTest("");
        setDeepSquatScore("");
        setAnkleClearanceScore("");
        setLegRaiseScore("");
        setDeepSquatComment("");
        setAnkleClearanceComment("");
        setLegRaiseComment("");
        setFlexibilityRules("");
        setSprint5mTime1("");
        setSprint5mTime2("");
        setSprint5mTime3("");
        setSprint10mTime1("");
        setSprint10mTime2("");
        setSprint10mTime3("");
        setProAgilityTime1("");
        setProAgilityTime2("");
        setProAgilityTime3("");
        setSprintRules("");
        setSprint10m1("");
        setSprint10m2("");
        setSprint20m1("");
        setSprint20m2("");
        setProAgilityRight1("");
        setProAgilityRight2("");
        setProAgilityLeft1("");
        setProAgilityLeft2("");
        setAgilityRules("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  async function createPfaUser() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        name: name,
        age: age,
        weight: weight,
        height: height,
        sport: sport,
        date: date,
        mf: mfData,
        room_temp_humidity_day1: roomTempHumidityDay1,
        room_temp_humidity_day2: roomTempHumidityDay2,
        timing_of_last_meal_day1: timingLastMealDay1,
        timing_of_last_meal_day2: timingLastMealDay2,
        what_did_you_eat_day1: whatDidYouEatDay1,
        what_did_you_eat_day2: whatDidYouEatDay2,
        feeling_day1: feelingDay1,
        feeling_day2: feelingDay2,
        notes: note,
        medical_and_injury_history: medicalAndInjuryHistory,
        body_composition: bodyComposition,
        flexibility_joint_stability_and_posture_screening: flexibilityJoint,
        // 3. Hand Grip Test
        hand_grip_test_grip_strength_kg_r1: handGripStrengthKgR1,
        hand_grip_test_grip_strength_kg_r2: handGripStrengthKgR2,
        hand_grip_test_grip_strength_kg_l1: handGripStrengthKgL1,
        hand_grip_test_grip_strength_kg_l2: handGripStrengthKgL2,
        hand_grip_test: handGripTest,
        // 4.Flexibility, Joint Stability and Posture Screening
        flexibilityjointscreeningtwo: flexibilityRules,
        flexibilityjointscreeningtestdeepsquartscore: deepSquatScore,
        flexibilityjointscreeningtestdeepsquartcomments: deepSquatComment,
        flexibilityjointscreeningtestankleclearancescore: ankleClearanceScore,
        flexibilityjointscreeningtestankleclearancecomments:
          ankleClearanceComment,
        flexibilityjointscreeningtestactivestraightlegraisescore: legRaiseScore,
        flexibilityjointscreeningtestactivestraightlegraisecomments:
          legRaiseComment,
        //5.Sprint & Agility Tests
        sprint_agility_tests_10msprint_5msec1: sprint5mTime1,
        sprint_agility_tests_10msprint_5msec2: sprint5mTime2,
        sprint_agility_tests_10msprint_5msec3: sprint5mTime3,
        rint_agility_tests_10msprint10msec1: sprint10mTime1,
        sprint_agility_tests_10msprint_10msec2: sprint10mTime2,
        sprint_agility_tests_10msprint_10msec3: sprint10mTime3,
        sprint_agility_tests_pro_agility_time_taken_sec_1: proAgilityTime1,
        sprint_agility_tests_pro_agility_time_taken_sec_2: proAgilityTime2,
        sprint_agility_tests_pro_agility_time_taken_sec_3: proAgilityTime3,
        sprint_agility_tests: sprintRules,
        // 6.Sprint & Agility Tests
        sprintagilitytests_20msprint10msec1: sprint10m1,
        sprintagilitytests_20msprint10msec2: sprint10m2,
        sprintagilitytests_20msprint20msec1: sprint20m1,
        sprintagilitytests_20msprint20msec2: sprint20m2,
        sprintagilitytests_arrowheadagility_timetakensecright1:
          proAgilityRight1,
        sprintagilitytests_arrowheadagility_timetakensecright2:
          proAgilityRight2,
        sprintagilitytests_arrowheadagility_timetakensecleft1: proAgilityLeft1,
        sprintagilitytests_arrowheadagility_timetakensecleft2: proAgilityLeft2,
        sprintagilitytests_20m: agilityRules,
      };
      // console.log("params===>", params);
      const response = await createPfaStepOne(params);
      const stepId = response?.data?.id;
      // console.log("response", response.status, id, stepId);

      if (response.status == true) {
        navigate(`/forms/step2/${id}/${stepId}`);
        toast.success(response.message, toastConfig);
        setName("");
        setAge("");
        setWeight("");
        setHeight("");
        setSport("");
        setDate("");
        setMfData("");
        setRoomTempHumidityDay1("");
        setRoomTempHumidityDay2("");
        setTimingLastMealDay1("");
        setTimingLastMealDay2("");
        setWhatDidYouEatDay1("");
        setWhatDidYouEatDay2("");
        setFeelingDay1("");
        setFeelingDay2("");
        setNote("");
        setMedicalAndInjuryHistory("");
        setBodyComposition("");
        setFlexibilityJoint("");
        setHandGripStrengthKgR1("");
        setHandGripStrengthKgR2("");
        setHandGripStrengthKgL1("");
        setHandGripStrengthKgL2("");
        setHandGripTest("");
        setDeepSquatScore("");
        setAnkleClearanceScore("");
        setLegRaiseScore("");
        setDeepSquatComment("");
        setAnkleClearanceComment("");
        setLegRaiseComment("");
        setFlexibilityRules("");
        setSprint5mTime1("");
        setSprint5mTime2("");
        setSprint5mTime3("");
        setSprint10mTime1("");
        setSprint10mTime2("");
        setSprint10mTime3("");
        setProAgilityTime1("");
        setProAgilityTime2("");
        setProAgilityTime3("");
        setSprintRules("");
        setSprint10m1("");
        setSprint10m2("");
        setSprint20m1("");
        setSprint20m2("");
        setProAgilityRight1("");
        setProAgilityRight2("");
        setProAgilityLeft1("");
        setProAgilityLeft2("");
        setAgilityRules("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  function formValidation() {
    let msgName = "";
    let msgAge = "";
    let msgWeight = "";
    let msgHeight = "";
    let msgRegistrationNo = "";
    let msgSport = "";
    let msgDate = "";
    let msgMfData = "";
    let isValid = false;

    if (name == "") {
      msgName = "Please enter name";
    }

    if (age == "") {
      msgAge = "Please enter age";
    }

    if (weight === "") {
      msgWeight = "Please enter weight";
    }

    if (height === "") {
      msgHeight = "Please enter height";
    }

    if (mfData === "") {
      msgMfData = "Please enter M/F";
    }

    if (registrationNo === "") {
      msgRegistrationNo = "Please enter registration number";
    }

    if (sport === "") {
      msgSport = "Please select a sport";
    }

    if (date === "") {
      msgDate = "Please select a date";
    }

    if (
      (!msgName,
      !msgAge,
      !msgWeight,
      !msgRegistrationNo,
      !msgSport,
      !msgDate,
      !msgHeight,
      !msgMfData)
    ) {
      isValid = true;
    }

    if (isValid) {
      setError(true);
      setErrorMessage({
        name: "",
        age: "",
        weight: "",
        registrationNo: "",
        sport: "",
        date: "",
        height: "",
        mfData: "",
      });
      return true;
    } else {
      setError(true);
      setErrorMessage({
        name: msgName,
        age: msgAge,
        weight: msgWeight,
        registrationNo: msgRegistrationNo,
        sport: msgSport,
        date: msgDate,
        height: msgHeight,
        mfData: msgMfData,
      });
      toast.error("Valid fields are required", toastConfig);
      window.scrollTo(0, 0);
      return false;
    }
  }

  const cursorPointer = {
    cursor: "pointer",
  };

  const fs23 = {
    fontSize: "23px",
  };

  // console.log("feelingDay1", feelingDay1, feelingDay2);

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body2"
              color="text"
              ml={1}
              fontWeight="regular"
              fontSize={30}
            >
              PHYSICAL FITNESS ASSESSMENT
            </MDTypography>
          </MDBox>

          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Name
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="name"
                    placeholder="Name"
                    value={name}
                    id="name"
                    onChange={(e) => setName(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["name"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["name"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Age(Years)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="age"
                    placeholder="age"
                    value={age}
                    id="age"
                    onChange={(e) => setAge(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["age"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["age"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                M/F
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="mf"
                    placeholder="M/F"
                    value={mfData}
                    id="mfData"
                    onChange={(e) => setMfData(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["mfData"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["mfData"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Sport
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="sport"
                    placeholder="Sport"
                    value={sport}
                    id="sport"
                    onChange={(e) => setSport(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sport"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sport"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Registration No
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="registration_no"
                    placeholder="Registration No"
                    id="registrationNo"
                    value={registrationNo}
                    onChange={(e) => setRegistrationNo(e.target.value)}
                    disabled={true}
                  />
                </MDBox>
                {error && errorMessage["registrationNo"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["registrationNo"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Height(cm)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="height"
                    placeholder="Height"
                    value={height}
                    onChange={(e) => setHeight(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["height"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["height"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Weight(Kg)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="weight"
                    placeholder="Weight"
                    value={weight}
                    id="age"
                    onChange={(e) => setWeight(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["weight"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["weight"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Date
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="date"
                    fullWidth
                    name="date"
                    placeholder="Date"
                    value={date}
                    id="date"
                    onChange={(e) => setDate(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["date"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["date"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* Room Temp/Humidity */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Room Temp/Humidity (Day 1):
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="room_temp_humidity_day1"
                    placeholder="Room Temp/Humidity (Day 1)"
                    value={roomTempHumidityDay1}
                    id="roomTempHumidityDay1"
                    onChange={(e) => setRoomTempHumidityDay1(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["roomTempHumidityDay1"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["roomTempHumidityDay1"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Room Temp/Humidity (Day 2):
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="room_temp_humidity_day2"
                    placeholder=" Room Temp/Humidity (Day 2)"
                    value={roomTempHumidityDay2}
                    id="roomTempHumidityDay2"
                    onChange={(e) => setRoomTempHumidityDay2(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["roomTempHumidityDay2"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["roomTempHumidityDay2"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* Timing of last meal  */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Timing of last meal (Day 1):
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="timing_of_last_meal_day1"
                    placeholder="Timing of last meal (Day 1)"
                    value={timingLastMealDay1}
                    id="timingLastMealDay1"
                    onChange={(e) => setTimingLastMealDay1(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["timingLastMealDay1"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["timingLastMealDay1"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Timing of last meal (Day 2)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="timing_of_last_meal_day2"
                    placeholder="Timing of last meal (Day 2)"
                    value={timingLastMealDay2}
                    id="timingLastMealDay2"
                    onChange={(e) => setTimingLastMealDay2(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["timingLastMealDay2"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["timingLastMealDay2"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* What did you eat */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                What did you eat (Day 1):
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="what_did_you_eat_day1"
                    placeholder=" What did you eat (Day 1)"
                    value={whatDidYouEatDay1}
                    id="whatDidYouEatDay1"
                    onChange={(e) => setWhatDidYouEatDay1(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["whatDidYouEatDay1"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["whatDidYouEatDay1"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                What did you eat (Day 2):
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="what_did_you_eat_day2"
                    placeholder=" What did you eat (Day 2)"
                    value={whatDidYouEatDay2}
                    id="whatDidYouEatDay2"
                    onChange={(e) => setWhatDidYouEatDay2(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["whatDidYouEatDay2"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["whatDidYouEatDay2"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* Feeling */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Feeling (Day 1):
                <MDBox mb={1} mr={2} pt={1} display="flex" width="100%">
                  <FontAwesomeIcon
                    icon={(feelingDay1 == 1 && DarkFaceSmile) || LightFaceSmile}
                    onClick={(e) => setFeelingDay1("1")}
                    style={{ ...cursorPointer, ...fs23 }}
                  />
                  &nbsp;&nbsp;
                  <FontAwesomeIcon
                    icon={(feelingDay1 == 2 && DarkFaceMeh) || LightFaceMeh}
                    onClick={(e) => setFeelingDay1("2")}
                    style={{ ...cursorPointer, ...fs23 }}
                  />
                  &nbsp;&nbsp;
                  <FontAwesomeIcon
                    icon={(feelingDay1 == 3 && DarkFaceFrown) || LightFaceFrown}
                    onClick={(e) => setFeelingDay1("3")}
                    style={{ ...cursorPointer, ...fs23 }}
                  />
                </MDBox>
                {error && errorMessage["feelingDay1"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["feelingDay1"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Feeling (Day 2):
                <MDBox mb={1} mr={2} pt={1} display="flex" width="100%">
                  <FontAwesomeIcon
                    icon={(feelingDay2 == 1 && DarkFaceSmile) || LightFaceSmile}
                    onClick={(e) => setFeelingDay2("1")}
                    style={{ ...cursorPointer, ...fs23 }}
                  />
                  &nbsp;&nbsp;
                  <FontAwesomeIcon
                    icon={(feelingDay2 == 2 && DarkFaceMeh) || LightFaceMeh}
                    onClick={(e) => setFeelingDay2("2")}
                    style={{ ...cursorPointer, ...fs23 }}
                  />
                  &nbsp;&nbsp;
                  <FontAwesomeIcon
                    icon={(feelingDay2 == 3 && DarkFaceFrown) || LightFaceFrown}
                    onClick={(e) => setFeelingDay2("3")}
                    style={{ ...cursorPointer, ...fs23 }}
                  />
                </MDBox>
                {error && errorMessage["feelingDay2"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["feelingDay2"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* Note */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={1}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Notes
                <MDBox mb={1} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Notes"
                    value={note}
                    id="note"
                    onChange={(e) => setNote(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["note"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["note"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* Medical and Injury History */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Medical and Injury History
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Medical and Injury History"
                    value={medicalAndInjuryHistory}
                    id="medicalAndInjuryHistory"
                    onChange={(e) => setMedicalAndInjuryHistory(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["medicalAndInjuryHistory"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["medicalAndInjuryHistory"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* 1.Body Composition */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={1}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Body Composition
                <MDBox mb={1} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    name="body_composition"
                    placeholder="Body Composition"
                    value={bodyComposition}
                    id="bodyComposition"
                    onChange={(e) => setBodyComposition(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["bodyComposition"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["bodyComposition"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* 2.Flexibility, Joint Stability and Posture Screening */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Flexibility, Joint Stability and Posture Screening
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    name="flexibility_joint_stability_and_posture_screening"
                    placeholder="Flexibility, Joint Stability and Posture Screening "
                    value={flexibilityJoint}
                    id="flexibilityJoint"
                    onChange={(e) => setFlexibilityJoint(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["flexibilityJoint"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["flexibilityJoint"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* 3.Hand Grip Test */}
            <MDBox mt={1}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Hand Grip Test
              </MDTypography>
              <MDBox display="flex" flexDirection="row" fontWeight="regular">
                <MDBox
                  flexDirection="column"
                  fontWeight="regular"
                  alignItems="flex-start"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Grip Strength (kg) R
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="50%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="hand_grip_test_grip_strength_kg_r1"
                        placeholder="R1"
                        value={handGripStrengthKgR1}
                        id="handGripStrengthKgR1"
                        onChange={(e) =>
                          setHandGripStrengthKgR1(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["handGripStrengthKgR1"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["handGripStrengthKgR1"]}
                      </div>
                    ) : (
                      ""
                    )}
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="50%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="hand_grip_test_grip_strength_kg_r2"
                        placeholder="R2"
                        value={handGripStrengthKgR2}
                        id="handGripStrengthKgR2"
                        onChange={(e) =>
                          setHandGripStrengthKgR2(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["handGripStrengthKgR2"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["handGripStrengthKgR2"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox
                  flexDirection="column"
                  fontWeight="regular"
                  alignItems="flex-start"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Grip Strength (kg) L
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="50%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="hand_grip_test_grip_strength_kg_l1"
                        placeholder="L1"
                        value={handGripStrengthKgL1}
                        id="handGripStrengthKgL1"
                        onChange={(e) =>
                          setHandGripStrengthKgL1(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["handGripStrengthKgL1"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["handGripStrengthKgL1"]}
                      </div>
                    ) : (
                      ""
                    )}
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="50%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="hand_grip_test_grip_strength_kg_l2"
                        placeholder="L2"
                        value={handGripStrengthKgL2}
                        id="handGripStrengthKgL2"
                        onChange={(e) =>
                          setHandGripStrengthKgL2(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["handGripStrengthKgL2"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["handGripStrengthKgL2"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
            </MDBox>
            {/* Hand Grip Test Comment */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Hand Grip Test
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Hand Grip Test"
                    value={handGripTest}
                    id="handGripTest"
                    onChange={(e) => setHandGripTest(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["handGripTest"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["handGripTest"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            {/* 4.Flexibility, Joint Stability and Posture Screening */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Flexibility, Joint Stability and Posture Screening
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    pt={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Test
                  </MDTypography>
                </MDBox>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    pt={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Score
                  </MDTypography>
                </MDBox>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    pt={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Comment
                  </MDTypography>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="bold"
                      fontSize={16}
                    >
                      Deep Squat
                    </MDTypography>
                  </MDBox>
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={2}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec2"
                      placeholder="score"
                      value={deepSquatScore}
                      id="deepSquatScore"
                      onChange={(e) => setDeepSquatScore(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["deepSquatScore"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["deepSquatScore"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={2}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="comment"
                      value={deepSquatComment}
                      id="deepSquatComment"
                      onChange={(e) => setDeepSquatComment(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["deepSquatComment"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["deepSquatComment"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="50%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="bold"
                      fontSize={16}
                    >
                      Ankle Clearance
                    </MDTypography>
                  </MDBox>
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={2}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec2"
                      placeholder="score"
                      value={ankleClearanceScore}
                      id="ankleClearanceScore"
                      onChange={(e) => setAnkleClearanceScore(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["ankleClearanceScore"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["ankleClearanceScore"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={2}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="comment"
                      value={ankleClearanceComment}
                      id="ankleClearanceComment"
                      onChange={(e) => setAnkleClearanceComment(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["ankleClearanceComment"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["ankleClearanceComment"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="50%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="bold"
                      fontSize={16}
                    >
                      Active Straight Leg Raise
                    </MDTypography>
                  </MDBox>
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={2}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec2"
                      placeholder="score"
                      value={legRaiseScore}
                      id="legRaiseScore"
                      onChange={(e) => setLegRaiseScore(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["legRaiseScore"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["legRaiseScore"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={2}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="comment"
                      value={legRaiseComment}
                      id="legRaiseComment"
                      onChange={(e) => setLegRaiseComment(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["legRaiseComment"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["legRaiseComment"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>
              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Flexibility, Joint Stability and Posture Screening"
                    value={flexibilityRules}
                    id="flexibilityRules"
                    onChange={(e) => setFlexibilityRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["flexibilityRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["flexibilityRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>
            {/* 5.Sprint & Agility Tests */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Sprint & Agility Tests
              </MDTypography>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                10m Sprint
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                5m(sec)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec1"
                      placeholder="1"
                      value={sprint5mTime1}
                      id="sprint5mTime1"
                      onChange={(e) => setSprint5mTime1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint5mTime1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint5mTime1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec2"
                      placeholder="2"
                      value={sprint5mTime2}
                      id="sprint5mTime2"
                      onChange={(e) => setSprint5mTime2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint5mTime2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint5mTime2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec3"
                      placeholder="3"
                      value={sprint5mTime3}
                      id="sprint5mTime3"
                      onChange={(e) => setSprint5mTime3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint5mTime3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint5mTime3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                10m(sec)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint10msec1"
                      placeholder="1"
                      value={sprint10mTime1}
                      id="sprint10mTime1"
                      onChange={(e) => setSprint10mTime1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint10mTime1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint10mTime1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_10msec2"
                      placeholder="2"
                      value={sprint10mTime2}
                      id="sprint10mTime2"
                      onChange={(e) => setSprint10mTime2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint10mTime2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint10mTime2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_10msec3"
                      placeholder="3"
                      value={sprint10mTime3}
                      id="sprint10mTime3"
                      onChange={(e) => setSprint10mTime3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint10mTime3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint10mTime3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Pro Agility
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                pt={0}
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Time Taken (sec)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_1"
                      placeholder="1"
                      value={proAgilityTime1}
                      id="proAgilityTime1"
                      onChange={(e) => setProAgilityTime1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityTime1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityTime1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_2"
                      placeholder="2"
                      value={proAgilityTime2}
                      id="proAgilityTime2"
                      onChange={(e) => setProAgilityTime2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityTime2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityTime2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_3"
                      placeholder="3"
                      value={proAgilityTime3}
                      id="proAgilityTime3"
                      onChange={(e) => setProAgilityTime3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityTime3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityTime3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Sprint & Agility Tests"
                    value={sprintRules}
                    id="sprintRules"
                    onChange={(e) => setSprintRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sprintRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sprintRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>
            {/* 6.Sprint & Agility Tests */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Sprint & Agility Tests
              </MDTypography>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                20m Sprint
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      10m(sec)
                    </MDTypography>
                  </MDBox>
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec2"
                      placeholder="1"
                      value={sprint10m1}
                      id="sprint10m1"
                      onChange={(e) => setSprint10m1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint10m1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint10m1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_5msec3"
                      placeholder="2"
                      value={sprint10m2}
                      id="sprint10m2"
                      onChange={(e) => setSprint10m2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint10m2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint10m2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      20m(sec)
                    </MDTypography>
                  </MDBox>
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_10msec2"
                      placeholder="1"
                      value={sprint20m1}
                      id="sprint20m1"
                      onChange={(e) => setSprint20m1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint20m1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint20m1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_10msprint_10msec3"
                      placeholder="2"
                      value={sprint20m2}
                      id="sprint20m2"
                      onChange={(e) => setSprint20m2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sprint20m2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sprint20m2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Arrowhead Agility
              </MDTypography>

              {/* <MDTypography
                variant="body2"
                color="text"
                pt={0}
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Time Taken (sec)
              </MDTypography> */}

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      Time Taken (sec)Right
                    </MDTypography>
                  </MDBox>
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_2"
                      placeholder="1"
                      value={proAgilityRight1}
                      id="proAgilityRight1"
                      onChange={(e) => setProAgilityRight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityRight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityRight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_3"
                      placeholder="2"
                      value={proAgilityRight2}
                      id="proAgilityRight2"
                      onChange={(e) => setProAgilityRight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityRight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityRight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body1"
                      color="text"
                      ml={1}
                      pt={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      Time Taken (sec)Left
                    </MDTypography>
                  </MDBox>
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_2"
                      placeholder="1"
                      value={proAgilityLeft1}
                      id="proAgilityLeft1"
                      onChange={(e) => setProAgilityLeft1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityLeft1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityLeft1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="sprint_agility_tests_pro_agility_time_taken_sec_3"
                      placeholder="2"
                      value={proAgilityLeft2}
                      id="proAgilityLeft2"
                      onChange={(e) => setProAgilityLeft2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["proAgilityLeft2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["proAgilityLeft2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Sprint & Agility Tests"
                    value={agilityRules}
                    id="agilityRules"
                    onChange={(e) => setAgilityRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["agilityRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["agilityRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>
            {/*  */}
            <MDBox mt={2} display="flex" justifyContent="flex-end">
              {" "}
              {(editPfa && (
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={updatePfaStep}
                >
                  Update / Next
                </MDButton>
              )) ||
                (backPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updatePfaStep}
                  >
                    Save / Next
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={createPfaUser}
                  >
                    Save / Next
                  </MDButton>
                )}
            </MDBox>
            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step1 / Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default PfaStep1;
