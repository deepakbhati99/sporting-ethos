import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { TextareaAutosize } from "@mui/material";
import { toast } from "react-toastify";
import "../../../style/loader.css";
import {
  createPfaProtocol,
  createPfaStepTwo,
  getPfaInstruction,
  getPfaStepDetail,
  getPfaStepTwoData,
  updatePfaStepTwo,
} from "../../../services/service_api";

const style = {
  height: "100%",
};

function PfaStep2() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editPfa = route.pathname?.split("/")?.slice(3, 4) == "edit-pfa";
  const backPfa = route.pathname?.split("/")?.slice(3, 4) == "back-pfa";
  // console.log("editPfa===>", editPfa, userId, id);
  // console.log(
  //   "object route.pathname?.split()?.slice(3, 4)",
  //   route.pathname?.split("/")?.slice(3, 4)
  // );

  const flexibilityRulesData =
    "0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds. Sprint & Agility Tests 0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds.";

  const [updateId, setUpdateId] = useState("");
  // 1.Lower Body Explosive Power
  const [squatJumpHeight1, setSquatJumpHeight1] = useState("");
  const [squatJumpHeight2, setSquatJumpHeight2] = useState("");
  const [squatJumpHeight3, setSquatJumpHeight3] = useState("");
  const [cmjHeight1, setCmjHeight1] = useState("");
  const [cmjHeight2, setCmjHeight2] = useState("");
  const [cmjHeight3, setCmjHeight3] = useState("");
  const [armSwingHeight1, setArmSwingHeight1] = useState("");
  const [armSwingHeight2, setArmSwingHeight2] = useState("");
  const [armSwingHeight3, setArmSwingHeight3] = useState("");
  const [lowerBodyRules, setLowerBodyRules] = useState("");

  // 2.Counter Movement Jump (Single leg)
  const [counterJumpRightLegHeight1, setCounterJumpRightLegHeight1] =
    useState("");
  const [counterJumpRightLegHeight2, setCounterJumpRightLegHeight2] =
    useState("");
  const [counterJumpRightLegHeight3, setCounterJumpRightLegHeight3] =
    useState("");
  const [counterJumpLeftLegHeight1, setCounterJumpLeftLegHeight1] =
    useState("");
  const [counterJumpLeftLegHeight2, setCounterJumpLeftLegHeight2] =
    useState("");
  const [counterJumpLeftLegHeight3, setCounterJumpLeftLegHeight3] =
    useState("");
  const [counterMovementRules, setCounterMovementRules] = useState("");

  // 3.Lower Body Explosive Power (Horizontal)
  const [rightLegHeight1, setRightLegHeight1] = useState("");
  const [rightLegHeight2, setRightLegHeight2] = useState("");
  const [rightLegHeight3, setRightLegHeight3] = useState("");
  const [leftLegHeight1, setLeftLegHeight1] = useState("");
  const [leftLegHeight2, setLeftLegHeight2] = useState("");
  const [leftLegHeight3, setLeftLegHeight3] = useState("");
  const [lowerBodyExplosiveRules, setLowerBodyExplosiveRules] = useState("");

  // 4.
  const [flamingoRight, setFlamingoRight] = useState("");
  const [flamingoLeft, setFlamingoLeft] = useState("");
  const [flamingoBalanceRules, setFlamingoBalanceRules] = useState("");

  const [loading, setLoading] = useState(false);
  const [stepDetail, setStepDetail] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    if (editPfa) {
      getPfaFormData();
    } else {
      getPfaInstructionData();
    }
  }, [editPfa]);

  React.useEffect(() => {
    getPfaFormData();
    handleUserSteps();
    if (backPfa) {
      getPfaFormData();
    } else {
      getPfaInstructionData();
    }
  }, [backPfa]);

  async function handleUserSteps() {
    // console.log("getPfaStepDetail");
    setLoading(true);
    const params = {
      registration_no: userId,
      id: id,
    };
    const response = await getPfaStepDetail(params);
    const data = response.data;
    // step_three_data
    // step_four_data
    // step_five_data
    // console.log("getPfaStepDetail response.data", data);
    if (response) {
      setStepDetail(data?.step_two_data);
    }
  }

  async function getPfaInstructionData() {
    setLoading(true);
    const response = await getPfaInstruction();
    const data = response.data[0];
    if (response) {
      setLowerBodyRules(data.lower_body_explosive_power);
      setCounterMovementRules(data.counter_movement_jump_single_leg);
      setLowerBodyExplosiveRules(data.lover_body_explosive_power_horizontal);
      setFlamingoBalanceRules(data.flamingoblancetext);
      // console.log("response", data);
      setLoading(false);
    }
  }

  async function getPfaFormData() {
    setLoading(true);
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
    };
    // console.log("params=====>", params);
    const response = await getPfaStepTwoData(params);
    const data = response.data;
    // console.log("response-------->", response?.data);
    if (response) {
      setUpdateId(data?.id);
      setSquatJumpHeight1(
        data?.lower_body_explosive_power_squat_jump_heightm_1
      );
      setSquatJumpHeight2(
        data?.lower_body_explosive_power_squat_jump_heightm_2
      );
      setSquatJumpHeight3(
        data?.lower_body_explosive_power_squat_jump_heightm_3
      );
      setCmjHeight1(
        data?.lower_body_counter_movement_jump_handsonhip_heightm_1
      );
      setCmjHeight2(
        data?.lower_body_counter_movement_jump_handsonhip_heightm_2
      );
      setCmjHeight3(
        data?.lower_body_counter_movement_jump_handsonhip_heightm_3
      );
      setArmSwingHeight1(
        data?.lower_body_counter_movement_jump_heightm_armswing_heightm_1
      );
      setArmSwingHeight2(
        data?.lower_body_countermovement_jump_heightm_armswing_heightm_2
      );
      setArmSwingHeight3(
        data?.lower_body_countermovement_jump_heightm_armswing_heightm_3
      );
      setLowerBodyRules(data?.lower_body_explosive_power);
      setCounterJumpRightLegHeight1(
        data?.counter_movement_jump_singleleg_rightleg_heightm_1
      );
      setCounterJumpRightLegHeight2(
        data?.counter_movement_jump_singleleg_rightleg_heightm_2
      );
      setCounterJumpRightLegHeight3(
        data?.counter_movement_jump_singleleg_rightleg_heightm_3
      );
      setCounterJumpLeftLegHeight1(
        data?.counter_movement_jump_singleleg_leftleg_heightm_1
      );
      setCounterJumpLeftLegHeight2(
        data?.counter_movement_jump_singleleg_leftleg_heightm_2
      );
      setCounterJumpLeftLegHeight3(
        data?.counter_movement_jump_singleleg_leftleg_heightm_3
      );
      setCounterMovementRules(data?.counter_movement_jump_singleleg);
      setRightLegHeight1(data?.loverbodyhorizontal_boardjump_distance_m1);
      setRightLegHeight2(data?.loverbodyhorizontal_boardjump_distance_m2);
      setRightLegHeight3(data?.loverbodyhorizontal_boardjump_distance_m3);
      setLeftLegHeight1(
        data?.loverbodyhorizontal_singlelegboardjump_right_distance_m1
      );
      setLeftLegHeight2(
        data?.loverbodyhorizontal_singlelegboardjump_right_distance_m2
      );
      setLeftLegHeight3(
        data?.loverbodyhorizontal_singlelegboardjump_right_distance_m3
      );
      setLowerBodyExplosiveRules(data?.lover_body_explosive_power_horizontal);
      setFlamingoRight(data?.flamingoblancetext_right_time);
      setFlamingoLeft(data?.flamingoblancetext_left_time);
      setFlamingoBalanceRules(data?.flamingoblancetext);
      setLoading(false);
    }
  }

  async function updatePfaStep() {
    // if (formValidation()) {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,
      lower_body_explosive_power: lowerBodyRules,
      lower_body_explosive_power_squat_jump_heightm_1: squatJumpHeight1,
      lower_body_explosive_power_squat_jump_heightm_2: squatJumpHeight2,
      lower_body_explosive_power_squat_jump_heightm_3: squatJumpHeight3,
      lower_body_counter_movement_jump_handsonhip_heightm_1: cmjHeight1,
      lower_body_counter_movement_jump_handsonhip_heightm_2: cmjHeight2,
      lower_body_counter_movement_jump_handsonhip_heightm_3: cmjHeight3,
      lower_body_counter_movement_jump_heightm_armswing_heightm_1:
        armSwingHeight1,
      lower_body_countermovement_jump_heightm_armswing_heightm_2:
        armSwingHeight2,
      lower_body_countermovement_jump_heightm_armswing_heightm_3:
        armSwingHeight3,

      counter_movement_jump_singleleg: counterMovementRules,
      counter_movement_jump_singleleg_rightleg_heightm_1:
        counterJumpRightLegHeight1,
      counter_movement_jump_singleleg_rightleg_heightm_2:
        counterJumpRightLegHeight2,
      counter_movement_jump_singleleg_rightleg_heightm_3:
        counterJumpRightLegHeight3,
      counter_movement_jump_singleleg_leftleg_heightm_1:
        counterJumpLeftLegHeight1,
      counter_movement_jump_singleleg_leftleg_heightm_2:
        counterJumpLeftLegHeight2,
      counter_movement_jump_singleleg_leftleg_heightm_3:
        counterJumpLeftLegHeight3,
      counter_movement_jump_singleleg_two: "",

      loverbodyhorizontal_boardjump_distance_m1: rightLegHeight1,
      loverbodyhorizontal_boardjump_distance_m2: rightLegHeight2,
      loverbodyhorizontal_boardjump_distance_m3: rightLegHeight3,
      loverbodyhorizontal_singlelegboardjump_right_distance_m1: leftLegHeight1,
      loverbodyhorizontal_singlelegboardjump_right_distance_m2: leftLegHeight2,
      loverbodyhorizontal_singlelegboardjump_right_distance_m3: leftLegHeight3,
      lover_body_explosive_power_horizontal: lowerBodyExplosiveRules,

      flamingoblancetext: flamingoBalanceRules,
      flamingoblancetext_right_time: flamingoRight,
      flamingoblancetext_left_time: flamingoLeft,
    };
    // console.log("params===>", params);
    const response = await updatePfaStepTwo(params);
    const stepId = response?.data?.id;
    // console.log("response", response.status, id, stepId);

    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(
        (backPfa && `/forms/step3/${userId}/${id}`) ||
          `/forms/step3/${"edit-pfa"}/${userId}/${id}`
      );
      setSquatJumpHeight1("");
      setSquatJumpHeight2("");
      setSquatJumpHeight3("");
      setCmjHeight1("");
      setCmjHeight2("");
      setCmjHeight3("");
      setArmSwingHeight1("");
      setArmSwingHeight2("");
      setArmSwingHeight3("");
      setLowerBodyRules("");
      setCounterJumpRightLegHeight1("");
      setCounterJumpRightLegHeight2("");
      setCounterJumpRightLegHeight3("");
      setCounterJumpLeftLegHeight1("");
      setCounterJumpLeftLegHeight2("");
      setCounterJumpLeftLegHeight3("");
      setCounterMovementRules("");
      setRightLegHeight1("");
      setRightLegHeight2("");
      setRightLegHeight3("");
      setLeftLegHeight1("");
      setLeftLegHeight2("");
      setLeftLegHeight3("");
      setLowerBodyExplosiveRules("");
      setFlamingoRight("");
      setFlamingoLeft("");
      setFlamingoBalanceRules("");
    } else {
      toast.error(response.message, toastConfig);
    }
    // }
  }

  async function handleUpdatePfaStep() {
    // if (formValidation()) {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,
      lower_body_explosive_power: lowerBodyRules,
      lower_body_explosive_power_squat_jump_heightm_1: squatJumpHeight1,
      lower_body_explosive_power_squat_jump_heightm_2: squatJumpHeight2,
      lower_body_explosive_power_squat_jump_heightm_3: squatJumpHeight3,
      lower_body_counter_movement_jump_handsonhip_heightm_1: cmjHeight1,
      lower_body_counter_movement_jump_handsonhip_heightm_2: cmjHeight2,
      lower_body_counter_movement_jump_handsonhip_heightm_3: cmjHeight3,
      lower_body_counter_movement_jump_heightm_armswing_heightm_1:
        armSwingHeight1,
      lower_body_countermovement_jump_heightm_armswing_heightm_2:
        armSwingHeight2,
      lower_body_countermovement_jump_heightm_armswing_heightm_3:
        armSwingHeight3,

      counter_movement_jump_singleleg: counterMovementRules,
      counter_movement_jump_singleleg_rightleg_heightm_1:
        counterJumpRightLegHeight1,
      counter_movement_jump_singleleg_rightleg_heightm_2:
        counterJumpRightLegHeight2,
      counter_movement_jump_singleleg_rightleg_heightm_3:
        counterJumpRightLegHeight3,
      counter_movement_jump_singleleg_leftleg_heightm_1:
        counterJumpLeftLegHeight1,
      counter_movement_jump_singleleg_leftleg_heightm_2:
        counterJumpLeftLegHeight2,
      counter_movement_jump_singleleg_leftleg_heightm_3:
        counterJumpLeftLegHeight3,
      counter_movement_jump_singleleg_two: "",

      loverbodyhorizontal_boardjump_distance_m1: rightLegHeight1,
      loverbodyhorizontal_boardjump_distance_m2: rightLegHeight2,
      loverbodyhorizontal_boardjump_distance_m3: rightLegHeight3,
      loverbodyhorizontal_singlelegboardjump_right_distance_m1: leftLegHeight1,
      loverbodyhorizontal_singlelegboardjump_right_distance_m2: leftLegHeight2,
      loverbodyhorizontal_singlelegboardjump_right_distance_m3: leftLegHeight3,
      lover_body_explosive_power_horizontal: lowerBodyExplosiveRules,

      flamingoblancetext: flamingoBalanceRules,
      flamingoblancetext_right_time: flamingoRight,
      flamingoblancetext_left_time: flamingoLeft,
    };
    // console.log("params===>", params);
    const response = await updatePfaStepTwo(params);
    const stepId = response?.data?.id;
    // console.log("response", response.status, id, stepId);

    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/forms/step3/${userId}/${id}`);
      setSquatJumpHeight1("");
      setSquatJumpHeight2("");
      setSquatJumpHeight3("");
      setCmjHeight1("");
      setCmjHeight2("");
      setCmjHeight3("");
      setArmSwingHeight1("");
      setArmSwingHeight2("");
      setArmSwingHeight3("");
      setLowerBodyRules("");
      setCounterJumpRightLegHeight1("");
      setCounterJumpRightLegHeight2("");
      setCounterJumpRightLegHeight3("");
      setCounterJumpLeftLegHeight1("");
      setCounterJumpLeftLegHeight2("");
      setCounterJumpLeftLegHeight3("");
      setCounterMovementRules("");
      setRightLegHeight1("");
      setRightLegHeight2("");
      setRightLegHeight3("");
      setLeftLegHeight1("");
      setLeftLegHeight2("");
      setLeftLegHeight3("");
      setLowerBodyExplosiveRules("");
      setFlamingoRight("");
      setFlamingoLeft("");
      setFlamingoBalanceRules("");
    } else {
      toast.error(response.message, toastConfig);
    }
    // }
  }

  async function createPfaUserSecond() {
    // if (formValidation()) {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,

      lower_body_explosive_power: lowerBodyRules,
      lower_body_explosive_power_squat_jump_heightm_1: squatJumpHeight1,
      lower_body_explosive_power_squat_jump_heightm_2: squatJumpHeight2,
      lower_body_explosive_power_squat_jump_heightm_3: squatJumpHeight3,
      lower_body_counter_movement_jump_handsonhip_heightm_1: cmjHeight1,
      lower_body_counter_movement_jump_handsonhip_heightm_2: cmjHeight2,
      lower_body_counter_movement_jump_handsonhip_heightm_3: cmjHeight3,
      lower_body_counter_movement_jump_heightm_armswing_heightm_1:
        armSwingHeight1,
      lower_body_countermovement_jump_heightm_armswing_heightm_2:
        armSwingHeight2,
      lower_body_countermovement_jump_heightm_armswing_heightm_3:
        armSwingHeight3,

      counter_movement_jump_singleleg: counterMovementRules,
      counter_movement_jump_singleleg_rightleg_heightm_1:
        counterJumpRightLegHeight1,
      counter_movement_jump_singleleg_rightleg_heightm_2:
        counterJumpRightLegHeight2,
      counter_movement_jump_singleleg_rightleg_heightm_3:
        counterJumpRightLegHeight3,
      counter_movement_jump_singleleg_leftleg_heightm_1:
        counterJumpLeftLegHeight1,
      counter_movement_jump_singleleg_leftleg_heightm_2:
        counterJumpLeftLegHeight2,
      counter_movement_jump_singleleg_leftleg_heightm_3:
        counterJumpLeftLegHeight3,
      counter_movement_jump_singleleg_two: "",

      loverbodyhorizontal_boardjump_distance_m1: rightLegHeight1,
      loverbodyhorizontal_boardjump_distance_m2: rightLegHeight2,
      loverbodyhorizontal_boardjump_distance_m3: rightLegHeight3,
      loverbodyhorizontal_singlelegboardjump_right_distance_m1: leftLegHeight1,
      loverbodyhorizontal_singlelegboardjump_right_distance_m2: leftLegHeight2,
      loverbodyhorizontal_singlelegboardjump_right_distance_m3: leftLegHeight3,
      lover_body_explosive_power_horizontal: lowerBodyExplosiveRules,

      flamingoblancetext: flamingoBalanceRules,
      flamingoblancetext_right_time: flamingoRight,
      flamingoblancetext_left_time: flamingoLeft,
    };
    // console.log("params===>", params);
    const response = await createPfaStepTwo(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/forms/step3/${userId}/${id}`);
      setSquatJumpHeight1("");
      setSquatJumpHeight2("");
      setSquatJumpHeight3("");
      setCmjHeight1("");
      setCmjHeight2("");
      setCmjHeight3("");
      setArmSwingHeight1("");
      setArmSwingHeight2("");
      setArmSwingHeight3("");
      setLowerBodyRules("");
      setCounterJumpRightLegHeight1("");
      setCounterJumpRightLegHeight2("");
      setCounterJumpRightLegHeight3("");
      setCounterJumpLeftLegHeight1("");
      setCounterJumpLeftLegHeight2("");
      setCounterJumpLeftLegHeight3("");
      setCounterMovementRules("");
      setRightLegHeight1("");
      setRightLegHeight2("");
      setRightLegHeight3("");
      setLeftLegHeight1("");
      setLeftLegHeight2("");
      setLeftLegHeight3("");
      setLowerBodyExplosiveRules("");
      setFlamingoRight("");
      setFlamingoLeft("");
      setFlamingoBalanceRules("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  // function formValidation() {
  //   let msgName = "";
  //   let msgAge = "";
  //   let msgWeight = "";
  //   let msgHeight = "";
  //   let msgRegistrationNo = "";
  //   let msgSport = "";
  //   let msgDate = "";
  //   let msgMfData = "";
  //   let isValid = false;

  //   if (name == "") {
  //     msgName = "Please enter name";
  //   }

  //   if (age == "") {
  //     msgAge = "Please enter age";
  //   }

  //   if (weight === "") {
  //     msgWeight = "Please enter weight";
  //   }

  //   if (height === "") {
  //     msgHeight = "Please enter height";
  //   }

  //   if (mfData === "") {
  //     msgMfData = "Please enter M/F";
  //   }

  //   if (registrationNo === "") {
  //     msgRegistrationNo = "Please enter registration number";
  //   }

  //   if (sport === "") {
  //     msgSport = "Please select a sport";
  //   }

  //   if (date === "") {
  //     msgDate = "Please select a date";
  //   }

  //   if (
  //     (!msgName,
  //     !msgAge,
  //     !msgWeight,
  //     !msgRegistrationNo,
  //     !msgSport,
  //     !msgDate,
  //     !msgHeight,
  //     !msgMfData)
  //   ) {
  //     isValid = true;
  //   }

  //   if (isValid) {
  //     setError(true);
  //     setErrorMessage({
  //       name: "",
  //       age: "",
  //       weight: "",
  //       registrationNo: "",
  //       sport: "",
  //       date: "",
  //       height: "",
  //       mfData: "",
  //     });
  //     return true;
  //   } else {
  //     setError(true);
  //     setErrorMessage({
  //       name: msgName,
  //       age: msgAge,
  //       weight: msgWeight,
  //       registrationNo: msgRegistrationNo,
  //       sport: msgSport,
  //       date: msgDate,
  //       height: msgHeight,
  //       mfData: msgMfData,
  //     });
  //     return false;
  //   }
  // }

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body2"
              color="text"
              ml={1}
              fontWeight="regular"
              fontSize={30}
            >
              PHYSICAL FITNESS ASSESSMENT
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            {/* 1.Lower Body Explosive Power */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Lower Body Explosive Power
              </MDTypography>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Squat Jump
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Height (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_squat_jump_heightm_1"
                      placeholder="1"
                      value={squatJumpHeight1}
                      id="squatJumpHeight1"
                      onChange={(e) => setSquatJumpHeight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["squatJumpHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["squatJumpHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_squat_jump_heightm_2"
                      placeholder="2"
                      value={squatJumpHeight2}
                      id="squatJumpHeight2"
                      onChange={(e) => setSquatJumpHeight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["squatJumpHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["squatJumpHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_squat_jump_heightm_3"
                      placeholder="3"
                      value={squatJumpHeight3}
                      id="squatJumpHeight3"
                      onChange={(e) => setSquatJumpHeight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["squatJumpHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["squatJumpHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Counter Movement Jump (Hands on Hip)
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Height (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_counter_movement_jump_handsonhip_heightm_1"
                      placeholder="1"
                      value={cmjHeight1}
                      id="cmjHeight1"
                      onChange={(e) => setCmjHeight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cmjHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cmjHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_counter_movement_jump_handsonhip_heightm_2"
                      placeholder="2"
                      value={cmjHeight2}
                      id="cmjHeight2"
                      onChange={(e) => setCmjHeight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cmjHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cmjHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_counter_movement_jump_handsonhip_heightm_3"
                      placeholder="3"
                      value={cmjHeight3}
                      id="cmjHeight3"
                      onChange={(e) => setCmjHeight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cmjHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cmjHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Arm Swing: Height (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_counter_movement_jump_handsonhip_heightm_armswing_heightm_1"
                      placeholder="1"
                      value={armSwingHeight1}
                      id="armSwingHeight1"
                      onChange={(e) => setArmSwingHeight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["armSwingHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["armSwingHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_countermovement_jump_handsonhip_heightm_armswing_heightm_2"
                      placeholder="2"
                      value={armSwingHeight2}
                      id="armSwingHeight2"
                      onChange={(e) => setArmSwingHeight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["armSwingHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["armSwingHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="lower_body_explosive_power_countermovement_jump_handsonhip_heightm_armswing_heightm_3"
                      placeholder="3"
                      value={armSwingHeight3}
                      id="armSwingHeight3"
                      onChange={(e) => setArmSwingHeight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["armSwingHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["armSwingHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Counter Movement Jump (Hands on Hip)"
                    value={lowerBodyRules}
                    id="lowerBodyRules"
                    onChange={(e) => setLowerBodyRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["lowerBodyRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["lowerBodyRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 2.Counter Movement Jump (Single leg) */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Counter Movement Jump (Single leg)
              </MDTypography>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Right Leg
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Height (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_rightleg_heightm_1"
                      placeholder="1"
                      value={counterJumpRightLegHeight1}
                      id="counterJumpRightLegHeight1"
                      onChange={(e) =>
                        setCounterJumpRightLegHeight1(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["counterJumpRightLegHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["counterJumpRightLegHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_rightleg_heightm_2"
                      placeholder="2"
                      value={counterJumpRightLegHeight2}
                      id="counterJumpRightLegHeight2"
                      onChange={(e) =>
                        setCounterJumpRightLegHeight2(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["counterJumpRightLegHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["counterJumpRightLegHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_rightleg_heightm_3"
                      placeholder="3"
                      value={counterJumpRightLegHeight3}
                      id="counterJumpRightLegHeight3"
                      onChange={(e) =>
                        setCounterJumpRightLegHeight3(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["counterJumpRightLegHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["counterJumpRightLegHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Left Leg
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Height (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_1"
                      placeholder="1"
                      value={counterJumpLeftLegHeight1}
                      id="counterJumpLeftLegHeight1"
                      onChange={(e) =>
                        setCounterJumpLeftLegHeight1(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["counterJumpLeftLegHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["counterJumpLeftLegHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_2"
                      placeholder="2"
                      value={counterJumpLeftLegHeight2}
                      id="counterJumpLeftLegHeight2"
                      onChange={(e) =>
                        setCounterJumpLeftLegHeight2(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["counterJumpLeftLegHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["counterJumpLeftLegHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_3"
                      placeholder="3"
                      value={counterJumpLeftLegHeight3}
                      id="counterJumpLeftLegHeight3"
                      onChange={(e) =>
                        setCounterJumpLeftLegHeight3(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["counterJumpLeftLegHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["counterJumpLeftLegHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Counter Movement Jump (Single leg)"
                    value={counterMovementRules}
                    id="counterMovementRules"
                    onChange={(e) => setCounterMovementRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["counterMovementRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["counterMovementRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 3.Lower Body Explosive Power (Horizontal) */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Lower Body Explosive Power (Horizontal)
              </MDTypography>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Broad Jump
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Distance (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_rightleg_heightm_1"
                      placeholder="1"
                      value={rightLegHeight1}
                      id="rightLegHeight1"
                      onChange={(e) => setRightLegHeight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rightLegHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rightLegHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_rightleg_heightm_2"
                      placeholder="2"
                      value={rightLegHeight2}
                      id="rightLegHeight2"
                      onChange={(e) => setRightLegHeight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rightLegHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rightLegHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_rightleg_heightm_3"
                      placeholder="3"
                      value={rightLegHeight3}
                      id="rightLegHeight3"
                      onChange={(e) => setRightLegHeight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rightLegHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rightLegHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                pt={1}
                fontWeight="bold"
                fontSize={16}
              >
                Single leg broad jump (Right)
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Distance (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_1"
                      placeholder="1"
                      value={leftLegHeight1}
                      id="leftLegHeight1"
                      onChange={(e) => setLeftLegHeight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["leftLegHeight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["leftLegHeight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_2"
                      placeholder="2"
                      value={leftLegHeight2}
                      id="leftLegHeight2"
                      onChange={(e) => setLeftLegHeight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["leftLegHeight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["leftLegHeight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_3"
                      placeholder="3"
                      value={leftLegHeight3}
                      id="leftLegHeight3"
                      onChange={(e) => setLeftLegHeight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["leftLegHeight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["leftLegHeight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Lower Body Explosive Power (Horizontal)"
                    value={lowerBodyExplosiveRules}
                    id="lowerBodyExplosiveRules"
                    onChange={(e) => setLowerBodyExplosiveRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["lowerBodyExplosiveRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["lowerBodyExplosiveRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 4.Flamingo Balance Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
                mt={2}
                mb={1}
              >
                Flamingo Balance Test
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                ></MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="bold"
                  textAlign="center"
                  mb={1}
                >
                  Right
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="bold"
                  textAlign="center"
                  mb={1}
                >
                  Left
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  mt={1}
                  fontWeight="bold"
                >
                  Time (sec)
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_2"
                      placeholder="Right"
                      value={flamingoRight}
                      id="flamingoRight"
                      onChange={(e) => setFlamingoRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["flamingoRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["flamingoRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="counter_movement_jump_singleleg_leftleg_heightm_3"
                      placeholder="Left"
                      value={flamingoLeft}
                      id="flamingoLeft"
                      onChange={(e) => setFlamingoLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["flamingoLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["flamingoLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Flamingo Balance Test"
                    value={flamingoBalanceRules}
                    id="flamingoBalanceRules"
                    onChange={(e) => setFlamingoBalanceRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["flamingoBalanceRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["flamingoBalanceRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* save button */}
            <MDBox display="flex" flexDirection="row" justifyContent="flex-end">
              <MDBox mt={2} mr={3} flexDirection="column">
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step1/${"edit-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step1/${"back-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )}
              </MDBox>
              <MDBox mt={2} flexDirection="column">
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updatePfaStep}
                  >
                    Update / Next
                  </MDButton>
                )) ||
                  (backPfa && (
                    <MDButton
                      variant="gradient"
                      color="info"
                      onClick={updatePfaStep}
                    >
                      save / Next
                    </MDButton>
                  )) || (
                    <MDButton
                      variant="gradient"
                      color="info"
                      type="button"
                      onClick={
                        (stepDetail && handleUpdatePfaStep) ||
                        createPfaUserSecond
                      }
                    >
                      Save / Next
                    </MDButton>
                  )}
              </MDBox>
            </MDBox>

            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step2 / Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default PfaStep2;
