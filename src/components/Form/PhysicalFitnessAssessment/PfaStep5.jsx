import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextareaAutosize,
} from "@mui/material";
import { toast } from "react-toastify";
import {
  createPfaProtocol,
  createPfaStepFive,
  getPfaStepFive,
  updatePfaStepFive,
} from "../../../services/service_api";
import { Speed } from "@mui/icons-material";

const style = {
  height: "100%",
};

const height45 = {
  height: "45px",
};

function PfaStep5() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editPfa = route.pathname?.split("/")?.slice(3, 4) == "edit-pfa";
  // console.log("editPfa===>", editPfa, userId, id);
  const [updateId, setUpdateId] = useState("");

  // 1.Injury/Niggle Management
  const [niggleManagement, setNiggleManagement] = useState("");
  const [niggleManagementCheck1, setNiggleManagementCheck1] = useState("");
  const [niggleManagementCheck2, setNiggleManagementCheck2] = useState("");
  const [niggleManagementCheck3, setNiggleManagementCheck3] = useState("");

  // 2.Functional Movement
  const [functionalMovement, setFunctionalMovement] = useState("");
  const [functionalMovementCheck1, setFunctionalMovementCheck1] = useState("");
  const [functionalMovementCheck2, setFunctionalMovementCheck2] = useState("");
  const [functionalMovementCheck3, setFunctionalMovementCheck3] = useState("");

  // 3.Core Endurance
  const [coreEndurance, setCoreEndurance] = useState("");
  const [coreEnduranceCheck1, setCoreEnduranceCheck1] = useState("");
  const [coreEnduranceCheck2, setCoreEnduranceCheck2] = useState("");
  const [coreEnduranceCheck3, setCoreEnduranceCheck3] = useState("");

  // 4.Reaction Time
  const [reactionTime, setReactionTime] = useState("");
  const [reactionTimeCheck1, setReactionTimeCheck1] = useState("");
  const [reactionTimeCheck2, setReactionTimeCheck2] = useState("");
  const [reactionTimeCheck3, setReactionTimeCheck3] = useState("");

  // 5.Muscular Endurance (lower)
  const [muscularEnduranceLower, setMuscularEnduranceLower] = useState("");
  const [muscularEnduranceLowerCheck1, setMuscularEnduranceLowerCheck1] =
    useState("");
  const [muscularEnduranceLowerCheck2, setMuscularEnduranceLowerCheck2] =
    useState("");
  const [muscularEnduranceLowerCheck3, setMuscularEnduranceLowerCheck3] =
    useState("");

  // 6.Muscular Endurance (upper)
  const [muscularEnduranceUpper, setMuscularEnduranceUpper] = useState("");
  const [muscularEnduranceUpperCheck1, setMuscularEnduranceUpperCheck1] =
    useState("");
  const [muscularEnduranceUpperCheck2, setMuscularEnduranceUpperCheck2] =
    useState("");
  const [muscularEnduranceUpperCheck3, setMuscularEnduranceUpperCheck3] =
    useState("");

  // 7.Speed
  const [speed, setSpeed] = useState("");
  const [speedCheck1, setSpeedCheck1] = useState("");
  const [speedCheck2, setSpeedCheck2] = useState("");
  const [speedCheck3, setSpeedCheck3] = useState("");

  // 8.Agility
  const [agility, setAgility] = useState("");
  const [agilityCheck1, setAgilityCheck1] = useState("");
  const [agilityCheck2, setAgilityCheck2] = useState("");
  const [agilityCheck3, setAgilityCheck3] = useState("");

  // 9.Balance
  const [balance, setBalance] = useState("");
  const [balanceCheck1, setBalanceCheck1] = useState("");
  const [balanceCheck2, setBalanceCheck2] = useState("");
  const [balanceCheck3, setBalanceCheck3] = useState("");

  // 10.Anaerobic Fitness
  const [anaerobicFitness, setAnaerobicFitness] = useState("");
  const [anaerobicFitnessCheck1, setAnaerobicFitnessCheck1] = useState("");
  const [anaerobicFitnessCheck2, setAnaerobicFitnessCheck2] = useState("");
  const [anaerobicFitnessCheck3, setAnaerobicFitnessCheck3] = useState("");

  // 11.Cardiovascular Endurance
  const [cardiovascularEndurance, setCardiovascularEndurance] = useState("");
  const [cardiovascularEnduranceCheck1, setCardiovascularEnduranceCheck1] =
    useState("");
  const [cardiovascularEnduranceCheck2, setCardiovascularEnduranceCheck2] =
    useState("");
  const [cardiovascularEnduranceCheck3, setCardiovascularEnduranceCheck3] =
    useState("");

  // 12.Explosive Power (lower body)
  const [explosivePowerLowerBody, setExplosivePowerLowerBody] = useState("");
  const [explosivePowerLowerBodyCheck1, setExplosivePowerLowerBodyCheck1] =
    useState("");
  const [explosivePowerLowerBodyCheck2, setExplosivePowerLowerBodyCheck2] =
    useState("");
  const [explosivePowerLowerBodyCheck3, setExplosivePowerLowerBodyCheck3] =
    useState("");

  // 13.Explosive Power (upper body)
  const [explosivePowerUpperBody, setExplosivePowerUpperBody] = useState("");
  const [explosivePowerUpperBodyCheck1, setExplosivePowerUpperBodyCheck1] =
    useState("");
  const [explosivePowerUpperBodyCheck2, setExplosivePowerUpperBodyCheck2] =
    useState("");
  const [explosivePowerUpperBodyCheck3, setExplosivePowerUpperBodyCheck3] =
    useState("");

  const [loading, setLoading] = useState(false);

  React.useEffect(() => {
    if (editPfa) {
      getPfaFormData();
    }
  }, []);

  async function getPfaFormData() {
    setLoading(true);
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
    };
    // console.log("params=====>", params);
    const response = await getPfaStepFive(params);
    const data = response.data;
    // console.log(
    //   "response-------->",
    //   data?.injury_niggle_management_needs_improvement == 1 ? true : false
    // );
    if (response) {
      setUpdateId(data?.id);
      setNiggleManagement(data?.injury_niggle_management_priority);
      setNiggleManagementCheck1(
        data?.injury_niggle_management_needs_improvement == 1 ? true : false
      );
      setNiggleManagementCheck2(
        data?.injury_niggle_management_at_par == 1 ? true : false
      );
      setNiggleManagementCheck3(
        data?.injury_niggle_management_good == 1 ? true : false
      );

      setFunctionalMovement(data?.functional_movement_priority);
      setFunctionalMovementCheck1(
        data?.functional_movement_needs_improvement == 1 ? true : false
      );
      setFunctionalMovementCheck2(
        data?.functional_movement_at_par == 1 ? true : false
      );
      setFunctionalMovementCheck3(
        data?.functional_movement_good == 1 ? true : false
      );

      setCoreEndurance(data?.core_endurance_priority);
      setCoreEnduranceCheck1(
        data?.core_endurance_needs_improvement == 1 ? true : false
      );
      setCoreEnduranceCheck2(data?.core_endurance_at_par == 1 ? true : false);
      setCoreEnduranceCheck3(data?.core_endurance_good == 1 ? true : false);

      setReactionTime(data?.reaction_time_priority);
      setReactionTimeCheck1(
        data?.reaction_time_needs_improvement == 1 ? true : false
      );
      setReactionTimeCheck2(data?.reaction_time_at_par == 1 ? true : false);
      setReactionTimeCheck3(data?.reaction_time_good == 1 ? true : false);

      setMuscularEnduranceLower(data?.muscular_endurance_lower_priority);
      setMuscularEnduranceLowerCheck1(
        data?.muscular_endurance_lower_needs_improvement == 1 ? true : false
      );
      setMuscularEnduranceLowerCheck2(
        data?.muscular_endurance_lower_at_par == 1 ? true : false
      );
      setMuscularEnduranceLowerCheck3(
        data?.muscular_endurance_lower_good == 1 ? true : false
      );

      setMuscularEnduranceUpper(data?.muscular_endurance_upper_priority);
      setMuscularEnduranceUpperCheck1(
        data?.muscular_endurance_upper_needs_improvement == 1 ? true : false
      );
      setMuscularEnduranceUpperCheck2(
        data?.muscular_endurance_upper_at_par == 1 ? true : false
      );
      setMuscularEnduranceUpperCheck3(
        data?.muscular_endurance_upper_good == 1 ? true : false
      );

      setSpeed(data?.speed_priority);
      setSpeedCheck1(data?.speed_needs_improvement == 1 ? true : false);
      setSpeedCheck2(data?.speed_at_par == 1 ? true : false);
      setSpeedCheck3(data?.speed_good == 1 ? true : false);

      setAgility(data?.agility_priority);
      setAgilityCheck1(data?.agility_needs_improvement == 1 ? true : false);
      setAgilityCheck2(data?.agility_at_par == 1 ? true : false);
      setAgilityCheck3(data?.agility_good == 1 ? true : false);

      setBalance(data?.balance_priority);
      setBalanceCheck1(data?.balance_needs_improvement == 1 ? true : false);
      setBalanceCheck2(data?.balance_at_par == 1 ? true : false);
      setBalanceCheck3(data?.balance_good == 1 ? true : false);

      setAnaerobicFitness(data?.anaerobic_fitness_priority);
      setAnaerobicFitnessCheck1(
        data?.anaerobic_fitness_needs_improvement == 1 ? true : false
      );
      setAnaerobicFitnessCheck2(
        data?.anaerobic_fitness_at_par == 1 ? true : false
      );
      setAnaerobicFitnessCheck3(
        data?.anaerobic_fitness_good == 1 ? true : false
      );

      setCardiovascularEndurance(data?.cardiovascular_endurance_priority);
      setCardiovascularEnduranceCheck1(
        data?.cardiovascular_endurance_needs_improvement == 1 ? true : false
      );
      setCardiovascularEnduranceCheck2(
        data?.cardiovascular_endurance_at_par == 1 ? true : false
      );
      setCardiovascularEnduranceCheck3(
        data?.cardiovascular_endurance_good == 1 ? true : false
      );

      setExplosivePowerLowerBody(data?.explosive_power_lower_body_priority);
      setExplosivePowerLowerBodyCheck1(
        data?.explosive_power_lower_body_needs_improvement == 1 ? true : false
      );
      setExplosivePowerLowerBodyCheck2(
        data?.explosive_power_lower_body_at_par == 1 ? true : false
      );
      setExplosivePowerLowerBodyCheck3(
        data?.explosive_power_lower_body_good == 1 ? true : false
      );

      setExplosivePowerUpperBody(data?.explosive_power_upper_body_priority);
      setExplosivePowerUpperBodyCheck1(
        data?.explosive_power_upper_body_needs_improvement == 1 ? true : false
      );
      setExplosivePowerUpperBodyCheck2(
        data?.explosive_power_upper_body_at_par == 1 ? true : false
      );
      setExplosivePowerUpperBodyCheck3(
        data?.explosive_power_upper_body_good == 1 ? true : false
      );
      setLoading(false);
    }
  }

  async function createPfaUser() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,

      injury_niggle_management_priority:
        (niggleManagement == "" && "none") || niggleManagement,
      injury_niggle_management_needs_improvement: niggleManagementCheck1
        ? "1"
        : "0",
      injury_niggle_management_at_par: niggleManagementCheck2 ? "1" : "0",
      injury_niggle_management_good: niggleManagementCheck3 ? "1" : "0",

      functional_movement_priority:
        (functionalMovement == "" && "none") || functionalMovement,
      functional_movement_needs_improvement: functionalMovementCheck1
        ? "1"
        : "0",
      functional_movement_at_par: functionalMovementCheck2 ? "1" : "0",
      functional_movement_good: functionalMovementCheck3 ? "1" : "0",

      core_endurance_priority: (coreEndurance == "" && "none") || coreEndurance,
      core_endurance_needs_improvement: coreEnduranceCheck1 ? "1" : "0",
      core_endurance_at_par: coreEnduranceCheck2 ? "1" : "0",
      core_endurance_good: coreEnduranceCheck3 ? "1" : "0",

      reaction_time_priority: (reactionTime == "" && "none") || reactionTime,
      reaction_time_needs_improvement: reactionTimeCheck1 ? "1" : "0",
      reaction_time_at_par: reactionTimeCheck2 ? "1" : "0",
      reaction_time_good: reactionTimeCheck3 ? "1" : "0",

      muscular_endurance_lower_priority:
        (muscularEnduranceLower == "" && "none") || muscularEnduranceLower,
      muscular_endurance_lower_needs_improvement: muscularEnduranceLowerCheck1
        ? "1"
        : "0",
      muscular_endurance_lower_at_par: muscularEnduranceLowerCheck2 ? "1" : "0",
      muscular_endurance_lower_good: muscularEnduranceLowerCheck3 ? "1" : "0",

      muscular_endurance_upper_priority:
        (muscularEnduranceUpper == "" && "none") || muscularEnduranceUpper,
      muscular_endurance_upper_needs_improvement: muscularEnduranceUpperCheck1
        ? "1"
        : "0",
      muscular_endurance_upper_at_par: muscularEnduranceUpperCheck2 ? "1" : "0",
      muscular_endurance_upper_good: muscularEnduranceUpperCheck3 ? "1" : "0",

      speed_priority: (speed == "" && "none") || speed,
      speed_needs_improvement: speedCheck1 ? "1" : "0",
      speed_at_par: speedCheck2 ? "1" : "0",
      speed_good: speedCheck3 ? "1" : "0",

      agility_priority: (agility == "" && "none") || agility,
      agility_needs_improvement: agilityCheck1 ? "1" : "0",
      agility_at_par: agilityCheck2 ? "1" : "0",
      agility_good: agilityCheck3 ? "1" : "0",

      balance_priority: (balance == "" && "none") || balance,
      balance_needs_improvement: balanceCheck1 ? "1" : "0",
      balance_at_par: balanceCheck2 ? "1" : "0",
      balance_good: balanceCheck3 ? "1" : "0",

      anaerobic_fitness_priority:
        (anaerobicFitness == "" && "none") || anaerobicFitness,
      anaerobic_fitness_needs_improvement: anaerobicFitnessCheck1 ? "1" : "0",
      anaerobic_fitness_at_par: anaerobicFitnessCheck2 ? "1" : "0",
      anaerobic_fitness_good: anaerobicFitnessCheck3 ? "1" : "0",

      cardiovascular_endurance_priority:
        (cardiovascularEndurance == "" && "none") || cardiovascularEndurance,
      cardiovascular_endurance_needs_improvement: cardiovascularEnduranceCheck1
        ? "1"
        : "0",
      cardiovascular_endurance_at_par: cardiovascularEnduranceCheck2
        ? "1"
        : "0",
      cardiovascular_endurance_good: cardiovascularEnduranceCheck3 ? "1" : "0",

      explosive_power_lower_body_priority:
        (explosivePowerLowerBody == "" && "none") || explosivePowerLowerBody,
      explosive_power_lower_body_needs_improvement:
        explosivePowerLowerBodyCheck1 ? "1" : "0",
      explosive_power_lower_body_at_par: explosivePowerLowerBodyCheck2
        ? "1"
        : "0",
      explosive_power_lower_body_good: explosivePowerLowerBodyCheck3
        ? "1"
        : "0",

      explosive_power_upper_body_priority:
        (explosivePowerUpperBody == "" && "none") || explosivePowerUpperBody,
      explosive_power_upper_body_needs_improvement:
        explosivePowerUpperBodyCheck1 ? "1" : "0",
      explosive_power_upper_body_at_par: explosivePowerUpperBodyCheck2
        ? "1"
        : "0",
      explosive_power_upper_body_good: explosivePowerUpperBodyCheck3
        ? "1"
        : "0",
    };
    // console.log("params===>", params);
    const response = await createPfaStepFive(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/all/forms/${userId}`);
      setNiggleManagement("");
      setNiggleManagementCheck1("");
      setNiggleManagementCheck2("");
      setNiggleManagementCheck3("");
      setFunctionalMovement("");
      setFunctionalMovementCheck1("");
      setFunctionalMovementCheck2("");
      setFunctionalMovementCheck3("");
      setCoreEndurance("");
      setCoreEnduranceCheck1("");
      setCoreEnduranceCheck2("");
      setCoreEnduranceCheck3("");
      setReactionTime("");
      setReactionTimeCheck1("");
      setReactionTimeCheck2("");
      setReactionTimeCheck3("");
      setMuscularEnduranceLower("");
      setMuscularEnduranceLowerCheck1("");
      setMuscularEnduranceLowerCheck2("");
      setMuscularEnduranceLowerCheck3("");
      setMuscularEnduranceUpper("");
      setMuscularEnduranceUpperCheck1("");
      setMuscularEnduranceUpperCheck2("");
      setMuscularEnduranceUpperCheck3("");
      setSpeed("");
      setSpeedCheck1("");
      setSpeedCheck2("");
      setSpeedCheck3("");
      setAgility("");
      setAgilityCheck1("");
      setAgilityCheck2("");
      setAgilityCheck3("");
      setBalance("");
      setBalanceCheck1("");
      setBalanceCheck2("");
      setBalanceCheck3("");
      setAnaerobicFitness("");
      setAnaerobicFitnessCheck1("");
      setAnaerobicFitnessCheck2("");
      setAnaerobicFitnessCheck3("");
      setCardiovascularEndurance("");
      setCardiovascularEnduranceCheck1("");
      setCardiovascularEnduranceCheck2("");
      setCardiovascularEnduranceCheck3("");
      setExplosivePowerLowerBody("");
      setExplosivePowerLowerBodyCheck1("");
      setExplosivePowerLowerBodyCheck2("");
      setExplosivePowerLowerBodyCheck3("");
      setExplosivePowerUpperBody("");
      setExplosivePowerUpperBodyCheck1("");
      setExplosivePowerUpperBodyCheck2("");
      setExplosivePowerUpperBodyCheck3("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  async function updatePfaStep() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,

      injury_niggle_management_priority: niggleManagement,
      injury_niggle_management_needs_improvement: niggleManagementCheck1
        ? "1"
        : "0",
      injury_niggle_management_at_par: niggleManagementCheck2 ? "1" : "0",
      injury_niggle_management_good: niggleManagementCheck3 ? "1" : "0",

      functional_movement_priority: functionalMovement,
      functional_movement_needs_improvement: functionalMovementCheck1
        ? "1"
        : "0",
      functional_movement_at_par: functionalMovementCheck2 ? "1" : "0",
      functional_movement_good: functionalMovementCheck3 ? "1" : "0",

      core_endurance_priority: coreEndurance,
      core_endurance_needs_improvement: coreEnduranceCheck1 ? "1" : "0",
      core_endurance_at_par: coreEnduranceCheck2 ? "1" : "0",
      core_endurance_good: coreEnduranceCheck3 ? "1" : "0",

      reaction_time_priority: reactionTime,
      reaction_time_needs_improvement: reactionTimeCheck1 ? "1" : "0",
      reaction_time_at_par: reactionTimeCheck2 ? "1" : "0",
      reaction_time_good: reactionTimeCheck3 ? "1" : "0",

      muscular_endurance_lower_priority: muscularEnduranceLower,
      muscular_endurance_lower_needs_improvement: muscularEnduranceLowerCheck1
        ? "1"
        : "0",
      muscular_endurance_lower_at_par: muscularEnduranceLowerCheck2 ? "1" : "0",
      muscular_endurance_lower_good: muscularEnduranceLowerCheck3 ? "1" : "0",

      muscular_endurance_upper_priority: muscularEnduranceUpper,
      muscular_endurance_upper_needs_improvement: muscularEnduranceUpperCheck1
        ? "1"
        : "0",
      muscular_endurance_upper_at_par: muscularEnduranceUpperCheck2 ? "1" : "0",
      muscular_endurance_upper_good: muscularEnduranceUpperCheck3 ? "1" : "0",

      speed_priority: speed,
      speed_needs_improvement: speedCheck1 ? "1" : "0",
      speed_at_par: speedCheck2 ? "1" : "0",
      speed_good: speedCheck3 ? "1" : "0",

      agility_priority: agility,
      agility_needs_improvement: agilityCheck1 ? "1" : "0",
      agility_at_par: agilityCheck2 ? "1" : "0",
      agility_good: agilityCheck3 ? "1" : "0",

      balance_priority: balance,
      balance_needs_improvement: balanceCheck1 ? "1" : "0",
      balance_at_par: balanceCheck2 ? "1" : "0",
      balance_good: balanceCheck3 ? "1" : "0",

      anaerobic_fitness_priority: anaerobicFitness,
      anaerobic_fitness_needs_improvement: anaerobicFitnessCheck1 ? "1" : "0",
      anaerobic_fitness_at_par: anaerobicFitnessCheck2 ? "1" : "0",
      anaerobic_fitness_good: anaerobicFitnessCheck3 ? "1" : "0",

      cardiovascular_endurance_priority: cardiovascularEndurance ? "1" : "0",
      cardiovascular_endurance_needs_improvement: cardiovascularEnduranceCheck1
        ? "1"
        : "0",
      cardiovascular_endurance_at_par: cardiovascularEnduranceCheck2
        ? "1"
        : "0",
      cardiovascular_endurance_good: cardiovascularEnduranceCheck3 ? "1" : "0",

      explosive_power_lower_body_priority: explosivePowerLowerBody ? "1" : "0",
      explosive_power_lower_body_needs_improvement:
        explosivePowerLowerBodyCheck1 ? "1" : "0",
      explosive_power_lower_body_at_par: explosivePowerLowerBodyCheck2
        ? "1"
        : "0",
      explosive_power_lower_body_good: explosivePowerLowerBodyCheck3
        ? "1"
        : "0",

      explosive_power_upper_body_priority: explosivePowerUpperBody ? "1" : "0",
      explosive_power_upper_body_needs_improvement:
        explosivePowerUpperBodyCheck1 ? "1" : "0",
      explosive_power_upper_body_at_par: explosivePowerUpperBodyCheck2
        ? "1"
        : "0",
      explosive_power_upper_body_good: explosivePowerUpperBodyCheck3
        ? "1"
        : "0",
    };
    // console.log("params===>", params);
    const response = await updatePfaStepFive(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/all/forms/${userId}`);
      setNiggleManagement("");
      setNiggleManagementCheck1("");
      setNiggleManagementCheck2("");
      setNiggleManagementCheck3("");
      setFunctionalMovement("");
      setFunctionalMovementCheck1("");
      setFunctionalMovementCheck2("");
      setFunctionalMovementCheck3("");
      setCoreEndurance("");
      setCoreEnduranceCheck1("");
      setCoreEnduranceCheck2("");
      setCoreEnduranceCheck3("");
      setReactionTime("");
      setReactionTimeCheck1("");
      setReactionTimeCheck2("");
      setReactionTimeCheck3("");
      setMuscularEnduranceLower("");
      setMuscularEnduranceLowerCheck1("");
      setMuscularEnduranceLowerCheck2("");
      setMuscularEnduranceLowerCheck3("");
      setMuscularEnduranceUpper("");
      setMuscularEnduranceUpperCheck1("");
      setMuscularEnduranceUpperCheck2("");
      setMuscularEnduranceUpperCheck3("");
      setSpeed("");
      setSpeedCheck1("");
      setSpeedCheck2("");
      setSpeedCheck3("");
      setAgility("");
      setAgilityCheck1("");
      setAgilityCheck2("");
      setAgilityCheck3("");
      setBalance("");
      setBalanceCheck1("");
      setBalanceCheck2("");
      setBalanceCheck3("");
      setAnaerobicFitness("");
      setAnaerobicFitnessCheck1("");
      setAnaerobicFitnessCheck2("");
      setAnaerobicFitnessCheck3("");
      setCardiovascularEndurance("");
      setCardiovascularEnduranceCheck1("");
      setCardiovascularEnduranceCheck2("");
      setCardiovascularEnduranceCheck3("");
      setExplosivePowerLowerBody("");
      setExplosivePowerLowerBodyCheck1("");
      setExplosivePowerLowerBodyCheck2("");
      setExplosivePowerLowerBodyCheck3("");
      setExplosivePowerUpperBody("");
      setExplosivePowerUpperBodyCheck1("");
      setExplosivePowerUpperBodyCheck2("");
      setExplosivePowerUpperBodyCheck3("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }
  // console.log(
  //   "niggleManagementCheck2",
  //   niggleManagementCheck2,
  //   niggleManagement
  // );

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <Card pt={5} mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body2"
              color="text"
              ml={1}
              fontWeight="regular"
              fontSize={30}
            >
              PHYSICAL FITNESS ASSESSMENT
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            mt={3}
            display="flex"
            flexDirection="column"
            width="100%"
          >
            <MDBox
              display="flex"
              flexDirection="row"
              width="100%"
              justifyContent="center"
            >
              <MDBox flexDirection="column" display="flex" width="30%">
                Parameter
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                justifyContent="center"
                textAlign="center"
                width="20%"
              >
                Priority
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                justifyContent="center"
                textAlign="center"
                width="20%"
              >
                Needs <br />
                Improvement
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                justifyContent="center"
                textAlign="center"
                width="20%"
              >
                At Par
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                justifyContent="center"
                textAlign="center"
                width="10%"
              >
                Good
              </MDBox>
            </MDBox>
            {/* 1.Injury/Niggle Management */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" pt={1} width="30%">
                1.Injury/Niggle Management
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ height: 45 }}
                    value={niggleManagement}
                    onChange={(e) => setNiggleManagement(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={niggleManagementCheck1}
                  onChange={(e) => setNiggleManagementCheck1(e.target.checked)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={niggleManagementCheck2}
                  onChange={(e) => setNiggleManagementCheck2(e.target.checked)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={niggleManagementCheck3}
                  onChange={() =>
                    setNiggleManagementCheck3(!niggleManagementCheck3)
                  }
                />
              </MDBox>
            </MDBox>

            {/* 2.Functional Movement */}
            <MDBox
              display="flex"
              flexDirection="row"
              mb={1}
              width="100%"
              sx={style}
            >
              <MDBox flexDirection="column" display="flex" width="30%">
                2.Functional Movement
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={functionalMovement}
                    onChange={(e) => setFunctionalMovement(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={functionalMovementCheck1}
                  onChange={() =>
                    setFunctionalMovementCheck1(!functionalMovementCheck1)
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={functionalMovementCheck2}
                  onChange={() =>
                    setFunctionalMovementCheck2(!functionalMovementCheck2)
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={functionalMovementCheck3}
                  onChange={() =>
                    setFunctionalMovementCheck3(!functionalMovementCheck3)
                  }
                />
              </MDBox>
            </MDBox>

            {/* 3.Core Endurance */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                3.Core Endurance
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={coreEndurance}
                    onChange={(e) => setCoreEndurance(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={coreEnduranceCheck1}
                  onChange={() => setCoreEnduranceCheck1(!coreEnduranceCheck1)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={coreEnduranceCheck2}
                  onChange={() => setCoreEnduranceCheck2(!coreEnduranceCheck2)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={coreEnduranceCheck3}
                  onChange={() => setCoreEnduranceCheck3(!coreEnduranceCheck3)}
                />
              </MDBox>
            </MDBox>

            {/* 4.Reaction Time */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                4.Reaction Time
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={reactionTime}
                    onChange={(e) => setReactionTime(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={reactionTimeCheck1}
                  onChange={() => setReactionTimeCheck1(!reactionTimeCheck1)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={reactionTimeCheck2}
                  onChange={() => setReactionTimeCheck2(!reactionTimeCheck2)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={reactionTimeCheck3}
                  onChange={() => setReactionTimeCheck3(!reactionTimeCheck3)}
                />
              </MDBox>
            </MDBox>

            {/* 5.Muscular Endurance (lower) */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                5.Muscular Endurance (lower)
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={muscularEnduranceLower}
                    onChange={(e) => setMuscularEnduranceLower(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={muscularEnduranceLowerCheck1}
                  onChange={() =>
                    setMuscularEnduranceLowerCheck1(
                      !muscularEnduranceLowerCheck1
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={muscularEnduranceLowerCheck2}
                  onChange={() =>
                    setMuscularEnduranceLowerCheck2(
                      !muscularEnduranceLowerCheck2
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={muscularEnduranceLowerCheck3}
                  onChange={() =>
                    setMuscularEnduranceLowerCheck3(
                      !muscularEnduranceLowerCheck3
                    )
                  }
                />
              </MDBox>
            </MDBox>

            {/* 6.Muscular Endurance (upper) */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                6.Muscular Endurance (upper)
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={muscularEnduranceUpper}
                    onChange={(e) => setMuscularEnduranceUpper(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={muscularEnduranceUpperCheck1}
                  onChange={() =>
                    setMuscularEnduranceUpperCheck1(
                      !muscularEnduranceUpperCheck1
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={muscularEnduranceUpperCheck2}
                  onChange={() =>
                    setMuscularEnduranceUpperCheck2(
                      !muscularEnduranceUpperCheck2
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={muscularEnduranceUpperCheck3}
                  onChange={() =>
                    setMuscularEnduranceUpperCheck3(
                      !muscularEnduranceUpperCheck3
                    )
                  }
                />
              </MDBox>
            </MDBox>

            {/* 7.Speed */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                7.Speed
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={speed}
                    onChange={(e) => setSpeed(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={speedCheck1}
                  onChange={() => setSpeedCheck1(!speedCheck1)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={speedCheck2}
                  onChange={() => setSpeedCheck2(!speedCheck2)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={speedCheck3}
                  onChange={() => setSpeedCheck3(!speedCheck3)}
                />
              </MDBox>
            </MDBox>

            {/* 8.Agility */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                8.Agility
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={agility}
                    onChange={(e) => setAgility(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={agilityCheck1}
                  onChange={() => setAgilityCheck1(!agilityCheck1)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={agilityCheck2}
                  onChange={() => setAgilityCheck2(!agilityCheck2)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={agilityCheck3}
                  onChange={() => setAgilityCheck3(!agilityCheck3)}
                />
              </MDBox>
            </MDBox>

            {/* 9.Balance */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                9.Balance
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={balance}
                    onChange={(e) => setBalance(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={balanceCheck1}
                  onChange={() => setBalanceCheck1(!balanceCheck1)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={balanceCheck2}
                  onChange={() => setBalanceCheck2(!balanceCheck2)}
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={balanceCheck3}
                  onChange={() => setBalanceCheck3(!balanceCheck3)}
                />
              </MDBox>
            </MDBox>

            {/* 10.Anaerobic Fitness */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                10.Anaerobic Fitness
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={anaerobicFitness}
                    onChange={(e) => setAnaerobicFitness(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={anaerobicFitnessCheck1}
                  onChange={() =>
                    setAnaerobicFitnessCheck1(!anaerobicFitnessCheck1)
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={anaerobicFitnessCheck2}
                  onChange={() =>
                    setAnaerobicFitnessCheck2(!anaerobicFitnessCheck2)
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={anaerobicFitnessCheck3}
                  onChange={() =>
                    setAnaerobicFitnessCheck3(!anaerobicFitnessCheck3)
                  }
                />
              </MDBox>
            </MDBox>

            {/* 11.Cardiovascular Endurance */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                11.Cardiovascular Endurance
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={cardiovascularEndurance}
                    onChange={(e) => setCardiovascularEndurance(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={cardiovascularEnduranceCheck1}
                  onChange={() =>
                    setCardiovascularEnduranceCheck1(
                      !cardiovascularEnduranceCheck1
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={cardiovascularEnduranceCheck2}
                  onChange={() =>
                    setCardiovascularEnduranceCheck2(
                      !cardiovascularEnduranceCheck2
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={cardiovascularEnduranceCheck3}
                  onChange={() =>
                    setCardiovascularEnduranceCheck3(
                      !cardiovascularEnduranceCheck3
                    )
                  }
                />
              </MDBox>
            </MDBox>

            {/* 12.Explosive Power (lower body) */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                12.Explosive Power (lower body)
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={explosivePowerLowerBody}
                    onChange={(e) => setExplosivePowerLowerBody(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={explosivePowerLowerBodyCheck1}
                  onChange={() =>
                    setExplosivePowerLowerBodyCheck1(
                      !explosivePowerLowerBodyCheck1
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={explosivePowerLowerBodyCheck2}
                  onChange={() =>
                    setExplosivePowerLowerBodyCheck2(
                      !explosivePowerLowerBodyCheck2
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={explosivePowerLowerBodyCheck3}
                  onChange={() =>
                    setExplosivePowerLowerBodyCheck3(
                      !explosivePowerLowerBodyCheck3
                    )
                  }
                />
              </MDBox>
            </MDBox>

            {/* 13.Explosive Power (upper body) */}
            <MDBox display="flex" flexDirection="row" mb={1} width="100%">
              <MDBox flexDirection="column" display="flex" width="30%">
                13.Explosive Power (upper body)
              </MDBox>
              <MDBox
                flexDirection="column"
                display="flex"
                pt={1}
                width="20%"
                p={1}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Select Priority
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Select Priority"
                    style={{ ...height45 }}
                    value={explosivePowerUpperBody}
                    onChange={(e) => setExplosivePowerUpperBody(e.target.value)}
                  >
                    <MenuItem value={"low"}>Low</MenuItem>
                    <MenuItem value={"medium"}>Medium</MenuItem>
                    <MenuItem value={"high"}>High</MenuItem>
                  </Select>
                </FormControl>
              </MDBox>

              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_needs_improvement"
                  checked={explosivePowerUpperBodyCheck1}
                  onChange={() =>
                    setExplosivePowerUpperBodyCheck1(
                      !explosivePowerUpperBodyCheck1
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="20%">
                <Checkbox
                  name="injury_niggle_management_at_par"
                  checked={explosivePowerUpperBodyCheck2}
                  onChange={() =>
                    setExplosivePowerUpperBodyCheck2(
                      !explosivePowerUpperBodyCheck2
                    )
                  }
                />
              </MDBox>
              <MDBox flexDirection="column" display="flex" pt={1} width="10%">
                <Checkbox
                  name="injury_niggle_management_good"
                  checked={explosivePowerUpperBodyCheck3}
                  onChange={() =>
                    setExplosivePowerUpperBodyCheck3(
                      !explosivePowerUpperBodyCheck3
                    )
                  }
                />
              </MDBox>
            </MDBox>

            <MDBox display="flex" flexDirection="row" justifyContent="flex-end">
              <MDBox
                mt={4}
                mr={3}
                flexDirection="column"
                display="flex"
                justifyContent="end"
              >
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step4/${"edit-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step4/${"back-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )}
              </MDBox>
              <MDBox
                mt={2}
                flexDirection="column"
                display="flex"
                justifyContent="flex-end"
              >
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updatePfaStep}
                  >
                    Update
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={createPfaUser}
                  >
                    Save / Next
                  </MDButton>
                )}
              </MDBox>
            </MDBox>

            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step5/Step 5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default PfaStep5;
