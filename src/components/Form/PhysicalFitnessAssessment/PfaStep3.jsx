import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { TextareaAutosize } from "@mui/material";
import { toast } from "react-toastify";
import "../../../style/loader.css";
import {
  createPfaProtocol,
  createPfaStepThree,
  getPfaInstruction,
  getPfaStepDetail,
  getPfaStepThreeData,
  updatePfaStepThree,
} from "../../../services/service_api";

const style = {
  height: "100%",
};

function PfaStep3() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editPfa = route.pathname?.split("/")?.slice(3, 4) == "edit-pfa";
  const backPfa = route.pathname?.split("/")?.slice(3, 4) == "back-pfa";
  // console.log("editPfa===>", editPfa, userId, id);
  const [updateId, setUpdateId] = useState("");

  const flexibilityRulesData =
    "0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds. Sprint & Agility Tests 0m Sprint 1.The athlete was required to sprint 10m. 2.Timings for 5m and 10m were recorded. Pro Agility 1.This tests the ability of the athlete to change direction linearly at high speeds.";
  // 8.Star Excursion Balance Test
  const [starExcursionRight1, setStarExcursionRight1] = useState("");
  const [starExcursionRight2, setStarExcursionRight2] = useState("");
  const [starExcursionRight3, setStarExcursionRight3] = useState("");
  const [starExcursionLeft1, setStarExcursionLeft1] = useState("");
  const [starExcursionLeft2, setStarExcursionLeft2] = useState("");
  const [starExcursionLeft3, setStarExcursionLeft3] = useState("");
  const [starExcursionRules, setStarExcursionRules] = useState("");

  // 9.Reaction Time Test
  const [averageReactionTime1, setAverageReactionTime1] = useState("");
  const [averageReactionTime2, setAverageReactionTime2] = useState("");
  const [averageReactionTime3, setAverageReactionTime3] = useState("");
  const [reactionRules, setReactionRules] = useState(flexibilityRulesData);

  // 10.Reaction Agility Test
  const [averageReactionAgility1, setAverageReactionAgility1] = useState("");
  const [averageReactionAgility2, setAverageReactionAgility2] = useState("");
  const [averageReactionAgility3, setAverageReactionAgility3] = useState("");
  const [attempt2ReactionAgility1, setAttempt2ReactionAgility1] = useState("");
  const [attempt2ReactionAgility2, setAttempt2ReactionAgility2] = useState("");
  const [attempt2ReactionAgility3, setAttempt2ReactionAgility3] = useState("");
  const [attempt2ReactionAgilityRules, setAttempt2ReactionAgilityRules] =
    useState("");

  //11.Sports Specific Reaction Test
  const [sportsSpecificTime1, setSportsSpecificTime1] = useState("");
  const [sportsSpecificTime2, setSportsSpecificTime2] = useState("");
  const [sportsSpecificTime3, setSportsSpecificTime3] = useState("");
  const [sportsSpecificTimeRules, setSportsSpecificTimeRules] = useState("");

  //12.Upper Body Explosive Power
  const [distance1m1, setDistance1m1] = useState("");
  const [distance1m2, setDistance1m2] = useState("");
  const [distance1m3, setDistance1m3] = useState("");
  const [distance2m1, setDistance2m1] = useState("");
  const [distance2m2, setDistance2m2] = useState("");
  const [distance2m3, setDistance2m3] = useState("");
  const [upperBodyRules, setUpperBodyRules] = useState("");

  // 13.
  const [benchPress30, setBenchPress30] = useState("30%=");
  const [benchPress40, setBenchPress40] = useState("40%=");
  const [benchPress50, setBenchPress50] = useState("50%=");
  const [benchBarbell30, setBenchBarbell30] = useState("");
  const [benchBarbell40, setBenchBarbell40] = useState("");
  const [benchBarbell50, setBenchBarbell50] = useState("");
  const [rightSidePeakPower1, setRightSidePeakPower1] = useState("");
  const [rightSidePeakPower2, setRightSidePeakPower2] = useState("");
  const [rightSidePeakPower3, setRightSidePeakPower3] = useState("");
  const [leftSidePeakPower1, setLeftSidePeakPower1] = useState("");
  const [leftSidePeakPower2, setLeftSidePeakPower2] = useState("");
  const [leftSidePeakPower3, setLeftSidePeakPower3] = useState("");
  const [upperBodyExplosiveRules, setUpperBodyExplosiveRules] = useState("");

  // 14.Rotational Power
  const [rotationalPowerRight1, setRotationalPowerRight1] = useState("");
  const [rotationalPowerRight2, setRotationalPowerRight2] = useState("");
  const [rotationalPowerRight3, setRotationalPowerRight3] = useState("");
  const [rotationalPowerLeft1, setRotationalPowerLeft1] = useState("");
  const [rotationalPowerLeft2, setRotationalPowerLeft2] = useState("");
  const [rotationalPowerLeft3, setRotationalPowerLeft3] = useState("");
  const [rotationalPowerRules, setRotationalPowerRules] = useState("");

  const [loading, setLoading] = useState(false);
  const [stepDetail, setStepDetail] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    getPfaFormData();
    if (editPfa) {
      getPfaFormData();
    } else {
      getPfaInstructionData();
    }
  }, [editPfa]);

  React.useEffect(() => {
    handleUserSteps();
    getPfaFormData();
    if (backPfa) {
      getPfaFormData();
    } else {
      getPfaInstructionData();
    }
  }, [backPfa]);

  async function handleUserSteps() {
    // console.log("getPfaStepDetail");
    setLoading(true);
    const params = {
      registration_no: userId,
      id: id,
    };
    const response = await getPfaStepDetail(params);
    const data = response.data;
    // console.log("getPfaStepDetail response.data", data);
    if (response) {
      setStepDetail(data?.step_three_data);
    }
  }

  async function getPfaInstructionData() {
    setLoading(true);
    const response = await getPfaInstruction();
    const data = response.data[0];
    if (response) {
      setReactionRules(data.star_excursion_balance_test);
      setAttempt2ReactionAgilityRules(data.reaction_time_test);
      setSportsSpecificTimeRules(data.reactive_agility_test);
      setUpperBodyRules(data.sports_specific_reaction_test);
      setUpperBodyExplosiveRules(data.upper_body_explosive_power);
      setUpperBodyRules(data.upper_body_explosive_power_two);
      // console.log("response", data);
      setLoading(false);
    }
  }

  async function getPfaFormData() {
    setLoading(true);
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
    };
    // console.log("params=====>", params);
    const response = await getPfaStepThreeData(params);
    const data = response.data;
    // console.log("response-------->", response?.data);
    if (response) {
      setUpdateId(data?.id);
      setStarExcursionRight1(data?.starexcursionbalancetest_anterior_right);
      setStarExcursionRight2(
        data?.starexcursionbalancetest_posterior_medial_right
      );
      setStarExcursionRight3(
        data?.starexcursionbalancetest_posterior_lateral_right
      );
      setStarExcursionLeft1(data?.starexcursionbalancetest_anterior_left);
      setStarExcursionLeft2(
        data?.starexcursionbalancetest_posterior_medial_left
      );
      setStarExcursionLeft3(
        data?.starexcursionbalancetest_posterior_lateral_left
      );
      setStarExcursionRules(data?.star_excursion_balance_test);
      setAverageReactionTime1(
        data?.reaction_time_test_average_reaction_time_sec_1
      );
      setAverageReactionTime2(
        data?.reaction_time_test_average_reaction_time_sec_2
      );
      setAverageReactionTime3(
        data?.reaction_time_test_average_reaction_time_sec_3
      );
      setReactionRules(data?.reaction_time_test);
      setAverageReactionAgility1(
        data?.reactive_time_test_average_reaction_time_sec_attempt_1
      );
      setAverageReactionAgility2(
        data?.reactive_time_test_average_reaction_time_sec_attempt_2
      );
      setAverageReactionAgility3(
        data?.reactive_time_test_decisionerrors_attempt_1
      );
      setAttempt2ReactionAgility1(
        data?.reactive_time_test_decisionerrors_attempt_2
      );
      setAttempt2ReactionAgility2(data?.reactive_time_test_misses_attempt_1);
      setAttempt2ReactionAgility3(data?.reactive_time_test_misses_attempt_2);
      setAttempt2ReactionAgilityRules(data?.reactive_agility_test);
      setSportsSpecificTime1(
        data?.sportsspecificreactiontest_awg_reaction_time_sec_1
      );
      setSportsSpecificTime2(
        data?.sportsspecificreactiontest_awg_reaction_time_sec_2
      );
      setSportsSpecificTime3(
        data?.sportsspecificreactiontest_awg_reaction_time_sec_3
      );
      setSportsSpecificTimeRules(data?.sports_specific_reaction_test);
      setDistance1m1(data?.upper_body_explosive_power_distance_1kg_m_1);
      setDistance1m2(data?.upper_body_explosive_power_distance_1kg_m_2);
      setDistance1m3(data?.upper_body_explosive_power_distance_1kg_m_3);
      setDistance2m1(data?.upper_body_explosive_power_distance_2kg_m_1);
      setDistance2m2(data?.upper_body_explosive_power_distance_2kg_m_2);
      setDistance2m3(data?.upper_body_explosive_power_distance_2kg_m_3);
      setUpperBodyRules(data?.upper_body_explosive_power);
      setBenchPress30(data?.upperbodypower_1rmbench_press_30);
      setBenchPress40(data?.upperbodypower_1rmbench_press_40);
      setBenchPress50(data?.upperbodypower_1rmbench_press_50);
      setBenchBarbell30(data?.upperbodypower_barbell_sensei_w30);
      setBenchBarbell40(data?.upperbodypower_barbell_sensei_w40);
      setBenchBarbell50(data?.upperbodypower_barbell_sensei_w50);
      setRightSidePeakPower1(
        data?.upperbody_rotationalpower_rightside_peakpower_w1
      );
      setRightSidePeakPower2(
        data?.upperbody_rotationalpower_rightside_peakpower_w2
      );
      setRightSidePeakPower3(
        data?.upperbody_rotationalpower_rightside_peakpower_w3
      );
      setLeftSidePeakPower1(
        data?.upperbody_rotationalpower_leftside_peakpower_w1
      );
      setLeftSidePeakPower2(
        data?.upperbody_rotationalpower_leftside_peakpower_w2
      );
      setLeftSidePeakPower3(
        data?.upperbody_rotationalpower_leftside_peakpower_w3
      );
      setUpperBodyExplosiveRules(data?.upper_body_explosive_power_two);
      setRotationalPowerRight1(data?.rotationalpower_right_w1);
      setRotationalPowerRight2(data?.rotationalpower_right_w2);
      setRotationalPowerRight3(data?.rotationalpower_right_w3);
      setRotationalPowerLeft1(data?.rotationalpower_left_w1);
      setRotationalPowerLeft2(data?.rotationalpower_left_w2);
      setRotationalPowerLeft3(data?.rotationalpower_left_w3);
      setRotationalPowerRules(data?.rotational_power);
      setLoading(false);
    }
  }

  async function updatePfaStep() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,

      starexcursionbalancetest_anterior_right: starExcursionRight1,
      starexcursionbalancetest_posterior_medial_right: starExcursionRight2,
      starexcursionbalancetest_posterior_lateral_right: starExcursionRight3,
      starexcursionbalancetest_anterior_left: starExcursionLeft1,
      starexcursionbalancetest_posterior_medial_left: starExcursionLeft2,
      starexcursionbalancetest_posterior_lateral_left: starExcursionLeft3,
      star_excursion_balance_test: starExcursionRules,

      reaction_time_test_average_reaction_time_sec_1: averageReactionTime1,
      reaction_time_test_average_reaction_time_sec_2: averageReactionTime2,
      reaction_time_test_average_reaction_time_sec_3: averageReactionTime3,
      reaction_time_test: reactionRules,

      reactive_time_test_average_reaction_time_sec_attempt_1:
        averageReactionAgility1,
      reactive_time_test_average_reaction_time_sec_attempt_2:
        averageReactionAgility2,
      reactive_time_test_decisionerrors_attempt_1: averageReactionAgility3,
      reactive_time_test_decisionerrors_attempt_2: attempt2ReactionAgility1,
      reactive_time_test_misses_attempt_1: attempt2ReactionAgility2,
      reactive_time_test_misses_attempt_2: attempt2ReactionAgility3,
      reactive_agility_test: attempt2ReactionAgilityRules,

      sportsspecificreactiontest_awg_reaction_time_sec_1: sportsSpecificTime1,
      sportsspecificreactiontest_awg_reaction_time_sec_2: sportsSpecificTime2,
      sportsspecificreactiontest_awg_reaction_time_sec_3: sportsSpecificTime3,
      sports_specific_reaction_test: sportsSpecificTimeRules,

      upper_body_explosive_power_distance_1kg_m_1: distance1m1,
      upper_body_explosive_power_distance_1kg_m_2: distance1m2,
      upper_body_explosive_power_distance_1kg_m_3: distance1m3,
      upper_body_explosive_power_distance_2kg_m_1: distance2m1,
      upper_body_explosive_power_distance_2kg_m_2: distance2m2,
      upper_body_explosive_power_distance_2kg_m_3: distance2m3,
      upper_body_explosive_power: upperBodyRules,

      upperbodypower_1rmbench_press_30: benchPress30,
      upperbodypower_1rmbench_press_40: benchPress40,
      upperbodypower_1rmbench_press_50: benchPress50,
      upperbodypower_barbell_sensei_w30: benchBarbell30,
      upperbodypower_barbell_sensei_w40: benchBarbell40,
      upperbodypower_barbell_sensei_w50: benchBarbell50,
      upperbody_rotationalpower_rightside_peakpower_w1: rightSidePeakPower1,
      upperbody_rotationalpower_rightside_peakpower_w2: rightSidePeakPower2,
      upperbody_rotationalpower_rightside_peakpower_w3: rightSidePeakPower3,
      upperbody_rotationalpower_leftside_peakpower_w1: leftSidePeakPower1,
      upperbody_rotationalpower_leftside_peakpower_w2: leftSidePeakPower2,
      upperbody_rotationalpower_leftside_peakpower_w3: leftSidePeakPower3,
      upper_body_explosive_power_two: upperBodyExplosiveRules,

      rotationalpower_right_w1: rotationalPowerRight1,
      rotationalpower_right_w2: rotationalPowerRight2,
      rotationalpower_right_w3: rotationalPowerRight3,
      rotationalpower_left_w1: rotationalPowerLeft1,
      rotationalpower_left_w2: rotationalPowerLeft2,
      rotationalpower_left_w3: rotationalPowerLeft3,
      rotational_power: rotationalPowerRules,
    };
    // console.log("params===>", params);
    const response = await updatePfaStepThree(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(
        (backPfa && `/forms/step4/${userId}/${id}`) ||
          `/forms/step4/${"edit-pfa"}/${userId}/${id}`
      );
      setStarExcursionRight1("");
      setStarExcursionRight2("");
      setStarExcursionRight3("");
      setStarExcursionLeft1("");
      setStarExcursionLeft2("");
      setStarExcursionLeft3("");
      setAverageReactionTime1("");
      setAverageReactionTime2("");
      setAverageReactionTime3("");
      setReactionRules("");
      setAverageReactionTime1("");
      setAverageReactionTime2("");
      setAverageReactionTime3("");
      setReactionRules("");
      setAverageReactionAgility1("");
      setAverageReactionAgility2("");
      setAverageReactionAgility3("");
      setAttempt2ReactionAgility1("");
      setAttempt2ReactionAgility2("");
      setAttempt2ReactionAgility3("");
      setAttempt2ReactionAgilityRules("");
      setSportsSpecificTime1("");
      setSportsSpecificTime2("");
      setSportsSpecificTime3("");
      setSportsSpecificTimeRules("");
      setDistance1m1("");
      setDistance1m2("");
      setDistance1m3("");
      setDistance2m1("");
      setDistance2m2("");
      setDistance2m3("");
      setUpperBodyRules("");
      setBenchPress30("");
      setBenchPress40("");
      setBenchPress50("");
      setRightSidePeakPower1("");
      setRightSidePeakPower2("");
      setRightSidePeakPower3("");
      setLeftSidePeakPower1("");
      setLeftSidePeakPower2("");
      setLeftSidePeakPower3("");
      setUpperBodyExplosiveRules("");
      setRotationalPowerRight1("");
      setRotationalPowerRight2("");
      setRotationalPowerRight3("");
      setRotationalPowerLeft1("");
      setRotationalPowerLeft2("");
      setRotationalPowerLeft3("");
      setRotationalPowerRules("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  async function handleUpdatePfaStep() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,
      id: updateId,

      starexcursionbalancetest_anterior_right: starExcursionRight1,
      starexcursionbalancetest_posterior_medial_right: starExcursionRight2,
      starexcursionbalancetest_posterior_lateral_right: starExcursionRight3,
      starexcursionbalancetest_anterior_left: starExcursionLeft1,
      starexcursionbalancetest_posterior_medial_left: starExcursionLeft2,
      starexcursionbalancetest_posterior_lateral_left: starExcursionLeft3,
      star_excursion_balance_test: starExcursionRules,

      reaction_time_test_average_reaction_time_sec_1: averageReactionTime1,
      reaction_time_test_average_reaction_time_sec_2: averageReactionTime2,
      reaction_time_test_average_reaction_time_sec_3: averageReactionTime3,
      reaction_time_test: reactionRules,

      reactive_time_test_average_reaction_time_sec_attempt_1:
        averageReactionAgility1,
      reactive_time_test_average_reaction_time_sec_attempt_2:
        averageReactionAgility2,
      reactive_time_test_decisionerrors_attempt_1: averageReactionAgility3,
      reactive_time_test_decisionerrors_attempt_2: attempt2ReactionAgility1,
      reactive_time_test_misses_attempt_1: attempt2ReactionAgility2,
      reactive_time_test_misses_attempt_2: attempt2ReactionAgility3,
      reactive_agility_test: attempt2ReactionAgilityRules,

      sportsspecificreactiontest_awg_reaction_time_sec_1: sportsSpecificTime1,
      sportsspecificreactiontest_awg_reaction_time_sec_2: sportsSpecificTime2,
      sportsspecificreactiontest_awg_reaction_time_sec_3: sportsSpecificTime3,
      sports_specific_reaction_test: sportsSpecificTimeRules,

      upper_body_explosive_power_distance_1kg_m_1: distance1m1,
      upper_body_explosive_power_distance_1kg_m_2: distance1m2,
      upper_body_explosive_power_distance_1kg_m_3: distance1m3,
      upper_body_explosive_power_distance_2kg_m_1: distance2m1,
      upper_body_explosive_power_distance_2kg_m_2: distance2m2,
      upper_body_explosive_power_distance_2kg_m_3: distance2m3,
      upper_body_explosive_power: upperBodyRules,

      upperbodypower_1rmbench_press_30: benchPress30,
      upperbodypower_1rmbench_press_40: benchPress40,
      upperbodypower_1rmbench_press_50: benchPress50,
      upperbodypower_barbell_sensei_w30: benchBarbell30,
      upperbodypower_barbell_sensei_w40: benchBarbell40,
      upperbodypower_barbell_sensei_w50: benchBarbell50,
      upperbody_rotationalpower_rightside_peakpower_w1: rightSidePeakPower1,
      upperbody_rotationalpower_rightside_peakpower_w2: rightSidePeakPower2,
      upperbody_rotationalpower_rightside_peakpower_w3: rightSidePeakPower3,
      upperbody_rotationalpower_leftside_peakpower_w1: leftSidePeakPower1,
      upperbody_rotationalpower_leftside_peakpower_w2: leftSidePeakPower2,
      upperbody_rotationalpower_leftside_peakpower_w3: leftSidePeakPower3,
      upper_body_explosive_power_two: upperBodyExplosiveRules,

      rotationalpower_right_w1: rotationalPowerRight1,
      rotationalpower_right_w2: rotationalPowerRight2,
      rotationalpower_right_w3: rotationalPowerRight3,
      rotationalpower_left_w1: rotationalPowerLeft1,
      rotationalpower_left_w2: rotationalPowerLeft2,
      rotationalpower_left_w3: rotationalPowerLeft3,
      rotational_power: rotationalPowerRules,
    };
    // console.log("params===>", params);
    const response = await updatePfaStepThree(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/forms/step4/${userId}/${id}`);
      setStarExcursionRight1("");
      setStarExcursionRight2("");
      setStarExcursionRight3("");
      setStarExcursionLeft1("");
      setStarExcursionLeft2("");
      setStarExcursionLeft3("");
      setAverageReactionTime1("");
      setAverageReactionTime2("");
      setAverageReactionTime3("");
      setReactionRules("");
      setAverageReactionTime1("");
      setAverageReactionTime2("");
      setAverageReactionTime3("");
      setReactionRules("");
      setAverageReactionAgility1("");
      setAverageReactionAgility2("");
      setAverageReactionAgility3("");
      setAttempt2ReactionAgility1("");
      setAttempt2ReactionAgility2("");
      setAttempt2ReactionAgility3("");
      setAttempt2ReactionAgilityRules("");
      setSportsSpecificTime1("");
      setSportsSpecificTime2("");
      setSportsSpecificTime3("");
      setSportsSpecificTimeRules("");
      setDistance1m1("");
      setDistance1m2("");
      setDistance1m3("");
      setDistance2m1("");
      setDistance2m2("");
      setDistance2m3("");
      setUpperBodyRules("");
      setBenchPress30("");
      setBenchPress40("");
      setBenchPress50("");
      setRightSidePeakPower1("");
      setRightSidePeakPower2("");
      setRightSidePeakPower3("");
      setLeftSidePeakPower1("");
      setLeftSidePeakPower2("");
      setLeftSidePeakPower3("");
      setUpperBodyExplosiveRules("");
      setRotationalPowerRight1("");
      setRotationalPowerRight2("");
      setRotationalPowerRight3("");
      setRotationalPowerLeft1("");
      setRotationalPowerLeft2("");
      setRotationalPowerLeft3("");
      setRotationalPowerRules("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  async function createPfaUser() {
    const params = {
      registration_no: userId,
      physical_fitness_assessment_id: id,

      star_excursion_balance_test: starExcursionRules,
      starexcursionbalancetest_anterior_right: starExcursionRight1,
      starexcursionbalancetest_posterior_medial_right: starExcursionRight2,
      starexcursionbalancetest_posterior_lateral_right: starExcursionRight3,
      starexcursionbalancetest_anterior_left: starExcursionLeft1,
      starexcursionbalancetest_posterior_medial_left: starExcursionLeft2,
      starexcursionbalancetest_posterior_lateral_left: starExcursionLeft3,

      reaction_time_test_average_reaction_time_sec_1: averageReactionTime1,
      reaction_time_test_average_reaction_time_sec_2: averageReactionTime2,
      reaction_time_test_average_reaction_time_sec_3: averageReactionTime3,
      reaction_time_test: reactionRules,

      reactive_time_test_average_reaction_time_sec_attempt_1:
        averageReactionAgility1,
      reactive_time_test_average_reaction_time_sec_attempt_2:
        averageReactionAgility2,
      reactive_time_test_decisionerrors_attempt_1: averageReactionAgility3,
      reactive_time_test_decisionerrors_attempt_2: attempt2ReactionAgility1,
      reactive_time_test_misses_attempt_1: attempt2ReactionAgility2,
      reactive_time_test_misses_attempt_2: attempt2ReactionAgility3,
      reactive_agility_test: attempt2ReactionAgilityRules,

      sportsspecificreactiontest_awg_reaction_time_sec_1: sportsSpecificTime1,
      sportsspecificreactiontest_awg_reaction_time_sec_2: sportsSpecificTime2,
      sportsspecificreactiontest_awg_reaction_time_sec_3: sportsSpecificTime3,
      sports_specific_reaction_test: sportsSpecificTimeRules,

      upper_body_explosive_power_distance_1kg_m_1: distance1m1,
      upper_body_explosive_power_distance_1kg_m_2: distance1m2,
      upper_body_explosive_power_distance_1kg_m_3: distance1m3,
      upper_body_explosive_power_distance_2kg_m_1: distance2m1,
      upper_body_explosive_power_distance_2kg_m_2: distance2m2,
      upper_body_explosive_power_distance_2kg_m_3: distance2m3,
      upper_body_explosive_power: upperBodyRules,

      upperbodypower_1rmbench_press_30: benchPress30,
      upperbodypower_1rmbench_press_40: benchPress40,
      upperbodypower_1rmbench_press_50: benchPress50,
      upperbodypower_barbell_sensei_w30: benchBarbell30,
      upperbodypower_barbell_sensei_w40: benchBarbell40,
      upperbodypower_barbell_sensei_w50: benchBarbell50,
      upperbody_rotationalpower_rightside_peakpower_w1: rightSidePeakPower1,
      upperbody_rotationalpower_rightside_peakpower_w2: rightSidePeakPower2,
      upperbody_rotationalpower_rightside_peakpower_w3: rightSidePeakPower3,
      upperbody_rotationalpower_leftside_peakpower_w1: leftSidePeakPower1,
      upperbody_rotationalpower_leftside_peakpower_w2: leftSidePeakPower2,
      upperbody_rotationalpower_leftside_peakpower_w3: leftSidePeakPower3,
      upper_body_explosive_power_two: upperBodyExplosiveRules,

      rotationalpower_right_w1: rotationalPowerRight1,
      rotationalpower_right_w2: rotationalPowerRight2,
      rotationalpower_right_w3: rotationalPowerRight3,
      rotationalpower_left_w1: rotationalPowerLeft1,
      rotationalpower_left_w2: rotationalPowerLeft2,
      rotationalpower_left_w3: rotationalPowerLeft3,
      rotational_power: rotationalPowerRules,
    };
    // console.log("params===>", params);
    const response = await createPfaStepThree(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/forms/step4/${userId}/${id}`);
      setStarExcursionRight1("");
      setStarExcursionRight2("");
      setStarExcursionRight3("");
      setStarExcursionLeft1("");
      setStarExcursionLeft2("");
      setStarExcursionLeft3("");
      setAverageReactionTime1("");
      setAverageReactionTime2("");
      setAverageReactionTime3("");
      setReactionRules("");
      setAverageReactionTime1("");
      setAverageReactionTime2("");
      setAverageReactionTime3("");
      setReactionRules("");
      setAverageReactionAgility1("");
      setAverageReactionAgility2("");
      setAverageReactionAgility3("");
      setAttempt2ReactionAgility1("");
      setAttempt2ReactionAgility2("");
      setAttempt2ReactionAgility3("");
      setAttempt2ReactionAgilityRules("");
      setSportsSpecificTime1("");
      setSportsSpecificTime2("");
      setSportsSpecificTime3("");
      setSportsSpecificTimeRules("");
      setDistance1m1("");
      setDistance1m2("");
      setDistance1m3("");
      setDistance2m1("");
      setDistance2m2("");
      setDistance2m3("");
      setUpperBodyRules("");
      setBenchPress30("");
      setBenchPress40("");
      setBenchPress50("");
      setRightSidePeakPower1("");
      setRightSidePeakPower2("");
      setRightSidePeakPower3("");
      setLeftSidePeakPower1("");
      setLeftSidePeakPower2("");
      setLeftSidePeakPower3("");
      setUpperBodyExplosiveRules("");
      setRotationalPowerRight1("");
      setRotationalPowerRight2("");
      setRotationalPowerRight3("");
      setRotationalPowerLeft1("");
      setRotationalPowerLeft2("");
      setRotationalPowerLeft3("");
      setRotationalPowerRules("");
    } else {
      toast.error(response.message, toastConfig);
    }
  }

  // function formValidation() {
  //   let msgName = "";
  //   let msgAge = "";
  //   let msgWeight = "";
  //   let msgHeight = "";
  //   let msgRegistrationNo = "";
  //   let msgSport = "";
  //   let msgDate = "";
  //   let msgMfData = "";
  //   let isValid = false;

  //   if (name == "") {
  //     msgName = "Please enter name";
  //   }

  //   if (age == "") {
  //     msgAge = "Please enter age";
  //   }

  //   if (weight === "") {
  //     msgWeight = "Please enter weight";
  //   }

  //   if (height === "") {
  //     msgHeight = "Please enter height";
  //   }

  //   if (mfData === "") {
  //     msgMfData = "Please enter M/F";
  //   }

  //   if (registrationNo === "") {
  //     msgRegistrationNo = "Please enter registration number";
  //   }

  //   if (sport === "") {
  //     msgSport = "Please select a sport";
  //   }

  //   if (date === "") {
  //     msgDate = "Please select a date";
  //   }

  //   if (
  //     (!msgName,
  //     !msgAge,
  //     !msgWeight,
  //     !msgRegistrationNo,
  //     !msgSport,
  //     !msgDate,
  //     !msgHeight,
  //     !msgMfData)
  //   ) {
  //     isValid = true;
  //   }

  //   if (isValid) {
  //     setError(true);
  //     setErrorMessage({
  //       name: "",
  //       age: "",
  //       weight: "",
  //       registrationNo: "",
  //       sport: "",
  //       date: "",
  //       height: "",
  //       mfData: "",
  //     });
  //     return true;
  //   } else {
  //     setError(true);
  //     setErrorMessage({
  //       name: msgName,
  //       age: msgAge,
  //       weight: msgWeight,
  //       registrationNo: msgRegistrationNo,
  //       sport: msgSport,
  //       date: msgDate,
  //       height: msgHeight,
  //       mfData: msgMfData,
  //     });
  //     return false;
  //   }
  // }

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <Card pt={5} mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body2"
              color="text"
              ml={1}
              fontWeight="regular"
              fontSize={30}
            >
              PHYSICAL FITNESS ASSESSMENT
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            {/* 8.Star Excursion Balance Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                8.Star Excursion Balance Test
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Anterior
                  </MDTypography>
                </MDBox>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    posterior-medial
                  </MDTypography>
                </MDBox>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Posterior-lateral
                  </MDTypography>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      Right
                    </MDTypography>

                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="1"
                      value={starExcursionRight1}
                      id="starExcursionRight1"
                      onChange={(e) => setStarExcursionRight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["starExcursionRight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["starExcursionRight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="2"
                      value={starExcursionRight2}
                      id="starExcursionRight2"
                      onChange={(e) => setStarExcursionRight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["starExcursionRight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["starExcursionRight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="3"
                      value={starExcursionRight3}
                      id="starExcursionRight3"
                      onChange={(e) => setStarExcursionRight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["starExcursionRight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["starExcursionRight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      Left
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="1"
                      value={starExcursionLeft1}
                      id="starExcursionLeft1"
                      onChange={(e) => setStarExcursionLeft1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["starExcursionLeft1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["starExcursionLeft1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="2"
                      value={starExcursionLeft2}
                      id="starExcursionLeft2"
                      onChange={(e) => setStarExcursionLeft2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["starExcursionLeft2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["starExcursionLeft2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="3"
                      value={starExcursionLeft3}
                      id="starExcursionLeft3"
                      onChange={(e) => setStarExcursionLeft3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["starExcursionLeft3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["starExcursionLeft3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              {/* <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Sprint & Agility Tests"
                    value={starExcursionRules}
                    id="starExcursionRules"
                    onChange={(e) => setStarExcursionRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["starExcursionRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["starExcursionRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox> */}
            </MDBox>

            {/* 9.Reaction Time Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                9.Reaction Time Test
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Average Reaction Time (sec)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="Average Reaction Time (sec) 1"
                      value={averageReactionTime1}
                      id="averageReactionTime1"
                      onChange={(e) => setAverageReactionTime1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["averageReactionTime1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["averageReactionTime1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="Average Reaction Time (sec) 2"
                      value={averageReactionTime2}
                      id="averageReactionTime2"
                      onChange={(e) => setAverageReactionTime2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["averageReactionTime2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["averageReactionTime2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="Average Reaction Time (sec) 3"
                      value={averageReactionTime3}
                      id="averageReactionTime3"
                      onChange={(e) => setAverageReactionTime3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["averageReactionTime3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["averageReactionTime3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Reaction Time Test"
                    value={reactionRules}
                    id="reactionRules"
                    onChange={(e) => setReactionRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["reactionRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["reactionRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 10.Reactive Agility Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                10.Reactive Agility Test
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Average Reaction Time (sec)
                  </MDTypography>
                </MDBox>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Decision errors
                  </MDTypography>
                </MDBox>
                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Misses
                  </MDTypography>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      Attempt 1
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="Average Reaction Time (sec) 1"
                      value={averageReactionAgility1}
                      id="averageReactionAgility1"
                      onChange={(e) =>
                        setAverageReactionAgility1(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["averageReactionAgility1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["averageReactionAgility1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="Decision errors 1"
                      value={averageReactionAgility2}
                      id="averageReactionAgility2"
                      onChange={(e) =>
                        setAverageReactionAgility2(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["averageReactionAgility2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["averageReactionAgility2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="Misses 1"
                      value={averageReactionAgility3}
                      id="averageReactionAgility3"
                      onChange={(e) =>
                        setAverageReactionAgility3(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["averageReactionAgility3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["averageReactionAgility3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      Attempt 2
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="Average Reaction Time (sec) 2"
                      value={attempt2ReactionAgility1}
                      id="attempt2ReactionAgility1"
                      onChange={(e) =>
                        setAttempt2ReactionAgility1(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["attempt2ReactionAgility1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["attempt2ReactionAgility1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="Decision errors 2"
                      value={attempt2ReactionAgility2}
                      id="attempt2ReactionAgility2"
                      onChange={(e) =>
                        setAttempt2ReactionAgility2(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["attempt2ReactionAgility2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["attempt2ReactionAgility2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDTypography
                      variant="body2"
                      color="text"
                      ml={1}
                      fontWeight="regular"
                      fontSize={16}
                    >
                      &nbsp;
                    </MDTypography>
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="Misses 2"
                      value={attempt2ReactionAgility3}
                      id="attempt2ReactionAgility3"
                      onChange={(e) =>
                        setAttempt2ReactionAgility3(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["attempt2ReactionAgility3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["attempt2ReactionAgility3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Reactive Agility Test"
                    value={attempt2ReactionAgilityRules}
                    id="attempt2ReactionAgilityRules"
                    onChange={(e) =>
                      setAttempt2ReactionAgilityRules(e.target.value)
                    }
                  />
                </MDBox>
                {error && errorMessage["attempt2ReactionAgilityRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["attempt2ReactionAgilityRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 11.Sports Specific Reaction Test */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                11.Sports Specific Reaction Test
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Average Reaction Time (sec)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="Average Reaction Time (sec) 1"
                      value={sportsSpecificTime1}
                      id="sportsSpecificTime1"
                      onChange={(e) => setSportsSpecificTime1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sportsSpecificTime1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sportsSpecificTime1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="Average Reaction Time (sec) 2"
                      value={sportsSpecificTime2}
                      id="sportsSpecificTime2"
                      onChange={(e) => setSportsSpecificTime2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sportsSpecificTime2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sportsSpecificTime2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="Average Reaction Time (sec) 3"
                      value={sportsSpecificTime3}
                      id="sportsSpecificTime3"
                      onChange={(e) => setSportsSpecificTime3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["sportsSpecificTime3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sportsSpecificTime3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Sports Specific Reaction Test"
                    value={sportsSpecificTimeRules}
                    id="sportsSpecificTimeRules"
                    onChange={(e) => setSportsSpecificTimeRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sportsSpecificTimeRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sportsSpecificTimeRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 12.Upper Body Explosive Power */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                12.Upper Body Explosive Power
              </MDTypography>
              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Distance 1Kg (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_1"
                      placeholder="Distance (m) 1"
                      value={distance1m1}
                      id="distance1m1"
                      onChange={(e) => setDistance1m1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["distance1m1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["distance1m1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_2"
                      placeholder="Distance (m) 2"
                      value={distance1m2}
                      id="distance1m2"
                      onChange={(e) => setDistance1m2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["distance1m2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["distance1m2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_3"
                      placeholder="Distance (m) 3"
                      value={distance1m3}
                      id="distance1m3"
                      onChange={(e) => setDistance1m3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["distance1m3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["distance1m3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Distance 2kg (m)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_1"
                      placeholder="Distance (m) 1"
                      value={distance2m1}
                      id="distance2m1"
                      onChange={(e) => setDistance2m1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["distance2m1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["distance2m1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_2"
                      placeholder="Distance (m) 2"
                      value={distance2m2}
                      id="distance2m2"
                      onChange={(e) => setDistance2m2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["distance2m2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["distance2m2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_3"
                      placeholder="Distance (m) 3"
                      value={distance2m3}
                      id="distance2m3"
                      onChange={(e) => setDistance2m3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["distance2m3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["distance2m3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Upper Body Explosive Power"
                    value={upperBodyRules}
                    id="upperBodyRules"
                    onChange={(e) => setUpperBodyRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["upperBodyRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["upperBodyRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 13.Upper Body Explosive Power */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                13.Upper Body Explosive Power
              </MDTypography>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                1 RM Bench Press (kg)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mr={1}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    fontWeight="bold"
                    fontSize={16}
                  >
                    <MDBox
                      mb={1}
                      mr={0}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="reaction_time_test_average_reaction_time_sec_1"
                        placeholder="1"
                        value={benchPress30}
                        id="benchPress30"
                        onChange={(e) => setBenchPress30(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["benchPress30"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["benchPress30"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDTypography>
                </MDBox>

                <MDBox
                  mb={1}
                  mr={1}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    <MDBox
                      mb={1}
                      mr={0}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="reaction_time_test_average_reaction_time_sec_1"
                        placeholder="1"
                        value={benchPress40}
                        id="benchPress40"
                        onChange={(e) => setBenchPress40(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["benchPress40"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["benchPress40"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDTypography>
                </MDBox>

                <MDBox
                  mb={1}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  width="50%"
                >
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    <MDBox
                      mb={1}
                      mr={0}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="reaction_time_test_average_reaction_time_sec_1"
                        placeholder="1"
                        value={benchPress50}
                        id="benchPress50"
                        onChange={(e) => setBenchPress50(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["benchPress50"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["benchPress50"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDTypography>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="1"
                      value={benchBarbell30}
                      id="benchBarbell30"
                      onChange={(e) => setBenchBarbell30(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["benchBarbell30"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["benchBarbell30"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="2"
                      value={benchBarbell40}
                      id="benchBarbell40"
                      onChange={(e) => setBenchBarbell40(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["benchBarbell40"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["benchBarbell40"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="3"
                      value={benchBarbell50}
                      id="benchBarbell50"
                      onChange={(e) => setBenchBarbell50(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["benchBarbell50"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["benchBarbell50"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                Rotational Power
              </MDTypography>

              <MDBox
                mb={1}
                mr={2}
                flexDirection="column"
                display="flex"
                width="50%"
              >
                <MDTypography
                  variant="body2"
                  color="text"
                  ml={1}
                  fontWeight="bold"
                  fontSize={16}
                >
                  Right Side
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                  mt={1}
                >
                  Peak Power (W)
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="1"
                      value={rightSidePeakPower1}
                      id="rightSidePeakPower1"
                      onChange={(e) => setRightSidePeakPower1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rightSidePeakPower1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rightSidePeakPower1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="2"
                      value={rightSidePeakPower2}
                      id="rightSidePeakPower2"
                      onChange={(e) => setRightSidePeakPower2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rightSidePeakPower2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rightSidePeakPower2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="3"
                      value={rightSidePeakPower3}
                      id="rightSidePeakPower3"
                      onChange={(e) => setRightSidePeakPower3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rightSidePeakPower3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rightSidePeakPower3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={16}
              >
                Left Side
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                  mt={1}
                >
                  Peak Power (W)
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_1"
                      placeholder="1"
                      value={leftSidePeakPower1}
                      id="leftSidePeakPower1"
                      onChange={(e) => setLeftSidePeakPower1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["leftSidePeakPower1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["leftSidePeakPower1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_2"
                      placeholder="2"
                      value={leftSidePeakPower2}
                      id="leftSidePeakPower2"
                      onChange={(e) => setLeftSidePeakPower2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["leftSidePeakPower2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["leftSidePeakPower2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="reaction_time_test_average_reaction_time_sec_3"
                      placeholder="3"
                      value={leftSidePeakPower3}
                      id="leftSidePeakPower3"
                      onChange={(e) => setLeftSidePeakPower3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["leftSidePeakPower3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["leftSidePeakPower3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Upper Body Explosive Power"
                    value={upperBodyExplosiveRules}
                    id="upperBodyExplosiveRules"
                    onChange={(e) => setUpperBodyExplosiveRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["upperBodyExplosiveRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["upperBodyExplosiveRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            {/* 14.Rotational Power */}
            <MDBox mt={1} mb={0}>
              <MDTypography
                variant="body1"
                color="text"
                ml={1}
                fontWeight="bold"
                fontSize={18}
              >
                14.Rotational Power
              </MDTypography>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Right (W)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_1"
                      placeholder="1"
                      value={rotationalPowerRight1}
                      id="rotationalPowerRight1"
                      onChange={(e) => setRotationalPowerRight1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotationalPowerRight1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotationalPowerRight1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_2"
                      placeholder="2"
                      value={rotationalPowerRight2}
                      id="rotationalPowerRight2"
                      onChange={(e) => setRotationalPowerRight2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotationalPowerRight2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotationalPowerRight2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_3"
                      placeholder="3"
                      value={rotationalPowerRight3}
                      id="rotationalPowerRight3"
                      onChange={(e) => setRotationalPowerRight3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotationalPowerRight3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotationalPowerRight3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDTypography
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
                fontSize={16}
              >
                Left (W)
              </MDTypography>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_1"
                      placeholder="1"
                      value={rotationalPowerLeft1}
                      id="rotationalPowerLeft1"
                      onChange={(e) => setRotationalPowerLeft1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotationalPowerLeft1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotationalPowerLeft1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_2"
                      placeholder="2"
                      value={rotationalPowerLeft2}
                      id="rotationalPowerLeft2"
                      onChange={(e) => setRotationalPowerLeft2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotationalPowerLeft2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotationalPowerLeft2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>

                <MDTypography
                  flexDirection="column"
                  mr={1}
                  variant="body2"
                  width="100%"
                  color="text"
                  ml={0}
                  fontWeight="regular"
                >
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="upper_body_explosive_power_distance_m_3"
                      placeholder="3"
                      value={rotationalPowerLeft3}
                      id="rotationalPowerLeft3"
                      onChange={(e) => setRotationalPowerLeft3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotationalPowerLeft3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotationalPowerLeft3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDTypography>
              </MDBox>

              <MDBox display="flex" flexDirection="row" ml={1}>
                <MDBox
                  mb={1}
                  mt={2}
                  mr={2}
                  flexDirection="column"
                  display="flex"
                  height="150%"
                  width="100%"
                >
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={3}
                    placeholder="Rotational Power"
                    value={upperBodyRules}
                    id="upperBodyRules"
                    onChange={(e) => setUpperBodyRules(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["upperBodyRules"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["upperBodyRules"]}
                  </div>
                ) : (
                  ""
                )}
              </MDBox>
            </MDBox>

            <MDBox display="flex" flexDirection="row" justifyContent="flex-end">
              <MDBox mt={2} mr={3} flexDirection="column" display="flex">
                {" "}
                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step2/${"edit-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={() =>
                      navigate(`/forms/step2/${"back-pfa"}/${userId}/${id}`)
                    }
                  >
                    Back
                  </MDButton>
                )}
              </MDBox>
              <MDBox mt={2} flexDirection="column" display="flex">
                {/* <Link to={"/forms/step4"}> */}

                {(editPfa && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updatePfaStep}
                  >
                    Update / Next
                  </MDButton>
                )) ||
                  (backPfa && (
                    <MDButton
                      variant="gradient"
                      color="info"
                      onClick={updatePfaStep}
                    >
                      Save / Next
                    </MDButton>
                  )) || (
                    <MDButton
                      variant="gradient"
                      color="info"
                      type="button"
                      onClick={
                        (stepDetail && handleUpdatePfaStep) || createPfaUser
                      }
                    >
                      Save / Next
                    </MDButton>
                  )}
                {/* </Link> */}
              </MDBox>
            </MDBox>
            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step3 / Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default PfaStep3;
