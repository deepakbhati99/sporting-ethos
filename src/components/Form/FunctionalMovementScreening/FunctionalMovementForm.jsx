import { useState, useEffect } from "react";
import * as React from "react";
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextareaAutosize,
} from "@mui/material";
import { toast } from "react-toastify";
import {
  createFmsForm,
  getFmsInstruction,
  getFmsSingleData,
  getSingleUserData,
  updateAclProtocol,
  updateFmsForm,
} from "../../../services/service_api";

import "../../../style/loader.css";

function FunctionalMovementForm() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editFms = route.pathname?.split("/")?.slice(2, 3) == "edit-fms";
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [registrationNo, setRegistrationNo] = useState(userId);
  const [sport, setSport] = useState("");
  const [date, setDate] = useState("");

  const [otherObservation, setOtherObservation] = useState("");
  const [injuryHistory, setInjuryHistory] = useState("");
  const [finalScore, setFinalScore] = useState("");

  const [deepSquatScore, setDeepSquatScore] = useState("");
  const [deepSquatComments, setDeepSquatComments] = useState("");

  const [hurdleStepRight, setHurdleStepRight] = useState("");
  const [hurdleStepLeft, setHurdleStepLeft] = useState("");
  const [hurdleStepFinalScore, setHurdleStepFinalScore] = useState("");
  const [hurdleStepComments, setHurdleStepComments] = useState("");

  const [inlineLungeRight, setInlineLungeRight] = useState("");
  const [inlineLungeLeft, setInlineLungeLeft] = useState("");
  const [inlineLungeFinalScore, setInlineLungeFinalScore] = useState("");
  const [inlineLungeComments, setInlineLungeComments] = useState("");

  const [shoulderMobilityRight, setShoulderMobilityRight] = useState("");
  const [shoulderMobilityLeft, setShoulderMobilityLeft] = useState("");
  const [shoulderMobilityFinalScore, setShoulderMobilityFinalScore] =
    useState("");
  const [shoulderMobilityComments, setShoulderMobilityComments] = useState("");
  const [clearance, setClearance] = useState("");
  const [negativeComment, setNegativeComment] = useState("");
  // const [negative, setNegative] = useState("");

  const [rightLegHeightm1, setRightLegHeightm1] = useState("");

  const [activeRightLegRaiseRight, setActiveRightLegRaiseRight] = useState("");
  const [activeRightLegRaiseLeft, setActiveRightLegRaiseLeft] = useState("");
  const [activeFinalScore1, setActiveFinalScore1] = useState("");
  const [activeFinalComment, setActiveFinalComment] = useState("");

  const [trunkStabilityScore, setTrunkStabilityScore] = useState("");
  const [trunkStabilityComment, setTrunkStabilityComment] = useState("");
  const [negativeTrunk, setNegativeTrunk] = useState("");
  const [negativeTrunkComment, setNegativeTrunkComment] = useState("");
  // const [clearanceTrunk, setClearanceTrunk] = useState("");

  // Define state variables for Rotary Stability
  const [rotaryStabilityScore, setRotaryStabilityScore] = useState("");
  const [rotaryStabilityScoreRight, setRotaryStabilityScoreRight] =
    useState("");
  const [rotaryStabilityScoreLeft, setRotaryStabilityScoreLeft] = useState("");
  const [rotaryStabilityComment, setRotaryStabilityComment] = useState("");
  const [negativeRotary, setNegativeRotary] = useState("");
  const [negativeRotaryComment, setNegativeRotaryComment] = useState("");
  // const [clearanceRotary, setClearanceRotary] = useState("");

  const [deepSquatScoreBottom, setDeepSquatScoreBottom] = useState("");
  const [hurdleStepBottom, setHurdleStepBottom] = useState("");
  const [inLineLungeBottom, setInLineLungeBottom] = useState("");
  const [shoulderMobilityBottom, setShoulderMobilityBottom] = useState("");
  const [activeStraightLegRaiseBottom, setActiveStraightLegRaiseBottom] =
    useState("");
  const [trunkStabilityPushUpBottom, setTrunkStabilityPushUpBottom] =
    useState("");
  const [rotaryStabilityBottom, setRotaryStabilityBottom] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  const [adminUserData, setAdminUserData] = useState("");

  useEffect(() => {
    if (editFms == true) {
      getFmsUserData();
    } else {
      getMainUserData();
    }
  }, [editFms]);

  useEffect(() => {
    getFmsInstructionData();
  }, []);

  async function getFmsInstructionData() {
    const response = await getFmsInstruction();
    // console.log("get Fms Instruction", response.data[0]);
    const data = response.data[0];
    if (response) {
      setDeepSquatScoreBottom(data?.deep_squat);
      setHurdleStepBottom(data?.hurdle_step);
      setInLineLungeBottom(data?.inline_lunge);
      setShoulderMobilityBottom(data?.shoulder_mobility);
      setActiveStraightLegRaiseBottom(data?.active_straight_leg_raise);
      setTrunkStabilityPushUpBottom(data?.trunk_stability_push_up);
      setRotaryStabilityBottom(data?.rotary_stability);
    }
  }

  async function getFmsUserData() {
    setLoading(true);
    const params = {
      id: id,
      registration_no: userId,
    };
    const response = await getFmsSingleData(params);
    // console.log("get fms user data", response.data);
    const data = response.data;
    if (response) {
      setName(data.name);
      setAge(data.age);
      setSport(data.sport);
      setDate(data.date);

      setFinalScore(data.final_sorce);

      setInjuryHistory(data.injury_history);
      setOtherObservation(data.other_observation);

      setDeepSquatScore(data.deep_squat);
      setDeepSquatComments(data.deep_squat_comments);

      setHurdleStepRight(data.hurdle_step_right);
      setHurdleStepLeft(data.hurdle_step_left);
      setHurdleStepFinalScore(data.hurdle_step_final_score);
      setHurdleStepComments(data.hurdle_step_comments);

      setInlineLungeRight(data.in_line_lunge_right);
      setInlineLungeLeft(data.in_line_lunge_left);
      setInlineLungeFinalScore(data.in_line_lunge_final_score);
      setInlineLungeComments(data.in_line_lunge_comments);

      setShoulderMobilityRight(data.shoulder_mobility_right);
      setShoulderMobilityLeft(data.shoulder_mobility_left);
      setShoulderMobilityFinalScore(data.shoulder_mobility_final_score);
      setShoulderMobilityComments(data.shoulder_mobility_comments);
      setClearance(data.shoulder_mobility_clearance);
      setNegativeComment(data.shoulder_mobility_clearance_comments);

      setActiveRightLegRaiseRight(data.active_straight_leg_raise_right);
      setActiveRightLegRaiseLeft(data.active_straight_leg_raise_left);
      setActiveFinalScore1(data.active_straight_leg_raise_final_score);
      setActiveFinalComment(data.active_straight_leg_raise_comments);

      setTrunkStabilityScore(data.trunk_stability_pushup);
      setTrunkStabilityComment(data.trunk_stability_pushup_comments);
      setNegativeTrunk(data.trunk_stability_pushup_clearance);
      setNegativeTrunkComment(data.trunk_stability_pushup_clearance_comments);

      setRotaryStabilityScoreRight(data.rotary_stability_right);
      setRotaryStabilityScoreLeft(data.rotary_stability_left);
      setRotaryStabilityScore(data.rotary_stability_final_score);
      setRotaryStabilityComment(data.rotary_stability_comments);
      setNegativeRotary(data.rotary_stability_clearance);
      setNegativeRotaryComment(data.rotary_stability_clearance_comments);
      setLoading(false);
    }
  }

  async function getMainUserData() {
    setLoading(true);
    const params = {
      registration_no: userId,
    };
    const response = await getSingleUserData(params);
    const data = response.data;
    if (response) {
      setAdminUserData(data.dob);
      setName(`${data.first_name}${" "}${data.last_name}`);
      setLoading(false);
    }
  }

  useEffect(() => {
    const birthDate = new Date(adminUserData);
    const currentDate = new Date();
    const ageInMilliseconds = currentDate - birthDate;

    // Calculate age in years
    const ageInYears = Math.floor(ageInMilliseconds / 31536000000); // 1 year = 31536000000 milliseconds
    setAge(ageInYears ? ageInYears : "");
    // console.log(`Years Difference: ${ageInYears}`);
  }, [adminUserData]);

  async function createFmsUser() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        name: name,
        age: age,
        sport: sport,
        date: date,
        injury_history: injuryHistory,
        other_observation: otherObservation,
        final_sorce: finalScore,

        deep_squat: deepSquatScore,
        deep_squat_comments: deepSquatComments,

        hurdle_step_right: hurdleStepRight,
        hurdle_step_left: hurdleStepLeft,
        hurdle_step_final_score: hurdleStepFinalScore,
        hurdle_step_comments: hurdleStepComments,

        in_line_lunge_right: inlineLungeRight,
        in_line_lunge_left: inlineLungeLeft,
        in_line_lunge_final_score: inlineLungeFinalScore,
        in_line_lunge_comments: inlineLungeComments,

        shoulder_mobility_right: shoulderMobilityRight,
        shoulder_mobility_left: shoulderMobilityLeft,
        shoulder_mobility_final_score: shoulderMobilityFinalScore,
        shoulder_mobility_comments: shoulderMobilityComments,
        shoulder_mobility_clearance: clearance,
        shoulder_mobility_clearance_comments: negativeComment,

        active_straight_leg_raise_right: activeRightLegRaiseRight,
        active_straight_leg_raise_left: activeRightLegRaiseLeft,
        active_straight_leg_raise_final_score: activeFinalScore1,
        active_straight_leg_raise_comments: activeFinalComment,

        trunk_stability_pushup: trunkStabilityScore,
        trunk_stability_pushup_comments: trunkStabilityComment,
        trunk_stability_pushup_clearance: negativeTrunk,
        trunk_stability_pushup_clearance_comments: negativeTrunkComment,

        rotary_stability_right: rotaryStabilityScoreRight,
        rotary_stability_left: rotaryStabilityScoreLeft,
        rotary_stability_final_score: rotaryStabilityScore,
        rotary_stability_comments: rotaryStabilityComment,
        rotary_stability_clearance: negativeRotary,
        rotary_stability_clearance_comments: negativeRotaryComment,

        deep_squat_h: deepSquatScoreBottom,
        hurdle_step: hurdleStepBottom,
        inline_lunge: inLineLungeBottom,
        shoulder_mobility: shoulderMobilityBottom,
        active_straight_leg_raise: activeStraightLegRaiseBottom,
        trunk_stability_push_up: trunkStabilityPushUpBottom,
        rotary_stability: rotaryStabilityBottom,
      };
      // console.log("params==>", params);
      const response = await createFmsForm(params);
      if (response.status == true) {
        toast.success(response.message, toastConfig);
        navigate(`/all/forms/${userId}`);

        setName("");
        setAge("");
        setSport("");
        setDate("");
        setOtherObservation("");
        setInjuryHistory("");
        setFinalScore("");

        setDeepSquatScore("");
        setDeepSquatComments("");

        setHurdleStepRight("");
        setHurdleStepLeft("");
        setHurdleStepFinalScore("");
        setHurdleStepComments("");

        setInlineLungeRight("");
        setInlineLungeLeft("");
        setInlineLungeFinalScore("");
        setInlineLungeComments("");

        setShoulderMobilityRight("");
        setShoulderMobilityLeft("");
        setShoulderMobilityFinalScore("");
        setShoulderMobilityComments("");
        setClearance("");
        setNegativeComment("");

        setActiveRightLegRaiseRight("");
        setActiveRightLegRaiseLeft("");
        setActiveFinalScore1("");
        setActiveFinalComment("");

        setTrunkStabilityScore("");
        setTrunkStabilityComment("");
        setNegativeTrunk("");
        setNegativeTrunkComment("");

        setRotaryStabilityScore("");
        setRotaryStabilityScoreRight("");
        setRotaryStabilityScoreLeft("");
        setRotaryStabilityComment("");
        setNegativeRotary("");
        setNegativeRotaryComment("");

        setDeepSquatScoreBottom("");
        setHurdleStepBottom("");
        setInLineLungeBottom("");
        setShoulderMobilityBottom("");
        setActiveStraightLegRaiseBottom("");
        setTrunkStabilityPushUpBottom("");
        setRotaryStabilityBottom("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  async function updateFmsUser() {
    if (formValidation()) {
      const params = {
        id: id,
        registration_no: registrationNo,
        name: name,
        age: age,
        sport: sport,
        date: date,
        injury_history: injuryHistory,
        other_observation: otherObservation,
        final_sorce: finalScore,

        deep_squat: deepSquatScore,
        deep_squat_comments: deepSquatComments,

        hurdle_step_right: hurdleStepRight,
        hurdle_step_left: hurdleStepLeft,
        hurdle_step_final_score: hurdleStepFinalScore,
        hurdle_step_comments: hurdleStepComments,

        in_line_lunge_right: inlineLungeRight,
        in_line_lunge_left: inlineLungeLeft,
        in_line_lunge_final_score: inlineLungeFinalScore,
        in_line_lunge_comments: inlineLungeComments,

        shoulder_mobility_right: shoulderMobilityRight,
        shoulder_mobility_left: shoulderMobilityLeft,
        shoulder_mobility_final_score: shoulderMobilityFinalScore,
        shoulder_mobility_comments: shoulderMobilityComments,
        shoulder_mobility_clearance: clearance,
        shoulder_mobility_clearance_comments: negativeComment,

        active_straight_leg_raise_right: activeRightLegRaiseRight,
        active_straight_leg_raise_left: activeRightLegRaiseLeft,
        active_straight_leg_raise_final_score: activeFinalScore1,
        active_straight_leg_raise_comments: activeFinalComment,

        trunk_stability_pushup: trunkStabilityScore,
        trunk_stability_pushup_comments: trunkStabilityComment,
        trunk_stability_pushup_clearance: negativeTrunk,
        trunk_stability_pushup_clearance_comments: negativeTrunkComment,

        rotary_stability_right: rotaryStabilityScoreRight,
        rotary_stability_left: rotaryStabilityScoreLeft,
        rotary_stability_final_score: rotaryStabilityScore,
        rotary_stability_comments: rotaryStabilityComment,
        rotary_stability_clearance: negativeRotary,
        rotary_stability_clearance_comments: negativeRotaryComment,

        deep_squat_h: deepSquatScoreBottom,
        hurdle_step: hurdleStepBottom,
        inline_lunge: inLineLungeBottom,
        shoulder_mobility: shoulderMobilityBottom,
        active_straight_leg_raise: activeStraightLegRaiseBottom,
        trunk_stability_push_up: trunkStabilityPushUpBottom,
        rotary_stability: rotaryStabilityBottom,
      };
      const response = await updateFmsForm(params);
      if (response.status == true) {
        toast.success(response.message, toastConfig);
        navigate(`/all/forms/${userId}`);
        setName("");
        setAge("");
        setSport("");
        setDate("");
        setOtherObservation("");
        setInjuryHistory("");
        setFinalScore("");

        setDeepSquatScore("");
        setDeepSquatComments("");

        setHurdleStepRight("");
        setHurdleStepLeft("");
        setHurdleStepFinalScore("");
        setHurdleStepComments("");

        setInlineLungeRight("");
        setInlineLungeLeft("");
        setInlineLungeFinalScore("");
        setInlineLungeComments("");

        setShoulderMobilityRight("");
        setShoulderMobilityLeft("");
        setShoulderMobilityFinalScore("");
        setShoulderMobilityComments("");
        setClearance("");
        setNegativeComment("");

        setActiveRightLegRaiseRight("");
        setActiveRightLegRaiseLeft("");
        setActiveFinalScore1("");
        setActiveFinalComment("");

        setTrunkStabilityScore("");
        setTrunkStabilityComment("");
        setNegativeTrunk("");
        setNegativeTrunkComment("");

        setRotaryStabilityScore("");
        setRotaryStabilityScoreRight("");
        setRotaryStabilityScoreLeft("");
        setRotaryStabilityComment("");
        setNegativeRotary("");
        setNegativeRotaryComment("");

        setDeepSquatScoreBottom("");
        setHurdleStepBottom("");
        setInLineLungeBottom("");
        setShoulderMobilityBottom("");
        setActiveStraightLegRaiseBottom("");
        setTrunkStabilityPushUpBottom("");
        setRotaryStabilityBottom("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  function formValidation() {
    let msgName = "";
    let msgAge = "";
    let msgSport = "";
    let msgDate = "";
    let msgInjuryHistory = "";
    let msgOtherObservation = "";
    let msgFinalScore = "";

    let msgDeepSquatScore = "";
    let msgDeepSquatComments = "";

    let msgHurdleStepRight = "";
    let msgHurdleStepLeft = "";
    // let msgHurdleStepFinalScore = "";
    let msgHurdleStepComments = "";

    let msgInlineLungeRight = "";
    let msgInlineLungeLeft = "";
    // let msgInlineLungeFinalScore = "";
    let msgInlineLungeComments = "";

    let msgShoulderMobilityRight = "";
    let msgShoulderMobilityLeft = "";
    // let msgShoulderMobilityFinalScore = "";
    let msgShoulderMobilityComments = "";

    let msgClearance = "";
    let msgNegativeComment = "";

    let msgActiveRightLegRaiseRight = "";
    let msgActiveRightLegRaiseLeft = "";
    // let msgActiveFinalScore1 = "";
    let msgActiveFinalComment = "";

    let msgTrunkStabilityScore = "";
    let msgTrunkStabilityComment = "";
    let msgNegativeTrunk = "";
    let msgNegativeTrunkComment = "";

    let msgRotaryStabilityScoreRight = "";
    let msgRotaryStabilityScoreLeft = "";
    // let msgRotaryStabilityScore = "";
    let msgRotaryStabilityComment = "";
    let msgNegativeRotary = "";
    let msgNegativeRotaryComment = "";

    let msgDeepSquatScoreBottom = "";
    let msgHurdleStepBottom = "";
    let msgInLineLungeBottom = "";
    let msgShoulderMobilityBottom = "";
    let msgActiveStraightLegRaiseBottom = "";
    let msgTrunkStabilityPushUpBottom = "";
    let msgRotaryStabilityBottom = "";

    let isValid = true;

    const regexPattern = /^(?![0123]$).*$/;

    if (name === "") {
      msgName = "Please enter name";
      isValid = false;
    }

    if (age === "") {
      msgAge = "Please enter age";
      isValid = false;
    }

    if (sport === "") {
      msgSport = "Please select a sport";
      isValid = false;
    }

    if (date === "") {
      msgDate = "Please select a date";
      isValid = false;
    }

    if (injuryHistory === "") {
      msgInjuryHistory = "Please enter injury history";
      isValid = false;
    }

    if (otherObservation === "") {
      msgOtherObservation = "Please enter other observations";
      isValid = false;
    }

    if (finalScore === "") {
      msgFinalScore = "Please enter a final score";
      isValid = false;
    }

    if (deepSquatScore === "") {
      msgDeepSquatScore = "Please enter a deep squat score";
      isValid = false;
    } else if (regexPattern.test(deepSquatScore)) {
      msgDeepSquatScore = "Please enter only 0 1 2 3";
      isValid = false;
    }

    if (deepSquatComments === "") {
      msgDeepSquatComments = "Please enter comments for the deep squat";
      isValid = false;
    }

    if (hurdleStepRight === "") {
      msgHurdleStepRight = "Please enter the right hurdle step score";
      isValid = false;
    } else if (regexPattern.test(hurdleStepRight)) {
      msgHurdleStepRight = "Please enter only 0 1 2 3";
      isValid = false;
    }

    if (hurdleStepLeft === "") {
      msgHurdleStepLeft = "Please enter the left hurdle step score";
      isValid = false;
    } else if (regexPattern.test(hurdleStepLeft)) {
      msgHurdleStepLeft = "Please enter only 0 1 2 3";
      isValid = false;
    }

    // if (hurdleStepFinalScore === "") {
    //   msgHurdleStepFinalScore =
    //     "Please provide a final score for the hurdle step";
    //   isValid = false;
    // }

    if (hurdleStepComments === "") {
      msgHurdleStepComments = "Please provide comments for the hurdle step";
      isValid = false;
    }

    if (inlineLungeRight === "") {
      msgInlineLungeRight = "Please provide the right inline lunge score";
      isValid = false;
    } else if (regexPattern.test(inlineLungeRight)) {
      msgInlineLungeRight = "Please enter only 0 1 2 3";
      isValid = false;
    }

    if (inlineLungeLeft === "") {
      msgInlineLungeLeft = "Please provide the left inline lunge score";
      isValid = false;
    } else if (regexPattern.test(inlineLungeLeft)) {
      msgInlineLungeLeft = "Please enter only 0 1 2 3";
      isValid = false;
    }

    // if (inlineLungeFinalScore === "") {
    //   msgInlineLungeFinalScore =
    //     "Please provide a final score for the inline lunge";
    //   isValid = false;
    // }

    if (inlineLungeComments === "") {
      msgInlineLungeComments = "Please provide comments for the inline lunge";
      isValid = false;
    }

    if (shoulderMobilityRight === "") {
      msgShoulderMobilityRight =
        "Please provide the right shoulder mobility score";
      isValid = false;
    } else if (regexPattern.test(shoulderMobilityRight)) {
      msgShoulderMobilityRight = "Please enter only 0 1 2 3";
      isValid = false;
    }

    if (shoulderMobilityLeft === "") {
      msgShoulderMobilityLeft =
        "Please provide the left shoulder mobility score";
      isValid = false;
    } else if (regexPattern.test(shoulderMobilityLeft)) {
      msgShoulderMobilityLeft = "Please enter only 0 1 2 3";
      isValid = false;
    }

    // if (shoulderMobilityFinalScore === "") {
    //   msgShoulderMobilityFinalScore =
    //     "Please provide a final score for shoulder mobility";
    //   isValid = false;
    // }

    if (shoulderMobilityComments === "") {
      msgShoulderMobilityComments =
        "Please provide comments for shoulder mobility";
      isValid = false;
    }

    if (clearance === "") {
      msgClearance = "Please select clearance information";
      isValid = false;
    }

    if (negativeComment === "") {
      msgNegativeComment = "Please provide comments for negative findings";
      isValid = false;
    }

    if (activeRightLegRaiseRight === "") {
      msgActiveRightLegRaiseRight =
        "Please provide the right active leg raise score";
      isValid = false;
    } else if (regexPattern.test(activeRightLegRaiseRight)) {
      msgActiveRightLegRaiseRight = "Please enter only 0 1 2 3";
      isValid = false;
    }

    if (activeRightLegRaiseLeft === "") {
      msgActiveRightLegRaiseLeft =
        "Please provide the left active leg raise score";
      isValid = false;
    } else if (regexPattern.test(activeRightLegRaiseLeft)) {
      msgActiveRightLegRaiseLeft = "Please enter only 0 1 2 3";
      isValid = false;
    }

    // if (activeFinalScore1 === "") {
    //   msgActiveFinalScore1 =
    //     "Please provide a final score for active leg raise";
    //   isValid = false;
    // }

    if (activeFinalComment === "") {
      msgActiveFinalComment = "Please provide comments for active leg raise";
      isValid = false;
    }

    if (trunkStabilityScore === "") {
      msgTrunkStabilityScore = "Please provide trunk stability score";
      isValid = false;
    }

    if (trunkStabilityComment === "") {
      msgTrunkStabilityComment = "Please provide comments for trunk stability";
      isValid = false;
    }

    if (negativeTrunk === "") {
      msgNegativeTrunk = "Please select negative findings for trunk stability";
      isValid = false;
    }

    if (negativeTrunkComment === "") {
      msgNegativeTrunkComment =
        "Please provide comments for negative trunk stability findings";
      isValid = false;
    }

    if (rotaryStabilityScoreRight === "") {
      msgRotaryStabilityScoreRight =
        "Please provide the right rotary stability score";
      isValid = false;
    } else if (regexPattern.test(rotaryStabilityScoreRight)) {
      msgRotaryStabilityScoreRight = "Please enter only 0 1 2 3";
      isValid = false;
    }

    if (rotaryStabilityScoreLeft === "") {
      msgRotaryStabilityScoreLeft =
        "Please provide the left rotary stability score";
      isValid = false;
    } else if (regexPattern.test(rotaryStabilityScoreLeft)) {
      msgRotaryStabilityScoreLeft = "Please enter only 0 1 2 3";
      isValid = false;
    }

    // if (rotaryStabilityScore === "") {
    //   msgRotaryStabilityScore =
    //     "Please provide the left rotary stability score";
    //   isValid = false;
    // }

    if (rotaryStabilityComment === "") {
      msgRotaryStabilityComment =
        "Please provide comments for rotary stability";
      isValid = false;
    }

    if (negativeRotary === "") {
      msgNegativeRotary =
        "Please select negative findings for rotary stability";
      isValid = false;
    }

    if (negativeRotaryComment === "") {
      msgNegativeRotaryComment =
        "Please provide comments for negative rotary stability findings";
      isValid = false;
    }

    // Check for other fields in a similar way...

    if (deepSquatScoreBottom === "") {
      msgDeepSquatScoreBottom = "Please enter deep squat score";
      isValid = false;
    }

    if (hurdleStepBottom === "") {
      msgHurdleStepBottom = "Please enter hurdle step";
      isValid = false;
    }

    if (inLineLungeBottom === "") {
      msgInLineLungeBottom = "Please enter inLine lunge";
      isValid = false;
    }

    if (shoulderMobilityBottom === "") {
      msgShoulderMobilityBottom = "Please enter shoulder mobility";
      isValid = false;
    }

    if (activeStraightLegRaiseBottom === "") {
      msgActiveStraightLegRaiseBottom =
        "Please enter active straight leg raise";
      isValid = false;
    }

    if (trunkStabilityPushUpBottom === "") {
      msgTrunkStabilityPushUpBottom = "Please enter trunk stability pushUp";
      isValid = false;
    }

    if (rotaryStabilityBottom === "") {
      msgRotaryStabilityBottom = "Please enter rotary stability";
      isValid = false;
    }

    if (!isValid) {
      setError(true);
      setErrorMessage({
        name: msgName,
        age: msgAge,
        sport: msgSport,
        date: msgDate,
        injuryHistory: msgInjuryHistory,
        otherObservation: msgOtherObservation,
        finalScore: msgFinalScore,
        deepSquatScore: msgDeepSquatScore,
        deepSquatComments: msgDeepSquatComments,
        hurdleStepRight: msgHurdleStepRight,
        hurdleStepLeft: msgHurdleStepLeft,
        // hurdleStepFinalScore: msgHurdleStepFinalScore,
        hurdleStepComments: msgHurdleStepComments,
        inlineLungeRight: msgInlineLungeRight,
        inlineLungeLeft: msgInlineLungeLeft,
        // inlineLungeFinalScore: msgInlineLungeFinalScore,
        inlineLungeComments: msgInlineLungeComments,
        shoulderMobilityRight: msgShoulderMobilityRight,
        shoulderMobilityLeft: msgShoulderMobilityLeft,
        // shoulderMobilityFinalScore: msgShoulderMobilityFinalScore,
        shoulderMobilityComments: msgShoulderMobilityComments,
        clearance: msgClearance,
        negativeComment: msgNegativeComment,
        activeRightLegRaiseRight: msgActiveRightLegRaiseRight,
        activeRightLegRaiseLeft: msgActiveRightLegRaiseLeft,
        // activeFinalScore1: msgActiveFinalScore1,
        activeFinalComment: msgActiveFinalComment,
        trunkStabilityScore: msgTrunkStabilityScore,
        trunkStabilityComment: msgTrunkStabilityComment,
        negativeTrunk: msgNegativeTrunk,
        negativeTrunkComment: msgNegativeTrunkComment,
        rotaryStabilityScoreRight: msgRotaryStabilityScoreRight,
        rotaryStabilityScoreLeft: msgRotaryStabilityScoreLeft,
        // rotaryStabilityScore: msgRotaryStabilityScore,
        rotaryStabilityComment: msgRotaryStabilityComment,
        negativeRotary: msgNegativeRotary,
        negativeRotaryComment: msgNegativeRotaryComment,
        // Include messages for other fields here

        deepSquatScoreBottom: msgDeepSquatScoreBottom,
        hurdleStepBottom: msgHurdleStepBottom,
        inLineLungeBottom: msgInLineLungeBottom,
        shoulderMobilityBottom: msgShoulderMobilityBottom,
        activeStraightLegRaiseBottom: msgActiveStraightLegRaiseBottom,
        trunkStabilityPushUpBottom: msgTrunkStabilityPushUpBottom,
        rotaryStabilityBottom: msgRotaryStabilityBottom,
      });
      toast.error("All fields are required", toastConfig);
      window.scrollTo(0, 0);
    } else {
      setError(false);
      setErrorMessage({
        name: "",
        age: "",
        sport: "",
        date: "",
        injuryHistory: "",
        otherObservation: "",
        finalScore: "",
        deepSquatScore: "",
        deepSquatComments: "",
        hurdleStepRight: "",
        hurdleStepLeft: "",
        hurdleStepFinalScore: "",
        hurdleStepComments: "",
        inlineLungeRight: "",
        inlineLungeLeft: "",
        inlineLungeFinalScore: "",
        inlineLungeComments: "",
        shoulderMobilityRight: "",
        shoulderMobilityLeft: "",
        shoulderMobilityFinalScore: "",
        shoulderMobilityComments: "",
        clearance: "",
        negativeComment: "",
        activeRightLegRaiseRight: "",
        activeRightLegRaiseLeft: "",
        activeFinalScore1: "",
        activeFinalComment: "",
        trunkStabilityScore: "",
        trunkStabilityComment: "",
        negativeTrunk: "",
        negativeTrunkComment: "",
        rotaryStabilityScoreRight: "",
        rotaryStabilityScoreLeft: "",
        rotaryStabilityScore: "",
        rotaryStabilityComment: "",
        negativeRotary: "",
        negativeRotaryComment: "",

        deepSquatScoreBottom: "",
        hurdleStepBottom: "",
        inLineLungeBottom: "",
        shoulderMobilityBottom: "",
        activeStraightLegRaiseBottom: "",
        trunkStabilityPushUpBottom: "",
        rotaryStabilityBottom: "",
        // Clear messages for other fields here
      });
    }

    return isValid;
  }

  const height45 = {
    height: "45px",
  };

  useEffect(() => {
    const scoresToSum = [
      hurdleStepFinalScore,
      inlineLungeFinalScore,
      shoulderMobilityFinalScore,
      activeFinalScore1,
      rotaryStabilityScore,
    ];

    const finalScoreCount = scoresToSum.reduce(
      (sum, score) => sum + (parseInt(score) || 0),
      0
    );
    setFinalScore(finalScoreCount);
  }, [
    deepSquatScore,
    hurdleStepFinalScore,
    inlineLungeFinalScore,
    shoulderMobilityFinalScore,
    activeFinalScore1,
    trunkStabilityScore,
    rotaryStabilityScore,
  ]);

  useEffect(() => {
    setHurdleStepFinalScore(
      hurdleStepRight > hurdleStepLeft ? hurdleStepLeft : hurdleStepRight
    );
  }, [hurdleStepRight, hurdleStepLeft]);

  useEffect(() => {
    setInlineLungeFinalScore(
      inlineLungeRight > inlineLungeLeft ? inlineLungeLeft : inlineLungeRight
    );
  }, [inlineLungeRight, inlineLungeLeft]);

  useEffect(() => {
    setShoulderMobilityFinalScore(
      shoulderMobilityRight > shoulderMobilityLeft
        ? shoulderMobilityLeft
        : shoulderMobilityRight
    );
  }, [shoulderMobilityRight, shoulderMobilityLeft]);

  useEffect(() => {
    setActiveFinalScore1(
      activeRightLegRaiseRight > activeRightLegRaiseLeft
        ? activeRightLegRaiseLeft
        : activeRightLegRaiseRight
    );
  }, [activeRightLegRaiseRight, activeRightLegRaiseLeft]);

  useEffect(() => {
    setRotaryStabilityScore(
      rotaryStabilityScoreRight > rotaryStabilityScoreLeft
        ? rotaryStabilityScoreLeft
        : rotaryStabilityScoreRight
    );
  }, [rotaryStabilityScoreRight, rotaryStabilityScoreLeft]);

  return (
    <>
      {loading && <div class="loading">Loading&#8230;</div>}
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body1"
              color="text"
              ml={1}
              fontWeight="bold"
              fontSize={30}
            >
              Functional Movement Screening Form
            </MDTypography>
          </MDBox>

          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Name */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Name
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="name"
                    placeholder="Name"
                    value={name}
                    id="name"
                    onChange={(e) => setName(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["name"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["name"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Age(years) */}
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Age(years)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="age"
                    placeholder="Age"
                    value={age}
                    id="age"
                    onChange={(e) => setAge(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["age"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["age"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Registration No */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Registration No
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="registrationno"
                    placeholder="Registration No"
                    id="registrationNo"
                    value={registrationNo}
                    onChange={(e) => setRegistrationNo(e.target.value)}
                    disabled={true}
                  />
                </MDBox>
                {error && errorMessage["registrationNo"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["registrationNo"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Sport */}
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Sport/Activity
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="sport"
                    placeholder="Sport/Activity"
                    value={sport}
                    id="sport"
                    onChange={(e) => setSport(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sport"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sport"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Date */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Date
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="date"
                    fullWidth
                    name="date"
                    placeholder="Date"
                    value={date}
                    id="date"
                    onChange={(e) => setDate(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["date"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["date"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            {/* Final Score */}
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                FMS Score
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="registrationNo"
                    placeholder="Final Score"
                    id="finalScore"
                    value={finalScore}
                    onChange={(e) => setFinalScore(e.target.value)}
                    disabled
                  />
                </MDBox>
                {error && errorMessage["finalScore"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["finalScore"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Injury history */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Injury history
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  {/* <MDInput
                    type="text"
                    fullWidth
                    name="injuryHistory"
                    placeholder="Injury history"
                    value={injuryHistory}
                    id="injuryHistory"
                    onChange={(e) => setInjuryHistory(e.target.value)}
                  /> */}
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="Injury history"
                    value={injuryHistory}
                    id="injuryHistory"
                    onChange={(e) => setInjuryHistory(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["injuryHistory"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["injuryHistory"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Other observation */}
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Other observation
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  {/* <MDInput
                    type="text"
                    fullWidth
                    name="otherObservation"
                    placeholder="otherObservation"
                    value={otherObservation}
                    id="otherObservation"
                    onChange={(e) => setOtherObservation(e.target.value)}
                  /> */}
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="Other observation"
                    value={otherObservation}
                    id="otherObservation"
                    onChange={(e) => setOtherObservation(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["otherObservation"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["otherObservation"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox mt={1} mb={0}>
              <MDBox display="flex" flexDirection="row" width="100%">
                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDTypography
                    display="flex"
                    variant="body1"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    width="100%"
                    fontSize={18}
                  >
                    NAME OF TEST
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDTypography
                    display="flex"
                    variant="body1"
                    color="text"
                    ml={3}
                    fontWeight="bold"
                    width="100%"
                    fontSize={18}
                  >
                    SCORE
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDTypography
                    display="flex"
                    variant="body1"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    width="100%"
                    fontSize={18}
                  >
                    FINAL SCORE
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDTypography
                    display="flex"
                    variant="body1"
                    color="text"
                    ml={3}
                    fontWeight="bold"
                    width="100%"
                    fontSize={18}
                  >
                    COMMENTS
                  </MDTypography>
                </MDBox>
              </MDBox>

              {/* Deep Squat */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Deep Squat
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Deep Squat Score"
                        placeholder="Deep Squat Score"
                        value={deepSquatScore}
                        id="deepSquatScore"
                        onChange={(e) => setDeepSquatScore(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["deepSquatScore"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["deepSquatScore"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Deep Squat Comments"
                        value={deepSquatComments}
                        id="deepSquatComments"
                        onChange={(e) => setDeepSquatComments(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["deepSquatComments"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["deepSquatComments"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* Hurdle Step */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Hurdle Step
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Hurdle Step Right"
                        placeholder="Right"
                        value={hurdleStepRight}
                        id="hurdleStepRight"
                        onChange={(e) => setHurdleStepRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["hurdleStepRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["hurdleStepRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Hurdle Step Left"
                        placeholder="Left"
                        value={hurdleStepLeft}
                        id="hurdleStepLeft"
                        onChange={(e) => setHurdleStepLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["hurdleStepLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["hurdleStepLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Hurdle Step Final Score "
                        placeholder="Score"
                        disabled
                        value={hurdleStepFinalScore}
                        id="hurdleStepFinalScore"
                        onChange={(e) =>
                          setHurdleStepFinalScore(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["hurdleStepFinalScore"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["hurdleStepFinalScore"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      {/* <MDInput
                        type="text"
                        fullWidth
                        name="Hurdle Step Comments"
                        placeholder=""
                        value={hurdleStepComments}
                        id="hurdleStepComments"
                        onChange={(e) => setHurdleStepComments(e.target.value)}
                      /> */}
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Hurdle Step Comment"
                        value={hurdleStepComments}
                        id="hurdleStepComments"
                        onChange={(e) => setHurdleStepComments(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["hurdleStepComments"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["hurdleStepComments"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* In-Line Lunge */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    In-Line Lunge
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="In-Line Lunge Right "
                        placeholder="Right"
                        value={inlineLungeRight}
                        id="inlineLungeRight"
                        onChange={(e) => setInlineLungeRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["inlineLungeRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["inlineLungeRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="In-Line Lunge Left"
                        placeholder="Left"
                        value={inlineLungeLeft}
                        id="inlineLungeLeft"
                        onChange={(e) => setInlineLungeLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["inlineLungeLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["inlineLungeLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="In-Line Lunge Final Score"
                        disabled
                        placeholder="Score"
                        value={inlineLungeFinalScore}
                        id="inlineLungeFinalScore"
                        onChange={(e) =>
                          setInlineLungeFinalScore(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["inlineLungeFinalScore"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["inlineLungeFinalScore"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="In-Line Lunge Comments"
                        value={inlineLungeComments}
                        id="inlineLungeComments"
                        onChange={(e) => setInlineLungeComments(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["inlineLungeComments"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["inlineLungeComments"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* Shoulder Mobility */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Shoulder Mobility
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Shoulder Mobility Right"
                        placeholder="Right"
                        value={shoulderMobilityRight}
                        id="shoulderMobilityRight"
                        onChange={(e) =>
                          setShoulderMobilityRight(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["shoulderMobilityRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["shoulderMobilityRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Shoulder Mobility Left"
                        placeholder="Left"
                        value={shoulderMobilityLeft}
                        id="shoulderMobilityLeft"
                        onChange={(e) =>
                          setShoulderMobilityLeft(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["shoulderMobilityLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["shoulderMobilityLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Shoulder Mobility Final Score"
                        placeholder="Score"
                        disabled
                        value={shoulderMobilityFinalScore}
                        id="shoulderMobilityFinalScore"
                        onChange={(e) =>
                          setShoulderMobilityFinalScore(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["shoulderMobilityFinalScore"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["shoulderMobilityFinalScore"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Shoulder Mobility Comment"
                        value={shoulderMobilityComments}
                        id="shoulderMobilityComments"
                        onChange={(e) =>
                          setShoulderMobilityComments(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["shoulderMobilityComments"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["shoulderMobilityComments"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Clearance:
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="70%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    &nbsp;
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">
                          Select Clearance
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          style={{ ...height45 }}
                          value={clearance}
                          label="Select Clearance"
                          onChange={(e) => setClearance(e.target.value)}
                        >
                          <MenuItem value={"positive"}>Positive</MenuItem>
                          <MenuItem value={"negative"}>Negative</MenuItem>
                        </Select>
                      </FormControl>
                    </MDBox>

                    {error && errorMessage["clearance"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["clearance"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Clearance Comment"
                        value={negativeComment}
                        id="negativeComment"
                        onChange={(e) => setNegativeComment(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["negativeComment"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["negativeComment"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* Active Straight Leg Raise */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Active Straight Leg Raise
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Active Straight Leg Raise Right"
                        placeholder="Right"
                        value={activeRightLegRaiseRight}
                        id="activeRightLegRaiseRight"
                        onChange={(e) =>
                          setActiveRightLegRaiseRight(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["activeRightLegRaiseRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["activeRightLegRaiseRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Active Straight Leg Raise Left"
                        placeholder="Left"
                        value={activeRightLegRaiseLeft}
                        id="activeRightLegRaiseLeft"
                        onChange={(e) =>
                          setActiveRightLegRaiseLeft(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["activeRightLegRaiseLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["activeRightLegRaiseLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="Active Straight Leg Raise"
                        disabled
                        placeholder="Score"
                        value={activeFinalScore1}
                        id="activeFinalScore1"
                        onChange={(e) => setActiveFinalScore1(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["activeFinalScore1"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["activeFinalScore1"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Active Straight Leg Raise Comment"
                        value={activeFinalComment}
                        id="activeFinalComment"
                        onChange={(e) => setActiveFinalComment(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["activeFinalComment"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["activeFinalComment"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* Trunk Stability Push-up */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Trunk Stability Push-up
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="100%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder="Score"
                        value={trunkStabilityScore}
                        id="trunkStabilityScore"
                        onChange={(e) => setTrunkStabilityScore(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["trunkStabilityScore"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["trunkStabilityScore"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Trunk Stability Push-up Comment"
                        value={trunkStabilityComment}
                        id="trunkStabilityComment"
                        onChange={(e) =>
                          setTrunkStabilityComment(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["trunkStabilityComment"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["trunkStabilityComment"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Clearance:
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="70%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    &nbsp;
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">
                          Select Clearance
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          style={{ ...height45 }}
                          value={negativeTrunk}
                          label="Select Clearance"
                          onChange={(e) => setNegativeTrunk(e.target.value)}
                        >
                          <MenuItem value={"positive"}>Positive</MenuItem>
                          <MenuItem value={"negative"}>Negative</MenuItem>
                        </Select>
                      </FormControl>
                    </MDBox>
                    {error && errorMessage["negativeTrunk"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["negativeTrunk"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Clearance Comment"
                        value={negativeTrunkComment}
                        id="negativeTrunkComment"
                        onChange={(e) =>
                          setNegativeTrunkComment(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["negativeTrunkComment"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["negativeTrunkComment"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* Rotary Stability */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Rotary Stability
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder="Right"
                        value={rotaryStabilityScoreRight}
                        id="rotaryStabilityScoreRight"
                        onChange={(e) =>
                          setRotaryStabilityScoreRight(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["rotaryStabilityScoreRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["rotaryStabilityScoreRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="25%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder="Left"
                        value={rotaryStabilityScoreLeft}
                        id="rotaryStabilityScoreLeft"
                        onChange={(e) =>
                          setRotaryStabilityScoreLeft(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["rotaryStabilityScoreLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["rotaryStabilityScoreLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="RightLegHeightm1"
                        disabled
                        placeholder="Score"
                        value={rotaryStabilityScore}
                        id="rotaryStabilityScore"
                        onChange={(e) =>
                          setRotaryStabilityScore(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["rotaryStabilityScore"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["rotaryStabilityScore"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>

                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Rotary Stability Comment"
                        value={rotaryStabilityComment}
                        id="rotaryStabilityComment"
                        onChange={(e) =>
                          setRotaryStabilityComment(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["rotaryStabilityComment"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["rotaryStabilityComment"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={5}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Clearance:
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="70%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {/* Negative */}
                    &nbsp;
                  </MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">
                          Select Clearance
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          style={{ ...height45 }}
                          value={negativeRotary}
                          label="Select Clearance"
                          onChange={(e) => setNegativeRotary(e.target.value)}
                        >
                          <MenuItem value={"positive"}>Positive</MenuItem>
                          <MenuItem value={"negative"}>Negative</MenuItem>
                        </Select>
                      </FormControl>
                    </MDBox>
                    {error && errorMessage["negativeRotary"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["negativeRotary"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={4}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  ></MDTypography>
                  <MDBox flexDirection="row" ml={1}>
                    <MDBox mb={1} mr={2} flexDirection="column" width="100%">
                      <TextareaAutosize
                        style={{
                          font: "inherit",
                          padding: "0.75rem",
                          border: "1px solid #d0d4d8",
                          borderRadius: "6px",
                          boxSizing: "content-box",
                          background: "none",
                          minheight: "1.4375em",
                          margin: "0",
                          WebkitTapHighlightColor: "transparent",
                          display: "block",
                          minWidth: "0",
                          width: "100%",
                          backgroundColor: "transparent",
                          fontSize: "14px",
                        }}
                        minRows={2}
                        placeholder="Clearance Comment"
                        value={negativeRotaryComment}
                        id="negativeRotaryComment"
                        onChange={(e) =>
                          setNegativeRotaryComment(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["negativeRotaryComment"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["negativeRotaryComment"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>

              {/* Comment box */}
              {/* Deep Squat */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Deep Squat
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Deep Squat Comments"
                      value={deepSquatScoreBottom}
                      id="deepSquatScoreBottom"
                      onChange={(e) => setDeepSquatScoreBottom(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["deepSquatScoreBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["deepSquatScoreBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              {/* Hurdle step */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Hurdle step
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Hurdle step Comments"
                      value={hurdleStepBottom}
                      id="hurdleStepBottom"
                      onChange={(e) => setHurdleStepBottom(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["hurdleStepBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["hurdleStepBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              {/* Inline lunge */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Inline lunge
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Inline lunge Comments"
                      value={inLineLungeBottom}
                      id="inLineLungeBottom"
                      onChange={(e) => setInLineLungeBottom(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["inLineLungeBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["inLineLungeBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              {/* Shoulder mobility */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Shoulder mobility
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Shoulder mobility Comments"
                      value={shoulderMobilityBottom}
                      id="shoulderMobilityBottom"
                      onChange={(e) =>
                        setShoulderMobilityBottom(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["shoulderMobilityBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["shoulderMobilityBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              {/* Active straight leg raise */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Active straight leg raise
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Active straight leg raise Comments"
                      value={activeStraightLegRaiseBottom}
                      id="activeStraightLegRaiseBottom"
                      onChange={(e) =>
                        setActiveStraightLegRaiseBottom(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["activeStraightLegRaiseBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["activeStraightLegRaiseBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              {/* Trunk stability push up */}
              {/* <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Trunk stability push up
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Trunk stability push up Comments"
                      value={trunkStabilityPushUpBottom}
                      id="trunkStabilityPushUpBottom"
                      onChange={(e) =>
                        setTrunkStabilityPushUpBottom(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["trunkStabilityPushUpBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["trunkStabilityPushUpBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox> */}

              {/* Rotary stability */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={1} display="flex" width="20%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Rotary stability
                  </MDTypography>
                </MDBox>

                <MDBox flexDirection="row" ml={1} width="80%">
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <TextareaAutosize
                      style={{
                        font: "inherit",
                        padding: "0.75rem",
                        border: "1px solid #d0d4d8",
                        borderRadius: "6px",
                        boxSizing: "content-box",
                        background: "none",
                        minheight: "1.4375em",
                        margin: "0",
                        WebkitTapHighlightColor: "transparent",
                        display: "block",
                        minWidth: "0",
                        width: "100%",
                        backgroundColor: "transparent",
                        fontSize: "14px",
                      }}
                      minRows={2}
                      placeholder="Rotary stability Comments"
                      value={rotaryStabilityBottom}
                      id="rotaryStabilityBottom"
                      onChange={(e) => setRotaryStabilityBottom(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["rotaryStabilityBottom"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["rotaryStabilityBottom"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox mt={5} mr={2} display="flex" justifyContent="flex-end">
                {/* <Link to={"/all/forms"}> */}
                {(editFms && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updateFmsUser}
                  >
                    Update
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={createFmsUser}
                  >
                    Submit
                  </MDButton>
                )}
                {/* </Link> */}
              </MDBox>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default FunctionalMovementForm;
