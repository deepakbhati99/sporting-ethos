import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { TextareaAutosize } from "@mui/material";
import { toast } from "react-toastify";
import {
  createAclProtocol,
  getAclByRGst,
  getAclSingleData,
  getSingleUserData,
  updateAclProtocol,
} from "../../../services/service_api";

const style = {
  height: "100%",
};

function AclProtocol() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editAcl = route.pathname?.split("/")?.slice(2, 3) == "edit-acl";
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [weight, setWeight] = useState("");
  const [registrationNo, setRegistrationNo] = useState(userId);
  const [sport, setSport] = useState("");
  const [date, setDate] = useState("");
  const [injuryHistory, setInjuryHistory] = useState("");
  const [onExamination, setOnExamination] = useState("");

  const [singleHopRight, setSingleHopRight] = useState("");
  const [singleHopLeft, setSingleHopLeft] = useState("");
  const [singleHopPercent, setSingleHopPercent] = useState("");

  const [tripleHopRight, setTripleHopRight] = useState("");
  const [tripleHopLeft, setTripleHopLeft] = useState("");
  const [tripleHopPercent, setTripleHopPercent] = useState("");

  const [tripleCrossLeft, setTripleCrossLeft] = useState("");
  const [tripleCrossRight, setTripleCrossRight] = useState("");
  const [tripleCrossPercent, setTripleCrossPercent] = useState("");

  const [sideHopLeft, setSideHopLeft] = useState("");
  const [sideHopRight, setSideHopRight] = useState("");
  const [sideHopPercent, setSideHopPercent] = useState("");

  const [singleLegSquatLeft, setSingleLegSquatLeft] = useState("");
  const [singleLegSquatRight, setSingleLegSquatRight] = useState("");
  const [singleLegSquatPercent, setSingleLegSquatPercent] = useState("");

  const [excursionBalanceLeft, setExcursionBalanceLeft] = useState("");
  const [excursionBalanceRight, setExcursionBalanceRight] = useState("");
  const [excursionBalancePercent, setExcursionBalancePercent] = useState("");

  const [singleLegJumpLeft, setSingleLegJumpLeft] = useState("");
  const [singleLegJumpRight, setSingleLegJumpRight] = useState("");
  const [singleLegJumpPercent, setSingleLegJumpPercent] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    if (editAcl) {
      getAclUserData();
    } else {
      getMainUserData();
    }
  }, [editAcl]);

  async function getAclUserData() {
    const params = {
      id: id,
      registration_no: userId,
    };
    const response = await getAclSingleData(params);
    // console.log("get fms user data", response.data);
    const data = response.data;
    if (response) {
      setName(data.name);
      setAge(data.age);
      setWeight(data.weight);
      setSport(data.sport);
      setDate(data.date);
      setInjuryHistory(data.injury_history);
      setOnExamination(data.on_examination);
      setSingleHopRight(data.single_hop_test_right);
      setSingleHopLeft(data.single_hop_test_left);
      //   single_hop_test_percentile
      setTripleHopRight(data.triple_hop_right);
      setTripleHopLeft(data.triple_hop_left);
      //   triple_hop_percentile
      setTripleCrossLeft(data.triple_cross_over_hop_test_left);
      setTripleCrossRight(data.triple_cross_over_hop_test_right);
      //   triple_cross_over_hop_test_percentile
      setSideHopLeft(data.side_hop_test_right);
      setSideHopRight(data.side_hop_test_left);
      //   side_hop_test_percentile
      setSingleLegSquatLeft(data.single_leg_squat22bl_left);
      setSingleLegSquatRight(data.single_leg_squat22bl_right);
      //   single_leg_squat22bl_percentile
      setExcursionBalanceLeft(data.balance_star_excursion_left);
      setExcursionBalanceRight(data.balance_star_excursion_right);
      setSingleLegJumpLeft(data.single_leg_jump_left);
      setSingleLegJumpRight(data.single_leg_jump_right);
      //   single_leg_jump_percentile
    }
  }

  async function getMainUserData() {
    const params = {
      registration_no: userId,
    };
    // console.log("params", params);
    const response = await getSingleUserData(params);
    const data = response.data;
    // console.log("response------>", response.data);
    if (response) {
      setName(`${data?.first_name}${" "}${data?.last_name}`);
    }
  }

  async function createAclUser() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        name: name,
        age: age,
        weight: weight,
        sport: sport,
        date: date,
        injury_history: injuryHistory,
        on_examination: onExamination,
        single_hop_test_right: singleHopRight,
        single_hop_test_left: singleHopLeft,
        single_hop_test_percentile: singleHopPercent,
        triple_hop_right: tripleHopRight,
        triple_hop_left: tripleHopLeft,
        triple_hop_percentile: tripleHopPercent,
        triple_cross_over_hop_test_right: tripleCrossLeft,
        triple_cross_over_hop_test_left: tripleCrossRight,
        triple_cross_over_hop_test_percentile: tripleCrossPercent,
        side_hop_test_right: sideHopLeft,
        side_hop_test_left: sideHopRight,
        side_hop_test_percentile: sideHopPercent,
        single_leg_squat22bl_right: singleLegSquatLeft,
        single_leg_squat22bl_left: singleLegSquatRight,
        single_leg_squat22bl_percentile: singleLegSquatPercent,
        balance_star_excursion_right: excursionBalanceLeft,
        balance_star_excursion_left: excursionBalanceRight,
        balance_star_excursion_percentile: excursionBalancePercent,
        single_leg_jump_right: singleLegJumpLeft,
        single_leg_jump_left: singleLegJumpRight,
        single_leg_jump_percentile: singleLegJumpPercent,

        // balance_vestibular_balance_right:,
        // balance_vestibular_balance_left:,
        // balance_vestibular_balance_percentile:
      };
      // console.log("params===>", params);
      const response = await createAclProtocol(params);
      // console.log("response===>", response);
      if (response.status == true) {
        toast.success(response.message, toastConfig);
        navigate(`/all/forms/${userId}`);
        setName("");
        setAge("");
        setWeight("");
        setSport("");
        setDate("");
        setInjuryHistory("");
        setOnExamination("");
        setSingleHopRight("");
        setSingleHopLeft("");
        setTripleHopRight("");
        setTripleHopLeft("");
        setTripleCrossLeft("");
        setTripleCrossRight("");
        setSideHopLeft("");
        setSideHopRight("");
        setSingleLegSquatLeft("");
        setSingleLegSquatRight("");
        setExcursionBalanceLeft("");
        setExcursionBalanceRight("");
        setSingleLegJumpLeft("");
        setSingleLegJumpRight("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  async function updateAclUser() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        id: id,
        name: name,
        age: age,
        weight: weight,
        sport: sport,
        date: date,
        injury_history: injuryHistory,
        on_examination: onExamination,
        single_hop_test_right: singleHopRight,
        single_hop_test_left: singleHopLeft,
        single_hop_test_percentile: singleHopPercent,
        triple_hop_right: tripleHopRight,
        triple_hop_left: tripleHopLeft,
        triple_hop_percentile: tripleHopPercent,
        triple_cross_over_hop_test_right: tripleCrossLeft,
        triple_cross_over_hop_test_left: tripleCrossRight,
        triple_cross_over_hop_test_percentile: tripleCrossPercent,
        side_hop_test_right: sideHopLeft,
        side_hop_test_left: sideHopRight,
        side_hop_test_percentile: sideHopPercent,
        single_leg_squat22bl_right: singleLegSquatLeft,
        single_leg_squat22bl_left: singleLegSquatRight,
        single_leg_squat22bl_percentile: singleLegSquatPercent,
        balance_star_excursion_right: excursionBalanceLeft,
        balance_star_excursion_left: excursionBalanceRight,
        balance_star_excursion_percentile: excursionBalancePercent,
        single_leg_jump_right: singleLegJumpLeft,
        single_leg_jump_left: singleLegJumpRight,
        single_leg_jump_percentile: singleLegJumpPercent,

        // balance_vestibular_balance_right:,
        // balance_vestibular_balance_left:,
        // balance_vestibular_balance_percentile:
      };
      // console.log("params===>", params);
      const response = await updateAclProtocol(params);
      // console.log("response===>", response);
      if (response.status == true) {
        toast.success(response.message, toastConfig);
        navigate(`/all/forms/${userId}`);
        setName("");
        setAge("");
        setWeight("");
        setSport("");
        setDate("");
        setInjuryHistory("");
        setOnExamination("");
        setSingleHopRight("");
        setSingleHopLeft("");
        setTripleHopRight("");
        setTripleHopLeft("");
        setTripleCrossLeft("");
        setTripleCrossRight("");
        setSideHopLeft("");
        setSideHopRight("");
        setSingleLegSquatLeft("");
        setSingleLegSquatRight("");
        setExcursionBalanceLeft("");
        setExcursionBalanceRight("");
        setSingleLegJumpLeft("");
        setSingleLegJumpRight("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  function formValidation() {
    let msgName = "";
    let msgAge = "";
    let msgWeight = "";
    let msgRegistrationNo = "";
    let msgSport = "";
    let msgDate = "";
    // let msgInjuryHistory = "";
    // let msgOnExamination = "";

    let isValid = false;

    if (name == "") {
      msgName = "Please enter name";
    }

    if (age == "") {
      msgAge = "Please enter age";
    }

    if (weight === "") {
      msgWeight = "Please enter weight";
    }

    if (registrationNo === "") {
      msgRegistrationNo = "Please enter registration number";
    }

    if (sport === "") {
      msgSport = "Please select a sport";
    }

    if (date === "") {
      msgDate = "Please select a date";
    }

    // if (injuryHistory === "") {
    //   msgInjuryHistory = "Please provide injury history";
    // }

    // if (onExamination === "") {
    //     msgOnExamination = "Please provide injury history";
    //   }

    if (
      (!msgName, !msgAge, !msgWeight, !msgRegistrationNo, !msgSport, !msgDate)
      // !msgInjuryHistory,
      // !msgOnExamination
    ) {
      isValid = true;
    }

    if (isValid) {
      setError(true);
      setErrorMessage({
        name: "",
        age: "",
        weight: "",
        registrationNo: "",
        sport: "",
        date: "",
        // injuryHistory: "",
        // onExamination: "",
      });
      return true;
    } else {
      setError(true);
      setErrorMessage({
        name: msgName,
        age: msgAge,
        weight: msgWeight,
        registrationNo: msgRegistrationNo,
        sport: msgSport,
        date: msgDate,
        // injuryHistory: msgInjuryHistory,
        // onExamination: msgOnExamination,
      });
      return false;
    }
  }
  return (
    <>
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body1"
              color="text"
              ml={1}
              fontWeight="bold"
              fontSize={30}
            >
              ACL Rehabilitation Protocol
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Name
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="name"
                    placeholder="Name"
                    value={name}
                    id="name"
                    onChange={(e) => setName(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["name"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["name"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Age(years)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="age"
                    placeholder="Age"
                    value={age}
                    id="age"
                    onChange={(e) => setAge(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["age"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["age"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Weight(Kg)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="weight"
                    placeholder="Weight"
                    value={weight}
                    id="age"
                    onChange={(e) => setWeight(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["weight"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["weight"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Registration No
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="registrationNo"
                    placeholder="Registration No"
                    id="registrationNo"
                    value={registrationNo}
                    onChange={(e) => setRegistrationNo(e.target.value)}
                    disabled={true}
                  />
                </MDBox>
                {error && errorMessage["registrationNo"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["registrationNo"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Sport
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="sport"
                    placeholder="Sport"
                    value={sport}
                    id="sport"
                    onChange={(e) => setSport(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sport"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sport"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Date
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="date"
                    fullWidth
                    name="date"
                    placeholder="Date"
                    value={date}
                    id="date"
                    onChange={(e) => setDate(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["date"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["date"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" flexDirection="row" mt={1}>
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Injury history
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="injuryhistory"
                    placeholder="Injury history"
                    value={injuryHistory}
                    id="injuryHistory"
                    onChange={(e) => setInjuryHistory(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["injuryHistory"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["injuryHistory"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                On examination
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="onexamination"
                    placeholder="On examination"
                    value={onExamination}
                    id="onExamination"
                    onChange={(e) => setOnExamination(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["onExamination"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["onExamination"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>
            <MDBox mt={1} mb={0}>
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex">
                  <MDTypography
                    display="flex"
                    variant="body1"
                    color="text"
                    ml={1}
                    fontWeight="bold"
                    width="100%"
                    fontSize={18}
                  >
                    Outcome Measure
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex">
                  <MDTypography
                    display="flex"
                    variant="body1"
                    color="text"
                    ml={3}
                    fontWeight="bold"
                    width="100%"
                    fontSize={18}
                  >
                    Result (based on clearance criteria)
                  </MDTypography>
                </MDBox>
              </MDBox>
              {/* Single Hop Test */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={6} display="flex" width="30%">
                  {/* Single Hop Test */}
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Single Hop Test
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    mb={2}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Right
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="number"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={singleHopRight}
                        id="singleHopRight"
                        onChange={(e) => setSingleHopRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleHopRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleHopRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    mb={2}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Left
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="number"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={singleHopLeft}
                        id="singleHopLeft"
                        onChange={(e) => setSingleHopLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleHopLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleHopLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    mb={2}
                    ml={1}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Percentile Difference &GT; 95% compared with other side
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="number"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        disabled
                        value={singleHopPercent}
                        id="singleHopPercent"
                        onChange={(e) => setSingleHopPercent(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleHopPercent"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleHopPercent"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Triple Hop */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    pt={1}
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Triple Hop
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Right</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="number"
                        fullWidth
                        name="LeftLegHeight1"
                        // name="Triple Hop"
                        placeholder=""
                        value={tripleHopRight}
                        id="age"
                        onChange={(e) => setTripleHopRight(e.target.value)}
                      />
                      {error && errorMessage["tripleHopRight"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["tripleHopRight"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDBox>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Left</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="number"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={tripleHopLeft}
                        id="age"
                        onChange={(e) => setTripleHopLeft(e.target.value)}
                      />
                      {error && errorMessage["tripleHopLeft"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["tripleHopLeft"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDBox>
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Percentile Difference > 95% compared with other side</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="number"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        value={tripleHopPercent}
                        id="age"
                        onChange={(e) => setTripleHopPercent(e.target.value)}
                        disabled
                      />
                      {error && errorMessage["tripleHopPercent"] ? (
                        <div style={{ color: "red", fontSize: "14px" }}>
                          {errorMessage["tripleHopPercent"]}
                        </div>
                      ) : (
                        ""
                      )}
                    </MDBox>
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Triple Cross Over Hop Test */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" mt={1} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Triple Cross Over Hop Test
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Right</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={tripleCrossRight}
                        id="age"
                        onChange={(e) => setTripleCrossRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["tripleCrossRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["tripleCrossRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Left</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={tripleCrossLeft}
                        id="age"
                        onChange={(e) => setTripleCrossLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["tripleCrossLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["tripleCrossLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Percentile Difference > 95% compared with other side</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        value={tripleCrossPercent}
                        id="age"
                        onChange={(e) => setTripleCrossPercent(e.target.value)}
                      />
                    </MDBox>

                    {error && errorMessage["tripleCrossPercent"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["tripleCrossPercent"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Side Hop Test */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" mt={1} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Side Hop Test
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Right</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={sideHopRight}
                        id="age"
                        onChange={(e) => setSideHopRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["sideHopRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["sideHopRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Left</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={sideHopLeft}
                        id="age"
                        onChange={(e) => setSideHopLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["sideHopLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["sideHopLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Percentile Difference > 95% compared with other side</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        value={sideHopPercent}
                        id="age"
                        onChange={(e) => setSideHopPercent(e.target.value)}
                      />
                    </MDBox>

                    {error && errorMessage["sideHopPercent"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["sideHopPercent"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Single Leg Squat >22 B/L */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" mt={1} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Single Leg Squat &gt;22 B/L
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Right</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={singleLegSquatRight}
                        id="age"
                        onChange={(e) => setSingleLegSquatRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleLegSquatRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleLegSquatRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Left</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={singleLegSquatLeft}
                        id="age"
                        onChange={(e) => setSingleLegSquatLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleLegSquatLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleLegSquatLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Percentile Difference > 95% compared with other side</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        value={singleLegSquatPercent}
                        id="age"
                        onChange={(e) =>
                          setSingleLegSquatPercent(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["singleLegSquatPercent"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleLegSquatPercent"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Balance (Star Excursion Balance) */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" mt={1} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Balance (Star Excursion Balance)
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Right</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={excursionBalanceRight}
                        id="age"
                        onChange={(e) =>
                          setExcursionBalanceRight(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["excursionBalanceRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["excursionBalanceRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Left</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={excursionBalanceLeft}
                        id="age"
                        onChange={(e) =>
                          setExcursionBalanceLeft(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["excursionBalanceLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["excursionBalanceLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Percentile Difference > 95% compared with other side</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        value={excursionBalancePercent}
                        id="age"
                        onChange={(e) =>
                          setExcursionBalancePercent(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["excursionBalancePercent"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["excursionBalancePercent"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Single Leg Jump */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" mt={1} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    Single Leg Jump
                  </MDTypography>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Right</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={singleLegJumpRight}
                        id="age"
                        onChange={(e) => setSingleLegJumpRight(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleLegJumpRight"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleLegJumpRight"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Left</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                        value={singleLegJumpLeft}
                        id="age"
                        onChange={(e) => setSingleLegJumpLeft(e.target.value)}
                      />
                    </MDBox>
                    {error && errorMessage["singleLegJumpLeft"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleLegJumpLeft"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
                <MDBox flexDirection="column" display="flex" width="50%">
                  {/* <MDTypography variant="body2" color="text"  ml={1} fontWeight="regular" fontSize={16}>Percentile Difference > 95% compared with other side</MDTypography> */}
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="RightLegHeightm1"
                        placeholder=""
                        value={singleLegJumpPercent}
                        id="age"
                        onChange={(e) =>
                          setSingleLegJumpPercent(e.target.value)
                        }
                      />
                    </MDBox>
                    {error && errorMessage["singleLegJumpPercent"] ? (
                      <div style={{ color: "red", fontSize: "14px" }}>
                        {errorMessage["singleLegJumpPercent"]}
                      </div>
                    ) : (
                      ""
                    )}
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* Balance (Vestibular Balance) */}
              <MDBox display="flex" flexDirection="row">
                <MDBox flexDirection="column" pt={6} display="flex" width="30%">
                  <MDTypography
                    variant="body2"
                    color="text"
                    ml={1}
                    fontWeight="regular"
                    fontSize={16}
                  >
                    {" "}
                    Balance (Vestibular Balance)
                  </MDTypography>
                </MDBox>
                {/* balance Side to side */}
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    mb={2}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Side to side
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={1}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                      />
                    </MDBox>
                  </MDBox>
                </MDBox>
                {/* balance up and down */}
                <MDBox flexDirection="column" display="flex" width="50%">
                  <MDTypography
                    variant="body1"
                    color="text"
                    ml={1}
                    mb={2}
                    fontWeight="bold"
                    fontSize={16}
                  >
                    Up and Down
                  </MDTypography>
                  <MDBox display="flex" flexDirection="row" ml={1}>
                    <MDBox
                      mb={1}
                      mr={2}
                      flexDirection="column"
                      display="flex"
                      width="100%"
                    >
                      <MDInput
                        type="text"
                        fullWidth
                        name="LeftLegHeight1"
                        placeholder=""
                      />
                    </MDBox>
                  </MDBox>
                </MDBox>
              </MDBox>
              {/* submit button */}
              <MDBox mt={3} display="flex" justifyContent="flex-end">
                {/* <Link to={"/all/forms"}> */}
                {(editAcl && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={updateAclUser}
                  >
                    Update
                  </MDButton>
                )) || (
                  <MDButton
                    variant="gradient"
                    color="info"
                    type="button"
                    onClick={createAclUser}
                  >
                    Submit
                  </MDButton>
                )}
                {/* </Link> */}
              </MDBox>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default AclProtocol;
