import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  RadioGroup,
  Select,
  TextareaAutosize,
} from "@mui/material";
import { toast } from "react-toastify";
import {
  createAclProtocol,
  getAclByRGst,
  getAclSingleData,
  getSingleUserData,
  updateAclProtocol,
} from "../../../services/service_api";
import { CheckBox } from "@mui/icons-material";

const style = {
  height: "100%",
};

function AclStep2() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editAcl = route.pathname?.split("/")?.slice(2, 3) == "edit-acl";
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [weight, setWeight] = useState("");
  const [registrationNo, setRegistrationNo] = useState(userId);
  const [sport, setSport] = useState("");
  const [date, setDate] = useState("");

  const [passiveKneeExtensionRight, setPassiveKneeExtensionRight] =
    useState("");
  const [passiveKneeExtensionLeft, setPassiveKneeExtensionLeft] = useState("");
  const [passiveKneeExtensionLsi, setPassiveKneeExtensionLsi] = useState("");

  const [passiveKneeFlexionRight, setPassiveKneeFlexionRight] = useState("");
  const [passiveKneeFlexionLeft, setPassiveKneeFlexionLeft] = useState("");
  const [passiveKneeFlexionLsi, setPassiveKneeFlexionLsi] = useState("");

  const [swellingEffusionRight, setSwellingEffusionRight] = useState("");
  const [swellingEffusionLeft, setSwellingEffusionLeft] = useState("");
  const [swellingEffusionLsi, setSwellingEffusionLsi] = useState("");

  const [functionalAlignmentTestRight, setFunctionalAlignmentTestRight] =
    useState("");
  const [functionalAlignmentTestLeft, setFunctionalAlignmentTestLeft] =
    useState("");
  const [functionalAlignmentTestLsi, setFunctionalAlignmentTestLsi] =
    useState("");

  const [singleLegBridgesRight, setSingleLegBridgesRight] = useState("");
  const [singleLegBridgesLeft, setSingleLegBridgesLeft] = useState("");
  const [singleLegBridgesLsi, setSingleLegBridgesLsi] = useState("");

  const [singleLegCalfRaisesRight, setSingleLegCalfRaisesRight] = useState("");
  const [singleLegCalfRaisesLeft, setSingleLegCalfRaisesLeft] = useState("");
  const [singleLegCalfRaisesLsi, setSingleLegCalfRaisesLsi] = useState("");

  const [sideBridgeEnduranceRight, setSideBridgeEnduranceRight] = useState("");
  const [sideBridgeEnduranceLeft, setSideBridgeEnduranceLeft] = useState("");
  const [sideBridgeEnduranceLsi, setSideBridgeEnduranceLsi] = useState("");

  const [singleLegSquatRight, setSingleLegSquatRight] = useState("");
  const [singleLegSquatLeft, setSingleLegSquatLeft] = useState("");
  const [singleLegSquatLsi, setSingleLegSquatLsi] = useState("");

  const [balanceEO43secRight, setBalanceEO43secRight] = useState("");
  const [balanceEO43secLeft, setBalanceEO43secLeft] = useState("");
  const [balanceEO43secLsi, setBalanceEO43secLsi] = useState("");

  const [balanceEc9secRight, setBalanceEc9secRight] = useState("");
  const [balanceEc9secLeft, setBalanceEc9secLeft] = useState("");
  const [balanceEc9secLsi, setBalanceEc9secLsi] = useState("");

  const [backSquatRight, setBackSquatRight] = useState("");
  const [backSquatLeft, setBackSquatLeft] = useState("");
  const [backSquatLsi, setBackSquatLsi] = useState("");

  const [singleLegBridge, setSingleLegBridge] = useState("");
  const [singleLegBridge20, setSingleLegBridge20] = useState("");
  const [calfRaises85, setCalfRaises85] = useState("");
  const [calfRaises20, setCalfRaises20] = useState("");
  const [sideBridge85, setSideBridge85] = useState("");
  const [sideBridge30, setSideBridge30] = useState("");
  const [singleLegSquat85, setSingleLegSquat85] = useState("");
  const [singleLegSquat10, setSingleLegSquat10] = useState("");
  const [balanceEO43, setBalanceEO43] = useState("");
  const [balanceEc9, setBalanceEc9] = useState("");
  const [backSquat15, setBackSquat15] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    if (editAcl) {
      getAclUserData();
    } else {
      getMainUserData();
    }
  }, [editAcl]);

  async function getAclUserData() {
    const params = {
      id: id,
      registration_no: userId,
    };
    const response = await getAclSingleData(params);
    // console.log("get fms user data", response.data);
    const data = response.data;
    if (response) {
      setName(data.name);
      setAge(data.age);
      setWeight(data.weight);
      setSport(data.sport);
      setDate(data.date);
    }
  }

  async function getMainUserData() {
    const params = {
      registration_no: userId,
    };
    // console.log("params", params);
    const response = await getSingleUserData(params);
    const data = response.data;
    // console.log("response------>", response.data);
    if (response) {
      setName(`${data?.first_name}${" "}${data?.last_name}`);
    }
  }

  async function createAclUser() {
    // if (formValidation()) {
    const params = {
      registration_no: registrationNo,
      name: name,
      age: age,
      weight: weight,
      sport: sport,
      date: date,
    };
    // console.log("params===>", params);
    const response = await createAclProtocol(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/all/forms/${userId}`);
      setName("");
      setAge("");
      setWeight("");
      setSport("");
      setDate("");
    } else {
      toast.error(response.message, toastConfig);
    }
    // }
  }

  async function updateAclUser() {
    // if (formValidation()) {
    const params = {
      registration_no: registrationNo,
      id: id,
      name: name,
      age: age,
      weight: weight,
      sport: sport,
      date: date,
    };
    // console.log("params===>", params);
    const response = await updateAclProtocol(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/all/forms/${userId}`);
      setName("");
      setAge("");
      setWeight("");
      setSport("");
      setDate("");
    } else {
      toast.error(response.message, toastConfig);
    }
    // }
  }

  //   function formValidation() {
  //     let msgName = "";
  //     let msgAge = "";
  //     let msgWeight = "";
  //     let msgRegistrationNo = "";
  //     let msgSport = "";
  //     let msgDate = "";
  //     let isValid = false;

  //     if (name == "") {
  //       msgName = "Please enter name";
  //     }

  //     if (age == "") {
  //       msgAge = "Please enter age";
  //     }

  //     if (weight === "") {
  //       msgWeight = "Please enter weight";
  //     }

  //     if (registrationNo === "") {
  //       msgRegistrationNo = "Please enter registration number";
  //     }

  //     if (sport === "") {
  //       msgSport = "Please select a sport";
  //     }

  //     if (date === "") {
  //       msgDate = "Please select a date";
  //     }

  //     if (
  //       (!msgName, !msgAge, !msgWeight, !msgRegistrationNo, !msgSport, !msgDate)
  //     ) {
  //       isValid = true;
  //     }

  //     if (isValid) {
  //       setError(true);
  //       setErrorMessage({
  //         name: "",
  //         age: "",
  //         weight: "",
  //         registrationNo: "",
  //         sport: "",
  //         date: "",
  //       });
  //       return true;
  //     } else {
  //       setError(true);
  //       setErrorMessage({
  //         name: msgName,
  //         age: msgAge,
  //         weight: msgWeight,
  //         registrationNo: msgRegistrationNo,
  //         sport: msgSport,
  //         date: msgDate,
  //       });
  //       return false;
  //     }
  //   }
  return (
    <>
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body1"
              color="text"
              ml={1}
              fontWeight="bold"
              fontSize={30}
            >
              ACL Rehabilitation Protocol
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            {/* Strength & Neuromuscular control */}
            <MDBox display="flex" flexDirection="row">
              <MDTypography
                variant="body2"
                color="text"
                pb={1}
                fontWeight="bold"
                fontSize={18}
              >
                Strength & Neuromuscular control
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Right
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Left
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  LSI
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Passive knee extension (equal to the other leg) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Passive knee extension (equal to the other leg)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={passiveKneeExtensionRight}
                      id="passiveKneeExtensionRight"
                      onChange={(e) =>
                        setPassiveKneeExtensionRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeExtensionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeExtensionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={passiveKneeExtensionLeft}
                      id="passiveKneeExtensionLeft"
                      onChange={(e) =>
                        setPassiveKneeExtensionLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeExtensionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeExtensionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={passiveKneeExtensionLsi}
                      id="passiveKneeExtensionLsi"
                      onChange={(e) =>
                        setPassiveKneeExtensionLsi(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeExtensionLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeExtensionLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Passive knee flexion (125+ degrees) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Passive knee flexion (125+ degrees)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={passiveKneeFlexionRight}
                      id="passiveKneeFlexionRight"
                      onChange={(e) =>
                        setPassiveKneeFlexionRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeFlexionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeFlexionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={passiveKneeFlexionLeft}
                      id="passiveKneeFlexionLeft"
                      onChange={(e) =>
                        setPassiveKneeFlexionLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeFlexionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeFlexionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={passiveKneeFlexionLsi}
                      id="passiveKneeFlexionLsi"
                      onChange={(e) => setPassiveKneeFlexionLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeFlexionLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeFlexionLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Swelling/effusion (zero) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Swelling/effusion (zero)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={swellingEffusionRight}
                      id="swellingEffusionRight"
                      onChange={(e) => setSwellingEffusionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["swellingEffusionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["swellingEffusionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={swellingEffusionLeft}
                      id="swellingEffusionLeft"
                      onChange={(e) => setSwellingEffusionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["swellingEffusionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["swellingEffusionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={swellingEffusionLsi}
                      id="swellingEffusionLsi"
                      onChange={(e) => setSwellingEffusionLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["swellingEffusionLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["swellingEffusionLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Functional Alignment test */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Functional Alignment test
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={functionalAlignmentTestRight}
                      id="functionalAlignmentTestRight"
                      onChange={(e) =>
                        setFunctionalAlignmentTestRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["functionalAlignmentTestRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["functionalAlignmentTestRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={functionalAlignmentTestLeft}
                      id="functionalAlignmentTestLeft"
                      onChange={(e) =>
                        setFunctionalAlignmentTestLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["functionalAlignmentTestLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["functionalAlignmentTestLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={functionalAlignmentTestLsi}
                      id="functionalAlignmentTestLsi"
                      onChange={(e) =>
                        setFunctionalAlignmentTestLsi(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["functionalAlignmentTestLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["functionalAlignmentTestLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Single leg bridges (24 inches, 20 degrees, max reps)
              (Hurdle= >20
              repetitions) (full range, standardized height) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single leg bridges (24 inches, 20 degrees, max reps) (Hurdle=
                  &gt;20 repetitions) (full range, standardized height)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={singleLegBridgesRight}
                      id="singleLegBridgesRight"
                      onChange={(e) => setSingleLegBridgesRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegBridgesRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegBridgesRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={singleLegBridgesLeft}
                      id="singleLegBridgesLeft"
                      onChange={(e) => setSingleLegBridgesLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegBridgesLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegBridgesLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={singleLegBridgesLsi}
                      id="singleLegBridgesLsi"
                      onChange={(e) => setSingleLegBridgesLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegBridgesLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegBridgesLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Single leg calf raises (max reps)
              (Hurdle= >20
              repetitions) (metronome 60bpm, 1:1sec) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single leg calf raises (max reps) (Hurdle= &gt;20 repetitions)
                  (metronome 60bpm, 1:1sec)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={singleLegCalfRaisesRight}
                      id="singleLegCalfRaisesRight"
                      onChange={(e) =>
                        setSingleLegCalfRaisesRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["singleLegCalfRaisesRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegCalfRaisesRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={singleLegCalfRaisesLeft}
                      id="singleLegCalfRaisesLeft"
                      onChange={(e) =>
                        setSingleLegCalfRaisesLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["singleLegCalfRaisesLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegCalfRaisesLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={singleLegCalfRaisesLsi}
                      id="singleLegCalfRaisesLsi"
                      onChange={(e) =>
                        setSingleLegCalfRaisesLsi(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["singleLegCalfRaisesLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegCalfRaisesLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Side Bridge Endurance (max time)
              (Hurdle= 30 sec) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Side Bridge Endurance (max time) (Hurdle= 30 sec)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={sideBridgeEnduranceRight}
                      id="sideBridgeEnduranceRight"
                      onChange={(e) =>
                        setSideBridgeEnduranceRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["sideBridgeEnduranceRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sideBridgeEnduranceRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={sideBridgeEnduranceLeft}
                      id="sideBridgeEnduranceLeft"
                      onChange={(e) =>
                        setSideBridgeEnduranceLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["sideBridgeEnduranceLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sideBridgeEnduranceLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={sideBridgeEnduranceLsi}
                      id="sideBridgeEnduranceLsi"
                      onChange={(e) =>
                        setSideBridgeEnduranceLsi(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["sideBridgeEnduranceLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["sideBridgeEnduranceLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Single Leg Squat (90 degree, max reps)
                  (Hurdle= >10) (metronome 30 bpm, 2:2 sec), arms across chest */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Squat (90 degree, max reps) (Hurdle= &gt;10)
                  (metronome 30 bpm, 2:2 sec), arms across chest
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={singleLegSquatRight}
                      id="singleLegSquatRight"
                      onChange={(e) => setSingleLegSquatRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegSquatRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegSquatRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={singleLegSquatLeft}
                      id="singleLegSquatLeft"
                      onChange={(e) => setSingleLegSquatLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegSquatLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegSquatLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={singleLegSquatLsi}
                      id="singleLegSquatLsi"
                      onChange={(e) => setSingleLegSquatLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegSquatLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegSquatLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Balance (EO= 43 sec) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Balance (EO= 43 sec)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={balanceEO43secRight}
                      id="balanceEO43secRight"
                      onChange={(e) => setBalanceEO43secRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["balanceEO43secRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["balanceEO43secRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={balanceEO43secLeft}
                      id="balanceEO43secLeft"
                      onChange={(e) => setBalanceEO43secLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["balanceEO43secLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["balanceEO43secLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={balanceEO43secLsi}
                      id="balanceEO43secLsi"
                      onChange={(e) => setBalanceEO43secLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["balanceEO43secLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["balanceEO43secLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Balance (EC= 9 sec) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Balance (EC= 9 sec)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={balanceEc9secRight}
                      id="balanceEc9secRight"
                      onChange={(e) => setBalanceEc9secRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["balanceEc9secRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["balanceEc9secRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={balanceEc9secLeft}
                      id="balanceEc9secLeft"
                      onChange={(e) => setBalanceEc9secLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["balanceEc9secLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["balanceEc9secLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={balanceEc9secLsi}
                      id="balanceEc9secLsi"
                      onChange={(e) => setBalanceEc9secLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["balanceEc9secLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["balanceEc9secLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Back Squat (kgs, 90 degrees, 1.5x Bodyweight) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Back Squat (kgs, 90 degrees, 1.5x Bodyweight)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={backSquatRight}
                      id="backSquatRight"
                      onChange={(e) => setBackSquatRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["backSquatRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["backSquatRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={backSquatLeft}
                      id="backSquatLeft"
                      onChange={(e) => setBackSquatLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["backSquatLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["backSquatLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="25%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="LSI"
                      value={backSquatLsi}
                      id="backSquatLsi"
                      onChange={(e) => setBackSquatLsi(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["backSquatLsi"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["backSquatLsi"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Outcomes met? */}
            <MDBox display="flex" flexDirection="row" mt={2}>
              <MDTypography
                variant="body2"
                color="text"
                fontWeight="bold"
                fontSize={18}
              >
                Outcomes met?
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Yes/No
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Single Leg Bridge >85% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Bridge &gt;85%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegBridge == "yes"}
                      onChange={() => setSingleLegBridge("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegBridge == "no"}
                      onChange={() => setSingleLegBridge("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg Bridge 20 rep Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Bridge 20 rep Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegBridge20 == "yes"}
                      onChange={() => setSingleLegBridge20("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegBridge20 == "no"}
                      onChange={() => setSingleLegBridge20("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Calf Raises >85% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Calf Raises &gt;85%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={calfRaises85 == "yes"}
                      onChange={() => setCalfRaises85("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={calfRaises85 == "no"}
                      onChange={() => setCalfRaises85("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Calf Raises 20 rep Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Calf Raises 20 rep Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={calfRaises20 == "yes"}
                      onChange={() => setCalfRaises20("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={calfRaises20 == "no"}
                      onChange={() => setCalfRaises20("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Side Bridge >85% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Side Bridge &gt;85%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={sideBridge85 == "yes"}
                      onChange={() => setSideBridge85("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={sideBridge85 == "no"}
                      onChange={() => setSideBridge85("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Side Bridge 30 sec Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Side Bridge 30 sec Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={sideBridge30 == "yes"}
                      onChange={() => setSideBridge30("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={sideBridge30 == "no"}
                      onChange={() => setSideBridge30("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg Squat >85% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Squat &gt;85%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegSquat85 == "yes"}
                      onChange={() => setSingleLegSquat85("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegSquat85 == "no"}
                      onChange={() => setSingleLegSquat85("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg Squat 10 rep Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Squat 10 rep Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegSquat10 == "yes"}
                      onChange={() => setSingleLegSquat10("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegSquat10 == "no"}
                      onChange={() => setSingleLegSquat10("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Balance EO 43 second Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Bridge &gt;85%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={balanceEO43 == "yes"}
                      onChange={() => setBalanceEO43("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={balanceEO43 == "no"}
                      onChange={() => setBalanceEO43("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Balance EC 9 second Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Balance EC 9 second Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={balanceEc9 == "yes"}
                      onChange={() => setBalanceEc9("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={balanceEc9 == "no"}
                      onChange={() => setBalanceEc9("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Back Squat 1.5x BW Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Back Squat 1.5x BW Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={backSquat15 == "yes"}
                      onChange={() => setBackSquat15("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={backSquat15 == "no"}
                      onChange={() => setBackSquat15("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* submit button */}
            <MDBox mt={3} display="flex" justifyContent="flex-end">
              {/* <Link to={"/all/forms"}> */}
              {(editAcl && (
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={updateAclUser}
                >
                  Update
                </MDButton>
              )) || (
                <MDButton
                  variant="gradient"
                  color="info"
                  type="button"
                  onClick={() =>
                    navigate("/forms/AclRehabilitationProtocolStep3")
                  }
                >
                  Next / Save
                </MDButton>
              )}
              {/* </Link> */}
            </MDBox>
            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step1 / Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default AclStep2;
