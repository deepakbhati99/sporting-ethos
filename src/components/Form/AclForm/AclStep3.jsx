import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  RadioGroup,
  Select,
  TextareaAutosize,
} from "@mui/material";
import { toast } from "react-toastify";
import {
  createAclProtocol,
  getAclByRGst,
  getAclSingleData,
  getSingleUserData,
  updateAclProtocol,
} from "../../../services/service_api";
import { CheckBox } from "@mui/icons-material";

const style = {
  height: "100%",
};

function AclStep3() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editAcl = route.pathname?.split("/")?.slice(2, 3) == "edit-acl";
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [weight, setWeight] = useState("");
  const [registrationNo, setRegistrationNo] = useState(userId);
  const [sport, setSport] = useState("");
  const [date, setDate] = useState("");

  // Single Leg Hop Test
  const [singleLegHopTest1, setSingleLegHopTest1] = useState("");
  const [singleLegHopTest2, setSingleLegHopTest2] = useState("");
  const [singleLegHopTest3, setSingleLegHopTest3] = useState("");
  const [singleLegHopTest4, setSingleLegHopTest4] = useState("");
  const [singleLegHopTest5, setSingleLegHopTest5] = useState("");

  // Triple Hop Test
  const [tripleHopTest1, setTripleHopTest1] = useState("");
  const [tripleHopTest2, setTripleHopTest2] = useState("");
  const [tripleHopTest3, setTripleHopTest3] = useState("");
  const [tripleHopTest4, setTripleHopTest4] = useState("");
  const [tripleHopTest5, setTripleHopTest5] = useState("");

  // Triple Cross Over Hop Test (6 m long, 15 cm wide)
  const [tripleCrossOver1, setTripleCrossOver1] = useState("");
  const [tripleCrossOver2, setTripleCrossOver2] = useState("");
  const [tripleCrossOver3, setTripleCrossOver3] = useState("");
  const [tripleCrossOver4, setTripleCrossOver4] = useState("");
  const [tripleCrossOver5, setTripleCrossOver5] = useState("");

  const [singleLegHopTest95, setSingleLegHopTest95] = useState("");
  const [singleLegBridge20, setSingleLegBridge20] = useState("");
  const [tripleCross, setTripleCross] = useState("");
  const [singleLegCounter95, setSingleLegCounter95] = useState("");
  const [balanceStarExcursion95, setBalanceStarExcursion95] = useState("");
  const [balanceDynamicUpDown, setBalanceDynamicUpDown] = useState("");
  const [balanceDynamicPSide, setBalanceDynamicPSide] = useState("");
  const [singleLegSquatTest, setSingleLegSquatTest] = useState("");
  const [singleLegPress18, setSingleLegPress18] = useState("");
  const [squat18Body, setSquat18Body] = useState("");
  const [backSquat15, setBackSquat15] = useState("");
  const [sideHopTest40cm, setSideHopTest40cm] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    if (editAcl) {
      getAclUserData();
    } else {
      getMainUserData();
    }
  }, [editAcl]);

  async function getAclUserData() {
    const params = {
      id: id,
      registration_no: userId,
    };
    const response = await getAclSingleData(params);
    // console.log("get fms user data", response.data);
    const data = response.data;
    if (response) {
      setName(data.name);
      setAge(data.age);
      setWeight(data.weight);
      setSport(data.sport);
      setDate(data.date);
    }
  }

  async function getMainUserData() {
    const params = {
      registration_no: userId,
    };
    // console.log("params", params);
    const response = await getSingleUserData(params);
    const data = response.data;
    // console.log("response------>", response.data);
    if (response) {
      setName(`${data?.first_name}${" "}${data?.last_name}`);
    }
  }

  async function createAclUser() {
    // if (formValidation()) {
    const params = {
      registration_no: registrationNo,
      name: name,
      age: age,
      weight: weight,
      sport: sport,
      date: date,
    };
    // console.log("params===>", params);
    const response = await createAclProtocol(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/all/forms/${userId}`);
      setName("");
      setAge("");
      setWeight("");
      setSport("");
      setDate("");
    } else {
      toast.error(response.message, toastConfig);
    }
    // }
  }

  async function updateAclUser() {
    // if (formValidation()) {
    const params = {
      registration_no: registrationNo,
      id: id,
      name: name,
      age: age,
      weight: weight,
      sport: sport,
      date: date,
    };
    // console.log("params===>", params);
    const response = await updateAclProtocol(params);
    // console.log("response===>", response);
    if (response.status == true) {
      toast.success(response.message, toastConfig);
      navigate(`/all/forms/${userId}`);
      setName("");
      setAge("");
      setWeight("");
      setSport("");
      setDate("");
    } else {
      toast.error(response.message, toastConfig);
    }
    // }
  }

  return (
    <>
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body1"
              color="text"
              ml={1}
              fontWeight="bold"
              fontSize={30}
            >
              ACL Rehabilitation Protocol
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            {/*  Running, Agility, Landings */}
            <MDBox display="flex" flexDirection="row">
              <MDTypography
                variant="body2"
                color="text"
                pb={1}
                fontWeight="bold"
                fontSize={18}
              >
                Running, Agility, Landings
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Injured Leg Attempt 1
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Uninjured Leg Attempt 1
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Injured Leg Attempt 2Limb Symmetry Index (Involved Mean
                  cm/Un-involved mean cm x 100)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Uninjured Leg Attempt 1
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Limb Symmetry Index (Involved Mean cm/Un-involved mean cm x
                  100)
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Single Leg Hop Test */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Hop Test
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="1"
                      value={singleLegHopTest1}
                      id="singleLegHopTest1"
                      onChange={(e) => setSingleLegHopTest1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegHopTest1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegHopTest1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="2"
                      value={singleLegHopTest2}
                      id="singleLegHopTest2"
                      onChange={(e) => setSingleLegHopTest2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegHopTest2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegHopTest2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="3"
                      value={singleLegHopTest3}
                      id="singleLegHopTest3"
                      onChange={(e) => setSingleLegHopTest3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegHopTest3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegHopTest3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="4"
                      value={singleLegHopTest4}
                      id="singleLegHopTest4"
                      onChange={(e) => setSingleLegHopTest4(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegHopTest4"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegHopTest4"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="5"
                      value={singleLegHopTest5}
                      id="singleLegHopTest5"
                      onChange={(e) => setSingleLegHopTest5(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["singleLegHopTest5"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["singleLegHopTest5"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Triple Hop Test */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Triple Hop Test
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="1"
                      value={tripleHopTest1}
                      id="tripleHopTest1"
                      onChange={(e) => setTripleHopTest1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleHopTest1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleHopTest1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="2"
                      value={tripleHopTest2}
                      id="tripleHopTest2"
                      onChange={(e) => setTripleHopTest2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleHopTest2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleHopTest2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="3"
                      value={tripleHopTest3}
                      id="tripleHopTest3"
                      onChange={(e) => setTripleHopTest3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleHopTest3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleHopTest3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="4"
                      value={tripleHopTest4}
                      id="tripleHopTest4"
                      onChange={(e) => setTripleHopTest4(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleHopTest4"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleHopTest4"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="5"
                      value={tripleHopTest5}
                      id="tripleHopTest5"
                      onChange={(e) => setTripleHopTest5(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleHopTest5"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleHopTest5"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Triple Cross Over Hop Test (6 m long, 15 cm wide) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Triple Cross Over Hop Test (6 m long, 15 cm wide)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="1"
                      value={tripleCrossOver1}
                      id="tripleCrossOver1"
                      onChange={(e) => setTripleCrossOver1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="2"
                      value={tripleCrossOver2}
                      id="tripleCrossOver2"
                      onChange={(e) => setTripleCrossOver2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="3"
                      value={tripleCrossOver3}
                      id="tripleCrossOver3"
                      onChange={(e) => setTripleCrossOver3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="4"
                      value={tripleCrossOver4}
                      id="tripleCrossOver4"
                      onChange={(e) => setTripleCrossOver4(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver4"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver4"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="5"
                      value={tripleCrossOver5}
                      id="tripleCrossOver5"
                      onChange={(e) => setTripleCrossOver5(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver5"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver5"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Side Hop Test (40 cm apart, 30 sec) */}
            {/* <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Side Hop Test (40 cm apart, 30 sec)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="1"
                      value={tripleCrossOver1}
                      id="tripleCrossOver1"
                      onChange={(e) => setTripleCrossOver1(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver1"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver1"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="2"
                      value={tripleCrossOver2}
                      id="tripleCrossOver2"
                      onChange={(e) => setTripleCrossOver2(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver2"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver2"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="3"
                      value={tripleCrossOver3}
                      id="tripleCrossOver3"
                      onChange={(e) => setTripleCrossOver3(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver3"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver3"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="4"
                      value={tripleCrossOver4}
                      id="tripleCrossOver4"
                      onChange={(e) => setTripleCrossOver4(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver4"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver4"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="16%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="5"
                      value={tripleCrossOver5}
                      id="tripleCrossOver5"
                      onChange={(e) => setTripleCrossOver5(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["tripleCrossOver5"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["tripleCrossOver5"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox> */}

            {/* Outcomes met? */}
            <MDBox display="flex" flexDirection="row" mt={2}>
              <MDTypography
                variant="body2"
                color="text"
                fontWeight="bold"
                fontSize={18}
              >
                Outcomes met?
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Yes/No
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Single Leg Hop Test > 95% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Hop Test &gt; 95%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegHopTest95 == "yes"}
                      onChange={() => setSingleLegHopTest95("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegHopTest95 == "no"}
                      onChange={() => setSingleLegHopTest95("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg Bridge 20 rep Hurdle Triple Hop Test > 95% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Bridge 20 rep Hurdle Triple Hop Test &gt; 95%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegBridge20 == "yes"}
                      onChange={() => setSingleLegBridge20("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegBridge20 == "no"}
                      onChange={() => setSingleLegBridge20("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Triple Cross
                Over Hop
                Test (6 m long, 15 cm wide) >95% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Triple Cross Over Hop Test (6 m long, 15 cm wide) &gt;95%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={tripleCross == "yes"}
                      onChange={() => setTripleCross("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={tripleCross == "no"}
                      onChange={() => setTripleCross("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg Counter Movement Jump* (Hands on Hips) > 95% */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Counter Movement Jump* (Hands on Hips) &gt; 95%
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegCounter95 == "yes"}
                      onChange={() => setSingleLegCounter95("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegCounter95 == "no"}
                      onChange={() => setSingleLegCounter95("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Balance: Star Excursion
                (Dynamic- >95%
                compared with
                other side) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Balance: Star Excursion (Dynamic- &gt;95% compared with other
                  side)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={balanceStarExcursion95 == "yes"}
                      onChange={() => setBalanceStarExcursion95("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={balanceStarExcursion95 == "no"}
                      onChange={() => setBalanceStarExcursion95("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Balance
                (Dynamic-Up-Down Pass
                both
                limbs) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Balance (Dynamic-Up-Down Pass both limbs)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={balanceDynamicUpDown == "yes"}
                      onChange={() => setBalanceDynamicUpDown("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={balanceDynamicUpDown == "no"}
                      onChange={() => setBalanceDynamicUpDown("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Balance
                (Dynamic-P Side to Side

                Pass both
                limbs) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Balance (Dynamic-P Side to Side Pass both limbs)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={balanceDynamicPSide == "yes"}
                      onChange={() => setBalanceDynamicPSide("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={balanceDynamicPSide == "no"}
                      onChange={() => setBalanceDynamicPSide("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg Squat Test (= >22 reps both limbs) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Squat Test (= &gt;22 reps both limbs)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegSquatTest == "yes"}
                      onChange={() => setSingleLegSquatTest("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegSquatTest == "no"}
                      onChange={() => setSingleLegSquatTest("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Single Leg
                Press(1.8 x
                Body
                Weight
                (sled +
                weight)) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Single Leg Press(1.8 x Body Weight (sled + weight))
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={singleLegPress18 == "yes"}
                      onChange={() => setSingleLegPress18("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={singleLegPress18 == "no"}
                      onChange={() => setSingleLegPress18("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Squat (1.8 x
                Body
                Weight) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Squat (1.8 x Body Weight)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={squat18Body == "yes"}
                      onChange={() => setSquat18Body("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={squat18Body == "no"}
                      onChange={() => setSquat18Body("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Back Squat 1.5x BW Hurdle Met */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Back Squat 1.5x BW Hurdle Met
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={backSquat15 == "yes"}
                      onChange={() => setBackSquat15("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={backSquat15 == "no"}
                      onChange={() => setBackSquat15("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* Side Hop Test (40 cm apart, 30 sec) - 4th on list*** */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="70%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Side Hop Test (40 cm apart, 30 sec) - 4th on list***
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>

                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue=""
                    name="radio-buttons-group"
                    row
                  >
                    <FormControlLabel
                      checked={sideHopTest40cm == "yes"}
                      onChange={() => setSideHopTest40cm("yes")}
                      control={<Radio />}
                      label="Yes"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      label="No"
                      checked={sideHopTest40cm == "no"}
                      onChange={() => setSideHopTest40cm("no")}
                    />
                  </RadioGroup>
                </FormControl>
              </MDBox>
            </MDBox>

            {/* submit button */}
            <MDBox mt={3} display="flex" justifyContent="flex-end">
              {/* <Link to={"/all/forms"}> */}
              {(editAcl && (
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={updateAclUser}
                >
                  Update
                </MDButton>
              )) || (
                <MDButton
                  variant="gradient"
                  color="info"
                  type="button"
                  onClick={() => navigate("AclRehabilitationProtocolStep3")}
                >
                  Next / Save
                </MDButton>
              )}
              {/* </Link> */}
            </MDBox>
            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step1 / Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default AclStep3;
