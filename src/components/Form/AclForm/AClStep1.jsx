import { useState } from "react";
import * as React from "react";
// Material Dashboard 2 React components
import MDBox from "../../MDBox";
import MDTypography from "../../MDTypography";
import MDInput from "../../MDInput";
import MDButton from "../../MDButton";
import MDAlert from "../../MDAlert";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
// Material Dashboard 2 React example components
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  RadioGroup,
  Select,
  TextareaAutosize,
} from "@mui/material";
import { toast } from "react-toastify";
import {
  createAclProtocol,
  getAclByRGst,
  getAclSingleData,
  getSingleUserData,
  updateAclProtocol,
} from "../../../services/service_api";
import { CheckBox } from "@mui/icons-material";

const style = {
  height: "100%",
};

function AClStep1() {
  const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };
  const navigate = useNavigate();
  const { userId } = useParams();
  const { id } = useParams();
  const route = useLocation();
  const editAcl = route.pathname?.split("/")?.slice(2, 3) == "edit-acl";
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [weight, setWeight] = useState("");
  const [registrationNo, setRegistrationNo] = useState(userId);
  const [sport, setSport] = useState("");
  const [date, setDate] = useState("");
  const [mechanismInjury, setMechanismInjury] = useState("");
  const [chiefComplaint, setChiefComplaint] = useState("");
  const [dominantLeg, setDominantLeg] = useState("");
  const [affectedSide, setAffectedSide] = useState("");
  const [surgeryBeenDone, setSurgeryBeenDone] = useState("");
  const [monthYearSurgery, setMonthYearSurgery] = useState("");
  const [graftUsed, setGraftUsed] = useState("");
  const [otherComponentsSurgery, setOtherComponentsSurgery] = useState("");
  const [activePain, setActivePain] = useState("");
  const [nprs, setNprs] = useState("");
  const [tendernessPresent, setTendernessPresent] = useState("");
  const [mriFindings, setMriFindings] = useState("");
  const [loationTenderness, setLoationTenderness] = useState("");

  const [atPatellaCheck, setAtPatellaCheck] = useState("");
  const [atPatellaRight, setAtPatellaRight] = useState("");
  const [atPatellaLeft, setAtPatellaLeft] = useState("");

  const [cm2Above, setCm2Above] = useState("");
  const [cm2AboveRight, setCm2AboveRight] = useState("");
  const [cm2AboveLeft, setCm2AboveLeft] = useState("");

  const [cm4Above, setCm4Above] = useState("");
  const [cm4AboveRight, setCm4AboveRight] = useState("");
  const [cm4AboveLeft, setCm4AboveLeft] = useState("");

  const [cm1Below, setCm1Below] = useState("");
  const [cm1BelowRight, setCm1BelowRight] = useState("");
  const [cm1BelowLeft, setCm1BelowLeft] = useState("");

  const [cm2Below, setCm2Below] = useState("");
  const [cm2BelowRight, setCm2BelowRight] = useState("");
  const [cm2BelowLeft, setCm2BelowLeft] = useState("");

  const [flexionRight, setFlexionRight] = useState("");
  const [flexionLeft, setFlexionLeft] = useState("");

  const [extensionRight, setExtensionRight] = useState("");
  const [extensionLeft, setExtensionLeft] = useState("");

  const [passiveKneeExtensionRight, setPassiveKneeExtensionRight] =
    useState("");
  const [passiveKneeExtensionLeft, setPassiveKneeExtensionLeft] = useState("");

  const [passiveKneeFlexionRight, setPassiveKneeFlexionRight] = useState("");
  const [passiveKneeFlexionLeft, setPassiveKneeFlexionLeft] = useState("");

  const [swellingEffusionLeft, setSwellingEffusionLeft] = useState("");
  const [swellingEffusionRight, setSwellingEffusionRight] = useState("");

  const [strengthLeft, setStrengthLeft] = useState("");
  const [strengthRight, setStrengthRight] = useState("");

  const [scarNumbness, setScarNumbness] = useState("");
  const [gait, setGait] = useState("");

  const [kneeExtensionRight, setKneeExtensionRight] = useState("");
  const [kneeExtensionLeft, setKneeExtensionLeft] = useState("");

  const [kneeFlexionRight, setKneeFlexionRight] = useState("");
  const [kneeFlexionLeft, setKneeFlexionLeft] = useState("");

  const [hipAbductionRight, setHipAbductionRight] = useState("");
  const [hipAbductionLeft, setHipAbductionLeft] = useState("");

  const [hipExtensionRight, setHipExtensionRight] = useState("");
  const [hipExtensionLeft, setHipExtensionLeft] = useState("");

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});

  React.useEffect(() => {
    if (editAcl) {
      getAclUserData();
    } else {
      getMainUserData();
    }
  }, [editAcl]);

  async function getAclUserData() {
    const params = {
      id: id,
      registration_no: userId,
    };
    const response = await getAclSingleData(params);
    // console.log("get fms user data", response.data);
    const data = response.data;
    if (response) {
      setName(data.name);
      setAge(data.age);
      setWeight(data.weight);
      setSport(data.sport);
      setDate(data.date);
    }
  }

  async function getMainUserData() {
    const params = {
      registration_no: userId,
    };
    // console.log("params", params);
    const response = await getSingleUserData(params);
    const data = response.data;
    // console.log("response------>", response.data);
    if (response) {
      setName(`${data?.first_name}${" "}${data?.last_name}`);
    }
  }

  async function createAclUser() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        name: name,
        age: age,
        weight: weight,
        sport: sport,
        date: date,
      };
      // console.log("params===>", params);
      const response = await createAclProtocol(params);
      // console.log("response===>", response);
      if (response.status == true) {
        toast.success(response.message, toastConfig);
        navigate(`/all/forms/${userId}`);
        setName("");
        setAge("");
        setWeight("");
        setSport("");
        setDate("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  async function updateAclUser() {
    if (formValidation()) {
      const params = {
        registration_no: registrationNo,
        id: id,
        name: name,
        age: age,
        weight: weight,
        sport: sport,
        date: date,
      };
      // console.log("params===>", params);
      const response = await updateAclProtocol(params);
      // console.log("response===>", response);
      if (response.status == true) {
        toast.success(response.message, toastConfig);
        navigate(`/all/forms/${userId}`);
        setName("");
        setAge("");
        setWeight("");
        setSport("");
        setDate("");
      } else {
        toast.error(response.message, toastConfig);
      }
    }
  }

  function formValidation() {
    let msgName = "";
    let msgAge = "";
    let msgWeight = "";
    let msgRegistrationNo = "";
    let msgSport = "";
    let msgDate = "";
    let isValid = false;

    if (name == "") {
      msgName = "Please enter name";
    }

    if (age == "") {
      msgAge = "Please enter age";
    }

    if (weight === "") {
      msgWeight = "Please enter weight";
    }

    if (registrationNo === "") {
      msgRegistrationNo = "Please enter registration number";
    }

    if (sport === "") {
      msgSport = "Please select a sport";
    }

    if (date === "") {
      msgDate = "Please select a date";
    }

    if (
      (!msgName, !msgAge, !msgWeight, !msgRegistrationNo, !msgSport, !msgDate)
    ) {
      isValid = true;
    }

    if (isValid) {
      setError(true);
      setErrorMessage({
        name: "",
        age: "",
        weight: "",
        registrationNo: "",
        sport: "",
        date: "",
      });
      return true;
    } else {
      setError(true);
      setErrorMessage({
        name: msgName,
        age: msgAge,
        weight: msgWeight,
        registrationNo: msgRegistrationNo,
        sport: msgSport,
        date: msgDate,
      });
      return false;
    }
  }
  return (
    <>
      <Card mb={5}>
        <MDBox mb={2} p={5}>
          <MDBox display="flex" justifyContent="center">
            <MDTypography
              variant="body1"
              color="text"
              ml={1}
              fontWeight="bold"
              fontSize={30}
            >
              ACL Rehabilitation Protocol
            </MDTypography>
          </MDBox>
          <MDBox
            component="form"
            role="form"
            display="flex"
            flexDirection="column"
          >
            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Name */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Name
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="name"
                    placeholder="Name"
                    value={name}
                    id="name"
                    onChange={(e) => setName(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["name"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["name"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Age(years) */}
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Age(years)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="age"
                    placeholder="Age"
                    value={age}
                    id="age"
                    onChange={(e) => setAge(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["age"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["age"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Weight(Kg) */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Weight(Kg)
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="weight"
                    placeholder="Weight"
                    value={weight}
                    id="age"
                    onChange={(e) => setWeight(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["weight"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["weight"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Registration No */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Registration No
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="registrationNo"
                    placeholder="Registration No"
                    id="registrationNo"
                    value={registrationNo}
                    onChange={(e) => setRegistrationNo(e.target.value)}
                    disabled={true}
                  />
                </MDBox>
                {error && errorMessage["registrationNo"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["registrationNo"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Sport/Activity */}
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Sport/Activity
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="sport"
                    placeholder="Sport"
                    value={sport}
                    id="sport"
                    onChange={(e) => setSport(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["sport"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["sport"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Date */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Date
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="date"
                    fullWidth
                    name="date"
                    placeholder="Date"
                    value={date}
                    id="date"
                    onChange={(e) => setDate(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["date"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["date"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Chief Complaint */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Chief Complaint
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="Chief Complaint"
                    value={chiefComplaint}
                    id="chiefComplaint"
                    onChange={(e) => setChiefComplaint(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["chiefComplaint"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["chiefComplaint"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Mechanism of Injury */}
              <MDTypography
                flexDirection="column"
                mr={1}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Mechanism of Injury
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="Mechanism of Injury"
                    value={mechanismInjury}
                    id="mechanismInjury"
                    onChange={(e) => setMechanismInjury(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["mechanismInjury"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["mechanismInjury"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Dominant Leg */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Dominant Leg
                <MDBox mb={2} mt={1} display="flex" width="100%">
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                      row
                    >
                      <FormControlLabel
                        checked={dominantLeg == "right"}
                        onChange={() => setDominantLeg("right")}
                        control={<Radio />}
                        label="Right"
                      />
                      <FormControlLabel
                        control={<Radio />}
                        label="Leg"
                        checked={dominantLeg == "leg"}
                        onChange={() => setDominantLeg("leg")}
                      />
                    </RadioGroup>
                  </FormControl>
                </MDBox>
              </MDTypography>

              {/* Has surgery been done? */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Has surgery been done?
                <MDBox mb={2} mt={1} display="flex" width="100%">
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                      row
                    >
                      <FormControlLabel
                        checked={surgeryBeenDone == "yes"}
                        onChange={() => setSurgeryBeenDone("yes")}
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        control={<Radio />}
                        label="No"
                        checked={surgeryBeenDone == "no"}
                        onChange={() => setSurgeryBeenDone("no")}
                      />
                    </RadioGroup>
                  </FormControl>
                </MDBox>
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Affected side */}
              <MDTypography
                width="100%"
                variant="body2"
                mr={2}
                color="text"
                fontWeight="regular"
              >
                Affected side
                <MDBox mb={2} display="flex" width="100%">
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">
                      Please Select
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Please Select"
                      style={{ height: 45 }}
                      value={affectedSide}
                      onChange={(e) => setAffectedSide(e.target.value)}
                    >
                      <MenuItem value={"right"}>Right</MenuItem>
                      <MenuItem value={"left"}>Left</MenuItem>
                      <MenuItem value={"bilateral"}>Bilateral</MenuItem>
                    </Select>
                  </FormControl>
                </MDBox>
              </MDTypography>

              {/* Month & Year of surgery */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                ml={1}
                fontWeight="regular"
              >
                Month & Year of surgery
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="monthYearSurgery"
                    placeholder="Month & Year of surgery"
                    value={monthYearSurgery}
                    id="monthYearSurgery"
                    onChange={(e) => setMonthYearSurgery(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["monthYearSurgery"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["monthYearSurgery"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>

              {/* Graft used */}
              <MDTypography
                flexDirection="column"
                mr={2}
                variant="body2"
                width="100%"
                color="text"
                ml={0}
                fontWeight="regular"
              >
                Graft used
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="graftUsed"
                    placeholder="Graft Used"
                    value={graftUsed}
                    id="graftUsed"
                    onChange={(e) => setGraftUsed(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["graftUsed"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["graftUsed"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Active Pain */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
                mr={2}
              >
                Active Pain
                <MDBox mb={2} mt={1} display="flex" width="100%">
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                      row
                    >
                      <FormControlLabel
                        checked={activePain == "yes"}
                        onChange={() => setActivePain("yes")}
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        control={<Radio />}
                        label="No"
                        checked={activePain == "no"}
                        onChange={() => setActivePain("no")}
                      />
                    </RadioGroup>
                  </FormControl>
                </MDBox>
              </MDTypography>

              {/*  NPRS: */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                mr={2}
                fontWeight="regular"
              >
                NPRS:
                <MDBox mb={2} display="flex" width="100%">
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">
                      Please Select
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Please Select"
                      style={{ height: 45 }}
                      value={nprs}
                      onChange={(e) => setNprs(e.target.value)}
                    >
                      <MenuItem value={"0"}>0</MenuItem>
                      <MenuItem value={"1"}>1</MenuItem>
                      <MenuItem value={"2"}>2</MenuItem>
                      <MenuItem value={"3"}>3</MenuItem>
                      <MenuItem value={"4"}>4</MenuItem>
                      <MenuItem value={"5"}>5</MenuItem>
                      <MenuItem value={"6"}>6</MenuItem>
                      <MenuItem value={"7"}>7</MenuItem>
                      <MenuItem value={"8"}>8</MenuItem>
                      <MenuItem value={"9"}>9</MenuItem>
                      <MenuItem value={"10"}>10</MenuItem>
                    </Select>
                  </FormControl>
                </MDBox>
              </MDTypography>

              {/* Other components of the surgery */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Other components of the surgery
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="Other components of the surgery"
                    value={otherComponentsSurgery}
                    id="otherComponentsSurgery"
                    onChange={(e) => setOtherComponentsSurgery(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["otherComponentsSurgery"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["otherComponentsSurgery"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Tenderness present */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
                mr={2}
              >
                Tenderness present
                <MDBox mb={2} mt={1} display="flex" width="100%">
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                      row
                    >
                      <FormControlLabel
                        checked={tendernessPresent == "yes"}
                        onChange={() => setTendernessPresent("yes")}
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        control={<Radio />}
                        label="No"
                        checked={tendernessPresent == "no"}
                        onChange={() => setTendernessPresent("no")}
                      />
                    </RadioGroup>
                  </FormControl>
                </MDBox>
              </MDTypography>

              {/*  Loation of tenderness */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                mr={2}
                fontWeight="regular"
              >
                Loation of tenderness
                <MDBox mb={2} display="flex" width="100%">
                  <MDInput
                    type="text"
                    fullWidth
                    name="loationTenderness"
                    placeholder="loationTenderness"
                    value={loationTenderness}
                    id="loationTenderness"
                    onChange={(e) => setLoationTenderness(e.target.value)}
                  />
                </MDBox>
              </MDTypography>

              {/* MRI findings */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                MRI findings
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="MRI findings"
                    value={mriFindings}
                    id="mriFindings"
                    onChange={(e) => setMriFindings(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["mriFindings"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["mriFindings"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            {/* Swelling and girth measurements: */}
            <MDBox display="flex" flexDirection="row">
              <MDTypography
                variant="body2"
                color="text"
                pb={1}
                fontWeight="bold"
                fontSize={18}
              >
                Swelling and girth measurements:
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Swelling
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Right girth
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Left girth
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* At patella */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  At patella
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="20%">
                <MDBox
                  flexDirection="column"
                  display="flex"
                  pt={1}
                  width="100%"
                >
                  <Checkbox
                    name="injury_niggle_management_needs_improvement"
                    checked={atPatellaCheck}
                    onChange={(e) => setAtPatellaCheck(e.target.checked)}
                  />
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={atPatellaRight}
                      id="atPatellaRight"
                      onChange={(e) => setAtPatellaRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["atPatellaRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["atPatellaRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={atPatellaLeft}
                      id="atPatellaLeft"
                      onChange={(e) => setAtPatellaLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["atPatellaLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["atPatellaLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* 2 cm above */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  2 cm above
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="20%">
                <MDBox
                  flexDirection="column"
                  display="flex"
                  pt={1}
                  width="100%"
                >
                  <Checkbox
                    name="injury_niggle_management_needs_improvement"
                    checked={cm2Above}
                    onChange={(e) => setCm2Above(e.target.checked)}
                  />
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={cm2AboveRight}
                      id="cm2AboveRight"
                      onChange={(e) => setCm2AboveRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm2AboveRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm2AboveRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={cm2AboveLeft}
                      id="cm2AboveLeft"
                      onChange={(e) => setCm2AboveLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm2AboveLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm2AboveLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* 4 cm above */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  4 cm above
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="20%">
                <MDBox
                  flexDirection="column"
                  display="flex"
                  pt={1}
                  width="100%"
                >
                  <Checkbox
                    checked={cm4Above}
                    onChange={(e) => setCm4Above(e.target.checked)}
                  />
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={cm4AboveRight}
                      id="cm4AboveRight"
                      onChange={(e) => setCm4AboveRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm4AboveRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm4AboveRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="left"
                      placeholder="Left"
                      value={cm4AboveLeft}
                      id="cm4AboveLeft"
                      onChange={(e) => setCm4AboveLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm4AboveLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm4AboveLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* 1 cm below */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  1 cm below
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="20%">
                <MDBox
                  flexDirection="column"
                  display="flex"
                  pt={1}
                  width="100%"
                >
                  <Checkbox
                    checked={cm1Below}
                    onChange={(e) => setCm1Below(e.target.checked)}
                  />
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="right"
                      placeholder="Right"
                      value={cm1BelowRight}
                      id="cm1BelowRight"
                      onChange={(e) => setCm1BelowRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm1BelowRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm1BelowRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="left"
                      placeholder="Left"
                      value={cm1BelowLeft}
                      id="cm1BelowLeft"
                      onChange={(e) => setCm1BelowLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm1BelowLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm1BelowLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* 2 cm below */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  2 cm below
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="20%">
                <MDBox
                  flexDirection="column"
                  display="flex"
                  pt={1}
                  width="100%"
                >
                  <Checkbox
                    checked={cm2Below}
                    onChange={(e) => setCm2Below(e.target.checked)}
                  />
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="right"
                      placeholder="Right"
                      value={cm2BelowRight}
                      id="cm2BelowRight"
                      onChange={(e) => setCm2BelowRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm2BelowRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm2BelowRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="30%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="left"
                      placeholder="Left"
                      value={cm2BelowLeft}
                      id="cm2BelowLeft"
                      onChange={(e) => setCm2BelowLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["cm2BelowLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["cm2BelowLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Knee Flexion/Extension */}
            <MDBox display="flex" flexDirection="row">
              <MDTypography
                variant="body2"
                color="text"
                pb={1}
                fontWeight="bold"
                fontSize={18}
              >
                Knee Flexion/Extension
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Right
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Left
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Flexion */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Flexion
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={flexionRight}
                      id="flexionRight"
                      onChange={(e) => setFlexionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["flexionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["flexionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={flexionLeft}
                      id="flexionLeft"
                      onChange={(e) => setFlexionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["flexionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["flexionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Extension */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Extension
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={extensionRight}
                      id="extensionRight"
                      onChange={(e) => setExtensionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["extensionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["extensionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={extensionRight}
                      id="extensionRight"
                      onChange={(e) => setExtensionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["extensionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["extensionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Phase 1: Recovery from surgery */}
            <MDBox display="flex" flexDirection="row">
              <MDTypography
                variant="body2"
                color="text"
                pb={1}
                fontWeight="bold"
                fontSize={18}
              >
                Phase 1: Recovery from surgery
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Right
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Left
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Passive knee extension (0 degree) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Passive knee extension (0 degree)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={passiveKneeExtensionRight}
                      id="passiveKneeExtensionRight"
                      onChange={(e) =>
                        setPassiveKneeExtensionRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeExtensionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeExtensionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={passiveKneeExtensionLeft}
                      id="passiveKneeExtensionLeft"
                      onChange={(e) =>
                        setPassiveKneeExtensionLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeExtensionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeExtensionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Passive knee flexion (125+) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Passive knee flexion (125+)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={passiveKneeFlexionRight}
                      id="passiveKneeFlexionRight"
                      onChange={(e) =>
                        setPassiveKneeFlexionRight(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeFlexionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeFlexionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={passiveKneeFlexionLeft}
                      id="passiveKneeFlexionLeft"
                      onChange={(e) =>
                        setPassiveKneeFlexionLeft(e.target.value)
                      }
                    />
                  </MDBox>
                  {error && errorMessage["passiveKneeFlexionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["passiveKneeFlexionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Swelling/effusion (zero-1+) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Swelling/effusion (zero-1+)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={swellingEffusionRight}
                      id="swellingEffusionRight"
                      onChange={(e) => setSwellingEffusionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["swellingEffusionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["swellingEffusionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={swellingEffusionLeft}
                      id="swellingEffusionLeft"
                      onChange={(e) => setSwellingEffusionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["swellingEffusionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["swellingEffusionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Strength (0 to 5 lag) */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Strength (0 to 5 lag)
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={strengthRight}
                      id="strengthRight"
                      onChange={(e) => setStrengthRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["strengthRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["strengthRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={strengthLeft}
                      id="strengthLeft"
                      onChange={(e) => setStrengthLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["strengthLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["strengthLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mt={1}>
              {/* Scar numbness */}
              <MDTypography
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
                mr={2}
              >
                Scar numbness
                <MDBox mb={2} mt={1} display="flex" width="100%">
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                      row
                    >
                      <FormControlLabel
                        checked={scarNumbness == "yes"}
                        onChange={() => setScarNumbness("yes")}
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        control={<Radio />}
                        label="No"
                        checked={scarNumbness == "no"}
                        onChange={() => setScarNumbness("no")}
                      />
                    </RadioGroup>
                  </FormControl>
                </MDBox>
              </MDTypography>

              {/* Gait */}
              <MDTypography
                flexDirection="column"
                mr={2}
                width="100%"
                variant="body2"
                color="text"
                fontWeight="regular"
              >
                Gait
                <MDBox mb={1} mr={2} display="flex" width="100%">
                  <TextareaAutosize
                    style={{
                      font: "inherit",
                      padding: "0.75rem",
                      border: "1px solid #d0d4d8",
                      borderRadius: "6px",
                      boxSizing: "content-box",
                      background: "none",
                      minheight: "1.4375em",
                      margin: "0",
                      WebkitTapHighlightColor: "transparent",
                      display: "block",
                      minWidth: "0",
                      width: "100%",
                      backgroundColor: "transparent",
                      fontSize: "14px",
                    }}
                    minRows={2}
                    placeholder="Gait"
                    value={gait}
                    id="gait"
                    onChange={(e) => setGait(e.target.value)}
                  />
                </MDBox>
                {error && errorMessage["gait"] ? (
                  <div style={{ color: "red", fontSize: "14px" }}>
                    {errorMessage["gait"]}
                  </div>
                ) : (
                  ""
                )}
              </MDTypography>
            </MDBox>

            {/* MMT */}
            <MDBox display="flex" flexDirection="row">
              <MDTypography
                variant="body2"
                color="text"
                pb={1}
                fontWeight="bold"
                fontSize={18}
              >
                MMT
              </MDTypography>
            </MDBox>

            <MDBox display="flex" flexDirection="row" mb={1}>
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  &nbsp;
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Right
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                >
                  Left
                </MDTypography>
              </MDBox>
            </MDBox>

            {/* Knee Extension */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Knee Extension
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={kneeExtensionRight}
                      id="kneeExtensionRight"
                      onChange={(e) => setKneeExtensionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["kneeExtensionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["kneeExtensionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={kneeExtensionLeft}
                      id="kneeExtensionLeft"
                      onChange={(e) => setKneeExtensionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["kneeExtensionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["kneeExtensionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Knee Flexion */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Knee Flexion
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={kneeFlexionRight}
                      id="kneeFlexionRight"
                      onChange={(e) => setKneeFlexionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["kneeFlexionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["kneeFlexionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={kneeFlexionLeft}
                      id="kneeFlexionLeft"
                      onChange={(e) => setKneeFlexionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["kneeFlexionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["kneeFlexionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Hip Abduction */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Hip Abduction
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={hipAbductionRight}
                      id="hipAbductionRight"
                      onChange={(e) => setHipAbductionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["hipAbductionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["hipAbductionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={hipAbductionLeft}
                      id="hipAbductionLeft"
                      onChange={(e) => setHipAbductionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["hipAbductionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["hipAbductionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>

            {/* Hip Extension */}
            <MDBox display="flex" flexDirection="row">
              <MDBox flexDirection="column" display="flex" width="20%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="regular"
                  fontSize={16}
                >
                  Hip Extension
                </MDTypography>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Right"
                      value={hipExtensionRight}
                      id="hipExtensionRight"
                      onChange={(e) => setHipExtensionRight(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["hipExtensionRight"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["hipExtensionRight"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>

              <MDBox flexDirection="column" display="flex" width="40%">
                <MDTypography
                  variant="body2"
                  color="text"
                  pt={1}
                  ml={1}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={16}
                ></MDTypography>
                <MDBox flexDirection="row" ml={1}>
                  <MDBox
                    mb={1}
                    mr={2}
                    flexDirection="column"
                    display="flex"
                    width="100%"
                  >
                    <MDInput
                      type="text"
                      fullWidth
                      name="Hurdle Step Right"
                      placeholder="Left"
                      value={hipExtensionLeft}
                      id="hipExtensionLeft"
                      onChange={(e) => setHipExtensionLeft(e.target.value)}
                    />
                  </MDBox>
                  {error && errorMessage["hipExtensionLeft"] ? (
                    <div style={{ color: "red", fontSize: "14px" }}>
                      {errorMessage["hipExtensionLeft"]}
                    </div>
                  ) : (
                    ""
                  )}
                </MDBox>
              </MDBox>
            </MDBox>
            {/* submit button */}
            <MDBox mt={3} display="flex" justifyContent="flex-end">
              {/* <Link to={"/all/forms"}> */}
              {(editAcl && (
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={updateAclUser}
                >
                  Update
                </MDButton>
              )) || (
                <MDButton
                  variant="gradient"
                  color="info"
                  type="button"
                  // onClick={createAclUser}
                  onClick={() =>
                    navigate("/forms/AclRehabilitationProtocolStep2")
                  }
                >
                  Next / Save
                </MDButton>
              )}
              {/* </Link> */}
            </MDBox>
            <MDBox mt={1} display="flex" justifyContent="center">
              <MDTypography variant="body2" fontWeight="regular" type="text">
                Step1 / Step5
              </MDTypography>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </>
  );
}

export default AClStep1;
