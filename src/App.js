import { useState, useEffect, useMemo, useContext } from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.css";

// react-router components
import { Routes, Route, Navigate, useLocation, useNavigate } from "react-router-dom";

// @mui material components
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


// Sporting Ethos React example components
import Sidenav from "./examples/Sidenav";
import Configurator from "./examples/Configurator";

// Sporting Ethos React themes
import theme from "./assets/theme";
import themeRTL from "./assets/theme/theme-rtl";

// Sporting Ethos React Dark Mode themes
import themeDark from "./assets/theme-dark";
import themeDarkRTL from "./assets/theme-dark/theme-rtl";

// RTL plugins
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";

// Sporting Ethos React routes
import routes from "./routes";

// Sporting Ethos React contexts
import { useMaterialUIController, setMiniSidenav } from "./context";

// Images
import brandWhite from "./assets/images/logo-ct.png";
import brandDark from "./assets/images/logo-ct-dark.png";

import { setupAxiosInterceptors } from "./services/interceptor";
import ProtectedRoute from "./examples/ProtectedRoute";
import ForgotPassword from "./auth/forgot-password";
// import ResetPassword from "./auth/reset-password";
import Login from "./auth/login";
// import Register from "./auth/register";
import { AuthContext } from "./context";
import UserProfile from "./layouts/user-profile";
import UserManagement from "./layouts/user-management";
import { MetaTags } from "react-meta-tags";
import Forms from "./layouts/Forms";
import { HandGripTestFormStep1, HandGripTestFormStep2, HandGripTestFormStep3, HandGripTestFormStep4, HandGripTestFormStep5, FunctionalMovementScreeningForm, AclRehabilitationOrotocol, AclRehabilitationProtocolStep2, AclRehabilitationProtocolStep3 } from "./components/Form";
// import FullWidthTabs from "./layouts/tabbarmanu";
import FullWidthTabs from "./layouts/tabbarmanu";
import FmsTemplate from "./layouts/PdfTemplate/FmsTemplate";
import Template from "./layouts/PdfTemplate";


export default function App() {
  const authContext = useContext(AuthContext);

  const [controller, dispatch] = useMaterialUIController();
  const {
    miniSidenav,
    direction,
    layout,
    sidenavColor,
    transparentSidenav,
    whiteSidenav,
    darkMode,
  } = controller;
  const [onMouseEnter, setOnMouseEnter] = useState(false);
  const [rtlCache, setRtlCache] = useState(null);
  const { pathname } = useLocation();

  // Cache for the rtl
  useMemo(() => {
    const cacheRtl = createCache({
      key: "rtl",
      stylisPlugins: [rtlPlugin],
    });

    setRtlCache(cacheRtl);
  }, []);

  // Open sidenav when mouse enter on mini sidenav
  const handleOnMouseEnter = () => {
    if (miniSidenav && !onMouseEnter) {
      setMiniSidenav(dispatch, false);
      setOnMouseEnter(true);
    }
  };

  // Close sidenav when mouse leave mini sidenav
  const handleOnMouseLeave = () => {
    if (onMouseEnter) {
      setMiniSidenav(dispatch, true);
      setOnMouseEnter(false);
    }
  };

  // Change the openConfigurator state
  // const handleConfiguratorOpen = () => setOpenConfigurator(dispatch, !openConfigurator);

  // if the token expired or other errors it logs out and goes to the login page
  const navigate = useNavigate();
  setupAxiosInterceptors(() => {
    authContext.logout();
    navigate("/auth/login");
  });

  const [isDemo, setIsDemo] = useState(false);

  useEffect(() => {
    setIsDemo(process.env.REACT_APP_IS_DEMO === "true");
  }, []);

  // Setting the dir attribute for the body element
  useEffect(() => {
    document.body.setAttribute("dir", direction);
  }, [direction]);

  // Setting page scroll to 0 when changing the route
  useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
  }, [pathname]);

  const getRoutes = (allRoutes) =>
    allRoutes.map((route) => {
      if (route.collapse) {
        return getRoutes(route.collapse);
      }

      if (route.route && route.type !== "auth") {
        return (
          <Route
            exact
            path={route.route}
            element={
              <ProtectedRoute isAuthenticated={authContext.isAuthenticated}>
                {route.component}
              </ProtectedRoute>
            }
            key={route.key}
          />
        );
      }
      return null;
    });

  const configsButton = (
    <div></div>
    // <MDBox
    //   display="flex"
    //   justifyContent="center"
    //   alignItems="center"
    //   width="3.25rem"
    //   height="3.25rem"
    //   bgColor="white"
    //   shadow="sm"
    //   borderRadius="50%"
    //   position="fixed"
    //   right="2rem"
    //   bottom="2rem"
    //   zIndex={99}
    //   color="dark"
    //   sx={{ cursor: "pointer" }}
    //   onClick={handleConfiguratorOpen}
    // >
    //   <Icon fontSize="small" color="inherit">
    //     settings
    //   </Icon>
    // </MDBox>
  );

  return (
    <>
      {isDemo && (
        <MetaTags>
          <meta
            name="keywords"
            content="creative tim, updivision, material, laravel json:api, html dashboard, laravel, react, api admin, react laravel, html css dashboard laravel, material dashboard laravel, laravel api, react material dashboard, material admin, react dashboard, react admin, web dashboard, bootstrap 5 dashboard laravel, bootstrap 5, css3 dashboard, bootstrap 5 admin laravel, material dashboard bootstrap 5 laravel, frontend, api dashboard, responsive bootstrap 5 dashboard, api, material dashboard, material laravel bootstrap 5 dashboard, json:api"
          />
          {/* <meta
            name="description"
            content="A free full stack app powered by MUI component library, React and Laravel, featuring dozens of handcrafted UI elements"
          /> */}
          <meta
            itemProp="name"
            content="Sporting Ethos"
          />
          {/* <meta
            itemProp="description"
            content="A free full stack app powered by MUI component library, React and Laravel, featuring dozens of handcrafted UI elements"
          /> */}
          <meta
            itemProp="image"
            content="https://s3.amazonaws.com/creativetim_bucket/products/686/original/react-material-dashboard-laravel.jpg?1664783836"
          />
          <meta name="twitter:card" content="product" />
          <meta name="twitter:site" content="@creativetim" />
          <meta
            name="twitter:title"
            content="Sporting Ethos"
          />
          {/* <meta
            name="twitter:description"
            content="A free full stack app powered by MUI component library, React and Laravel, featuring dozens of handcrafted UI elements"
          /> */}
          <meta name="twitter:creator" content="@creativetim" />
          <meta
            name="twitter:image"
            content="https://s3.amazonaws.com/creativetim_bucket/products/686/original/react-material-dashboard-laravel.jpg?1664783836"
          />
          <meta property="fb:app_id" content="655968634437471" />
          <meta
            property="og:title"
            content="Sporting Ethos"
          />
          <meta property="og:type" content="article" />
          <meta
            property="og:url"
            content="https://www.creative-tim.com/live/material-dashboard-react-laravel/"
          />
          <meta
            property="og:image"
            content="https://s3.amazonaws.com/creativetim_bucket/products/686/original/react-material-dashboard-laravel.jpg?1664783836"
          />
          {/* <meta
            property="og:description"
            content="A free full stack app powered by MUI component library, React and Laravel, featuring dozens of handcrafted UI elements"
          /> */}
          <meta property="og:site_name" content="Creative Tim" />
        </MetaTags>
      )}
      {direction === "rtl" ? (
        <CacheProvider value={rtlCache}>
          <ThemeProvider theme={darkMode ? themeDarkRTL : themeRTL}>
            <CssBaseline />
            {layout === "dashboard" && (
              <>
                <Sidenav
                  color={sidenavColor}
                  brand={(transparentSidenav && !darkMode) || whiteSidenav ? brandDark : brandWhite}
                  brandName="Sporting Ethos"
                  routes={routes}
                  onMouseEnter={handleOnMouseEnter}
                  onMouseLeave={handleOnMouseLeave}
                />
                <Configurator />
                {configsButton}
              </>
            )}
            {layout === "vr" && <Configurator />}
            <Routes>
              <Route path="login" element={<Navigate to="/auth/login" />} />
              {/* <Route path="register" element={<Navigate to="/auth/register" />} /> */}
              <Route path="forgot-password" element={<Navigate to="/auth/forgot-password" />} />
              {getRoutes(routes)}
              <Route path="*" element={<Navigate to="/user-management" />} />
            </Routes>
          </ThemeProvider>
        </CacheProvider>
      ) : (
        <ThemeProvider theme={darkMode ? themeDark : theme}>
          <CssBaseline />
          {layout === "dashboard" && (
            <>
              <Sidenav
                color={sidenavColor}
                brand={(transparentSidenav && !darkMode) || whiteSidenav ? brandDark : brandWhite}
                brandName="Sporting Ethos"
                routes={routes}
                onMouseEnter={handleOnMouseEnter}
                onMouseLeave={handleOnMouseLeave}
              />
              <Configurator />
              {configsButton}
            </>
          )}
          {layout === "vr" && <Configurator />}
          <Routes>
            <Route path="/auth/login" element={<Login />} />
            {/* <Route path="/auth/register" element={<Register />} /> */}
            <Route path="/auth/forgot-password" element={<ForgotPassword />} />
            {/* <Route path="/auth/reset-password" element={<ResetPassword />} /> */}
            <Route path="/forms" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><Forms /></ProtectedRoute>}>
              {/* <Route index element={<Forms />} /> */}
              <Route path="/forms/step1/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep1 /></ProtectedRoute>} />
              <Route path="/forms/step2/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep2 /></ProtectedRoute>} />
              <Route path="/forms/step3/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep3 /></ProtectedRoute>} />
              <Route path="/forms/step4/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep4 /></ProtectedRoute>} />
              <Route path="/forms/step5/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep5 /></ProtectedRoute>} />
              {/* Edit PFA */}
              <Route path="/forms/step1/edit-pfa/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep1 /></ProtectedRoute>} />
              <Route path="/forms/step2/edit-pfa/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep2 /></ProtectedRoute>} />
              <Route path="/forms/step3/edit-pfa/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep3 /></ProtectedRoute>} />
              <Route path="/forms/step4/edit-pfa/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep4 /></ProtectedRoute>} />
              <Route path="/forms/step5/edit-pfa/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep5 /></ProtectedRoute>} />
               {/* Back PFA */}
               <Route path="/forms/step1/:step/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep1 /></ProtectedRoute>} />
              <Route path="/forms/step2/:step/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep2 /></ProtectedRoute>} />
              <Route path="/forms/step3/:step/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep3 /></ProtectedRoute>} />
              <Route path="/forms/step4/:step/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep4 /></ProtectedRoute>} />
              <Route path="/forms/step5/:step/:userId/:id" element={ <ProtectedRoute isAuthenticated={authContext.isAuthenticated}><HandGripTestFormStep5 /></ProtectedRoute>} />

              <Route path="/forms/FunctionalMovementScreeningForm/:userId" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><FunctionalMovementScreeningForm /></ProtectedRoute>} />
              <Route path="/forms/edit-fms/FunctionalMovementScreeningForm/:userId/:id" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><FunctionalMovementScreeningForm /></ProtectedRoute>} />
              <Route path="/forms/AclRehabilitationOrotocol/:userId" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><AclRehabilitationOrotocol /></ProtectedRoute>} />
              <Route path="/forms/edit-acl/AclRehabilitationProtocol/:userId/:id" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><AclRehabilitationOrotocol /></ProtectedRoute>} />
              <Route path="/forms/AclRehabilitationProtocolStep2" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><AclRehabilitationProtocolStep2 /></ProtectedRoute>} />
              <Route path="/forms/AclRehabilitationProtocolStep3" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><AclRehabilitationProtocolStep3 /></ProtectedRoute>} />
              {/* <Route path="/forms/AclRehabilitationProtocolStep2/:userId/:id" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><AclRehabilitationProtocolStep2 /></ProtectedRoute>} /> */}
              <Route path="/forms/edit-acl/AclRehabilitationProtocolStep2/:userId/:id" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><AclRehabilitationProtocolStep2 /></ProtectedRoute>} />
            </Route>
            <Route path="/template" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><Template /></ProtectedRoute>} />
            <Route path="/all/forms/:id" element={<ProtectedRoute isAuthenticated={authContext.isAuthenticated}><FullWidthTabs /></ProtectedRoute>} />
            <Route
              exact
              path="user-profile"
              element={
                <ProtectedRoute isAuthenticated={authContext.isAuthenticated}>
                  <UserProfile />
                </ProtectedRoute>
              }
              key="user-profile"
            />
            <Route
              exact
              path="user-management"
              element={
                <ProtectedRoute isAuthenticated={authContext.isAuthenticated}>
                  <UserManagement />
                </ProtectedRoute>
              }
              key="user-management"
            />
            {getRoutes(routes)}
            <Route path="*" element={<Navigate to="/user-management" />} />
          </Routes>
        </ThemeProvider>
      )}
    </>
  );
}
